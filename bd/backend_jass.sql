-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-07-2018 a las 00:30:22
-- Versión del servidor: 10.1.31-MariaDB
-- Versión de PHP: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `backend_jass`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `auth_group`
--

INSERT INTO `auth_group` (`id`, `name`) VALUES
(2, 'Presidente'),
(1, 'Secretario/a'),
(4, 'Socio'),
(3, 'Tesorero'),
(8, 'Usuario');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `auth_group_permissions`
--

INSERT INTO `auth_group_permissions` (`id`, `group_id`, `permission_id`) VALUES
(1, 1, 115),
(3, 2, 230),
(2, 2, 234),
(4, 2, 412),
(5, 2, 413),
(6, 2, 414),
(7, 2, 416),
(8, 2, 417),
(9, 2, 418),
(10, 2, 419),
(11, 2, 420),
(12, 2, 421),
(13, 2, 422),
(14, 2, 423),
(15, 2, 424),
(16, 2, 425),
(17, 2, 426),
(18, 2, 427),
(19, 2, 428),
(20, 2, 429),
(21, 2, 430),
(22, 2, 431),
(23, 2, 432),
(24, 2, 433),
(25, 2, 434),
(26, 2, 435),
(27, 2, 436);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can add permission', 2, 'add_permission'),
(5, 'Can change permission', 2, 'change_permission'),
(6, 'Can delete permission', 2, 'delete_permission'),
(7, 'Can add group', 3, 'add_group'),
(8, 'Can change group', 3, 'change_group'),
(9, 'Can delete group', 3, 'delete_group'),
(10, 'Can add content type', 4, 'add_contenttype'),
(11, 'Can change content type', 4, 'change_contenttype'),
(12, 'Can delete content type', 4, 'delete_contenttype'),
(13, 'Can add session', 5, 'add_session'),
(14, 'Can change session', 5, 'change_session'),
(15, 'Can delete session', 5, 'delete_session'),
(43, 'Can add Usuario', 15, 'add_usuario'),
(44, 'Can change Usuario', 15, 'change_usuario'),
(45, 'Can delete Usuario', 15, 'delete_usuario'),
(46, 'Can add application', 16, 'add_application'),
(47, 'Can change application', 16, 'change_application'),
(48, 'Can delete application', 16, 'delete_application'),
(49, 'Can add access token', 17, 'add_accesstoken'),
(50, 'Can change access token', 17, 'change_accesstoken'),
(51, 'Can delete access token', 17, 'delete_accesstoken'),
(52, 'Can add grant', 18, 'add_grant'),
(53, 'Can change grant', 18, 'change_grant'),
(54, 'Can delete grant', 18, 'delete_grant'),
(55, 'Can add refresh token', 19, 'add_refreshtoken'),
(56, 'Can change refresh token', 19, 'change_refreshtoken'),
(57, 'Can delete refresh token', 19, 'delete_refreshtoken'),
(82, 'Can add Ubigeo', 28, 'add_ubigeo'),
(83, 'Can change Ubigeo', 28, 'change_ubigeo'),
(84, 'Can delete Ubigeo', 28, 'delete_ubigeo'),
(113, 'Listado de monedas', 8, 'view_moneda'),
(114, 'Agregar monedas', 8, 'add_moneda'),
(115, 'Actualizar monedas', 8, 'update_moneda'),
(116, 'Eliminar monedas', 8, 'delete_moneda'),
(117, 'Listado de monedas en formulario', 8, 'view_form_moneda'),
(118, 'Listado de continentes', 6, 'view_continente'),
(119, 'Agregar continentes', 6, 'add_continente'),
(120, 'Actualizar continentes', 6, 'update_continente'),
(121, 'Eliminar continentes', 6, 'delete_continente'),
(122, 'Listado de continentes en formulario', 6, 'view_form_continente'),
(127, 'Listado de tipos de menu', 39, 'view_tipo_menu'),
(128, 'Agregar tipos de menu', 39, 'add_tipo_menu'),
(129, 'Actualizar tipos de menu', 39, 'update_tipo_menu'),
(130, 'Eliminar tipos de menu', 39, 'delete_tipo_menu'),
(131, 'Listado de tipos de menu en formulario', 39, 'view_form_tipo_menu'),
(132, 'Listado de Módulos', 40, 'view_modulo'),
(133, 'Agregar Módulos', 40, 'add_modulo'),
(134, 'Actualizar Módulos', 40, 'update_modulo'),
(135, 'Eliminar Módulos', 40, 'delete_modulo'),
(136, 'Listado de Módulos en formulario', 40, 'view_form_modulo'),
(137, 'Listado de Menús', 41, 'view_menu'),
(138, 'Agregar Menús', 41, 'add_menu'),
(139, 'Actualizar Menús', 41, 'update_menu'),
(140, 'Eliminar Menús', 41, 'delete_menu'),
(141, 'Listado de Menús en formulario', 41, 'view_form_menu'),
(150, 'Listado de paises', 11, 'view_pais'),
(151, 'Agregar paises', 11, 'add_pais'),
(152, 'Actualizar paises', 11, 'update_pais'),
(153, 'Eliminar paises', 11, 'delete_pais'),
(154, 'Listado de paises en formulario', 11, 'view_form_pais'),
(205, 'Listar Contadores', 7, 'view_contador'),
(206, 'Agregar Contadores', 7, 'add_contador'),
(207, 'Actualizar Contadores', 7, 'update_contador'),
(208, 'Eliminar Contadores', 7, 'delete_contador'),
(209, 'Listar Contadores en formulario', 7, 'view_form_contador'),
(210, 'Listar Clasificaciones iiu', 9, 'view_clasificacioniiu'),
(211, 'Agregar Clasificaciones iiu', 9, 'add_clasificacioniiu'),
(212, 'Actualizar Clasificaciones iiu', 9, 'update_clasificacioniiu'),
(213, 'Eliminar Clasificaciones iiu', 9, 'delete_clasificacioniiu'),
(214, 'Listar Clasificaciones iiu en formulario', 9, 'view_form_clasificacioniiu'),
(215, 'Listar Meses', 10, 'view_mes'),
(216, 'Agregar Meses', 10, 'add_mes'),
(217, 'Actualizar Meses', 10, 'update_mes'),
(218, 'Eliminar Meses', 10, 'delete_mes'),
(219, 'Listar Meses en formulario', 10, 'view_form_mes'),
(220, 'Listar Tipos de persona', 12, 'view_tipopersona'),
(221, 'Agregar Tipos de persona', 12, 'add_tipopersona'),
(222, 'Actualizar Tipos de persona', 12, 'update_tipopersona'),
(223, 'Eliminar Tipos de persona', 12, 'delete_tipopersona'),
(224, 'Listar Tipos de persona en formulario', 12, 'view_form_tipopersona'),
(225, 'Listar Tipos de contribuyentes', 13, 'view_tipocontribuyente'),
(226, 'Agregar Tipos de contribuyentes', 13, 'add_tipocontribuyente'),
(227, 'Actualizar Tipos de contribuyentes', 13, 'update_tipocontribuyente'),
(228, 'Eliminar Tipos de contribuyentes', 13, 'delete_tipocontribuyente'),
(229, 'Listar Tipos de contribuyentes en formulario', 13, 'view_form_tipocontribuyente'),
(230, 'Listar Personas', 14, 'view_persona'),
(231, 'Agregar Personas', 14, 'add_persona'),
(232, 'Actualizar Personas', 14, 'update_persona'),
(233, 'Eliminar Personas', 14, 'delete_persona'),
(234, 'Listar Personas en formulario', 14, 'view_form_persona'),
(235, 'Listar Profesiones u Oficios', 20, 'view_profesionoficio'),
(236, 'Agregar Profesiones u Oficios', 20, 'add_profesionoficio'),
(237, 'Actualizar Profesiones u Oficios', 20, 'update_profesionoficio'),
(238, 'Eliminar Profesiones u Oficios', 20, 'delete_profesionoficio'),
(239, 'Listar Profesiones u Oficios en formulario', 20, 'view_form_profesionoficio'),
(240, 'Listar Tipos Numero Via', 21, 'view_tiponumerovia'),
(241, 'Agregar Tipos Numero Via', 21, 'add_tiponumerovia'),
(242, 'Actualizar Tipos Numero Via', 21, 'update_tiponumerovia'),
(243, 'Eliminar Tipos Numero Via', 21, 'delete_tiponumerovia'),
(244, 'Listar Tipos Numero Via en formulario', 21, 'view_form_tiponumerovia'),
(245, 'Listar Cuentas de Contacto', 26, 'view_cuentacontacto'),
(246, 'Agregar Cuentas de Contacto', 26, 'add_cuentacontacto'),
(247, 'Actualizar Cuentas de Contacto', 26, 'update_cuentacontacto'),
(248, 'Eliminar Cuentas de Contacto', 26, 'delete_cuentacontacto'),
(249, 'Listar Cuentas de Contacto en formulario', 26, 'view_form_cuentacontacto'),
(250, 'Listado de ubigeos', 28, 'view_ubigeo'),
(251, 'Actualizar ubigeos', 28, 'update_ubigeo'),
(252, 'Listado de ubigeos en formulario', 28, 'view_form_ubigeo'),
(253, 'Listar Tipos de vías', 22, 'view_tipovia'),
(254, 'Actualizar Tipos de vías', 22, 'update_tipovia'),
(255, 'Listar Tipos de vías en formulario', 22, 'view_form_tipovia'),
(256, 'Agregar Tipos de vías', 22, 'add_tipovia'),
(257, 'Eliminar Tipos de vías', 22, 'delete_tipovia'),
(258, 'Listar Tipos de Zona', 23, 'view_tipozona'),
(259, 'Agregar Tipos de Zona', 23, 'add_tipozona'),
(260, 'Actualizar Tipos de Zona', 23, 'update_tipozona'),
(261, 'Eliminar Tipos de Zona', 23, 'delete_tipozona'),
(262, 'Listar Tipos de Zona en formulario', 23, 'view_form_tipozona'),
(263, 'Listar Tipos de entidades', 24, 'view_tipoentidad'),
(264, 'Agregar Tipos de entidades', 24, 'add_tipoentidad'),
(265, 'Actualizar Tipos de entidades', 24, 'update_tipoentidad'),
(266, 'Eliminar Tipos de entidades', 24, 'delete_tipoentidad'),
(267, 'Listar Tipos de entidades en formulario', 24, 'view_form_tipoentidad'),
(268, 'Listar Tipos de documentos', 25, 'view_tipodocumento'),
(269, 'Agregar Tipos de documentos', 25, 'add_tipodocumento'),
(270, 'Actualizar Tipos de documentos', 25, 'update_tipodocumento'),
(271, 'Eliminar Tipos de documentos', 25, 'delete_tipodocumento'),
(272, 'Listar Tipos de documentos en formulario', 25, 'view_form_tipodocumento'),
(273, 'Listar Tipos de Contactos', 27, 'view_tipocontacto'),
(274, 'Agregar Tipos de Contactos', 27, 'add_tipocontacto'),
(275, 'Actualizar Tipos de Contactos', 27, 'update_tipocontacto'),
(276, 'Eliminar Tipos de Contactos', 27, 'delete_tipocontacto'),
(277, 'Listar Tipos de Contactos en formulario', 27, 'view_form_tipocontacto'),
(278, 'Listar Paises Tipo Ubigeo', 29, 'view_paistipoubigeo'),
(279, 'Agregar Paises Tipo Ubigeo', 29, 'add_paistipoubigeo'),
(280, 'Actualizar Paises Tipo Ubigeo', 29, 'update_paistipoubigeo'),
(281, 'Eliminar Paises Tipo Ubigeo', 29, 'delete_paistipoubigeo'),
(282, 'Listar Paises Tipo Ubigeo en formulario', 29, 'view_form_paistipoubigeo'),
(283, 'Listar Tipos de Ubigeos', 30, 'view_tipoubigeo'),
(284, 'Agregar Tipos de Ubigeos', 30, 'add_tipoubigeo'),
(285, 'Actualizar Tipos de Ubigeos', 30, 'update_tipoubigeo'),
(286, 'Eliminar Tipos de Ubigeos', 30, 'delete_tipoubigeo'),
(287, 'Listar Tipos de Ubigeos en formulario', 30, 'view_form_tipoubigeo'),
(288, 'Listar Regiones naturales', 31, 'view_regionnatural'),
(289, 'Agregar Regiones naturales', 31, 'add_regionnatural'),
(290, 'Actualizar Regiones naturales', 31, 'update_regionnatural'),
(291, 'Eliminar Regiones naturales', 31, 'delete_regionnatural'),
(292, 'Listar Regiones naturales en formulario', 31, 'view_form_regionnatural'),
(293, 'Listar Tipos de Ambitos', 32, 'view_tipoambito'),
(294, 'Agregar Tipos de Ambitos', 32, 'add_tipoambito'),
(295, 'Actualizar Tipos de Ambitos', 32, 'update_tipoambito'),
(296, 'Eliminar Tipos de Ambitos', 32, 'delete_tipoambito'),
(297, 'Listar Tipos de Ambitos en formulario', 32, 'view_form_tipoambito'),
(298, 'Listar Tipos de Direcciones', 33, 'view_tipodireccion'),
(299, 'Agregar Tipos de Direcciones', 33, 'add_tipodireccion'),
(300, 'Actualizar Tipos de Direcciones', 33, 'update_tipodireccion'),
(301, 'Eliminar Tipos de Direcciones', 33, 'delete_tipodireccion'),
(302, 'Listar Tipos de Direcciones en formulario', 33, 'view_form_tipodireccion'),
(303, 'Listar Tipos de Fechas', 34, 'view_tipofecha'),
(304, 'Agregar Tipos de Fechas', 34, 'add_tipofecha'),
(305, 'Actualizar Tipos de Fechas', 34, 'update_tipofecha'),
(306, 'Eliminar Tipos de Fechas', 34, 'delete_tipofecha'),
(307, 'Listar Tipos de Fechas en formulario', 34, 'view_form_tipofecha'),
(308, 'Listar Config. de Direcciones de Tipos de Personas', 35, 'view_configdirecciontipopersona'),
(309, 'Actualizar Config. de Direcciones de Tipos de Personas', 35, 'update_configdirecciontipopersona'),
(310, 'Listar Config. de Direcciones de Tipos de Personas en formulario', 35, 'view_form_configdirecciontipopersona'),
(311, 'Agregar Config. de Direcciones de Tipos de Personas', 35, 'add_configdirecciontipopersona'),
(312, 'Eliminar Config. de Direcciones de Tipos de Personas', 35, 'delete_configdirecciontipopersona'),
(313, 'Listar Config. de Documentos de Tipos de Personas', 36, 'view_configdocumentotipopersona'),
(314, 'Agregar Config. de Documentos de Tipos de Personas', 36, 'add_configdocumentotipopersona'),
(315, 'Actualizar Config. de Documentos de Tipos de Personas', 36, 'update_configdocumentotipopersona'),
(316, 'Eliminar Config. de Documentos de Tipos de Personas', 36, 'delete_configdocumentotipopersona'),
(317, 'Listar Config. de Documentos de Tipos de Personas en formulario', 36, 'view_form_configdocumentotipopersona'),
(321, 'Listar Etiquetas de Contactos', 37, 'view_etiquetacontacto'),
(322, 'Agregar Etiquetas de Contactos', 37, 'add_etiquetacontacto'),
(323, 'Actualizar Etiquetas de Contactos', 37, 'update_etiquetacontacto'),
(324, 'Eliminar Etiquetas de Contactos', 37, 'delete_etiquetacontacto'),
(325, 'Listar Etiquetas de Contactos en formulario', 37, 'view_form_etiquetacontacto'),
(326, 'Listar Entidades', 38, 'view_entidad'),
(327, 'Agregar Entidades', 38, 'add_entidad'),
(328, 'Actualizar Entidades', 38, 'update_entidad'),
(329, 'Eliminar Entidades', 38, 'delete_entidad'),
(330, 'Listar Entidades en formulario', 38, 'view_form_entidad'),
(397, 'Listar Comprobantes', 44, 'view_comprobante'),
(398, 'Agregar Comprobantes', 44, 'add_comprobante'),
(399, 'Actualizar Comprobantes', 44, 'update_comprobante'),
(400, 'Eliminar Comprobantes', 44, 'delete_comprobante'),
(401, 'Listar Comprobantes en formulario', 44, 'view_form_comprobante'),
(402, 'Listar Comprob. Configs', 46, 'view_comprobconfig'),
(403, 'Agregar Comprob. Configs', 46, 'add_comprobconfig'),
(404, 'Actualizar Comprob. Configs', 46, 'update_comprobconfig'),
(405, 'Eliminar Comprob. Configs', 46, 'delete_comprobconfig'),
(406, 'Listar Comprob. Configs en formulario', 46, 'view_form_comprobconfig'),
(407, 'Listar Tipos de uso de servicios', 47, 'view_tipousoservicio'),
(408, 'Agregar Tipos de uso de servicios', 47, 'add_tipousoservicio'),
(409, 'Actualizar Tipos de uso de servicios', 47, 'update_tipousoservicio'),
(410, 'Eliminar Tipos de uso de servicios', 47, 'delete_tipousoservicio'),
(411, 'Listar Tipos de uso de servicios en formulario', 47, 'view_form_tipousoservicio'),
(412, 'Listar Servicios', 48, 'view_servicio'),
(413, 'Agregar Servicios', 48, 'add_servicio'),
(414, 'Actualizar Servicios', 48, 'update_servicio'),
(415, 'Eliminar Servicios', 48, 'delete_servicio'),
(416, 'Listar Servicios en formulario', 48, 'view_form_servicio'),
(417, 'Listar Estados de los Predios', 49, 'view_estadopredio'),
(418, 'Agregar Estados de los Predios', 49, 'add_estadopredio'),
(419, 'Actualizar Estados de los Predios', 49, 'update_estadopredio'),
(420, 'Eliminar Estados de los Predios', 49, 'delete_estadopredio'),
(421, 'Listar Estados de los Predios en formulario', 49, 'view_form_estadopredio'),
(422, 'Listar zonas', 53, 'view_zona'),
(423, 'Agregar zonas', 53, 'add_zona'),
(424, 'Actualizar zonas', 53, 'update_zona'),
(425, 'Eliminar zonas', 53, 'delete_zona'),
(426, 'Listar zonas en formulario', 53, 'view_form_zona'),
(427, 'Listar Predios', 50, 'view_predio'),
(428, 'Agregar Predios', 50, 'add_predio'),
(429, 'Actualizar Predios', 50, 'update_predio'),
(430, 'Eliminar Predios', 50, 'delete_predio'),
(431, 'Listar Predios en formulario', 50, 'view_form_predio'),
(432, 'Listar Contratos', 51, 'view_contrato'),
(433, 'Agregar Contratos', 51, 'add_contrato'),
(434, 'Actualizar Contratos', 51, 'update_contrato'),
(435, 'Eliminar Contratos', 51, 'delete_contrato'),
(436, 'Listar Contratos en formulario', 51, 'view_form_contrato'),
(437, 'Listar Contratos de Servicios', 52, 'view_contratoservicio'),
(438, 'Agregar Contratos de Servicios', 52, 'add_contratoservicio'),
(439, 'Actualizar Contratos de Servicios', 52, 'update_contratoservicio'),
(440, 'Eliminar Contratos de Servicios', 52, 'delete_contratoservicio'),
(441, 'Listar Contratos de Servicios en formulario', 52, 'view_form_contratoservicio'),
(462, 'Listado de cronogramas pago', 63, 'view_cronogramapago'),
(463, 'Agregar cronogramas pago', 63, 'add_cronogramapago'),
(464, 'Actualizar cronogramas pago', 63, 'update_cronogramapago'),
(465, 'Eliminar cronogramas pago', 63, 'delete_cronogramapago'),
(466, 'Listado de cronogramas pago en formulario', 63, 'view_form_cronogramapago'),
(467, 'Listar Estados de los Cronogramas', 65, 'view_estadocronograma'),
(468, 'Agregar Estados de los Cronogramas', 65, 'add_estadocronograma'),
(469, 'Actualizar Estados de los Cronogramas', 65, 'update_estadocronograma'),
(470, 'Eliminar Estados de los Cronogramas', 65, 'delete_estadocronograma'),
(471, 'Listar Estados de los Cronogramas en formulario', 65, 'view_form_estadocronograma'),
(472, 'Listado de motivos movimiento cuenta', 66, 'view_motivomovimientocuenta'),
(473, 'Agregar motivos movimiento cuenta', 66, 'add_motivomovimientocuenta'),
(474, 'Actualizar motivos movimiento cuenta', 66, 'update_motivomovimientocuenta'),
(475, 'Eliminar motivos movimiento cuenta', 66, 'delete_motivomovimientocuenta'),
(476, 'Listado de motivos movimiento cuenta en formulario', 66, 'view_form_motivomovimientocuenta'),
(477, 'Listado de tipos de movimientos de cuentas', 67, 'view_tipomovimientocuenta'),
(478, 'Agregar tipos de movimientos de cuentas', 67, 'add_tipomovimientocuenta'),
(479, 'Actualizar tipos de movimientos de cuentas', 67, 'update_tipomovimientocuenta'),
(480, 'Eliminar tipos de movimientos de cuentas', 67, 'delete_tipomovimientocuenta'),
(481, 'Listado de tipos de movimientos de cuentas en formulario', 67, 'view_form_tipomovimientocuenta'),
(482, 'Can list permission', 2, 'view_permission'),
(483, 'Can list in form permission', 2, 'view_form_permission'),
(484, 'Can list group', 3, 'view_group'),
(485, 'Can list in form group', 3, 'view_form_group'),
(486, 'Can view Usuario', 15, 'view_usuario'),
(487, 'Can view in form Usuario', 15, 'view_form_usuario'),
(488, 'Listado de tipos de menu', 39, 'view_tipomenu'),
(489, 'Agregar tipos de menu', 39, 'add_tipomenu'),
(490, 'Actualizar tipos de menu', 39, 'update_tipomenu'),
(491, 'Eliminar tipos de menu', 39, 'delete_tipomenu'),
(492, 'Listado de tipos de menu en formulario', 39, 'view_form_tipomenu'),
(493, 'Listar Mes recibo', 68, 'view_mesrecibo'),
(494, 'Agregar Mes recibo', 68, 'add_mesrecibo'),
(495, 'Actualizar Mes recibo', 68, 'update_mesrecibo'),
(496, 'Eliminar Mes recibo', 68, 'delete_mesrecibo'),
(497, 'Listar Mes recibo en formulario', 68, 'view_form_mesrecibo'),
(498, 'Listar Recibo', 69, 'view_recibo'),
(499, 'Agregar Recibo', 69, 'add_recibo'),
(500, 'Actualizar Recibo', 69, 'update_recibo'),
(501, 'Eliminar Recibo', 69, 'delete_recibo'),
(502, 'Listar Recibo en formulario', 69, 'view_form_recibo'),
(503, 'Listar Acceso Cuenta', 70, 'view_accesocuenta'),
(504, 'Agregar Acceso Cuenta', 70, 'add_accesocuenta'),
(505, 'Actualizar Acceso Cuenta', 70, 'update_accesocuenta'),
(506, 'Eliminar Acceso Cuenta', 70, 'delete_accesocuenta'),
(507, 'Listar Acceso Cuenta en formulario', 70, 'view_form_accesocuenta'),
(508, 'Listar Cuenta Entidad', 72, 'view_cuentaentidad'),
(509, 'Agregar Cuenta Entidad', 72, 'add_cuentaentidad'),
(510, 'Actualizar Cuenta Entidad', 72, 'update_cuentaentidad'),
(511, 'Eliminar Cuenta Entidad', 72, 'delete_cuentaentidad'),
(512, 'Listar Cuenta Entidad en formulario', 72, 'view_form_cuentaentidad'),
(513, 'Listar Entidades Financieras', 73, 'view_entidadfinanciera'),
(514, 'Agregar Entidades Financieras', 73, 'add_entidadfinanciera'),
(515, 'Actualizar Entidades Financieras', 73, 'update_entidadfinanciera'),
(516, 'Eliminar Entidades Financieras', 73, 'delete_entidadfinanciera'),
(517, 'Listar Entidades Financieras en formulario', 73, 'view_form_entidadfinanciera'),
(518, 'Listar Estado Cuenta', 74, 'view_estadocuenta'),
(519, 'Agregar Estado Cuenta', 74, 'add_estadocuenta'),
(520, 'Actualizar Estado Cuenta', 74, 'update_estadocuenta'),
(521, 'Eliminar Estado Cuenta', 74, 'delete_estadocuenta'),
(522, 'Listar Estado Cuenta en formulario', 74, 'view_form_estadocuenta'),
(523, 'Listar Movimiento Cuenta', 75, 'view_movimientocuenta'),
(524, 'Agregar Movimiento Cuenta', 75, 'add_movimientocuenta'),
(525, 'Actualizar Movimiento Cuenta', 75, 'update_movimientocuenta'),
(526, 'Eliminar Movimiento Cuenta', 75, 'delete_movimientocuenta'),
(527, 'Listar Movimiento Cuenta en formulario', 75, 'view_form_movimientocuenta'),
(528, 'Listar Persona Cuentas', 76, 'view_personacuenta'),
(529, 'Agregar Persona Cuentas', 76, 'add_personacuenta'),
(530, 'Actualizar Persona Cuentas', 76, 'update_personacuenta'),
(531, 'Eliminar Persona Cuentas', 76, 'delete_personacuenta'),
(532, 'Listar Persona Cuentas en formulario', 76, 'view_form_personacuenta'),
(533, 'Listar Tipo cuenta', 77, 'view_tipocuenta'),
(534, 'Agregar Tipo cuenta', 77, 'add_tipocuenta'),
(535, 'Actualizar Tipo cuenta', 77, 'update_tipocuenta'),
(536, 'Eliminar Tipo cuenta', 77, 'delete_tipocuenta'),
(537, 'Listar Tipo cuenta en formulario', 77, 'view_form_tipocuenta'),
(538, 'Listar Tipo cuenta finan', 78, 'view_tipocuentafinan'),
(539, 'Agregar Tipo cuenta finan', 78, 'add_tipocuentafinan'),
(540, 'Actualizar Tipo cuenta finan', 78, 'update_tipocuentafinan'),
(541, 'Eliminar Tipo cuenta finan', 78, 'delete_tipocuentafinan'),
(542, 'Listar Tipo cuenta finan en formulario', 78, 'view_form_tipocuentafinan'),
(543, 'Listar Tipo entidad finan', 79, 'view_tipoentfinan'),
(544, 'Agregar Tipo entidad finan', 79, 'add_tipoentfinan'),
(545, 'Actualizar Tipo entidad finan', 79, 'update_tipoentfinan'),
(546, 'Eliminar Tipo entidad finan', 79, 'delete_tipoentfinan'),
(547, 'Listar Tipo entidad finan en formulario', 79, 'view_form_tipoentfinan'),
(548, 'Listar Tipo Medio Pago', 80, 'view_tipomediopago'),
(549, 'Agregar Tipo Medio Pago', 80, 'add_tipomediopago'),
(550, 'Actualizar Tipo Medio Pago', 80, 'update_tipomediopago'),
(551, 'Eliminar Tipo Medio Pago', 80, 'delete_tipomediopago'),
(552, 'Listar Tipo Medio Pago en formulario', 80, 'view_form_tipomediopago');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caja_acceso_cuenta`
--

CREATE TABLE `caja_acceso_cuenta` (
  `id` int(11) NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date DEFAULT NULL,
  `estado` varchar(1) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `cuenta_entidad_id` int(11) NOT NULL,
  `persona_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caja_amortizacion`
--

CREATE TABLE `caja_amortizacion` (
  `id` int(11) NOT NULL,
  `importe` decimal(12,2) NOT NULL,
  `estado` varchar(1) NOT NULL,
  `cronograma_pago_id` int(11) NOT NULL,
  `movimiento_cuenta_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caja_cuenta_entidad`
--

CREATE TABLE `caja_cuenta_entidad` (
  `id` int(11) NOT NULL,
  `nombre` varchar(60) DEFAULT NULL,
  `saldo` decimal(12,2) NOT NULL,
  `estado` varchar(1) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `entidad_id` int(11) NOT NULL,
  `persona_cuenta_id` int(11) DEFAULT NULL,
  `tipo_cuenta_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `caja_cuenta_entidad`
--

INSERT INTO `caja_cuenta_entidad` (`id`, `nombre`, `saldo`, `estado`, `created_at`, `updated_at`, `entidad_id`, `persona_cuenta_id`, `tipo_cuenta_id`, `usuario_id`) VALUES
(1, 'CAJA 1', '0.00', '1', '2018-07-25 09:24:24.421436', '2018-07-25 10:26:26.468460', 1, NULL, 1, 18),
(2, NULL, '1200.00', '1', '2018-07-25 09:22:22.358358', '2018-07-25 10:23:29.468460', 1, 3, 2, 18),
(3, NULL, '900.00', '1', '2018-07-25 09:22:22.366390', '2018-07-25 08:26:26.460452', 1, 4, 2, 18),
(4, 'CAJA 2', '0.00', '1', '2018-07-26 15:21:22.819973', '2018-07-26 15:21:22.819973', 1, NULL, 1, 18);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caja_entidad_financiera`
--

CREATE TABLE `caja_entidad_financiera` (
  `id` int(11) NOT NULL,
  `logo` varchar(100) DEFAULT NULL,
  `nombre` varchar(80) NOT NULL,
  `abreviatura` varchar(20) NOT NULL,
  `pagina_web` varchar(200) DEFAULT NULL,
  `estado` varchar(1) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `tipo_ent_finan_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `caja_entidad_financiera`
--

INSERT INTO `caja_entidad_financiera` (`id`, `logo`, `nombre`, `abreviatura`, `pagina_web`, `estado`, `created_at`, `updated_at`, `tipo_ent_finan_id`) VALUES
(1, NULL, 'Banco de Comercio', 'Banco de Comercio', 'http://www.bancomercio.com.pe/', '0', '2018-06-28 05:00:00.000000', '2018-06-28 05:00:00.000000', 1),
(2, NULL, 'Banco de Crédito del Perú', 'BCP', 'http://www.viabcp.com/', '1', '2018-06-28 05:00:00.000000', '2018-06-28 05:00:00.000000', 1),
(3, NULL, 'Banco Interamericano de Finanzas', 'BanBif', 'http://www.bif.com.pe/', '0', '2018-06-28 05:00:00.000000', '2018-06-28 05:00:00.000000', 1),
(4, NULL, 'Banco Financiero', 'Banco Financiero', 'http://www.financiero.com.pe/', '1', '2018-06-28 05:00:00.000000', '2018-06-28 05:00:00.000000', 1),
(5, NULL, 'BBVA Continental', 'BBVA Continental', 'http://www.bbvabancocontinental.com/', '1', '2018-06-28 05:00:00.000000', '2018-06-28 05:00:00.000000', 1),
(6, NULL, 'Citibank Perú', 'Citibank Perú', 'http://www.citibank.com.pe/', '0', '2018-06-28 05:00:00.000000', '2018-06-28 05:00:00.000000', 1),
(7, NULL, 'Interbank', 'Interbank', 'http://www.interbank.com.pe/', '1', '2018-06-28 05:00:00.000000', '2018-06-28 05:00:00.000000', 1),
(8, NULL, 'MiBanco', 'MiBanco', 'http://www.mibanco.com.pe/', '1', '2018-06-28 05:00:00.000000', '2018-06-28 05:00:00.000000', 1),
(9, NULL, 'Scotiabank Perú', 'Scotiabank Perú', 'http://www.scotiabank.com.pe/', '1', '2018-06-28 05:00:00.000000', '2018-06-28 05:00:00.000000', 1),
(11, NULL, 'Banco GNB Perú', 'Banco GNB Perú', 'http://www.bancognb.com.pe/', '0', '2018-06-28 05:00:00.000000', '2018-06-28 05:00:00.000000', 1),
(12, NULL, 'Banco Falabella', 'Banco Falabella', 'http://www.bancofalabella.com.pe/', '0', '2018-06-28 05:00:00.000000', '2018-06-28 05:00:00.000000', 1),
(13, NULL, 'Banco Ripley', 'Banco Ripley', 'http://www.bancoripley.com.pe/', '0', '2018-06-28 05:00:00.000000', '2018-06-28 05:00:00.000000', 1),
(14, NULL, 'Banco Santander Perú', 'Banco Santander Perú', 'http://www.santander.com.pe/', '0', '2018-06-28 05:00:00.000000', '2018-06-28 05:00:00.000000', 1),
(15, NULL, 'Banco Azteca', 'Banco Azteca', 'http://www.bancoazteca.com.pe/', '0', '2018-06-28 05:00:00.000000', '2018-06-28 05:00:00.000000', 1),
(16, NULL, 'Banco Cencosud', 'Banco Cencosud', 'http://www.bancocencosud.com.pe/', '0', '2018-06-28 05:00:00.000000', '2018-06-28 05:00:00.000000', 1),
(17, NULL, 'ICBC PERU BANK', 'ICBC PERU BANK', 'http://www.icbc.com.cn/ICBC/%E6%B5%B7%E5%A4%96%E5%88%86%E8%A1%8C/%E5%B7%A5%E9%93%B6%E7%A7%98%E9%B2%81%E7%BD%91%E7%AB%99/es/', '0', '2018-06-28 05:00:00.000000', '2018-06-28 05:00:00.000000', 1),
(18, NULL, 'Agrobanco', 'Agrobanco', 'http://www.agrobanco.com.pe/', '1', '2018-06-28 05:00:00.000000', '2018-06-28 05:00:00.000000', 2),
(19, NULL, 'Banco de la Nación', 'BN', 'http://www.bn.com.pe/', '1', '2018-06-28 05:00:00.000000', '2018-06-28 05:00:00.000000', 2),
(20, NULL, 'COFIDE', 'COFIDE', 'http://www.cofide.com.pe/', '0', '2018-06-28 05:00:00.000000', '2018-06-28 05:00:00.000000', 2),
(21, NULL, 'Piura', 'Piura', 'http://www.cmacpiura.com.pe/', '1', '2018-06-28 05:00:00.000000', '2018-06-28 05:00:00.000000', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caja_estado_cuenta`
--

CREATE TABLE `caja_estado_cuenta` (
  `id` int(11) NOT NULL,
  `saldo` decimal(12,2) NOT NULL,
  `deuda` decimal(12,2) NOT NULL,
  `estado` varchar(1) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `entidad_id` int(11) NOT NULL,
  `persona_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `caja_estado_cuenta`
--

INSERT INTO `caja_estado_cuenta` (`id`, `saldo`, `deuda`, `estado`, `created_at`, `updated_at`, `entidad_id`, `persona_id`) VALUES
(1, '0.00', '46.00', '1', '2018-07-26 09:24:23.000000', '2018-07-26 09:21:23.397000', 1, 18);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caja_movimiento_cuenta`
--

CREATE TABLE `caja_movimiento_cuenta` (
  `id` int(11) NOT NULL,
  `numero` varchar(10) NOT NULL,
  `detalle` varchar(120) DEFAULT NULL,
  `tipo_cambio` decimal(12,4) NOT NULL,
  `importe` decimal(12,2) NOT NULL,
  `comisiones` decimal(12,2) DEFAULT NULL,
  `recibido` decimal(12,2) DEFAULT NULL,
  `vuelto` decimal(12,2) DEFAULT NULL,
  `fecha` date NOT NULL,
  `hora` time(6) NOT NULL,
  `estado` varchar(1) NOT NULL,
  `detalle_anulado` varchar(120) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `cuenta_entidad_id` int(11) NOT NULL,
  `moneda_id` int(11) NOT NULL,
  `moneda_cambio_id` int(11) DEFAULT NULL,
  `motivo_mov_cuenta_id` int(11) NOT NULL,
  `persona_id` int(11) NOT NULL,
  `tipo_medio_pago_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caja_persona_cuenta`
--

CREATE TABLE `caja_persona_cuenta` (
  `id` int(11) NOT NULL,
  `numero` varchar(60) NOT NULL,
  `numero_cci` varchar(70) DEFAULT NULL,
  `estado` varchar(1) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `entidad_financiera_id` int(11) NOT NULL,
  `persona_id` int(11) NOT NULL,
  `tipo_cuenta_finan_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `caja_persona_cuenta`
--

INSERT INTO `caja_persona_cuenta` (`id`, `numero`, `numero_cci`, `estado`, `created_at`, `updated_at`, `entidad_financiera_id`, `persona_id`, `tipo_cuenta_finan_id`) VALUES
(1, '0011-0310-02-00805080', '011-310-000200805084-07', '1', '2018-07-25 16:58:33.493762', '2018-07-25 16:58:33.493762', 5, 18, 5),
(2, '045-81033708', '018-581-004581033708-94', '1', '2018-07-25 16:58:33.592403', '2018-07-25 16:58:33.592403', 19, 18, 1),
(3, '456-4567878', NULL, '1', '2018-07-25 19:03:40.006874', '2018-07-25 19:03:40.006874', 19, 48, 2),
(4, '46239887883-89', NULL, '1', '2018-07-25 19:03:40.176118', '2018-07-25 19:03:40.176118', 5, 48, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caja_tipo_cuenta`
--

CREATE TABLE `caja_tipo_cuenta` (
  `id` int(11) NOT NULL,
  `codigo` varchar(10) NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `estado` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `caja_tipo_cuenta`
--

INSERT INTO `caja_tipo_cuenta` (`id`, `codigo`, `nombre`, `estado`) VALUES
(1, 'CAJA', 'CAJA', '1'),
(2, 'BANCOS', 'BANCOS', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caja_tipo_cuenta_finan`
--

CREATE TABLE `caja_tipo_cuenta_finan` (
  `id` int(11) NOT NULL,
  `nombre` varchar(120) NOT NULL,
  `estado` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `caja_tipo_cuenta_finan`
--

INSERT INTO `caja_tipo_cuenta_finan` (`id`, `nombre`, `estado`) VALUES
(1, 'Ahorros', '1'),
(2, 'Corriente', '1'),
(3, 'Crédito', '1'),
(4, 'Débito', '1'),
(5, 'Sueldo', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caja_tipo_ent_finan`
--

CREATE TABLE `caja_tipo_ent_finan` (
  `id` int(11) NOT NULL,
  `nombre` varchar(120) NOT NULL,
  `abreviatura` varchar(40) NOT NULL,
  `concatenar` varchar(60) DEFAULT NULL,
  `estado` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `caja_tipo_ent_finan`
--

INSERT INTO `caja_tipo_ent_finan` (`id`, `nombre`, `abreviatura`, `concatenar`, `estado`) VALUES
(1, 'Empresas Bancarias', 'Empresas Bancarias', NULL, '1'),
(2, 'Entidades Financieras Estatales', 'Entidad financiera', NULL, '1'),
(3, 'Empresas Financieras', 'Empresas Financieras', 'Financiera', '1'),
(4, 'Cajas Municipales de Ahorro y Crédito', 'CMAC', 'Caja', '1'),
(5, 'Cajas Municipales de Crédito y Popular', 'CMCP', NULL, '1'),
(6, 'Cajas Rurales de Ahorro y Crédito', 'CRAC', 'Caja', '1'),
(7, 'Edpymes', 'Edpyme', 'Edpyme', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caja_tipo_medio_pago`
--

CREATE TABLE `caja_tipo_medio_pago` (
  `id` int(11) NOT NULL,
  `codigo` varchar(10) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `descripcion` varchar(120) DEFAULT NULL,
  `estado` varchar(1) NOT NULL,
  `tipo_cuenta_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `caja_tipo_medio_pago`
--

INSERT INTO `caja_tipo_medio_pago` (`id`, `codigo`, `nombre`, `descripcion`, `estado`, `tipo_cuenta_id`) VALUES
(1, 'EFECTIVO', 'Efectivo', 'Pago en efectivo', '1', 1),
(2, 'DEPOCUE', 'Depósito en cuenta', 'Diferentes depósitos en cuenta', '1', 2),
(3, 'PAGOVISA', 'Pago con VISA', 'Pagos por medio de tarjeta VISA', '1', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comun_clasificacion_iiu`
--

CREATE TABLE `comun_clasificacion_iiu` (
  `id` int(11) NOT NULL,
  `codigo` varchar(10) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `estado` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comun_clasificacion_iiu`
--

INSERT INTO `comun_clasificacion_iiu` (`id`, `codigo`, `nombre`, `estado`) VALUES
(1, '0111', 'Cultivo de cereales (excepto arroz), legumbres y semillas oleaginosas', '1'),
(2, '0162', 'Actividades de apoyo a la ganadería', '1'),
(3, '1071', 'Elaboración de productos de panadería.', '1'),
(4, '1410', 'Fabricación de prendas de vestir, excepto prendas de piel', '1'),
(5, '1520', 'Fabricación de calzado', '1'),
(6, '1811', 'Impresión', '1'),
(7, '1812', 'Actividades de servicios relacionadas con la impresión.', '1'),
(8, '2511', 'Fabricación de productos metálicos para uso estructural', '1'),
(9, '3100', 'Fabricación de muebles', '1'),
(10, '4100', 'Construcción de edificios', '1'),
(11, '4321', 'Instalaciones eléctricas', '1'),
(12, '4330', 'Terminación y acabado de edificios', '1'),
(13, '4520', 'Mantenimiento y reparación de vehículos automotores', '1'),
(14, '4530', 'Ventas de partes, piezas y accesorios de vehículos automotores.', '1'),
(15, '4730', 'Venta al por menor de combustible para vehículos automotores en almacenes especializados.', '1'),
(16, '4610', 'Venta al por mayor a cambio de una retribución o por contrata.', '1'),
(17, '4630', 'Venta al por mayor de alimentos, bebidas y tabaco.', '1'),
(18, '4641', 'Venta al por mayor de productos textiles, prendas de vestir y calzado.', '1'),
(19, '4663', 'Venta al por mayor de materiales de construcción, artículos de ferretería y equipo y materiales de fontanería y calefacción.', '1'),
(20, '4690', 'Venta al por mayor no especializada.', '1'),
(21, '4711', 'Venta al por menor en almacenes no especializados con surtido compuesto principalmente de alimentos, bebidas y tabaco.', '1'),
(22, '4719', 'Venta al por menor de otros productos en almacenes no especializados.', '1'),
(23, '4721', 'Venta al por menor de alimentos en comercios especializados', '1'),
(24, '4772', 'Venta al por menor de productos farmacéuticos y medicinales, cosméticos y artículos de tocador en almacenes especializados.', '1'),
(25, '4751', 'Venta al por menor de productos textiles en comercios especializados', '1'),
(26, '4759', 'Venta al por menor de aparatos eléctricos de uso doméstico, muebles, equipo de iluminación y otros enseres domésticos en comercios especializados', '1'),
(27, '4752', 'Venta al por menor de artículos de ferretería, pinturas y productos de vidrio en almacenes especializados.', '1'),
(28, '4753', 'Venta al por menor de tapices, alfombras y cubrimientos para paredes y pisos en comercios especializados', '1'),
(29, '4781', 'Venta al por menor de alimentos, bebidas y tabaco en puestos de venta y mercados', '1'),
(30, '4799', 'Otras actividades de venta al por menor no realizadas en comercios, puestos de venta o mercados', '1'),
(31, '9522', 'Reparación de aparatos de uso doméstico y equipo doméstico y de jardinería', '1'),
(32, '5510', 'Actividades de alojamiento para estancias cortas', '1'),
(33, '5610', 'Actividades de restaurantes y de servicio móvil de comidas', '1'),
(34, '4921', 'Transporte urbano y suburbano de pasajeros por vía terrestre', '1'),
(35, '4922', 'Otros tipos de transporte no regular de pasajeros por vía terrestre.', '1'),
(36, '4923', 'Transporte de carga por carretera.', '1'),
(37, '7911', 'Actividades de agencias de viajes', '1'),
(38, '6810', 'Actividades inmobiliarias realizadas con bienes propios o arrendados', '1'),
(39, '6201', 'Programación informática', '1'),
(40, '6311', 'Procesamiento de datos, hospedaje y actividades conexas', '1'),
(41, '9511', 'Reparación de ordenadores y equipo periférico', '1'),
(42, '6209', 'Otros servicios informáticos y de tecnologías de la información.', '1'),
(43, '7220', 'Investigaciones y desarrollo experimental en el campo de las ciencias sociales y las humanidades.', '1'),
(44, '6910', 'Actividades jurídicas.', '1'),
(45, '6920', 'Actividades de contabilidad, teneduría de libros y auditoría; asesoramiento en materia de impuestos.', '1'),
(46, '7020', 'Actividades de consultoría de gestión', '1'),
(47, '7110', 'Actividades de arquitectura e ingeniería y actividades conexas de consultoría técnica', '1'),
(48, '7310', 'Publicidad.', '1'),
(49, '8020', 'Actividades de servicio de sistemas de seguridad', '1'),
(50, '8129', 'Otras actividades de limpieza de edificios e instalaciones industriales', '1'),
(51, '8299', 'Otras actividades de servicios de apoyo a las empresas n.c.p', '1'),
(52, '8411', 'Actividades de la administración pública en general.', '1'),
(53, '8510', 'Enseñanza pre-escolar y primaria.', '1'),
(54, '8521', 'Enseñanza secundaria de formación general.', '1'),
(55, '8530', 'Enseñanza superior.', '1'),
(56, '8549', 'Otros tipos de enseñanza n.c.p.', '1'),
(57, '8620', 'Actividades de médicos y odontólogos.', '1'),
(58, '8690', 'Otras actividades de atención de la salud humana', '1'),
(59, '9499', 'Actividades de otras asociaciones n.c.p.', '1'),
(60, '6020', 'Programación y transmisiones de televisión', '1'),
(61, '9000', 'Actividades creativas, artísticas y de entretenimiento', '1'),
(62, '9312', 'Actividades de clubes deportivos', '1'),
(63, '9602', 'Peluquería y otros tratamientos de belleza.', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comun_config_direccion_tipo_persona`
--

CREATE TABLE `comun_config_direccion_tipo_persona` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(60) DEFAULT NULL,
  `orden` int(11) NOT NULL,
  `tipo_direccion_id` int(11) NOT NULL,
  `tipo_persona_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comun_config_direccion_tipo_persona`
--

INSERT INTO `comun_config_direccion_tipo_persona` (`id`, `descripcion`, `orden`, `tipo_direccion_id`, `tipo_persona_id`) VALUES
(1, NULL, 3, 1, 1),
(2, NULL, 2, 2, 1),
(3, NULL, 1, 3, 1),
(4, NULL, 1, 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comun_config_documento_tipo_persona`
--

CREATE TABLE `comun_config_documento_tipo_persona` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(60) DEFAULT NULL,
  `orden` int(11) NOT NULL,
  `tipo_documento_id` int(11) NOT NULL,
  `tipo_persona_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comun_config_documento_tipo_persona`
--

INSERT INTO `comun_config_documento_tipo_persona` (`id`, `descripcion`, `orden`, `tipo_documento_id`, `tipo_persona_id`) VALUES
(1, NULL, 1, 1, 1),
(2, NULL, 1, 2, 1),
(3, NULL, 1, 3, 1),
(4, NULL, 1, 4, 1),
(5, NULL, 1, 5, 1),
(6, NULL, 1, 5, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comun_contador`
--

CREATE TABLE `comun_contador` (
  `id` int(11) NOT NULL,
  `codigo` varchar(6) NOT NULL,
  `descripcion` varchar(60) NOT NULL,
  `contador` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comun_contador`
--

INSERT INTO `comun_contador` (`id`, `codigo`, `descripcion`, `contador`) VALUES
(1, 'CDPERS', 'Generación del código de la persona', 15507),
(2, 'ZONAS', 'Generación del código para la tablas zonas de procesos', 115),
(3, 'MOTMOV', 'Generación del código para la tabla MOTIVO MOV CUENTA', 12);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comun_continente`
--

CREATE TABLE `comun_continente` (
  `id` int(11) NOT NULL,
  `codigo` varchar(12) DEFAULT NULL,
  `nombre` varchar(120) NOT NULL,
  `abreviatura` varchar(30) DEFAULT NULL,
  `estado` varchar(1) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comun_continente`
--

INSERT INTO `comun_continente` (`id`, `codigo`, `nombre`, `abreviatura`, `estado`, `created_at`, `updated_at`) VALUES
(1, 'ANORTE', 'América del Norte', 'América del Norte', '1', '2018-02-22 02:10:51.000000', '2018-02-22 02:10:51.000000'),
(2, 'ASUR', 'América del Sur', 'América del Sur', '1', '2018-02-22 02:11:10.000000', '2018-02-22 02:11:10.000000'),
(3, 'ACENTRO', 'Centro América', 'Centro América', '1', '2018-02-22 02:11:43.000000', '2018-02-22 02:11:43.000000'),
(4, 'EURO', 'Europa', 'Europa', '1', '2018-02-22 02:12:15.000000', '2018-02-22 02:12:15.000000'),
(5, 'AFRI', 'África', 'África', '1', '2018-02-22 02:13:04.000000', '2018-04-06 14:23:19.000000'),
(6, 'ASIA', 'Asia', 'Asia', '1', '2018-02-22 02:13:17.000000', '2018-02-22 02:14:11.000000'),
(7, 'OCEA', 'Oceanía', 'Oceanía', '1', '2018-02-22 02:13:34.000000', '2018-05-18 22:05:16.000000'),
(9, '456', 'CamPer', 'CPERU', '1', '2018-07-10 20:35:14.948178', '2018-07-10 20:35:28.689009');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comun_cuenta_contacto`
--

CREATE TABLE `comun_cuenta_contacto` (
  `id` int(11) NOT NULL,
  `codigo` varchar(10) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `logo` varchar(200) DEFAULT NULL,
  `url_web` varchar(200) DEFAULT NULL,
  `orden` int(11) NOT NULL,
  `estado` varchar(1) NOT NULL,
  `tipo_contacto_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comun_cuenta_contacto`
--

INSERT INTO `comun_cuenta_contacto` (`id`, `codigo`, `nombre`, `logo`, `url_web`, `orden`, `estado`, `tipo_contacto_id`) VALUES
(1, 'MOVIS', 'Movistar', NULL, NULL, 1, '1', 1),
(2, 'CLARO', 'Claro', NULL, NULL, 2, '1', 1),
(3, 'BITEL', 'Bitel', NULL, NULL, 3, '1', 1),
(4, 'ENTEL', 'Entel', NULL, NULL, 4, '1', 1),
(5, 'GMAIL', 'Gmail', NULL, NULL, 5, '1', 3),
(6, 'HOTMA', 'Hotmail', NULL, NULL, 6, '1', 3),
(7, 'YAHOO', 'Yahoo!', NULL, NULL, 7, '1', 3),
(8, 'FACEB', 'Facebook', NULL, NULL, 8, '1', 4),
(9, 'TWITT', 'Twitter', NULL, NULL, 9, '1', 4),
(10, 'YOUTU', 'Toutube', NULL, NULL, 10, '1', 4),
(11, 'GITHU', 'GitHub', NULL, NULL, 11, '1', 5),
(12, 'GITLA', 'GitLab', NULL, NULL, 12, '1', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comun_etiqueta_contacto`
--

CREATE TABLE `comun_etiqueta_contacto` (
  `id` int(11) NOT NULL,
  `codigo` varchar(10) NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `tiene_cod_ubigeo` varchar(2) DEFAULT NULL,
  `tiene_anexo` varchar(2) DEFAULT NULL,
  `orden` int(11) NOT NULL,
  `estado` varchar(1) NOT NULL,
  `tipo_contacto_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comun_etiqueta_contacto`
--

INSERT INTO `comun_etiqueta_contacto` (`id`, `codigo`, `nombre`, `tiene_cod_ubigeo`, `tiene_anexo`, `orden`, `estado`, `tipo_contacto_id`) VALUES
(1, 'TELPERS', 'Personal', '', '', 1, '1', 1),
(2, 'TELPROF', 'Profesional', '', '', 2, '1', 1),
(3, 'TELCASA', 'Fijo casa', 'SI', '', 3, '1', 1),
(4, 'TELTRAB', 'Fijo trabajo', 'SI', 'SI', 4, '1', 1),
(5, 'FAXTRAB', 'Trabajo', '', '', 5, '0', 2),
(6, 'FAXCASA', 'Casa', '', '', 6, '0', 2),
(7, 'CORPERS', 'Personal', '', '', 7, '1', 3),
(8, 'CORINST', 'Institucional', '', '', 8, '1', 3),
(9, 'CORTRAB', 'Trabajo', '', '', 9, '1', 3),
(10, 'RDPERS', 'Personal', '', '', 10, '1', 4),
(11, 'DSTRAB', 'Trabajo', '', '', 11, '1', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comun_mes`
--

CREATE TABLE `comun_mes` (
  `id` varchar(2) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `abreviatura` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comun_mes`
--

INSERT INTO `comun_mes` (`id`, `nombre`, `abreviatura`) VALUES
('01', 'Enero', 'Ene'),
('02', 'Febrero', 'Feb'),
('03', 'Marzo', 'Mar'),
('04', 'Abril', 'Abr'),
('05', 'Mayo', 'May'),
('06', 'Junio', 'Jun'),
('07', 'Julio', 'Jul'),
('08', 'Agosto', 'Ago'),
('09', 'Setiembre', 'Set'),
('10', 'Octubre', 'Oct'),
('11', 'Noviembre', 'Nov'),
('12', 'Diciembre', 'Dic');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comun_moneda`
--

CREATE TABLE `comun_moneda` (
  `id` int(11) NOT NULL,
  `codigo` varchar(10) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `nombre_plural` varchar(60) NOT NULL,
  `simbolo` varchar(8) NOT NULL,
  `orden` int(11) NOT NULL,
  `estado` varchar(1) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comun_moneda`
--

INSERT INTO `comun_moneda` (`id`, `codigo`, `nombre`, `nombre_plural`, `simbolo`, `orden`, `estado`, `created_at`, `updated_at`) VALUES
(1, 'PEN', 'Sol', 'Soles', 'S/.', 1, '1', '2018-03-26 15:53:39.000000', '2018-07-10 17:02:49.495351'),
(2, 'USD', 'Dólar', 'Dólares', 'US$', 2, '1', '2018-03-26 15:54:13.000000', '2018-07-09 01:32:21.933172'),
(3, 'EUR', 'Euro', 'Euros', '€', 3, '1', '2018-03-26 15:54:50.000000', '2018-03-26 15:54:50.000000'),
(4, 'ARG', 'Peso argentino', 'Pesos argentinos', '$(ARG)', 4, '1', '2018-03-26 16:05:17.000000', '2018-07-08 22:02:29.252750'),
(5, 'BOB', 'Peso boliviano', 'Pesos bolivianos', '$B', 5, '1', '2018-03-26 16:08:45.000000', '2018-07-09 01:33:50.592719'),
(6, '2345', 'real grane', 'reales granes', '$', 6, '1', '2018-07-09 15:14:39.040475', '2018-07-09 20:58:51.898409'),
(7, '$B', 'Bitcoin', 'Bitcoins', '$B$', 7, '1', '2018-07-09 20:58:17.342052', '2018-07-09 23:42:00.980196');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comun_monedap`
--

CREATE TABLE `comun_monedap` (
  `id` int(11) NOT NULL,
  `moneda_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comun_monedap`
--

INSERT INTO `comun_monedap` (`id`, `moneda_id`) VALUES
(0, 1),
(1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comun_pais`
--

CREATE TABLE `comun_pais` (
  `id` int(11) NOT NULL,
  `codigo` varchar(12) DEFAULT NULL,
  `nombre` varchar(120) NOT NULL,
  `abreviatura` varchar(30) DEFAULT NULL,
  `capital` varchar(80) DEFAULT NULL,
  `codigo_telefono` varchar(10) DEFAULT NULL,
  `estado` varchar(1) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `continente_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comun_pais`
--

INSERT INTO `comun_pais` (`id`, `codigo`, `nombre`, `abreviatura`, `capital`, `codigo_telefono`, `estado`, `created_at`, `updated_at`, `continente_id`) VALUES
(1, 'PE', 'Perú', 'PE', 'Lima', '+51', '1', '2018-02-22 02:31:49.000000', '2018-07-10 18:29:27.173837', 2),
(2, 'ARG', 'Argentina', 'Arg', 'Buenos Aires', '+54', '1', '2018-02-22 02:35:41.000000', '2018-07-10 15:59:48.139582', 2),
(3, 'CL', 'Chile', 'Chile', 'Santiago', '+56', '1', '2018-02-22 02:35:59.000000', '2018-03-26 20:13:45.000000', 2),
(4, 'BO', 'Bolivia', 'Bolivia', 'La Paz', '+591', '1', '2018-02-22 02:36:14.000000', '2018-03-26 20:13:21.000000', 2),
(5, 'EC', 'Ecuador', 'Ecuador', 'Quito', '+593', '1', '2018-02-22 02:36:29.000000', '2018-03-26 20:15:34.000000', 2),
(6, 'ES', 'España', 'España', 'Madrid', '+34', '1', '2018-02-22 02:45:53.000000', '2018-03-26 20:16:04.000000', 4),
(7, 'VN', 'Venezuela', 'Venezuela', 'Caracas', '+58', '1', '2018-03-12 00:43:30.000000', '2018-03-26 20:14:10.000000', 2),
(8, 'BR', 'Brasil', 'Brasil', 'Brasilia', '+55', '1', '2018-03-26 20:12:50.000000', '2018-03-26 20:12:50.000000', 2),
(9, 'CO', 'Colombia', 'Colombia', 'Bogotá', '+57', '1', '2018-03-26 20:16:52.000000', '2018-03-26 20:16:52.000000', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comun_pais_tipo_ubigeo`
--

CREATE TABLE `comun_pais_tipo_ubigeo` (
  `id` int(11) NOT NULL,
  `orden_nivel` int(11) NOT NULL,
  `pais_id` int(11) NOT NULL,
  `tipo_ubigeo_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comun_pais_tipo_ubigeo`
--

INSERT INTO `comun_pais_tipo_ubigeo` (`id`, `orden_nivel`, `pais_id`, `tipo_ubigeo_id`) VALUES
(1, 1, 1, 1),
(2, 2, 1, 2),
(3, 3, 1, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comun_persona`
--

CREATE TABLE `comun_persona` (
  `id` int(11) NOT NULL,
  `codigo` varchar(12) DEFAULT NULL,
  `nombre_completo` varchar(140) NOT NULL,
  `foto_logo` varchar(100) DEFAULT NULL,
  `foto_logo_rec` varchar(100) DEFAULT NULL,
  `estado` varchar(1) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `nacionalidad_id` int(11) DEFAULT NULL,
  `tipo_contribuyente_id` int(11) DEFAULT NULL,
  `tipo_persona_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comun_persona`
--

INSERT INTO `comun_persona` (`id`, `codigo`, `nombre_completo`, `foto_logo`, `foto_logo_rec`, `estado`, `created_at`, `updated_at`, `nacionalidad_id`, `tipo_contribuyente_id`, `tipo_persona_id`) VALUES
(15, '015470', ' María Solano Flores', 'storage/comun/personas/fotos/c704015622f3421388f2c670f1713ce8.png', 'storage/comun/personas/fotos/a0d1ec070f394f518bb7f775ead0dd54.png', '1', '2018-07-11 22:16:45.701144', '2018-07-16 02:15:47.383554', NULL, NULL, 1),
(16, '015471', 'Henry Goodman Solano', 'storage/comun/personas/fotos/15a946d109894faabadb764f47732273.jpg', 'storage/comun/personas/fotos/b99a8d173020443d90c4e2a164bc1644.jpg', '1', '2018-07-11 22:26:17.158434', '2018-07-16 02:16:33.049951', 1, 2, 1),
(18, '015472', 'Persy Quiroz Menor', 'storage/comun/personas/fotos/4b593ab8ef1643cbb7130babba3c357e.png', 'storage/comun/personas/fotos/dac0f6a8d77e4ecb8a15373a3cd66b6c.png', '1', '2018-07-12 13:54:53.023560', '2018-07-16 02:09:27.185821', 1, 1, 1),
(19, '015473', ' Demóstenes Cabrera', 'storage/comun/personas/fotos/b45711f86d0044b78a114d0ea26f1856.jpg', 'storage/comun/personas/fotos/bb83229058b842e79f960cc844f88363.jpg', '1', '2018-07-12 22:45:39.272210', '2018-07-12 22:45:39.273200', NULL, NULL, 1),
(20, '015474', ' José Carlos Marátequi', 'storage/comun/personas/fotos/b5f2f3bda480481594a907c75318b910.jpg', 'storage/comun/personas/fotos/891d7038ce63491c823b3d8dc80651bd.jpg', '1', '2018-07-12 23:01:06.314046', '2018-07-12 23:01:06.314046', NULL, NULL, 1),
(21, '015475', ' José Carlos Marátequi', 'storage/comun/personas/fotos/d09babe1edb645fb8e2923f86da6a64e.jpg', 'storage/comun/personas/fotos/85efb8fdcd094aa58d9f85766a916b1f.jpg', '1', '2018-07-12 23:01:32.246272', '2018-07-12 23:01:32.246272', NULL, NULL, 1),
(22, '015476', ' María Julca Carpio', 'storage/comun/personas/fotos/8419ef794cdb42fb93bf2ad4d2e49e10.jpg', 'storage/comun/personas/fotos/4e230a3d99e54fe7962d51e2e4abc516.jpg', '1', '2018-07-12 23:25:29.527434', '2018-07-12 23:25:29.527434', NULL, NULL, 1),
(23, '015477', ' Demósntes Saposoa Díaz', 'storage/comun/personas/fotos/f87d0f00c6334935a9d89461a243a051.png', 'storage/comun/personas/fotos/f2e553523dac4224980697ed22cc2edf.png', '1', '2018-07-12 23:29:21.783306', '2018-07-12 23:29:21.783306', NULL, NULL, 1),
(24, '015478', ' Carlos Gonzales Díaz', 'storage/comun/personas/fotos/edf4b27937034fa0a4aa07050de93491.png', 'storage/comun/personas/fotos/78f6b5ebaf40450491fdba455013ac16.png', '1', '2018-07-12 23:32:30.109574', '2018-07-12 23:32:30.109574', NULL, NULL, 1),
(25, '015479', ' Mariano Melgar Solano', 'storage/comun/personas/fotos/ea42965ad0044a84b0cfc5734f9942fb.png', 'storage/comun/personas/fotos/d87bc3f0ee0c48bfb851ea57709e5ff8.png', '1', '2018-07-13 00:01:11.248347', '2018-07-13 00:01:11.248347', NULL, NULL, 1),
(26, '015480', 'Luis Alberto Lavado Llaro', 'storage/comun/personas/fotos/ed28d064993b492fae055c6916329e67.jpg', 'storage/comun/personas/fotos/1d8711e7c40d46948c04c4575ecaebf5.jpg', '1', '2018-07-13 17:46:49.767124', '2018-07-16 02:17:09.970484', 1, 1, 1),
(27, '015481', ' Marcos Murillo Sebastian', 'storage/comun/personas/fotos/67bbca8a483646b49871beaa7f8de8ab.png', 'storage/comun/personas/fotos/fcf302e8a64e4b28b6c50a5361b3c8ee.png', '1', '2018-07-15 17:11:04.479576', '2018-07-15 17:11:04.479576', 1, NULL, 1),
(28, '015482', ' Roberto Díaz Cien Fuegos', 'storage/comun/personas/fotos/510afad518504132aee5a9f39e6e7d7a.png', 'storage/comun/personas/fotos/8b5565b13fc04591894ff5fae1cd907f.png', '1', '2018-07-15 17:20:05.060739', '2018-07-15 17:20:05.060739', 1, 2, 1),
(29, '015483', ' Mariano Melgar', '', '', '1', '2018-07-15 17:24:36.654846', '2018-07-15 17:24:36.654846', NULL, NULL, 1),
(30, '015484', ' Henry Taywan Soria', '', '', '1', '2018-07-15 17:29:21.521017', '2018-07-15 17:29:21.521017', NULL, NULL, 1),
(31, '015485', ' Henry Taywan Soria', '', '', '1', '2018-07-15 17:50:54.425122', '2018-07-15 17:50:54.425122', NULL, NULL, 1),
(32, '015486', ' Juan Izquierdo', '', '', '1', '2018-07-15 18:03:46.893062', '2018-07-15 18:03:46.893062', NULL, NULL, 1),
(33, '015489', ' José Carpio Fernández', '', '', '1', '2018-07-15 22:14:27.541138', '2018-07-15 22:14:27.541138', 1, 2, 1),
(34, '015487', ' Sebastian Piñera Solano', '', '', '1', '2018-07-15 22:44:29.255167', '2018-07-15 22:44:29.255167', 1, 2, 1),
(40, '015488', ' Juanita Del Águila Soriano', 'storage/comun/personas/fotos/7961b77025984ff1aecb565f7f6093a1.jpg', 'storage/comun/personas/fotos/1537cda4a5544638b7304f694a8cc7e9.jpg', '1', '2018-07-15 23:16:01.709212', '2018-07-15 23:16:01.709212', 1, 2, 1),
(42, '015490', 'Universidad Peruana Unión', 'storage/comun/personas/fotos/b1a6bd71dcb343ba8a4015e96ca77d96.png', 'storage/comun/personas/fotos/27ab154343584de096589871ecac8bda.png', '1', '2018-07-15 23:54:05.086710', '2018-07-17 18:27:02.940997', 1, 19, 2),
(43, '015491', 'Colegio de Ingeniero del Perú', NULL, NULL, '1', '2018-07-16 00:25:34.943098', '2018-07-16 00:25:34.943098', 1, 49, 2),
(47, '015501', ' Sebatian Vásquez Moltalvo', NULL, NULL, '1', '2018-07-16 01:55:49.958363', '2018-07-16 01:55:49.958363', NULL, NULL, 1),
(48, '015505', 'JUNTA ADMINISTRADORA DE SERVICIOS DE SANEAMIENTO SL', 'storage/comun/personas/fotos/a9bd377e0c43483aa3b8e63031b09a7a.png', 'storage/comun/personas/fotos/0b4e614070894449997cf39a2f21d915.png', '1', '2018-07-17 11:15:05.857089', '2018-07-17 11:15:05.857089', 1, 11, 2),
(49, '015506', 'INDUSTRIA PERUANA SANTA LUCIA', '', '', '1', '2018-07-17 16:23:56.127230', '2018-07-17 16:23:56.128213', 1, 39, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comun_persona_contacto`
--

CREATE TABLE `comun_persona_contacto` (
  `id` int(11) NOT NULL,
  `valor_contacto` varchar(90) NOT NULL,
  `anexo` varchar(10) DEFAULT NULL,
  `area_oficina` varchar(60) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `cuenta_contacto_id` int(11) DEFAULT NULL,
  `etiqueta_contacto_id` int(11) DEFAULT NULL,
  `pais_id` int(11) DEFAULT NULL,
  `persona_id` int(11) NOT NULL,
  `tipo_contacto_id` int(11) NOT NULL,
  `ubigeo_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comun_persona_contacto`
--

INSERT INTO `comun_persona_contacto` (`id`, `valor_contacto`, `anexo`, `area_oficina`, `created_at`, `updated_at`, `cuenta_contacto_id`, `etiqueta_contacto_id`, `pais_id`, `persona_id`, `tipo_contacto_id`, `ubigeo_id`) VALUES
(6, '02998333', '123', 'Nueva Solución', '2018-07-12 23:32:30.524253', '2018-07-12 23:32:30.524253', 1, 4, 1, 24, 1, NULL),
(7, '4343244', '123', 'Nueva Requena', '2018-07-13 00:01:11.856375', '2018-07-13 00:01:11.856375', 1, 4, 1, 25, 1, 1),
(8, '943986745', NULL, NULL, '2018-07-15 22:14:27.811439', '2018-07-15 22:14:27.811439', 1, 1, 1, 33, 1, NULL),
(9, '941946943', '123', 'Nueva Requena', '2018-07-13 00:01:11.856375', '2018-07-13 00:01:11.856375', 1, 1, 1, 18, 1, 1),
(10, 'persy.quiroz@upeu.edu.pe', NULL, NULL, '2018-07-19 15:22:10.759663', '2018-07-19 15:22:10.759663', 5, 7, NULL, 18, 3, NULL),
(11, 'persy.quiroz@hotmail.com', NULL, NULL, '2018-07-19 15:31:41.129905', '2018-07-19 15:31:41.129905', 6, 7, NULL, 18, 3, NULL),
(12, '564517', NULL, NULL, '2018-07-26 14:34:13.625509', '2018-07-26 14:34:13.625509', 1, 4, 1, 48, 1, 22);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comun_persona_documento`
--

CREATE TABLE `comun_persona_documento` (
  `id` int(11) NOT NULL,
  `numero` varchar(20) NOT NULL,
  `orden` int(11) NOT NULL,
  `persona_id` int(11) NOT NULL,
  `tipo_documento_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comun_persona_documento`
--

INSERT INTO `comun_persona_documento` (`id`, `numero`, `orden`, `persona_id`, `tipo_documento_id`) VALUES
(4, '48160622', 1, 18, 1),
(5, '10481606221', 2, 18, 5),
(6, '47370895', 1, 26, 1),
(7, '10473708952', 2, 26, 5),
(8, '99922828', 1, 27, 1),
(9, '45464749', 1, 28, 1),
(10, '20138122256', 1, 32, 5),
(11, '45464746', 1, 33, 1),
(12, '45463637', 1, 34, 1),
(13, '82872727', 1, 40, 1),
(14, '20156736363', 1, 42, 5),
(15, '67637373333', 1, 43, 5),
(19, '48160621', 1, 47, 1),
(21, '20450401863', 1, 48, 5),
(22, '20542390507', 1, 49, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comun_persona_fecha`
--

CREATE TABLE `comun_persona_fecha` (
  `id` int(11) NOT NULL,
  `anio` int(11) DEFAULT NULL,
  `dia` varchar(2) DEFAULT NULL,
  `mes_id` varchar(2) DEFAULT NULL,
  `persona_id` int(11) NOT NULL,
  `tipo_fecha_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comun_persona_fecha`
--

INSERT INTO `comun_persona_fecha` (`id`, `anio`, `dia`, `mes_id`, `persona_id`, `tipo_fecha_id`) VALUES
(2, 1993, '22', '04', 18, 1),
(3, NULL, NULL, NULL, 19, 1),
(4, 1991, '17', '03', 26, 1),
(5, 2012, '10', '04', 28, 1),
(6, 2012, '23', '04', 33, 1),
(7, 2000, '02', '11', 34, 1),
(9, 2010, '30', '03', 40, 1),
(10, 1919, '30', '04', 42, 2),
(11, 1992, '21', '04', 43, 2),
(12, 2013, '02', '10', 49, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comun_persona_juridica`
--

CREATE TABLE `comun_persona_juridica` (
  `id` int(11) NOT NULL,
  `razon_social` varchar(90) NOT NULL,
  `nombre_comercial` varchar(70) DEFAULT NULL,
  `nombre_repres_legal` varchar(120) DEFAULT NULL,
  `nro_resolucion` varchar(120) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `repres_legal_id` int(11) DEFAULT NULL,
  `tipo_ambito_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comun_persona_juridica`
--

INSERT INTO `comun_persona_juridica` (`id`, `razon_social`, `nombre_comercial`, `nombre_repres_legal`, `nro_resolucion`, `created_at`, `updated_at`, `repres_legal_id`, `tipo_ambito_id`) VALUES
(42, 'Universidad Peruana Unión', 'UPeU', NULL, '9983838832', '2018-07-15 23:54:05.161769', '2018-07-15 23:54:05.161769', NULL, 5),
(43, 'Colegio de Ingeniero del Perú', 'CIP', NULL, '8277272727', '2018-07-16 00:25:35.029167', '2018-07-16 00:25:35.029167', 18, 2),
(48, 'JUNTA ADMINISTRADORA DE SERVICIOS DE SANEAMIENTO SL', 'JASS SANTA LUCÍA', NULL, NULL, '2018-07-17 11:15:05.948156', '2018-07-17 11:15:05.948156', NULL, NULL),
(49, 'INDUSTRIA PERUANA SANTA LUCIA', NULL, 'Manosalva Cubas Roger Salvador', NULL, '2018-07-17 16:23:56.214268', '2018-07-17 16:23:56.214268', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comun_persona_juridica_actividad`
--

CREATE TABLE `comun_persona_juridica_actividad` (
  `id` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `clasificacion_iiu_id` int(11) NOT NULL,
  `persona_natural_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comun_persona_juridica_actividad`
--

INSERT INTO `comun_persona_juridica_actividad` (`id`, `orden`, `clasificacion_iiu_id`, `persona_natural_id`) VALUES
(4, 1, 37, 42),
(5, 1, 36, 49),
(6, 2, 62, 42);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comun_persona_natural`
--

CREATE TABLE `comun_persona_natural` (
  `id` int(11) NOT NULL,
  `nombres` varchar(90) NOT NULL,
  `papellido` varchar(70) NOT NULL,
  `sapellido` varchar(70) DEFAULT NULL,
  `sexo` varchar(3) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comun_persona_natural`
--

INSERT INTO `comun_persona_natural` (`id`, `nombres`, `papellido`, `sapellido`, `sexo`, `created_at`, `updated_at`) VALUES
(15, 'María', 'Solano', 'Flores', NULL, '2018-07-11 22:16:45.821075', '2018-07-11 22:16:45.821075'),
(16, 'Henry', 'Goodman', 'Solano', 'MAS', '2018-07-11 22:26:17.282207', '2018-07-11 22:26:17.282207'),
(18, 'Persy', 'Quiroz', 'Menor', 'MAS', '2018-07-12 13:54:53.213351', '2018-07-12 13:54:53.213351'),
(19, 'Demóstenes', 'Cabrera', NULL, NULL, '2018-07-12 22:45:40.780428', '2018-07-12 22:45:40.780428'),
(20, 'José', 'Carlos', 'Marátequi', 'MAS', '2018-07-12 23:01:06.423949', '2018-07-12 23:01:06.423949'),
(21, 'José', 'Carlos', 'Marátequi', 'MAS', '2018-07-12 23:01:32.364922', '2018-07-12 23:01:32.365924'),
(22, 'María', 'Julca', 'Carpio', 'FEM', '2018-07-12 23:25:29.645442', '2018-07-12 23:25:29.645442'),
(23, 'Demósntes', 'Saposoa', 'Díaz', 'MAS', '2018-07-12 23:29:21.963072', '2018-07-12 23:29:21.963072'),
(24, 'Carlos', 'Gonzales', 'Díaz', NULL, '2018-07-12 23:32:30.262088', '2018-07-12 23:32:30.262088'),
(25, 'Mariano', 'Melgar', 'Solano', 'MAS', '2018-07-13 00:01:11.620368', '2018-07-13 00:01:11.620368'),
(26, 'Luis Alberto', 'Lavado', 'Llaro', 'MAS', '2018-07-13 17:46:49.875098', '2018-07-13 17:46:49.875098'),
(27, 'Marcos', 'Murillo', 'Sebastian', 'MAS', '2018-07-15 17:11:04.667436', '2018-07-15 17:11:04.667436'),
(28, 'Roberto', 'Díaz', 'Cien Fuegos', 'MAS', '2018-07-15 17:20:05.140796', '2018-07-15 17:20:05.140796'),
(29, 'Mariano', 'Melgar', NULL, NULL, '2018-07-15 17:24:36.773925', '2018-07-15 17:24:36.773925'),
(30, 'Henry', 'Taywan', 'Soria', 'MAS', '2018-07-15 17:29:21.636582', '2018-07-15 17:29:21.637580'),
(31, 'Henry', 'Taywan', 'Soria', 'MAS', '2018-07-15 17:50:54.557129', '2018-07-15 17:50:54.557129'),
(32, 'Juan', 'Izquierdo', NULL, 'MAS', '2018-07-15 18:03:46.988073', '2018-07-15 18:03:46.988073'),
(33, 'José', 'Carpio', 'Fernández', 'MAS', '2018-07-15 22:14:27.633274', '2018-07-15 22:14:27.633274'),
(34, 'Sebastian', 'Piñera', 'Solano', 'MAS', '2018-07-15 22:44:29.329215', '2018-07-15 22:44:29.329215'),
(40, 'Juanita', 'Del Águila', 'Soriano', 'FEM', '2018-07-15 23:16:01.774968', '2018-07-15 23:16:01.774968'),
(47, 'Sebatian', 'Vásquez', 'Moltalvo', NULL, '2018-07-16 01:55:50.028095', '2018-07-16 01:55:50.029097');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comun_persona_natural_oficio`
--

CREATE TABLE `comun_persona_natural_oficio` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(60) DEFAULT NULL,
  `orden` int(11) NOT NULL,
  `persona_natural_id` int(11) NOT NULL,
  `profesion_oficio_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comun_persona_natural_oficio`
--

INSERT INTO `comun_persona_natural_oficio` (`id`, `descripcion`, `orden`, `persona_natural_id`, `profesion_oficio_id`) VALUES
(3, NULL, 1, 40, 5),
(4, NULL, 2, 40, 7),
(5, NULL, 3, 40, 3),
(10, 'de Sistemas', 1, 18, 34),
(11, NULL, 2, 18, 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comun_profesion_oficio`
--

CREATE TABLE `comun_profesion_oficio` (
  `id` int(11) NOT NULL,
  `codigo` varchar(10) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `estado` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comun_profesion_oficio`
--

INSERT INTO `comun_profesion_oficio` (`id`, `codigo`, `nombre`, `estado`) VALUES
(1, '1', 'Abogado', '1'),
(2, '2', 'Actor, artista y director de espectáculos', '1'),
(3, '3', 'Administrador de empresas (profesional)', '1'),
(4, '4', 'Agrimensor y topógrafo', '1'),
(5, '5', 'Agrónomo', '1'),
(6, '6', 'Albañil', '1'),
(7, '7', 'Analistas de sistema y computación', '1'),
(8, '8', 'Antropólogo, arqueólogo y etnólogo', '1'),
(9, '9', 'Arquitecto', '1'),
(10, '10', 'Artesano de cuero', '1'),
(11, '11', 'Artesano textil', '1'),
(12, '12', 'Autor literario, escritor y critico', '1'),
(13, '13', 'Bacteriólogo, farmacólogo', '1'),
(14, '14', 'Biólogo', '1'),
(15, '15', 'Carpintero', '1'),
(16, '16', 'Conductor de vehículos de motor', '1'),
(17, '17', 'Contador', '1'),
(18, '18', 'Coreógrafo y bailarines', '1'),
(19, '19', 'Cosmetólogo, peluquero y barbero', '1'),
(20, '20', 'Decorador, dibujante, publicista, diseñador de publicidad', '1'),
(21, '21', 'Deportista profesional y atleta', '1'),
(22, '22', 'Director de empresas', '1'),
(23, '23', 'Economista', '1'),
(24, '24', 'Electricista (técnico)', '1'),
(25, '25', 'Enfermero', '1'),
(26, '26', 'Entrenador deportivo', '1'),
(27, '27', 'Escenógrafo', '1'),
(28, '28', 'Escultor', '1'),
(29, '29', 'Especialista en tratamiento de belleza', '1'),
(30, '30', 'Farmacéutico', '1'),
(31, '31', 'Fotógrafo y operadores cámara, cine y tv', '1'),
(32, '32', 'Gasfitero', '1'),
(33, '33', 'Geógrafo', '1'),
(34, '34', 'Ingeniero', '1'),
(35, '35', 'Interprete, traductor, filosofo', '1'),
(36, '36', 'Joyero y/o platero', '1'),
(37, '37', 'Laboratorista (técnico)', '1'),
(38, '38', 'Locutor de radio, tv', '1'),
(39, '39', 'Mecánico motores aviones y naves marinas', '1'),
(40, '40', 'Mecánico de vehículos de motor', '1'),
(41, '41', 'Médico y cirujano', '1'),
(42, '42', 'Modelo', '1'),
(43, '43', 'Músico', '1'),
(44, '44', 'Nutricionista', '1'),
(45, '45', 'Obstetriz', '1'),
(46, '46', 'Odontólogo', '1'),
(47, '47', 'Periodista', '1'),
(48, '48', 'Piloto de aeronaves', '1'),
(49, '49', 'Pintor', '1'),
(50, '50', 'Profesor', '1'),
(51, '51', 'Psicólogo', '1'),
(52, '52', 'Radio técnico', '1'),
(53, '53', 'Regidores de municipalidades', '1'),
(54, '54', 'Relacionista público e industrial', '1'),
(55, '55', 'Sastre', '1'),
(56, '56', 'Sociólogo', '1'),
(57, '57', 'Tapicero', '1'),
(58, '58', 'Taxidermista, disecador de animales', '1'),
(59, '59', 'Veterinario', '1'),
(60, '60', 'Podólogos', '1'),
(61, '61', 'Archivero', '1'),
(62, '62', 'Albacea', '1'),
(63, '63', 'Gestor de negocio', '1'),
(64, '64', 'Mandatario', '1'),
(65, '65', 'Sindico', '1'),
(66, '66', 'Tecnólogos médicos', '1'),
(67, '99', 'Profesión u ocupación no especificada', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comun_region_natural`
--

CREATE TABLE `comun_region_natural` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `orden` int(11) NOT NULL,
  `estado` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comun_region_natural`
--

INSERT INTO `comun_region_natural` (`id`, `nombre`, `orden`, `estado`) VALUES
(1, 'Costa', 1, '1'),
(2, 'Sierra', 2, '1'),
(3, 'Selva Alta', 3, '1'),
(4, 'Selva Baja', 4, '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comun_tipo_ambito`
--

CREATE TABLE `comun_tipo_ambito` (
  `id` int(11) NOT NULL,
  `nombre` varchar(90) NOT NULL,
  `abreviatura` varchar(30) DEFAULT NULL,
  `estado` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comun_tipo_ambito`
--

INSERT INTO `comun_tipo_ambito` (`id`, `nombre`, `abreviatura`, `estado`) VALUES
(1, 'Entidad Religiosa', 'Ent. Relig.', '1'),
(2, 'Instituciones Educativas', 'Inst. Educ.', '1'),
(3, 'Fundación', 'Fund.', '1'),
(4, 'Sector Público', 'Sec. Pub.', '1'),
(5, 'IPREDA', 'IPREDA', '1'),
(6, 'ONG', 'ONG', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comun_tipo_contacto`
--

CREATE TABLE `comun_tipo_contacto` (
  `id` int(11) NOT NULL,
  `codigo` varchar(10) NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `nombre_plural` varchar(50) NOT NULL,
  `denom_valor` varchar(40) DEFAULT NULL,
  `icono_material` varchar(40) DEFAULT NULL,
  `orden` int(11) NOT NULL,
  `estado` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comun_tipo_contacto`
--

INSERT INTO `comun_tipo_contacto` (`id`, `codigo`, `nombre`, `nombre_plural`, `denom_valor`, `icono_material`, `orden`, `estado`) VALUES
(1, 'TEL', 'Teléfono', 'Teléfonos', 'Número', '', 1, '1'),
(2, 'FAX', 'Fax', 'Fax', 'Número', '', 2, '0'),
(3, 'COR', 'Correo', 'Correos', 'Correo', '', 3, '1'),
(4, 'RSO', 'Red social', 'Redes sociales', 'Nombre de cuenta', '', 4, '1'),
(5, 'REV', 'Repositorio virtual', 'Repositorios virtuales', 'Nombre de cuenta', '', 5, '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comun_tipo_contribuyente`
--

CREATE TABLE `comun_tipo_contribuyente` (
  `id` int(11) NOT NULL,
  `codigo` varchar(8) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `abreviatura` varchar(30) DEFAULT NULL,
  `estado` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comun_tipo_contribuyente`
--

INSERT INTO `comun_tipo_contribuyente` (`id`, `codigo`, `nombre`, `abreviatura`, `estado`) VALUES
(1, '01', 'Persona natural sin negocio', NULL, '1'),
(2, '02', 'Persona natural con negocio', NULL, '1'),
(3, '03', 'Sociedad conyugal sin negocio', NULL, '1'),
(4, '04', 'Sociedad conyugal con negocio', NULL, '1'),
(5, '05', 'Sucesión indivisa sin negocio', NULL, '1'),
(6, '06', 'Sucesión indivisa con negocio', NULL, '1'),
(7, '07', 'Empresa individual de resp. Ltda', NULL, '1'),
(8, '08', 'Sociedad civil', NULL, '1'),
(9, '09', 'Sociedad irregular', NULL, '1'),
(10, '10', 'Asociación en participación', NULL, '1'),
(11, '11', 'Asociación', NULL, '1'),
(12, '12', 'Fundación', NULL, '1'),
(13, '13', 'Sociedad en comandita simple', NULL, '1'),
(14, '14', 'Sociedad colectiva', NULL, '1'),
(15, '15', 'Instituciones publicas', NULL, '1'),
(16, '16', 'Instituciones religiosas', NULL, '1'),
(17, '17', 'Sociedad de beneficencia', NULL, '1'),
(18, '18', 'Entidades de auxilio mutuo', NULL, '1'),
(19, '19', 'Univers. Centros educat. Y cult.', NULL, '1'),
(20, '20', 'Gobierno regional, local', NULL, '1'),
(21, '21', 'Gobierno central', NULL, '1'),
(22, '22', 'Comunidad laboral', NULL, '1'),
(23, '23', 'Comunidad campesina,nativa,comunal', NULL, '1'),
(24, '24', 'Cooperativas, sais, caps', NULL, '1'),
(25, '25', 'Empresa de propiedad social', NULL, '1'),
(26, '26', 'Sociedad anonima', NULL, '1'),
(27, '27', 'Sociedad en comandita por acciones', NULL, '1'),
(28, '28', 'Soc.com.respons. Ltda', NULL, '1'),
(29, '29', 'Suc,ag.emp.extranj,est.perm no dom.', NULL, '1'),
(30, '30', 'Empresa de derecho publico', NULL, '1'),
(31, '31', 'Empresa estatal de derecho privado', NULL, '1'),
(32, '32', 'Empresa de economía mixta', NULL, '1'),
(33, '33', 'Accionariado del estado', NULL, '1'),
(34, '34', 'Misiones diplomáticas y org. Inter.', NULL, '1'),
(35, '35', 'Junta de propietarios', NULL, '1'),
(36, '36', 'Of. Representación de no domiciliado', NULL, '1'),
(37, '37', 'Fondos mutuos de inversión', NULL, '1'),
(38, '38', 'Sociedad anónima abierta', NULL, '1'),
(39, '39', 'Sociedad anónima cerrada', NULL, '1'),
(40, '40', 'Contratos colaboración empresarial', NULL, '1'),
(41, '41', 'Ent.inst.cooperac.tecnica - eniex', NULL, '1'),
(42, '42', 'Comunidad de bienes', NULL, '1'),
(43, '43', 'Sociedad minera de resp.limitada', NULL, '1'),
(44, '44', 'Asoc. Fundac. Y comite no inscritos', NULL, '1'),
(45, '45', 'Partidos,movim, alianzas politicas', NULL, '1'),
(46, '46', 'Asoc. De hecho de profesionales', NULL, '1'),
(47, '47', 'Cafaes y subcafaes', NULL, '1'),
(48, '48', 'Sindicatos y federaciones', NULL, '1'),
(49, '49', 'Colegios profesionales', NULL, '1'),
(50, '50', 'Comités inscritos', NULL, '1'),
(51, '51', 'Organizaciones sociales de base', NULL, '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comun_tipo_direccion`
--

CREATE TABLE `comun_tipo_direccion` (
  `id` int(11) NOT NULL,
  `codigo` varchar(8) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `abreviatura` varchar(12) NOT NULL,
  `orden` int(11) NOT NULL,
  `estado` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comun_tipo_direccion`
--

INSERT INTO `comun_tipo_direccion` (`id`, `codigo`, `nombre`, `abreviatura`, `orden`, `estado`) VALUES
(1, 'DOMFIS', 'Domicilio fiscal', 'Domic. Fisca', 2, '1'),
(2, 'PROCED', 'Procedencia', 'Proced.', 3, '0'),
(3, 'DOMACT', 'Domicilio actual', 'Domic. act.', 1, '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comun_tipo_documento`
--

CREATE TABLE `comun_tipo_documento` (
  `id` int(11) NOT NULL,
  `codigo` varchar(6) NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `descripcion` varchar(60) NOT NULL,
  `patron_angular` varchar(60) DEFAULT NULL,
  `orden` int(11) NOT NULL,
  `estado` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comun_tipo_documento`
--

INSERT INTO `comun_tipo_documento` (`id`, `codigo`, `nombre`, `descripcion`, `patron_angular`, `orden`, `estado`) VALUES
(1, '1', 'DNI', 'Documento Nacional de Identidad', '^[0-9]{8,8}$', 1, '1'),
(2, '4', 'Carné de Extranjería', 'Carné de Extranjería', '', 2, '1'),
(3, '7', 'Pasaporte', 'Pasaporte', '^[a-zA-Z0-9]{12,12}$', 3, '1'),
(4, 'A', 'Cédula de Identidad', 'Cédula Diplomática de Identidad', '^[0-9]{8,8}$', 4, '1'),
(5, 'RUC', 'RUC', 'Registro Único de Contribuyentes', '^[0-9]{11,11}$', 5, '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comun_tipo_entidad`
--

CREATE TABLE `comun_tipo_entidad` (
  `id` int(11) NOT NULL,
  `codigo` varchar(6) NOT NULL,
  `nombre` varchar(120) NOT NULL,
  `estado` varchar(1) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comun_tipo_entidad`
--

INSERT INTO `comun_tipo_entidad` (`id`, `codigo`, `nombre`, `estado`, `created_at`, `updated_at`) VALUES
(1, 'PRINCI', 'Principal', '1', '2018-07-04 10:12:56.000000', '2018-07-04 10:12:56.000000'),
(2, 'SUCURS', 'Sucursal', '1', '2018-07-04 10:12:56.000000', '2018-07-04 10:12:56.000000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comun_tipo_fecha`
--

CREATE TABLE `comun_tipo_fecha` (
  `id` int(11) NOT NULL,
  `codigo` varchar(8) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `orden` int(11) NOT NULL,
  `estado` varchar(1) NOT NULL,
  `tipo_persona_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comun_tipo_fecha`
--

INSERT INTO `comun_tipo_fecha` (`id`, `codigo`, `nombre`, `orden`, `estado`, `tipo_persona_id`) VALUES
(1, 'NACI', 'Nacimiento', 1, '1', 1),
(2, 'FUND', 'Fundación', 2, '1', 2),
(3, 'INIC', 'Inicio', 3, '1', 2),
(4, 'APER', 'Apertura', 4, '1', 2),
(5, 'LANZ', 'Lanzamiento', 5, '1', 2),
(6, 'CREA', 'Creación', 6, '1', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comun_tipo_numero_via`
--

CREATE TABLE `comun_tipo_numero_via` (
  `id` int(11) NOT NULL,
  `codigo` varchar(8) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `abreviatura` varchar(12) NOT NULL,
  `orden` int(11) NOT NULL,
  `estado` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comun_tipo_numero_via`
--

INSERT INTO `comun_tipo_numero_via` (`id`, `codigo`, `nombre`, `abreviatura`, `orden`, `estado`) VALUES
(1, 'Nro', 'Número', 'Nro.', 1, '1'),
(2, 'Km', 'Kilómetro', 'Km.', 2, '1'),
(3, 'S/N', 'Sin número', 's/n', 3, '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comun_tipo_persona`
--

CREATE TABLE `comun_tipo_persona` (
  `id` int(11) NOT NULL,
  `codigo` varchar(8) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `orden` int(11) NOT NULL,
  `estado` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comun_tipo_persona`
--

INSERT INTO `comun_tipo_persona` (`id`, `codigo`, `nombre`, `orden`, `estado`) VALUES
(1, '01', 'Persona natural', 1, '1'),
(2, '02', 'Persona jurídica', 2, '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comun_tipo_ubigeo`
--

CREATE TABLE `comun_tipo_ubigeo` (
  `id` int(11) NOT NULL,
  `codigo` varchar(8) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `nombre_plural` varchar(60) NOT NULL,
  `orden` int(11) NOT NULL,
  `estado` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comun_tipo_ubigeo`
--

INSERT INTO `comun_tipo_ubigeo` (`id`, `codigo`, `nombre`, `nombre_plural`, `orden`, `estado`) VALUES
(1, 'DEPA', 'Departamento', 'Departamentos', 1, '1'),
(2, 'PROV', 'Provincia', 'Provincias', 2, '1'),
(3, 'DIST', 'Distrito', 'Distritos', 3, '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comun_tipo_via`
--

CREATE TABLE `comun_tipo_via` (
  `id` int(11) NOT NULL,
  `codigo` varchar(6) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `abreviatura` varchar(30) NOT NULL,
  `estado` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comun_tipo_via`
--

INSERT INTO `comun_tipo_via` (`id`, `codigo`, `nombre`, `abreviatura`, `estado`) VALUES
(1, '01', 'Avenida', 'Av.', '1'),
(2, '02', 'Jirón', 'Jr.', '1'),
(3, '03', 'Calle', 'Call.', '1'),
(4, '04', 'Pasaje', 'Pje.', '1'),
(5, '05', 'Alameda', 'Alam.', '1'),
(6, '06', 'Malecón', 'Male.', '1'),
(7, '07', 'Ovalo', 'Ovl.', '1'),
(8, '08', 'Parque', 'Pque.', '1'),
(9, '09', 'Plaza', 'Pza.', '1'),
(10, '10', 'Carretera', 'Carr.', '1'),
(11, '11', 'Block', 'Block', '1'),
(12, '123', 'camilosalas', 'camilo', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comun_tipo_zona`
--

CREATE TABLE `comun_tipo_zona` (
  `id` int(11) NOT NULL,
  `codigo` varchar(6) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `abreviatura` varchar(30) NOT NULL,
  `estado` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comun_tipo_zona`
--

INSERT INTO `comun_tipo_zona` (`id`, `codigo`, `nombre`, `abreviatura`, `estado`) VALUES
(1, '01', 'Urbanización', 'Urb.', '1'),
(2, '02', 'Pueblo Joven', 'P.J.', '1'),
(3, '03', 'Unidad Vecinal', 'Unid. Vec.', '1'),
(4, '04', 'Conjunto Habitacional', 'Cjto. Hab.', '1'),
(5, '05', 'Asentamiento Humano', 'Asen. Hum.', '1'),
(6, '06', 'Cooperativa', 'Coop.', '1'),
(7, '07', 'Residencial', 'Resid.', '1'),
(8, '08', 'Zona Industrial', 'Zon. Ind.', '1'),
(9, '09', 'Grupo', 'Gpo.', '1'),
(10, '10', 'Caserío', 'Cas.', '1'),
(11, '11', 'Fundo', 'Fundo', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comun_ubigeo`
--

CREATE TABLE `comun_ubigeo` (
  `id` int(11) NOT NULL,
  `codigo` varchar(20) DEFAULT NULL,
  `nombre` varchar(70) NOT NULL,
  `codigo_telefono` varchar(20) DEFAULT NULL,
  `capital` varchar(80) DEFAULT NULL,
  `poblacion` int(11) DEFAULT NULL,
  `area_km2` decimal(12,2) DEFAULT NULL,
  `padre_id` int(11) DEFAULT NULL,
  `pais_id` int(11) NOT NULL,
  `region_natural_id` int(11) DEFAULT NULL,
  `tipo_ubigeo_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comun_ubigeo`
--

INSERT INTO `comun_ubigeo` (`id`, `codigo`, `nombre`, `codigo_telefono`, `capital`, `poblacion`, `area_km2`, `padre_id`, `pais_id`, `region_natural_id`, `tipo_ubigeo_id`) VALUES
(1, '01', 'Amazonas', '041', 'Chachapoyas', NULL, NULL, NULL, 1, NULL, 1),
(2, '02', 'Áncash', '043', 'Huaraz', NULL, NULL, NULL, 1, NULL, 1),
(3, '03', 'Apurímac', '083', 'Abancay', NULL, NULL, NULL, 1, NULL, 1),
(4, '04', 'Arequipa', '054', 'Arequipa', NULL, NULL, NULL, 1, NULL, 1),
(5, '05', 'Ayacucho', '066', 'Ayacucho', NULL, NULL, NULL, 1, NULL, 1),
(6, '06', 'Cajamarca', '076', 'Cajamarca', NULL, NULL, NULL, 1, NULL, 1),
(7, '07', 'Callao', '001', 'Callao', NULL, NULL, NULL, 1, NULL, 1),
(8, '08', 'Cusco', '084', 'Cusco', NULL, NULL, NULL, 1, NULL, 1),
(9, '09', 'Huancavelica', '067', 'Huancavelica', NULL, NULL, NULL, 1, NULL, 1),
(10, '10', 'Huánuco', '062', 'Huánuco', NULL, NULL, NULL, 1, NULL, 1),
(11, '11', 'Ica', '056', 'Ica', NULL, NULL, NULL, 1, NULL, 1),
(12, '12', 'Junín', '064', 'Huancayo', NULL, NULL, NULL, 1, NULL, 1),
(13, '13', 'La Libertad', '044', 'Trujillo', NULL, NULL, NULL, 1, NULL, 1),
(14, '14', 'Lambayeque', '074', 'Chiclayo', NULL, NULL, NULL, 1, NULL, 1),
(15, '15', 'Lima', '01', 'Lima', NULL, NULL, NULL, 1, NULL, 1),
(16, '16', 'Loreto', '065', 'Iquitos', NULL, NULL, NULL, 1, NULL, 1),
(17, '17', 'Madre De Dios', '082', 'Puerto Maldonado', NULL, NULL, NULL, 1, NULL, 1),
(18, '18', 'Moquegua', '053', 'Moquegua', NULL, NULL, NULL, 1, NULL, 1),
(19, '19', 'Pasco', '063', 'Cerro de Pasco', NULL, NULL, NULL, 1, NULL, 1),
(20, '20', 'Piura', '073', 'Piura', NULL, NULL, NULL, 1, NULL, 1),
(21, '21', 'Puno', '051', 'Puno', NULL, NULL, NULL, 1, NULL, 1),
(22, '22', 'San Martín', '042', 'Moyobamba', NULL, NULL, NULL, 1, NULL, 1),
(23, '23', 'Tacna', '052', 'Tacna', NULL, NULL, NULL, 1, NULL, 1),
(24, '24', 'Tumbes', '072', 'Tumbes', NULL, NULL, NULL, 1, NULL, 1),
(25, '25', 'Ucayali', '061', 'Pucallpa', NULL, NULL, NULL, 1, NULL, 1),
(26, '0101', 'Chachapoyas', NULL, NULL, NULL, NULL, 1, 1, NULL, 2),
(27, '0102', 'Bagua', NULL, NULL, NULL, NULL, 1, 1, NULL, 2),
(28, '0103', 'Bongara', NULL, NULL, NULL, NULL, 1, 1, NULL, 2),
(29, '0104', 'Condorcanqui', NULL, NULL, NULL, NULL, 1, 1, NULL, 2),
(30, '0105', 'Luya', NULL, NULL, NULL, NULL, 1, 1, NULL, 2),
(31, '0106', 'Rodriguez De Mendoza', NULL, NULL, NULL, NULL, 1, 1, NULL, 2),
(32, '0107', 'Utcubamba', NULL, NULL, NULL, NULL, 1, 1, NULL, 2),
(33, '0201', 'Huaraz', NULL, NULL, NULL, NULL, 2, 1, NULL, 2),
(34, '0202', 'Aija', NULL, NULL, NULL, NULL, 2, 1, NULL, 2),
(35, '0203', 'Antonio Raymondi', NULL, NULL, NULL, NULL, 2, 1, NULL, 2),
(36, '0204', 'Asuncion', NULL, NULL, NULL, NULL, 2, 1, NULL, 2),
(37, '0205', 'Bolognesi', NULL, NULL, NULL, NULL, 2, 1, NULL, 2),
(38, '0206', 'Carhuaz', NULL, NULL, NULL, NULL, 2, 1, NULL, 2),
(39, '0207', 'Carlos Fermin Fitzca', NULL, NULL, NULL, NULL, 2, 1, NULL, 2),
(40, '0208', 'Casma', NULL, NULL, NULL, NULL, 2, 1, NULL, 2),
(41, '0209', 'Corongo', NULL, NULL, NULL, NULL, 2, 1, NULL, 2),
(42, '0210', 'Huari', NULL, NULL, NULL, NULL, 2, 1, NULL, 2),
(43, '0211', 'Huarmey', NULL, NULL, NULL, NULL, 2, 1, NULL, 2),
(44, '0212', 'Huaylas', NULL, NULL, NULL, NULL, 2, 1, NULL, 2),
(45, '0213', 'Mariscal Luzuriaga', NULL, NULL, NULL, NULL, 2, 1, NULL, 2),
(46, '0214', 'Ocros', NULL, NULL, NULL, NULL, 2, 1, NULL, 2),
(47, '0215', 'Pallasca', NULL, NULL, NULL, NULL, 2, 1, NULL, 2),
(48, '0216', 'Pomabamba', NULL, NULL, NULL, NULL, 2, 1, NULL, 2),
(49, '0217', 'Recuay', NULL, NULL, NULL, NULL, 2, 1, NULL, 2),
(50, '0218', 'Santa', NULL, NULL, NULL, NULL, 2, 1, NULL, 2),
(51, '0219', 'Sihuas', NULL, NULL, NULL, NULL, 2, 1, NULL, 2),
(52, '0220', 'Yungay', NULL, NULL, NULL, NULL, 2, 1, NULL, 2),
(53, '0301', 'Abancay', NULL, NULL, NULL, NULL, 3, 1, NULL, 2),
(54, '0302', 'Andahuaylas', NULL, NULL, NULL, NULL, 3, 1, NULL, 2),
(55, '0303', 'Antabamba', NULL, NULL, NULL, NULL, 3, 1, NULL, 2),
(56, '0304', 'Aymaraes', NULL, NULL, NULL, NULL, 3, 1, NULL, 2),
(57, '0305', 'Cotabambas', NULL, NULL, NULL, NULL, 3, 1, NULL, 2),
(58, '0306', 'Chincheros', NULL, NULL, NULL, NULL, 3, 1, NULL, 2),
(59, '0307', 'Grau', NULL, NULL, NULL, NULL, 3, 1, NULL, 2),
(60, '0401', 'Arequipa', NULL, NULL, NULL, NULL, 4, 1, NULL, 2),
(61, '0402', 'Camana', NULL, NULL, NULL, NULL, 4, 1, NULL, 2),
(62, '0403', 'Caraveli', NULL, NULL, NULL, NULL, 4, 1, NULL, 2),
(63, '0404', 'Castilla', NULL, NULL, NULL, NULL, 4, 1, NULL, 2),
(64, '0405', 'Caylloma', NULL, NULL, NULL, NULL, 4, 1, NULL, 2),
(65, '0406', 'Condesuyos', NULL, NULL, NULL, NULL, 4, 1, NULL, 2),
(66, '0407', 'Islay', NULL, NULL, NULL, NULL, 4, 1, NULL, 2),
(67, '0408', 'La Union', NULL, NULL, NULL, NULL, 4, 1, NULL, 2),
(68, '0501', 'Huamanga', NULL, NULL, NULL, NULL, 5, 1, NULL, 2),
(69, '0502', 'Cangallo', NULL, NULL, NULL, NULL, 5, 1, NULL, 2),
(70, '0503', 'Huanca Sancos', NULL, NULL, NULL, NULL, 5, 1, NULL, 2),
(71, '0504', 'Huanta', NULL, NULL, NULL, NULL, 5, 1, NULL, 2),
(72, '0505', 'La Mar', NULL, NULL, NULL, NULL, 5, 1, NULL, 2),
(73, '0506', 'Lucanas', NULL, NULL, NULL, NULL, 5, 1, NULL, 2),
(74, '0507', 'Parinacochas', NULL, NULL, NULL, NULL, 5, 1, NULL, 2),
(75, '0508', 'Paucar Del Sara Sara', NULL, NULL, NULL, NULL, 5, 1, NULL, 2),
(76, '0509', 'Sucre', NULL, NULL, NULL, NULL, 5, 1, NULL, 2),
(77, '0510', 'Victor Fajardo', NULL, NULL, NULL, NULL, 5, 1, NULL, 2),
(78, '0511', 'Vilcas Huaman', NULL, NULL, NULL, NULL, 5, 1, NULL, 2),
(79, '0601', 'Cajamarca', NULL, NULL, NULL, NULL, 6, 1, NULL, 2),
(80, '0602', 'Cajabamba', NULL, NULL, NULL, NULL, 6, 1, NULL, 2),
(81, '0603', 'Celendin', NULL, NULL, NULL, NULL, 6, 1, NULL, 2),
(82, '0604', 'Chota', NULL, NULL, NULL, NULL, 6, 1, NULL, 2),
(83, '0605', 'Contumaza', NULL, NULL, NULL, NULL, 6, 1, NULL, 2),
(84, '0606', 'Cutervo', NULL, NULL, NULL, NULL, 6, 1, NULL, 2),
(85, '0607', 'Hualgayoc', NULL, NULL, NULL, NULL, 6, 1, NULL, 2),
(86, '0608', 'Jaen', NULL, NULL, NULL, NULL, 6, 1, NULL, 2),
(87, '0609', 'San Ignacio', NULL, NULL, NULL, NULL, 6, 1, NULL, 2),
(88, '0610', 'San Marcos', NULL, NULL, NULL, NULL, 6, 1, NULL, 2),
(89, '0611', 'San Miguel', NULL, NULL, NULL, NULL, 6, 1, NULL, 2),
(90, '0612', 'San Pablo', NULL, NULL, NULL, NULL, 6, 1, NULL, 2),
(91, '0613', 'Santa Cruz', NULL, NULL, NULL, NULL, 6, 1, NULL, 2),
(92, '0701', 'Callao', NULL, NULL, NULL, NULL, 7, 1, NULL, 2),
(93, '0801', 'Cusco', NULL, NULL, NULL, NULL, 8, 1, NULL, 2),
(94, '0802', 'Acomayo', NULL, NULL, NULL, NULL, 8, 1, NULL, 2),
(95, '0803', 'Anta', NULL, NULL, NULL, NULL, 8, 1, NULL, 2),
(96, '0804', 'Calca', NULL, NULL, NULL, NULL, 8, 1, NULL, 2),
(97, '0805', 'Canas', NULL, NULL, NULL, NULL, 8, 1, NULL, 2),
(98, '0806', 'Canchis', NULL, NULL, NULL, NULL, 8, 1, NULL, 2),
(99, '0807', 'Chumbivilcas', NULL, NULL, NULL, NULL, 8, 1, NULL, 2),
(100, '0808', 'Espinar', NULL, NULL, NULL, NULL, 8, 1, NULL, 2),
(101, '0809', 'La Convencion', NULL, NULL, NULL, NULL, 8, 1, NULL, 2),
(102, '0810', 'Paruro', NULL, NULL, NULL, NULL, 8, 1, NULL, 2),
(103, '0811', 'Paucartambo', NULL, NULL, NULL, NULL, 8, 1, NULL, 2),
(104, '0812', 'Quispicanchi', NULL, NULL, NULL, NULL, 8, 1, NULL, 2),
(105, '0813', 'Urubamba', NULL, NULL, NULL, NULL, 8, 1, NULL, 2),
(106, '0901', 'Huancavelica', NULL, NULL, NULL, NULL, 9, 1, NULL, 2),
(107, '0902', 'Acobamba', NULL, NULL, NULL, NULL, 9, 1, NULL, 2),
(108, '0903', 'Angaraes', NULL, NULL, NULL, NULL, 9, 1, NULL, 2),
(109, '0904', 'Castrovirreyna', NULL, NULL, NULL, NULL, 9, 1, NULL, 2),
(110, '0905', 'Churcampa', NULL, NULL, NULL, NULL, 9, 1, NULL, 2),
(111, '0906', 'Huaytara', NULL, NULL, NULL, NULL, 9, 1, NULL, 2),
(112, '0907', 'Tayacaja', NULL, NULL, NULL, NULL, 9, 1, NULL, 2),
(113, '1001', 'Huanuco', NULL, NULL, NULL, NULL, 10, 1, NULL, 2),
(114, '1002', 'Ambo', NULL, NULL, NULL, NULL, 10, 1, NULL, 2),
(115, '1003', 'Dos De Mayo', NULL, NULL, NULL, NULL, 10, 1, NULL, 2),
(116, '1004', 'Huacaybamba', NULL, NULL, NULL, NULL, 10, 1, NULL, 2),
(117, '1005', 'Huamalies', NULL, NULL, NULL, NULL, 10, 1, NULL, 2),
(118, '1006', 'Leoncio Prado', NULL, NULL, NULL, NULL, 10, 1, NULL, 2),
(119, '1007', 'Marañon', NULL, NULL, NULL, NULL, 10, 1, NULL, 2),
(120, '1008', 'Pachitea', NULL, NULL, NULL, NULL, 10, 1, NULL, 2),
(121, '1009', 'Puerto Inca', NULL, NULL, NULL, NULL, 10, 1, NULL, 2),
(122, '1010', 'Lauricocha', NULL, NULL, NULL, NULL, 10, 1, NULL, 2),
(123, '1011', 'Yarowilca', NULL, NULL, NULL, NULL, 10, 1, NULL, 2),
(124, '1101', 'Ica', NULL, NULL, NULL, NULL, 11, 1, NULL, 2),
(125, '1102', 'Chincha', NULL, NULL, NULL, NULL, 11, 1, NULL, 2),
(126, '1103', 'Nazca', NULL, NULL, NULL, NULL, 11, 1, NULL, 2),
(127, '1104', 'Palpa', NULL, NULL, NULL, NULL, 11, 1, NULL, 2),
(128, '1105', 'Pisco', NULL, NULL, NULL, NULL, 11, 1, NULL, 2),
(129, '1201', 'Huancayo', NULL, NULL, NULL, NULL, 12, 1, NULL, 2),
(130, '1202', 'Concepcion', NULL, NULL, NULL, NULL, 12, 1, NULL, 2),
(131, '1203', 'Chanchamayo', NULL, NULL, NULL, NULL, 12, 1, NULL, 2),
(132, '1204', 'Jauja', NULL, NULL, NULL, NULL, 12, 1, NULL, 2),
(133, '1205', 'Junin', NULL, NULL, NULL, NULL, 12, 1, NULL, 2),
(134, '1206', 'Satipo', NULL, NULL, NULL, NULL, 12, 1, NULL, 2),
(135, '1207', 'Tarma', NULL, NULL, NULL, NULL, 12, 1, NULL, 2),
(136, '1208', 'Yauli', NULL, NULL, NULL, NULL, 12, 1, NULL, 2),
(137, '1209', 'Chupaca', NULL, NULL, NULL, NULL, 12, 1, NULL, 2),
(138, '1301', 'Trujillo', NULL, NULL, NULL, NULL, 13, 1, NULL, 2),
(139, '1302', 'Ascope', NULL, NULL, NULL, NULL, 13, 1, NULL, 2),
(140, '1303', 'Bolivar', NULL, NULL, NULL, NULL, 13, 1, NULL, 2),
(141, '1304', 'Chepen', NULL, NULL, NULL, NULL, 13, 1, NULL, 2),
(142, '1305', 'Julcan', NULL, NULL, NULL, NULL, 13, 1, NULL, 2),
(143, '1306', 'Otuzco', NULL, NULL, NULL, NULL, 13, 1, NULL, 2),
(144, '1307', 'Pacasmayo', NULL, NULL, NULL, NULL, 13, 1, NULL, 2),
(145, '1308', 'Pataz', NULL, NULL, NULL, NULL, 13, 1, NULL, 2),
(146, '1309', 'Sanchez Carrion', NULL, NULL, NULL, NULL, 13, 1, NULL, 2),
(147, '1310', 'Santiago De Chuco', NULL, NULL, NULL, NULL, 13, 1, NULL, 2),
(148, '1311', 'Gran Chimu', NULL, NULL, NULL, NULL, 13, 1, NULL, 2),
(149, '1312', 'Viru', NULL, NULL, NULL, NULL, 13, 1, NULL, 2),
(150, '1401', 'Chiclayo', NULL, NULL, NULL, NULL, 14, 1, NULL, 2),
(151, '1402', 'Ferreñafe', NULL, NULL, NULL, NULL, 14, 1, NULL, 2),
(152, '1403', 'Lambayeque', NULL, NULL, NULL, NULL, 14, 1, NULL, 2),
(153, '1501', 'Lima', NULL, NULL, NULL, NULL, 15, 1, NULL, 2),
(154, '1502', 'Barranca', NULL, NULL, NULL, NULL, 15, 1, NULL, 2),
(155, '1503', 'Cajatambo', NULL, NULL, NULL, NULL, 15, 1, NULL, 2),
(156, '1504', 'Canta', NULL, NULL, NULL, NULL, 15, 1, NULL, 2),
(157, '1505', 'Cañete', NULL, NULL, NULL, NULL, 15, 1, NULL, 2),
(158, '1506', 'Huaral', NULL, NULL, NULL, NULL, 15, 1, NULL, 2),
(159, '1507', 'Huarochiri', NULL, NULL, NULL, NULL, 15, 1, NULL, 2),
(160, '1508', 'Huaura', NULL, NULL, NULL, NULL, 15, 1, NULL, 2),
(161, '1509', 'Oyon', NULL, NULL, NULL, NULL, 15, 1, NULL, 2),
(162, '1510', 'Yauyos', NULL, NULL, NULL, NULL, 15, 1, NULL, 2),
(163, '1601', 'Maynas', NULL, NULL, NULL, NULL, 16, 1, NULL, 2),
(164, '1602', 'Alto Amazonas', NULL, NULL, NULL, NULL, 16, 1, NULL, 2),
(165, '1603', 'Loreto', NULL, NULL, NULL, NULL, 16, 1, NULL, 2),
(166, '1604', 'Mariscal Ramon Castilla', NULL, NULL, NULL, NULL, 16, 1, NULL, 2),
(167, '1605', 'Requena', NULL, NULL, NULL, NULL, 16, 1, NULL, 2),
(168, '1606', 'Ucayali', NULL, NULL, NULL, NULL, 16, 1, NULL, 2),
(169, '1607', 'Datem Del Marañon', NULL, NULL, NULL, NULL, 16, 1, NULL, 2),
(170, '1701', 'Tambopata', NULL, NULL, NULL, NULL, 17, 1, NULL, 2),
(171, '1702', 'Manu', NULL, NULL, NULL, NULL, 17, 1, NULL, 2),
(172, '1703', 'Tahuamanu', NULL, NULL, NULL, NULL, 17, 1, NULL, 2),
(173, '1801', 'Mariscal Nieto', NULL, NULL, NULL, NULL, 18, 1, NULL, 2),
(174, '1802', 'General Sanchez Cerro', NULL, NULL, NULL, NULL, 18, 1, NULL, 2),
(175, '1803', 'Ilo', NULL, NULL, NULL, NULL, 18, 1, NULL, 2),
(176, '1901', 'Pasco', NULL, NULL, NULL, NULL, 19, 1, NULL, 2),
(177, '1902', 'Daniel Alcides Carrion', NULL, NULL, NULL, NULL, 19, 1, NULL, 2),
(178, '1903', 'Oxapampa', NULL, NULL, NULL, NULL, 19, 1, NULL, 2),
(179, '2001', 'Piura', NULL, NULL, NULL, NULL, 20, 1, NULL, 2),
(180, '2002', 'Ayabaca', NULL, NULL, NULL, NULL, 20, 1, NULL, 2),
(181, '2003', 'Huancabamba', NULL, NULL, NULL, NULL, 20, 1, NULL, 2),
(182, '2004', 'Morropon', NULL, NULL, NULL, NULL, 20, 1, NULL, 2),
(183, '2005', 'Paita', NULL, NULL, NULL, NULL, 20, 1, NULL, 2),
(184, '2006', 'Sullana', NULL, NULL, NULL, NULL, 20, 1, NULL, 2),
(185, '2007', 'Talara', NULL, NULL, NULL, NULL, 20, 1, NULL, 2),
(186, '2008', 'Sechura', NULL, NULL, NULL, NULL, 20, 1, NULL, 2),
(187, '2101', 'Puno', NULL, NULL, NULL, NULL, 21, 1, NULL, 2),
(188, '2102', 'Azangaro', NULL, NULL, NULL, NULL, 21, 1, NULL, 2),
(189, '2103', 'Carabaya', NULL, NULL, NULL, NULL, 21, 1, NULL, 2),
(190, '2104', 'Chucuito', NULL, NULL, NULL, NULL, 21, 1, NULL, 2),
(191, '2105', 'El Collao', NULL, NULL, NULL, NULL, 21, 1, NULL, 2),
(192, '2106', 'Huancane', NULL, NULL, NULL, NULL, 21, 1, NULL, 2),
(193, '2107', 'Lampa', NULL, NULL, NULL, NULL, 21, 1, NULL, 2),
(194, '2108', 'Melgar', NULL, NULL, NULL, NULL, 21, 1, NULL, 2),
(195, '2109', 'Moho', NULL, NULL, NULL, NULL, 21, 1, NULL, 2),
(196, '2110', 'San Antonio De Putin', NULL, NULL, NULL, NULL, 21, 1, NULL, 2),
(197, '2111', 'San Roman', NULL, NULL, NULL, NULL, 21, 1, NULL, 2),
(198, '2112', 'Sandia', NULL, NULL, NULL, NULL, 21, 1, NULL, 2),
(199, '2113', 'Yunguyo', NULL, NULL, NULL, NULL, 21, 1, NULL, 2),
(200, '2201', 'Moyobamba', NULL, NULL, NULL, NULL, 22, 1, NULL, 2),
(201, '2202', 'Bellavista', NULL, NULL, NULL, NULL, 22, 1, NULL, 2),
(202, '2203', 'El Dorado', NULL, NULL, NULL, NULL, 22, 1, NULL, 2),
(203, '2204', 'Huallaga', NULL, NULL, NULL, NULL, 22, 1, NULL, 2),
(204, '2205', 'Lamas', NULL, NULL, NULL, NULL, 22, 1, NULL, 2),
(205, '2206', 'Mariscal Caceres', NULL, NULL, NULL, NULL, 22, 1, NULL, 2),
(206, '2207', 'Picota', NULL, NULL, NULL, NULL, 22, 1, NULL, 2),
(207, '2208', 'Rioja', NULL, NULL, NULL, NULL, 22, 1, NULL, 2),
(208, '2209', 'San Martin', NULL, NULL, NULL, NULL, 22, 1, NULL, 2),
(209, '2210', 'Tocache', NULL, NULL, NULL, NULL, 22, 1, NULL, 2),
(210, '2301', 'Tacna', NULL, NULL, NULL, NULL, 23, 1, NULL, 2),
(211, '2302', 'Candarave', NULL, NULL, NULL, NULL, 23, 1, NULL, 2),
(212, '2303', 'Jorge Basadre', NULL, NULL, NULL, NULL, 23, 1, NULL, 2),
(213, '2304', 'Tarata', NULL, NULL, NULL, NULL, 23, 1, NULL, 2),
(214, '2401', 'Tumbes', NULL, NULL, NULL, NULL, 24, 1, NULL, 2),
(215, '2402', 'Contralmirante Villa', NULL, NULL, NULL, NULL, 24, 1, NULL, 2),
(216, '2403', 'Zarumilla', NULL, NULL, NULL, NULL, 24, 1, NULL, 2),
(217, '2501', 'Coronel Portillo', NULL, NULL, NULL, NULL, 25, 1, NULL, 2),
(218, '2502', 'Atalaya', NULL, NULL, NULL, NULL, 25, 1, NULL, 2),
(219, '2503', 'Padre Abad', NULL, NULL, NULL, NULL, 25, 1, NULL, 2),
(220, '2504', 'Purus', NULL, NULL, NULL, NULL, 25, 1, NULL, 2),
(221, '010101', 'Chachapoyas', NULL, NULL, 28731, '135.00', 26, 1, NULL, 3),
(222, '010102', 'Asuncion', NULL, NULL, 288, '27.00', 26, 1, NULL, 3),
(223, '010103', 'Balsas', NULL, NULL, 1625, '326.00', 26, 1, NULL, 3),
(224, '010104', 'Cheto', NULL, NULL, 591, '76.00', 26, 1, NULL, 3),
(225, '010105', 'Chiliquin', NULL, NULL, 711, '141.00', 26, 1, NULL, 3),
(226, '010106', 'Chuquibamba', NULL, NULL, 2069, '176.00', 26, 1, NULL, 3),
(227, '010107', 'Granada', NULL, NULL, 385, '170.00', 26, 1, NULL, 3),
(228, '010108', 'Huancas', NULL, NULL, 1305, '48.00', 26, 1, NULL, 3),
(229, '010109', 'La Jalca', NULL, NULL, 5505, '127.00', 26, 1, NULL, 3),
(230, '010110', 'Leimebamba', NULL, NULL, 4190, '462.00', 26, 1, NULL, 3),
(231, '010111', 'Levanto', NULL, NULL, 873, '93.00', 26, 1, NULL, 3),
(232, '010112', 'Magdalena', NULL, NULL, 795, '134.00', 26, 1, NULL, 3),
(233, '010113', 'Mariscal Castilla', NULL, NULL, 1006, '99.00', 26, 1, NULL, 3),
(234, '010114', 'Molinopampa', NULL, NULL, 2740, '344.00', 26, 1, NULL, 3),
(235, '010115', 'Montevideo', NULL, NULL, 589, '81.00', 26, 1, NULL, 3),
(236, '010116', 'Olleros', NULL, NULL, 362, '122.00', 26, 1, NULL, 3),
(237, '010117', 'Quinjalca', NULL, NULL, 843, '89.00', 26, 1, NULL, 3),
(238, '010118', 'San Francisco De Daguas', NULL, NULL, 349, '47.00', 26, 1, NULL, 3),
(239, '010119', 'San Isidro De Maino', NULL, NULL, 706, '107.00', 26, 1, NULL, 3),
(240, '010120', 'Soloco', NULL, NULL, 1318, '77.00', 26, 1, NULL, 3),
(241, '010121', 'Sonche', NULL, NULL, 220, '117.00', 26, 1, NULL, 3),
(242, '010201', 'Bagua', NULL, NULL, 25965, '75.00', 27, 1, NULL, 3),
(243, '010202', 'Aramango', NULL, NULL, 11032, '839.00', 27, 1, NULL, 3),
(244, '010203', 'Copallin', NULL, NULL, 6328, '106.00', 27, 1, NULL, 3),
(245, '010204', 'El Parco', NULL, NULL, 1476, '14.00', 27, 1, NULL, 3),
(246, '010205', 'Imaza', NULL, NULL, 24114, '4672.00', 27, 1, NULL, 3),
(247, '010206', 'La Peca', NULL, NULL, 8006, '183.00', 27, 1, NULL, 3),
(248, '010301', 'Jumbilla', NULL, NULL, 1748, '161.00', 28, 1, NULL, 3),
(249, '010302', 'Chisquilla', NULL, NULL, 336, '177.00', 28, 1, NULL, 3),
(250, '010303', 'Churuja', NULL, NULL, 269, '31.00', 28, 1, NULL, 3),
(251, '010304', 'Corosha', NULL, NULL, 1025, '47.00', 28, 1, NULL, 3),
(252, '010305', 'Cuispes', NULL, NULL, 895, '104.00', 28, 1, NULL, 3),
(253, '010306', 'Florida', NULL, NULL, 8493, '196.00', 28, 1, NULL, 3),
(254, '010307', 'Jazan', NULL, NULL, 9260, '89.00', 28, 1, NULL, 3),
(255, '010308', 'Recta', NULL, NULL, 206, '22.00', 28, 1, NULL, 3),
(256, '010309', 'San Carlos', NULL, NULL, 317, '99.00', 28, 1, NULL, 3),
(257, '010310', 'Shipasbamba', NULL, NULL, 1786, '130.00', 28, 1, NULL, 3),
(258, '010311', 'Valera', NULL, NULL, 1281, '88.00', 28, 1, NULL, 3),
(259, '010312', 'Yambrasbamba', NULL, NULL, 8304, '1686.00', 28, 1, NULL, 3),
(260, '010401', 'Nieva', NULL, NULL, 28726, '4436.00', 29, 1, NULL, 3),
(261, '010402', 'El Cenepa', NULL, NULL, 9537, '5405.00', 29, 1, NULL, 3),
(262, '010403', 'Rio Santiago', NULL, NULL, 16686, '8107.00', 29, 1, NULL, 3),
(263, '010501', 'Lamud', NULL, NULL, 2300, '72.00', 30, 1, NULL, 3),
(264, '010502', 'Camporredondo', NULL, NULL, 7048, '378.00', 30, 1, NULL, 3),
(265, '010503', 'Cocabamba', NULL, NULL, 2498, '336.00', 30, 1, NULL, 3),
(266, '010504', 'Colcamar', NULL, NULL, 2284, '110.00', 30, 1, NULL, 3),
(267, '010505', 'Conila', NULL, NULL, 2083, '228.00', 30, 1, NULL, 3),
(268, '010506', 'Inguilpata', NULL, NULL, 603, '123.00', 30, 1, NULL, 3),
(269, '010507', 'Longuita', NULL, NULL, 1146, '64.00', 30, 1, NULL, 3),
(270, '010508', 'Lonya Chico', NULL, NULL, 975, '57.00', 30, 1, NULL, 3),
(271, '010509', 'Luya', NULL, NULL, 4404, '99.00', 30, 1, NULL, 3),
(272, '010510', 'Luya Viejo', NULL, NULL, 483, '61.00', 30, 1, NULL, 3),
(273, '010511', 'Maria', NULL, NULL, 940, '83.00', 30, 1, NULL, 3),
(274, '010512', 'Ocalli', NULL, NULL, 4211, '171.00', 30, 1, NULL, 3),
(275, '010513', 'Ocumal', NULL, NULL, 4164, '369.00', 30, 1, NULL, 3),
(276, '010514', 'Pisuquia', NULL, NULL, 6081, '309.00', 30, 1, NULL, 3),
(277, '010515', 'Providencia', NULL, NULL, 1533, '117.00', 30, 1, NULL, 3),
(278, '010516', 'San Cristobal', NULL, NULL, 690, '37.00', 30, 1, NULL, 3),
(279, '010517', 'San Francisco Del Yeso', NULL, NULL, 820, '110.00', 30, 1, NULL, 3),
(280, '010518', 'San Jeronimo', NULL, NULL, 890, '218.00', 30, 1, NULL, 3),
(281, '010519', 'San Juan De Lopecancha', NULL, NULL, 513, '83.00', 30, 1, NULL, 3),
(282, '010520', 'Santa Catalina', NULL, NULL, 1893, '126.00', 30, 1, NULL, 3),
(283, '010521', 'Santo Tomas', NULL, NULL, 3562, '93.00', 30, 1, NULL, 3),
(284, '010522', 'Tingo', NULL, NULL, 1355, '105.00', 30, 1, NULL, 3),
(285, '010523', 'Trita', NULL, NULL, 1373, '12.00', 30, 1, NULL, 3),
(286, '010601', 'San Nicolas', NULL, NULL, 5224, '105.00', 31, 1, NULL, 3),
(287, '010602', 'Chirimoto', NULL, NULL, 2052, '161.00', 31, 1, NULL, 3),
(288, '010603', 'Cochamal', NULL, NULL, 506, '213.00', 31, 1, NULL, 3),
(289, '010604', 'Huambo', NULL, NULL, 2598, '88.00', 31, 1, NULL, 3),
(290, '010605', 'Limabamba', NULL, NULL, 3002, '645.00', 31, 1, NULL, 3),
(291, '010606', 'Longar', NULL, NULL, 1624, '50.00', 31, 1, NULL, 3),
(292, '010607', 'Mariscal Benavides', NULL, NULL, 1381, '186.00', 31, 1, NULL, 3),
(293, '010608', 'Milpuc', NULL, NULL, 604, '45.00', 31, 1, NULL, 3),
(294, '010609', 'Omia', NULL, NULL, 9562, '223.00', 31, 1, NULL, 3),
(295, '010610', 'Santa Rosa', NULL, NULL, 464, '19.00', 31, 1, NULL, 3),
(296, '010611', 'Totora', NULL, NULL, 450, '20.00', 31, 1, NULL, 3),
(297, '010612', 'Vista Alegre', NULL, NULL, 3725, '927.00', 31, 1, NULL, 3),
(298, '010701', 'Bagua Grande', NULL, NULL, 53537, '717.00', 32, 1, NULL, 3),
(299, '010702', 'Cajaruro', NULL, '', 28403, '1797.00', 32, 1, 3, 3),
(300, '010703', 'Cumba', NULL, NULL, 8815, '300.00', 32, 1, NULL, 3),
(301, '010704', 'El Milagro', NULL, NULL, 6369, '310.00', 32, 1, NULL, 3),
(302, '010705', 'Jamalca', NULL, NULL, 8219, '387.00', 32, 1, NULL, 3),
(303, '010706', 'Lonya Grande', NULL, NULL, 10377, '216.00', 32, 1, NULL, 3),
(304, '010707', 'Yamon', NULL, NULL, 2877, '143.00', 32, 1, NULL, 3),
(305, '020101', 'Huaraz', NULL, NULL, 64109, '423.00', 33, 1, NULL, 3),
(306, '020102', 'Cochabamba', NULL, NULL, 1989, '138.00', 33, 1, NULL, 3),
(307, '020103', 'Colcabamba', NULL, NULL, 800, '52.00', 33, 1, NULL, 3),
(308, '020104', 'Huanchay', NULL, NULL, 2283, '208.00', 33, 1, NULL, 3),
(309, '020105', 'Independencia', NULL, NULL, 73556, '346.00', 33, 1, NULL, 3),
(310, '020106', 'Jangas', NULL, NULL, 4988, '62.00', 33, 1, NULL, 3),
(311, '020107', 'La Libertad', NULL, NULL, 1162, '160.00', 33, 1, NULL, 3),
(312, '020108', 'Olleros', NULL, NULL, 2230, '230.00', 33, 1, NULL, 3),
(313, '020109', 'Pampas', NULL, NULL, 1190, '351.00', 33, 1, NULL, 3),
(314, '020110', 'Pariacoto', NULL, NULL, 4718, '168.00', 33, 1, NULL, 3),
(315, '020111', 'Pira', NULL, NULL, 3763, '248.00', 33, 1, NULL, 3),
(316, '020112', 'Tarica', NULL, NULL, 5837, '116.00', 33, 1, NULL, 3),
(317, '020201', 'Aija', NULL, NULL, 1873, '163.00', 34, 1, NULL, 3),
(318, '020202', 'Coris', NULL, NULL, 2239, '259.00', 34, 1, NULL, 3),
(319, '020203', 'Huacllan', NULL, NULL, 616, '40.00', 34, 1, NULL, 3),
(320, '020204', 'La Merced', NULL, NULL, 2220, '158.00', 34, 1, NULL, 3),
(321, '020205', 'Succha', NULL, NULL, 841, '78.00', 34, 1, NULL, 3),
(322, '020301', 'Llamellin', NULL, NULL, 3591, '95.00', 35, 1, NULL, 3),
(323, '020302', 'Aczo', NULL, NULL, 2176, '68.00', 35, 1, NULL, 3),
(324, '020303', 'Chaccho', NULL, NULL, 1718, '73.00', 35, 1, NULL, 3),
(325, '020304', 'Chingas', NULL, NULL, 1936, '50.00', 35, 1, NULL, 3),
(326, '020305', 'Mirgas', NULL, NULL, 5338, '172.00', 35, 1, NULL, 3),
(327, '020306', 'San Juan De Rontoy', NULL, NULL, 1642, '103.00', 35, 1, NULL, 3),
(328, '020401', 'Chacas', NULL, NULL, 5573, '449.00', 36, 1, NULL, 3),
(329, '020402', 'Acochaca', NULL, NULL, 3222, '76.00', 36, 1, NULL, 3),
(330, '020501', 'Chiquian', NULL, NULL, 3641, '187.00', 37, 1, NULL, 3),
(331, '020502', 'Abelardo Pardo Lezameta', NULL, NULL, 1208, '11.00', 37, 1, NULL, 3),
(332, '020503', 'Antonio Raymondi', NULL, NULL, 1078, '121.00', 37, 1, NULL, 3),
(333, '020504', 'Aquia', NULL, NULL, 2513, '435.00', 37, 1, NULL, 3),
(334, '020505', 'Cajacay', NULL, NULL, 1602, '193.00', 37, 1, NULL, 3),
(335, '020506', 'Canis', NULL, NULL, 1250, '20.00', 37, 1, NULL, 3),
(336, '020507', 'Colquioc', NULL, NULL, 4002, '274.00', 37, 1, NULL, 3),
(337, '020508', 'Huallanca', NULL, NULL, 8220, '817.00', 37, 1, NULL, 3),
(338, '020509', 'Huasta', NULL, NULL, 2571, '395.00', 37, 1, NULL, 3),
(339, '020510', 'Huayllacayan', NULL, NULL, 1104, '126.00', 37, 1, NULL, 3),
(340, '020511', 'La Primavera', NULL, NULL, 911, '64.00', 37, 1, NULL, 3),
(341, '020512', 'Mangas', NULL, NULL, 567, '127.00', 37, 1, NULL, 3),
(342, '020513', 'Pacllon', NULL, NULL, 1722, '210.00', 37, 1, NULL, 3),
(343, '020514', 'San Miguel De Corpanqui', NULL, NULL, 1241, '44.00', 37, 1, NULL, 3),
(344, '020515', 'Ticllos', NULL, NULL, 1243, '92.00', 37, 1, NULL, 3),
(345, '020601', 'Carhuaz', NULL, NULL, 15373, '198.00', 38, 1, NULL, 3),
(346, '020602', 'Acopampa', NULL, NULL, 2655, '15.00', 38, 1, NULL, 3),
(347, '020603', 'Amashca', NULL, NULL, 1581, '13.00', 38, 1, NULL, 3),
(348, '020604', 'Anta', NULL, NULL, 2494, '43.00', 38, 1, NULL, 3),
(349, '020605', 'Ataquero', NULL, NULL, 1377, '45.00', 38, 1, NULL, 3),
(350, '020606', 'Marcara', NULL, NULL, 9304, '176.00', 38, 1, NULL, 3),
(351, '020607', 'Pariahuanca', NULL, NULL, 1609, '12.00', 38, 1, NULL, 3),
(352, '020608', 'San Miguel De Aco', NULL, NULL, 2750, '123.00', 38, 1, NULL, 3),
(353, '020609', 'Shilla', NULL, NULL, 3306, '128.00', 38, 1, NULL, 3),
(354, '020610', 'Tinco', NULL, NULL, 3240, '16.00', 38, 1, NULL, 3),
(355, '020611', 'Yungar', NULL, NULL, 3408, '45.00', 38, 1, NULL, 3),
(356, '020701', 'San Luis', NULL, NULL, 12566, '248.00', 39, 1, NULL, 3),
(357, '020702', 'San Nicolas', NULL, NULL, 3738, '204.00', 39, 1, NULL, 3),
(358, '020703', 'Yauya', NULL, NULL, 5527, '175.00', 39, 1, NULL, 3),
(359, '020801', 'Casma', NULL, NULL, 32824, '1205.00', 40, 1, NULL, 3),
(360, '020802', 'Buena Vista Alta', NULL, NULL, 4213, '481.00', 40, 1, NULL, 3),
(361, '020803', 'Comandante Noel', NULL, NULL, 2058, '218.00', 40, 1, NULL, 3),
(362, '020804', 'Yautan', NULL, NULL, 8383, '365.00', 40, 1, NULL, 3),
(363, '020901', 'Corongo', NULL, NULL, 1487, '151.00', 41, 1, NULL, 3),
(364, '020902', 'Aco', NULL, NULL, 460, '58.00', 41, 1, NULL, 3),
(365, '020903', 'Bambas', NULL, NULL, 537, '154.00', 41, 1, NULL, 3),
(366, '020904', 'Cusca', NULL, NULL, 2941, '426.00', 41, 1, NULL, 3),
(367, '020905', 'La Pampa', NULL, NULL, 1030, '95.00', 41, 1, NULL, 3),
(368, '020906', 'Yanac', NULL, NULL, 708, '47.00', 41, 1, NULL, 3),
(369, '020907', 'Yupan', NULL, NULL, 1002, '85.00', 41, 1, NULL, 3),
(370, '021001', 'Huari', NULL, NULL, 10283, '400.00', 42, 1, NULL, 3),
(371, '021002', 'Anra', NULL, NULL, 1618, '78.00', 42, 1, NULL, 3),
(372, '021003', 'Cajay', NULL, NULL, 2638, '166.00', 42, 1, NULL, 3),
(373, '021004', 'Chavin De Huantar', NULL, NULL, 9221, '418.00', 42, 1, NULL, 3),
(374, '021005', 'Huacachi', NULL, NULL, 1876, '90.00', 42, 1, NULL, 3),
(375, '021006', 'Huacchis', NULL, NULL, 2075, '76.00', 42, 1, NULL, 3),
(376, '021007', 'Huachis', NULL, NULL, 3509, '152.00', 42, 1, NULL, 3),
(377, '021008', 'Huantar', NULL, NULL, 3044, '162.00', 42, 1, NULL, 3),
(378, '021009', 'Masin', NULL, NULL, 1706, '75.00', 42, 1, NULL, 3),
(379, '021010', 'Paucas', NULL, NULL, 1863, '140.00', 42, 1, NULL, 3),
(380, '021011', 'Ponto', NULL, NULL, 3349, '119.00', 42, 1, NULL, 3),
(381, '021012', 'Rahuapampa', NULL, NULL, 802, '10.00', 42, 1, NULL, 3),
(382, '021013', 'Rapayan', NULL, NULL, 1793, '145.00', 42, 1, NULL, 3),
(383, '021014', 'San Marcos', NULL, NULL, 14781, '564.00', 42, 1, NULL, 3),
(384, '021015', 'San Pedro De Chana', NULL, NULL, 2814, '144.00', 42, 1, NULL, 3),
(385, '021016', 'Uco', NULL, NULL, 1685, '52.00', 42, 1, NULL, 3),
(386, '021101', 'Huarmey', NULL, NULL, 24316, '2909.00', 43, 1, NULL, 3),
(387, '021102', 'Cochapeti', NULL, NULL, 771, '99.00', 43, 1, NULL, 3),
(388, '021103', 'Culebras', NULL, NULL, 3661, '629.00', 43, 1, NULL, 3),
(389, '021104', 'Huayan', NULL, NULL, 1065, '112.00', 43, 1, NULL, 3),
(390, '021105', 'Malvas', NULL, NULL, 931, '168.00', 43, 1, NULL, 3),
(391, '021201', 'Caraz', NULL, NULL, 26208, '245.00', 44, 1, NULL, 3),
(392, '021202', 'Huallanca', NULL, NULL, 703, '173.00', 44, 1, NULL, 3),
(393, '021203', 'Huata', NULL, NULL, 1638, '70.00', 44, 1, NULL, 3),
(394, '021204', 'Huaylas', NULL, NULL, 1466, '54.00', 44, 1, NULL, 3),
(395, '021205', 'Mato', NULL, NULL, 2030, '106.00', 44, 1, NULL, 3),
(396, '021206', 'Pamparomas', NULL, NULL, 9153, '500.00', 44, 1, NULL, 3),
(397, '021207', 'Pueblo Libre', NULL, NULL, 7186, '128.00', 44, 1, NULL, 3),
(398, '021208', 'Santa Cruz', NULL, NULL, 5164, '359.00', 44, 1, NULL, 3),
(399, '021209', 'Santo Toribio', NULL, NULL, 1100, '84.00', 44, 1, NULL, 3),
(400, '021210', 'Yuracmarca', NULL, NULL, 1780, '566.00', 44, 1, NULL, 3),
(401, '021301', 'Piscobamba', NULL, NULL, 3774, '46.00', 45, 1, NULL, 3),
(402, '021302', 'Casca', NULL, NULL, 4507, '75.00', 45, 1, NULL, 3),
(403, '021303', 'Eleazar Guzman Barron', NULL, NULL, 1376, '97.00', 45, 1, NULL, 3),
(404, '021304', 'Fidel Olivas Escudero', NULL, NULL, 2249, '213.00', 45, 1, NULL, 3),
(405, '021305', 'Llama', NULL, NULL, 1254, '49.00', 45, 1, NULL, 3),
(406, '021306', 'Llumpa', NULL, NULL, 6321, '139.00', 45, 1, NULL, 3),
(407, '021307', 'Lucma', NULL, NULL, 3246, '75.00', 45, 1, NULL, 3),
(408, '021308', 'Musga', NULL, NULL, 1027, '39.00', 45, 1, NULL, 3),
(409, '021401', 'Ocros', NULL, NULL, 1020, '225.00', 46, 1, NULL, 3),
(410, '021402', 'Acas', NULL, NULL, 1024, '256.00', 46, 1, NULL, 3),
(411, '021403', 'Cajamarquilla', NULL, NULL, 574, '79.00', 46, 1, NULL, 3),
(412, '021404', 'Carhuapampa', NULL, NULL, 826, '106.00', 46, 1, NULL, 3),
(413, '021405', 'Cochas', NULL, NULL, 1442, '415.00', 46, 1, NULL, 3),
(414, '021406', 'Congas', NULL, NULL, 1220, '131.00', 46, 1, NULL, 3),
(415, '021407', 'Llipa', NULL, NULL, 1739, '31.00', 46, 1, NULL, 3),
(416, '021408', 'San Cristobal De Rajan', NULL, NULL, 624, '70.00', 46, 1, NULL, 3),
(417, '021409', 'San Pedro', NULL, NULL, 1951, '548.00', 46, 1, NULL, 3),
(418, '021410', 'Santiago De Chilcas', NULL, NULL, 382, '88.00', 46, 1, NULL, 3),
(419, '021501', 'Cabana', NULL, NULL, 2724, '144.00', 47, 1, NULL, 3),
(420, '021502', 'Bolognesi', NULL, NULL, 1303, '84.00', 47, 1, NULL, 3),
(421, '021503', 'Conchucos', NULL, NULL, 8359, '578.00', 47, 1, NULL, 3),
(422, '021504', 'Huacaschuque', NULL, NULL, 583, '18.00', 47, 1, NULL, 3),
(423, '021505', 'Huandoval', NULL, NULL, 1124, '109.00', 47, 1, NULL, 3),
(424, '021506', 'Lacabamba', NULL, NULL, 576, '63.00', 47, 1, NULL, 3),
(425, '021507', 'Llapo', NULL, NULL, 723, '31.00', 47, 1, NULL, 3),
(426, '021508', 'Pallasca', NULL, NULL, 2447, '110.00', 47, 1, NULL, 3),
(427, '021509', 'Pampas', NULL, NULL, 8502, '437.00', 47, 1, NULL, 3),
(428, '021510', 'Santa Rosa', NULL, NULL, 1057, '291.00', 47, 1, NULL, 3),
(429, '021511', 'Tauca', NULL, NULL, 3172, '199.00', 47, 1, NULL, 3),
(430, '021601', 'Pomabamba', NULL, NULL, 16294, '345.00', 48, 1, NULL, 3),
(431, '021602', 'Huayllan', NULL, NULL, 3673, '90.00', 48, 1, NULL, 3),
(432, '021603', 'Parobamba', NULL, NULL, 6994, '340.00', 48, 1, NULL, 3),
(433, '021604', 'Quinuabamba', NULL, NULL, 2414, '149.00', 48, 1, NULL, 3),
(434, '021701', 'Recuay', NULL, NULL, 4462, '149.00', 49, 1, NULL, 3),
(435, '021702', 'Catac', NULL, NULL, 4020, '1024.00', 49, 1, NULL, 3),
(436, '021703', 'Cotaparaco', NULL, NULL, 644, '175.00', 49, 1, NULL, 3),
(437, '021704', 'Huayllapampa', NULL, NULL, 1305, '110.00', 49, 1, NULL, 3),
(438, '021705', 'Llacllin', NULL, NULL, 1806, '97.00', 49, 1, NULL, 3),
(439, '021706', 'Marca', NULL, NULL, 980, '183.00', 49, 1, NULL, 3),
(440, '021707', 'Pampas Chico', NULL, NULL, 2036, '97.00', 49, 1, NULL, 3),
(441, '021708', 'Pararin', NULL, NULL, 1373, '259.00', 49, 1, NULL, 3),
(442, '021709', 'Tapacocha', NULL, NULL, 464, '78.00', 49, 1, NULL, 3),
(443, '021710', 'Ticapampa', NULL, NULL, 2258, '146.00', 49, 1, NULL, 3),
(444, '021801', 'Chimbote', NULL, NULL, 214804, '1450.00', 50, 1, NULL, 3),
(445, '021802', 'Caceres Del Peru', NULL, NULL, 4884, '487.00', 50, 1, NULL, 3),
(446, '021803', 'Coishco', NULL, NULL, 15811, '10.00', 50, 1, NULL, 3),
(447, '021804', 'Macate', NULL, NULL, 3425, '586.00', 50, 1, NULL, 3),
(448, '021805', 'Moro', NULL, NULL, 7528, '399.00', 50, 1, NULL, 3),
(449, '021806', 'Nepeña', NULL, NULL, 15589, '457.00', 50, 1, NULL, 3),
(450, '021807', 'Samanco', NULL, NULL, 4590, '146.00', 50, 1, NULL, 3),
(451, '021808', 'Santa', NULL, NULL, 20532, '40.00', 50, 1, NULL, 3),
(452, '021809', 'Nuevo Chimbote', NULL, NULL, 151127, '397.00', 50, 1, NULL, 3),
(453, '021901', 'Sihuas', NULL, NULL, 5705, '62.00', 51, 1, NULL, 3),
(454, '021902', 'Acobamba', NULL, NULL, 2183, '151.00', 51, 1, NULL, 3),
(455, '021903', 'Alfonso Ugarte', NULL, NULL, 783, '90.00', 51, 1, NULL, 3),
(456, '021904', 'Cashapampa', NULL, NULL, 2874, '73.00', 51, 1, NULL, 3),
(457, '021905', 'Chingalpo', NULL, NULL, 1058, '172.00', 51, 1, NULL, 3),
(458, '021906', 'Huayllabamba', NULL, NULL, 4014, '258.00', 51, 1, NULL, 3),
(459, '021907', 'Quiches', NULL, NULL, 2924, '148.00', 51, 1, NULL, 3),
(460, '021908', 'Ragash', NULL, NULL, 2642, '209.00', 51, 1, NULL, 3),
(461, '021909', 'San Juan', NULL, NULL, 6522, '212.00', 51, 1, NULL, 3),
(462, '021910', 'Sicsibamba', NULL, NULL, 1824, '82.00', 51, 1, NULL, 3),
(463, '022001', 'Yungay', NULL, NULL, 21911, '275.00', 52, 1, NULL, 3),
(464, '022002', 'Cascapara', NULL, NULL, 2287, '137.00', 52, 1, NULL, 3),
(465, '022003', 'Mancos', NULL, NULL, 6977, '63.00', 52, 1, NULL, 3),
(466, '022004', 'Matacoto', NULL, NULL, 1635, '47.00', 52, 1, NULL, 3),
(467, '022005', 'Quillo', NULL, NULL, 13798, '370.00', 52, 1, NULL, 3),
(468, '022006', 'Ranrahirca', NULL, NULL, 2707, '22.00', 52, 1, NULL, 3),
(469, '022007', 'Shupluy', NULL, NULL, 2399, '166.00', 52, 1, NULL, 3),
(470, '022008', 'Yanama', NULL, NULL, 6969, '284.00', 52, 1, NULL, 3),
(471, '030101', 'Abancay', NULL, NULL, 56093, '288.00', 53, 1, NULL, 3),
(472, '030102', 'Chacoche', NULL, NULL, 1213, '176.00', 53, 1, NULL, 3),
(473, '030103', 'Circa', NULL, NULL, 2506, '635.00', 53, 1, NULL, 3),
(474, '030104', 'Curahuasi', NULL, NULL, 18328, '866.00', 53, 1, NULL, 3),
(475, '030105', 'Huanipaca', NULL, NULL, 4749, '427.00', 53, 1, NULL, 3),
(476, '030106', 'Lambrama', NULL, NULL, 5561, '524.00', 53, 1, NULL, 3),
(477, '030107', 'Pichirhua', NULL, NULL, 4042, '373.00', 53, 1, NULL, 3),
(478, '030108', 'San Pedro De Cachora', NULL, NULL, 3838, '118.00', 53, 1, NULL, 3),
(479, '030109', 'Tamburco', NULL, NULL, 9884, '54.00', 53, 1, NULL, 3),
(480, '030201', 'Andahuaylas', NULL, NULL, 48547, '376.00', 54, 1, NULL, 3),
(481, '030202', 'Andarapa', NULL, NULL, 6380, '212.00', 54, 1, NULL, 3),
(482, '030203', 'Chiara', NULL, NULL, 1350, '148.00', 54, 1, NULL, 3),
(483, '030204', 'Huancarama', NULL, NULL, 7441, '157.00', 54, 1, NULL, 3),
(484, '030205', 'Huancaray', NULL, NULL, 4632, '112.00', 54, 1, NULL, 3),
(485, '030206', 'Huayana', NULL, NULL, 1058, '95.00', 54, 1, NULL, 3),
(486, '030207', 'Kishuara', NULL, NULL, 9282, '310.00', 54, 1, NULL, 3),
(487, '030208', 'Pacobamba', NULL, NULL, 4794, '258.00', 54, 1, NULL, 3),
(488, '030209', 'Pacucha', NULL, NULL, 9994, '176.00', 54, 1, NULL, 3),
(489, '030210', 'Pampachiri', NULL, NULL, 2780, '589.00', 54, 1, NULL, 3),
(490, '030211', 'Pomacocha', NULL, NULL, 1042, '123.00', 54, 1, NULL, 3),
(491, '030212', 'San Antonio De Cachi', NULL, NULL, 3237, '179.00', 54, 1, NULL, 3),
(492, '030213', 'San Jeronimo', NULL, NULL, 27665, '244.00', 54, 1, NULL, 3),
(493, '030214', 'San Miguel De Chaccrampa', NULL, NULL, 2057, '85.00', 54, 1, NULL, 3),
(494, '030215', 'Santa Maria De Chicmo', NULL, NULL, 9910, '157.00', 54, 1, NULL, 3),
(495, '030216', 'Talavera', NULL, NULL, 18313, '109.00', 54, 1, NULL, 3),
(496, '030217', 'Tumay Huaraca', NULL, NULL, 2415, '454.00', 54, 1, NULL, 3),
(497, '030218', 'Turpo', NULL, NULL, 4197, '123.00', 54, 1, NULL, 3),
(498, '030219', 'Kaquiabamba', NULL, NULL, 2962, '113.00', 54, 1, NULL, 3),
(499, '030301', 'Antabamba', NULL, NULL, 3164, '603.00', 55, 1, NULL, 3),
(500, '030302', 'El Oro', NULL, NULL, 552, '67.00', 55, 1, NULL, 3),
(501, '030303', 'Huaquirca', NULL, NULL, 1571, '352.00', 55, 1, NULL, 3),
(502, '030304', 'Juan Espinoza Medrano', NULL, NULL, 2066, '624.00', 55, 1, NULL, 3),
(503, '030305', 'Oropesa', NULL, NULL, 3110, '1171.00', 55, 1, NULL, 3),
(504, '030306', 'Pachaconas', NULL, NULL, 1286, '229.00', 55, 1, NULL, 3),
(505, '030307', 'Sabaino', NULL, NULL, 1648, '176.00', 55, 1, NULL, 3),
(506, '030401', 'Chalhuanca', NULL, NULL, 5015, '329.00', 56, 1, NULL, 3),
(507, '030402', 'Capaya', NULL, NULL, 970, '84.00', 56, 1, NULL, 3),
(508, '030403', 'Caraybamba', NULL, NULL, 1472, '237.00', 56, 1, NULL, 3),
(509, '030404', 'Chapimarca', NULL, NULL, 2160, '204.00', 56, 1, NULL, 3),
(510, '030405', 'Colcabamba', NULL, NULL, 933, '91.00', 56, 1, NULL, 3),
(511, '030406', 'Cotaruse', NULL, NULL, 5326, '1737.00', 56, 1, NULL, 3),
(512, '030407', 'Huayllo', NULL, NULL, 728, '73.00', 56, 1, NULL, 3),
(513, '030408', 'Justo Apu Sahuaraura', NULL, NULL, 1292, '101.00', 56, 1, NULL, 3),
(514, '030409', 'Lucre', NULL, NULL, 2159, '104.00', 56, 1, NULL, 3),
(515, '030410', 'Pocohuanca', NULL, NULL, 1177, '88.00', 56, 1, NULL, 3),
(516, '030411', 'San Juan De Chacña', NULL, NULL, 856, '96.00', 56, 1, NULL, 3),
(517, '030412', 'Sañayca', NULL, NULL, 1441, '364.00', 56, 1, NULL, 3),
(518, '030413', 'Soraya', NULL, NULL, 825, '45.00', 56, 1, NULL, 3),
(519, '030414', 'Tapairihua', NULL, NULL, 2259, '160.00', 56, 1, NULL, 3),
(520, '030415', 'Tintay', NULL, NULL, 3227, '143.00', 56, 1, NULL, 3),
(521, '030416', 'Toraya', NULL, NULL, 1962, '168.00', 56, 1, NULL, 3),
(522, '030417', 'Yanaca', NULL, NULL, 1193, '104.00', 56, 1, NULL, 3),
(523, '030501', 'Tambobamba', NULL, NULL, 11582, '715.00', 57, 1, NULL, 3),
(524, '030502', 'Cotabambas', NULL, NULL, 4237, '326.00', 57, 1, NULL, 3),
(525, '030503', 'Coyllurqui', NULL, NULL, 8542, '419.00', 57, 1, NULL, 3),
(526, '030504', 'Haquira', NULL, NULL, 11802, '483.00', 57, 1, NULL, 3),
(527, '030505', 'Mara', NULL, NULL, 6695, '223.00', 57, 1, NULL, 3),
(528, '030506', 'Challhuahuacho', NULL, NULL, 9908, '455.00', 57, 1, NULL, 3),
(529, '030601', 'Chincheros', NULL, NULL, 6809, '136.00', 58, 1, NULL, 3),
(530, '030602', 'Anco_huallo', NULL, NULL, 12477, '118.00', 58, 1, NULL, 3),
(531, '030603', 'Cocharcas', NULL, NULL, 2675, '109.00', 58, 1, NULL, 3),
(532, '030604', 'Huaccana', NULL, NULL, 10327, '479.00', 58, 1, NULL, 3),
(533, '030605', 'Ocobamba', NULL, NULL, 8316, '231.00', 58, 1, NULL, 3),
(534, '030606', 'Ongoy', NULL, NULL, 9131, '230.00', 58, 1, NULL, 3),
(535, '030607', 'Uranmarca', NULL, NULL, 3649, '147.00', 58, 1, NULL, 3),
(536, '030608', 'Ranracancha', NULL, NULL, 5298, '74.00', 58, 1, NULL, 3),
(537, '030701', 'Chuquibambilla', NULL, NULL, 5402, '426.00', 59, 1, NULL, 3),
(538, '030702', 'Curpahuasi', NULL, NULL, 2369, '311.00', 59, 1, NULL, 3),
(539, '030703', 'Gamarra', NULL, NULL, 3985, '350.00', 59, 1, NULL, 3),
(540, '030704', 'Huayllati', NULL, NULL, 1685, '124.00', 59, 1, NULL, 3),
(541, '030705', 'Mamara', NULL, NULL, 981, '62.00', 59, 1, NULL, 3),
(542, '030706', 'Micaela Bastidas', NULL, NULL, 1680, '105.00', 59, 1, NULL, 3),
(543, '030707', 'Pataypampa', NULL, NULL, 1121, '147.00', 59, 1, NULL, 3),
(544, '030708', 'Progreso', NULL, NULL, 3325, '241.00', 59, 1, NULL, 3),
(545, '030709', 'San Antonio', NULL, NULL, 366, '25.00', 59, 1, NULL, 3),
(546, '030710', 'Santa Rosa', NULL, NULL, 716, '31.00', 59, 1, NULL, 3),
(547, '030711', 'Turpay', NULL, NULL, 769, '51.00', 59, 1, NULL, 3),
(548, '030712', 'Vilcabamba', NULL, NULL, 1401, '7.00', 59, 1, NULL, 3),
(549, '030713', 'Virundo', NULL, NULL, 1296, '114.00', 59, 1, NULL, 3),
(550, '030714', 'Curasco', NULL, NULL, 1624, '135.00', 59, 1, NULL, 3),
(551, '040101', 'Arequipa', NULL, NULL, 54095, '10.00', 60, 1, NULL, 3),
(552, '040102', 'Alto Selva Alegre', NULL, NULL, 82412, '67.00', 60, 1, NULL, 3),
(553, '040103', 'Cayma', NULL, NULL, 91802, '236.00', 60, 1, NULL, 3),
(554, '040104', 'Cerro Colorado', NULL, NULL, 148164, '173.00', 60, 1, NULL, 3),
(555, '040105', 'Characato', NULL, NULL, 9288, '85.00', 60, 1, NULL, 3),
(556, '040106', 'Chiguata', NULL, NULL, 2940, '437.00', 60, 1, NULL, 3),
(557, '040107', 'Jacobo Hunter', NULL, NULL, 48326, '20.00', 60, 1, NULL, 3),
(558, '040108', 'La Joya', NULL, NULL, 30233, '698.00', 60, 1, NULL, 3),
(559, '040109', 'Mariano Melgar', NULL, NULL, 52667, '29.00', 60, 1, NULL, 3),
(560, '040110', 'Miraflores', NULL, NULL, 48677, '26.00', 60, 1, NULL, 3),
(561, '040111', 'Mollebaya', NULL, NULL, 1868, '31.00', 60, 1, NULL, 3),
(562, '040112', 'Paucarpata', NULL, NULL, 124755, '28.00', 60, 1, NULL, 3),
(563, '040113', 'Pocsi', NULL, NULL, 547, '167.00', 60, 1, NULL, 3),
(564, '040114', 'Polobaya', NULL, NULL, 1477, '399.00', 60, 1, NULL, 3),
(565, '040115', 'Quequeña', NULL, NULL, 1376, '35.00', 60, 1, NULL, 3),
(566, '040116', 'Sabandia', NULL, NULL, 4136, '38.00', 60, 1, NULL, 3),
(567, '040117', 'Sachaca', NULL, NULL, 19581, '11.00', 60, 1, NULL, 3),
(568, '040118', 'San Juan De Siguas', NULL, NULL, 1535, '93.00', 60, 1, NULL, 3),
(569, '040119', 'San Juan De Tarucani', NULL, NULL, 2179, '2240.00', 60, 1, NULL, 3),
(570, '040120', 'Santa Isabel De Siguas', NULL, NULL, 1264, '186.00', 60, 1, NULL, 3),
(571, '040121', 'Santa Rita De Siguas', NULL, NULL, 5592, '369.00', 60, 1, NULL, 3),
(572, '040122', 'Socabaya', NULL, NULL, 78135, '25.00', 60, 1, NULL, 3),
(573, '040123', 'Tiabaya', NULL, NULL, 14768, '39.00', 60, 1, NULL, 3),
(574, '040124', 'Uchumayo', NULL, NULL, 12436, '271.00', 60, 1, NULL, 3),
(575, '040125', 'Vitor', NULL, NULL, 2345, '1532.00', 60, 1, NULL, 3),
(576, '040126', 'Yanahuara', NULL, NULL, 25483, '6.00', 60, 1, NULL, 3),
(577, '040127', 'Yarabamba', NULL, NULL, 1125, '480.00', 60, 1, NULL, 3),
(578, '040128', 'Yura', NULL, NULL, 25367, '1918.00', 60, 1, NULL, 3),
(579, '040129', 'Jose Luis Bustamante Y Rivero', NULL, NULL, 76711, '11.00', 60, 1, NULL, 3),
(580, '040201', 'Camana', NULL, NULL, 14477, '11.00', 61, 1, NULL, 3),
(581, '040202', 'Jose Maria Quimper', NULL, NULL, 4134, '16.00', 61, 1, NULL, 3),
(582, '040203', 'Mariano Nicolas Valcarcel', NULL, NULL, 6890, '557.00', 61, 1, NULL, 3),
(583, '040204', 'Mariscal Caceres', NULL, NULL, 6376, '593.00', 61, 1, NULL, 3),
(584, '040205', 'Nicolas De Pierola', NULL, NULL, 6310, '387.00', 61, 1, NULL, 3),
(585, '040206', 'Ocoña', NULL, NULL, 4810, '1417.00', 61, 1, NULL, 3),
(586, '040207', 'Quilca', NULL, NULL, 661, '904.00', 61, 1, NULL, 3),
(587, '040208', 'Samuel Pastor', NULL, NULL, 15294, '114.00', 61, 1, NULL, 3),
(588, '040301', 'Caraveli', NULL, NULL, 3722, '756.00', 62, 1, NULL, 3),
(589, '040302', 'Acari', NULL, NULL, 3201, '771.00', 62, 1, NULL, 3),
(590, '040303', 'Atico', NULL, NULL, 4147, '3099.00', 62, 1, NULL, 3),
(591, '040304', 'Atiquipa', NULL, NULL, 913, '423.00', 62, 1, NULL, 3),
(592, '040305', 'Bella Union', NULL, NULL, 6586, '1584.00', 62, 1, NULL, 3),
(593, '040306', 'Cahuacho', NULL, NULL, 906, '1386.00', 62, 1, NULL, 3),
(594, '040307', 'Chala', NULL, NULL, 6756, '340.00', 62, 1, NULL, 3),
(595, '040308', 'Chaparra', NULL, NULL, 5368, '1497.00', 62, 1, NULL, 3),
(596, '040309', 'Huanuhuanu', NULL, NULL, 3258, '703.00', 62, 1, NULL, 3),
(597, '040310', 'Jaqui', NULL, NULL, 1256, '422.00', 62, 1, NULL, 3),
(598, '040311', 'Lomas', NULL, NULL, 1328, '445.00', 62, 1, NULL, 3),
(599, '040312', 'Quicacha', NULL, NULL, 1881, '1037.00', 62, 1, NULL, 3),
(600, '040313', 'Yauca', NULL, NULL, 1582, '546.00', 62, 1, NULL, 3),
(601, '040401', 'Aplao', NULL, NULL, 8844, '616.00', 63, 1, NULL, 3),
(602, '040402', 'Andagua', NULL, NULL, 1152, '473.00', 63, 1, NULL, 3),
(603, '040403', 'Ayo', NULL, NULL, 396, '284.00', 63, 1, NULL, 3),
(604, '040404', 'Chachas', NULL, NULL, 1720, '1197.00', 63, 1, NULL, 3),
(605, '040405', 'Chilcaymarca', NULL, NULL, 1243, '182.00', 63, 1, NULL, 3),
(606, '040406', 'Choco', NULL, NULL, 1009, '905.00', 63, 1, NULL, 3),
(607, '040407', 'Huancarqui', NULL, NULL, 1317, '795.00', 63, 1, NULL, 3),
(608, '040408', 'Machaguay', NULL, NULL, 723, '245.00', 63, 1, NULL, 3),
(609, '040409', 'Orcopampa', NULL, NULL, 9661, '729.00', 63, 1, NULL, 3),
(610, '040410', 'Pampacolca', NULL, NULL, 2713, '266.00', 63, 1, NULL, 3),
(611, '040411', 'Tipan', NULL, NULL, 522, '55.00', 63, 1, NULL, 3),
(612, '040412', 'Uñon', NULL, NULL, 442, '339.00', 63, 1, NULL, 3),
(613, '040413', 'Uraca', NULL, NULL, 7216, '696.00', 63, 1, NULL, 3),
(614, '040414', 'Viraco', NULL, NULL, 1712, '137.00', 63, 1, NULL, 3),
(615, '040501', 'Chivay', NULL, NULL, 7688, '243.00', 64, 1, NULL, 3),
(616, '040502', 'Achoma', NULL, NULL, 908, '365.00', 64, 1, NULL, 3),
(617, '040503', 'Cabanaconde', NULL, NULL, 2406, '460.00', 64, 1, NULL, 3),
(618, '040504', 'Callalli', NULL, NULL, 2003, '1542.00', 64, 1, NULL, 3),
(619, '040505', 'Caylloma', NULL, NULL, 3173, '1517.00', 64, 1, NULL, 3),
(620, '040506', 'Coporaque', NULL, NULL, 1520, '114.00', 64, 1, NULL, 3),
(621, '040507', 'Huambo', NULL, NULL, 614, '717.00', 64, 1, NULL, 3),
(622, '040508', 'Huanca', NULL, NULL, 1450, '377.00', 64, 1, NULL, 3),
(623, '040509', 'Ichupampa', NULL, NULL, 663, '74.00', 64, 1, NULL, 3),
(624, '040510', 'Lari', NULL, NULL, 1526, '371.00', 64, 1, NULL, 3),
(625, '040511', 'Lluta', NULL, NULL, 1275, '1218.00', 64, 1, NULL, 3),
(626, '040512', 'Maca', NULL, NULL, 723, '237.00', 64, 1, NULL, 3),
(627, '040513', 'Madrigal', NULL, NULL, 498, '160.00', 64, 1, NULL, 3),
(628, '040514', 'San Antonio De Chuca', NULL, NULL, 1547, '1538.00', 64, 1, NULL, 3),
(629, '040515', 'Sibayo', NULL, NULL, 675, '284.00', 64, 1, NULL, 3),
(630, '040516', 'Tapay', NULL, NULL, 545, '415.00', 64, 1, NULL, 3),
(631, '040517', 'Tisco', NULL, NULL, 1450, '1441.00', 64, 1, NULL, 3),
(632, '040518', 'Tuti', NULL, NULL, 758, '240.00', 64, 1, NULL, 3),
(633, '040519', 'Yanque', NULL, NULL, 2137, '1111.00', 64, 1, NULL, 3),
(634, '040520', 'Majes', NULL, NULL, 62661, '1638.00', 64, 1, NULL, 3),
(635, '040601', 'Chuquibamba', NULL, NULL, 3346, '1188.00', 65, 1, NULL, 3),
(636, '040602', 'Andaray', NULL, NULL, 670, '811.00', 65, 1, NULL, 3),
(637, '040603', 'Cayarani', NULL, NULL, 3159, '1396.00', 65, 1, NULL, 3),
(638, '040604', 'Chichas', NULL, NULL, 672, '390.00', 65, 1, NULL, 3),
(639, '040605', 'Iray', NULL, NULL, 646, '238.00', 65, 1, NULL, 3),
(640, '040606', 'Rio Grande', NULL, NULL, 2751, '531.00', 65, 1, NULL, 3),
(641, '040607', 'Salamanca', NULL, NULL, 879, '1244.00', 65, 1, NULL, 3),
(642, '040608', 'Yanaquihua', NULL, NULL, 5820, '1055.00', 65, 1, NULL, 3),
(643, '040701', 'Mollendo', NULL, NULL, 22389, '930.00', 66, 1, NULL, 3),
(644, '040702', 'Cocachacra', NULL, NULL, 8984, '1548.00', 66, 1, NULL, 3),
(645, '040703', 'Dean Valdivia', NULL, NULL, 6619, '134.00', 66, 1, NULL, 3),
(646, '040704', 'Islay', NULL, NULL, 7124, '365.00', 66, 1, NULL, 3),
(647, '040705', 'Mejia', NULL, NULL, 1037, '102.00', 66, 1, NULL, 3),
(648, '040706', 'Punta De Bombon', NULL, NULL, 6477, '757.00', 66, 1, NULL, 3),
(649, '040801', 'Cotahuasi', NULL, NULL, 2937, '166.00', 67, 1, NULL, 3),
(650, '040802', 'Alca', NULL, NULL, 2019, '191.00', 67, 1, NULL, 3),
(651, '040803', 'Charcana', NULL, NULL, 556, '163.00', 67, 1, NULL, 3),
(652, '040804', 'Huaynacotas', NULL, NULL, 2251, '935.00', 67, 1, NULL, 3),
(653, '040805', 'Pampamarca', NULL, NULL, 1265, '787.00', 67, 1, NULL, 3),
(654, '040806', 'Puyca', NULL, NULL, 2807, '1505.00', 67, 1, NULL, 3),
(655, '040807', 'Quechualla', NULL, NULL, 236, '135.00', 67, 1, NULL, 3),
(656, '040808', 'Sayla', NULL, NULL, 574, '71.00', 67, 1, NULL, 3),
(657, '040809', 'Tauria', NULL, NULL, 323, '318.00', 67, 1, NULL, 3),
(658, '040810', 'Tomepampa', NULL, NULL, 826, '96.00', 67, 1, NULL, 3),
(659, '040811', 'Toro', NULL, NULL, 808, '526.00', 67, 1, NULL, 3),
(660, '050101', 'Ayacucho', NULL, NULL, 113380, '82.00', 68, 1, NULL, 3),
(661, '050102', 'Acocro', NULL, NULL, 10199, '387.00', 68, 1, NULL, 3),
(662, '050103', 'Acos Vinchos', NULL, NULL, 5948, '155.00', 68, 1, NULL, 3),
(663, '050104', 'Carmen Alto', NULL, NULL, 21350, '21.00', 68, 1, NULL, 3),
(664, '050105', 'Chiara', NULL, NULL, 7163, '497.00', 68, 1, NULL, 3),
(665, '050106', 'Ocros', NULL, NULL, 5508, '203.00', 68, 1, NULL, 3),
(666, '050107', 'Pacaycasa', NULL, NULL, 3192, '58.00', 68, 1, NULL, 3),
(667, '050108', 'Quinua', NULL, NULL, 6200, '121.00', 68, 1, NULL, 3),
(668, '050109', 'San Jose De Ticllas', NULL, NULL, 3688, '65.00', 68, 1, NULL, 3),
(669, '050110', 'San Juan Bautista', NULL, NULL, 50429, '18.00', 68, 1, NULL, 3),
(670, '050111', 'Santiago De Pischa', NULL, NULL, 1799, '123.00', 68, 1, NULL, 3),
(671, '050112', 'Socos', NULL, NULL, 7108, '82.00', 68, 1, NULL, 3),
(672, '050113', 'Tambillo', NULL, NULL, 5715, '179.00', 68, 1, NULL, 3),
(673, '050114', 'Vinchos', NULL, NULL, 16710, '951.00', 68, 1, NULL, 3),
(674, '050115', 'Jesus Nazareno', NULL, NULL, 18054, '17.00', 68, 1, NULL, 3),
(675, '050201', 'Cangallo', NULL, NULL, 6747, '185.00', 69, 1, NULL, 3),
(676, '050202', 'Chuschi', NULL, NULL, 7965, '412.00', 69, 1, NULL, 3),
(677, '050203', 'Los Morochucos', NULL, NULL, 8205, '249.00', 69, 1, NULL, 3),
(678, '050204', 'Maria Parado De Bellido', NULL, NULL, 2574, '131.00', 69, 1, NULL, 3),
(679, '050205', 'Paras', NULL, NULL, 4575, '780.00', 69, 1, NULL, 3),
(680, '050206', 'Totos', NULL, NULL, 3720, '115.00', 69, 1, NULL, 3),
(681, '050301', 'Sancos', NULL, NULL, 3584, '1357.00', 70, 1, NULL, 3),
(682, '050302', 'Carapo', NULL, NULL, 2504, '196.00', 70, 1, NULL, 3),
(683, '050303', 'Sacsamarca', NULL, NULL, 1613, '634.00', 70, 1, NULL, 3),
(684, '050304', 'Santiago De Lucanamarca', NULL, NULL, 2638, '645.00', 70, 1, NULL, 3),
(685, '050401', 'Huanta', NULL, NULL, 47373, '355.00', 71, 1, NULL, 3),
(686, '050402', 'Ayahuanco', NULL, NULL, 14485, '1076.00', 71, 1, NULL, 3),
(687, '050403', 'Huamanguilla', NULL, NULL, 4981, '77.00', 71, 1, NULL, 3),
(688, '050404', 'Iguain', NULL, NULL, 3163, '71.00', 71, 1, NULL, 3),
(689, '050405', 'Luricocha', NULL, NULL, 4991, '140.00', 71, 1, NULL, 3),
(690, '050406', 'Santillana', NULL, NULL, 7168, '518.00', 71, 1, NULL, 3),
(691, '050407', 'Sivia', NULL, NULL, 12345, '793.00', 71, 1, NULL, 3),
(692, '050408', 'Llochegua', NULL, NULL, 14047, '783.00', 71, 1, NULL, 3),
(693, '050501', 'San Miguel', NULL, NULL, 17240, '809.00', 72, 1, NULL, 3),
(694, '050502', 'Anco', NULL, NULL, 16875, '1062.00', 72, 1, NULL, 3),
(695, '050503', 'Ayna', NULL, NULL, 10560, '315.00', 72, 1, NULL, 3),
(696, '050504', 'Chilcas', NULL, NULL, 3060, '154.00', 72, 1, NULL, 3),
(697, '050505', 'Chungui', NULL, NULL, 7287, '1047.00', 72, 1, NULL, 3),
(698, '050506', 'Luis Carranza', NULL, NULL, 1838, '212.00', 72, 1, NULL, 3),
(699, '050507', 'Santa Rosa', NULL, NULL, 11286, '399.00', 72, 1, NULL, 3),
(700, '050508', 'Tambo', NULL, NULL, 20567, '315.00', 72, 1, NULL, 3),
(701, '050601', 'Puquio', NULL, NULL, 13813, '861.00', 73, 1, NULL, 3),
(702, '050602', 'Aucara', NULL, NULL, 5433, '880.00', 73, 1, NULL, 3),
(703, '050603', 'Cabana', NULL, NULL, 4489, '405.00', 73, 1, NULL, 3),
(704, '050604', 'Carmen Salcedo', NULL, NULL, 3985, '463.00', 73, 1, NULL, 3),
(705, '050605', 'Chaviña', NULL, NULL, 2018, '372.00', 73, 1, NULL, 3),
(706, '050606', 'Chipao', NULL, NULL, 3741, '1156.00', 73, 1, NULL, 3),
(707, '050607', 'Huac-huas', NULL, NULL, 2781, '312.00', 73, 1, NULL, 3),
(708, '050608', 'Laramate', NULL, NULL, 1443, '781.00', 73, 1, NULL, 3),
(709, '050609', 'Leoncio Prado', NULL, NULL, 1374, '1112.00', 73, 1, NULL, 3),
(710, '050610', 'Llauta', NULL, NULL, 1145, '492.00', 73, 1, NULL, 3),
(711, '050611', 'Lucanas', NULL, NULL, 4089, '1209.00', 73, 1, NULL, 3),
(712, '050612', 'Ocaña', NULL, NULL, 2914, '856.00', 73, 1, NULL, 3),
(713, '050613', 'Otoca', NULL, NULL, 3020, '718.00', 73, 1, NULL, 3),
(714, '050614', 'Saisa', NULL, NULL, 906, '577.00', 73, 1, NULL, 3),
(715, '050615', 'San Cristobal', NULL, NULL, 2105, '407.00', 73, 1, NULL, 3);
INSERT INTO `comun_ubigeo` (`id`, `codigo`, `nombre`, `codigo_telefono`, `capital`, `poblacion`, `area_km2`, `padre_id`, `pais_id`, `region_natural_id`, `tipo_ubigeo_id`) VALUES
(716, '050616', 'San Juan', NULL, NULL, 1559, '47.00', 73, 1, NULL, 3),
(717, '050617', 'San Pedro', NULL, NULL, 3000, '729.00', 73, 1, NULL, 3),
(718, '050618', 'San Pedro De Palco', NULL, NULL, 1371, '521.00', 73, 1, NULL, 3),
(719, '050619', 'Sancos', NULL, NULL, 7236, '1505.00', 73, 1, NULL, 3),
(720, '050620', 'Santa Ana De Huaycahuacho', NULL, NULL, 667, '45.00', 73, 1, NULL, 3),
(721, '050621', 'Santa Lucia', NULL, NULL, 914, '1031.00', 73, 1, NULL, 3),
(722, '050701', 'Coracora', NULL, NULL, 15378, '1389.00', 74, 1, NULL, 3),
(723, '050702', 'Chumpi', NULL, NULL, 2656, '372.00', 74, 1, NULL, 3),
(724, '050703', 'Coronel Castañeda', NULL, NULL, 1872, '1089.00', 74, 1, NULL, 3),
(725, '050704', 'Pacapausa', NULL, NULL, 2874, '147.00', 74, 1, NULL, 3),
(726, '050705', 'Pullo', NULL, NULL, 4893, '1562.00', 74, 1, NULL, 3),
(727, '050706', 'Puyusca', NULL, NULL, 2076, '711.00', 74, 1, NULL, 3),
(728, '050707', 'San Francisco De Ravacayco', NULL, NULL, 753, '103.00', 74, 1, NULL, 3),
(729, '050708', 'Upahuacho', NULL, NULL, 2740, '577.00', 74, 1, NULL, 3),
(730, '050801', 'Pausa', NULL, NULL, 2792, '251.00', 75, 1, NULL, 3),
(731, '050802', 'Colta', NULL, NULL, 1140, '248.00', 75, 1, NULL, 3),
(732, '050803', 'Corculla', NULL, NULL, 455, '101.00', 75, 1, NULL, 3),
(733, '050804', 'Lampa', NULL, NULL, 2528, '281.00', 75, 1, NULL, 3),
(734, '050805', 'Marcabamba', NULL, NULL, 773, '120.00', 75, 1, NULL, 3),
(735, '050806', 'Oyolo', NULL, NULL, 1195, '792.00', 75, 1, NULL, 3),
(736, '050807', 'Pararca', NULL, NULL, 660, '56.00', 75, 1, NULL, 3),
(737, '050808', 'San Javier De Alpabamba', NULL, NULL, 538, '117.00', 75, 1, NULL, 3),
(738, '050809', 'San Jose De Ushua', NULL, NULL, 177, '29.00', 75, 1, NULL, 3),
(739, '050810', 'Sara Sara', NULL, NULL, 731, '86.00', 75, 1, NULL, 3),
(740, '050901', 'Querobamba', NULL, NULL, 2733, '282.00', 76, 1, NULL, 3),
(741, '050902', 'Belen', NULL, NULL, 756, '37.00', 76, 1, NULL, 3),
(742, '050903', 'Chalcos', NULL, NULL, 637, '55.00', 76, 1, NULL, 3),
(743, '050904', 'Chilcayoc', NULL, NULL, 575, '30.00', 76, 1, NULL, 3),
(744, '050905', 'Huacaña', NULL, NULL, 674, '144.00', 76, 1, NULL, 3),
(745, '050906', 'Morcolla', NULL, NULL, 1074, '289.00', 76, 1, NULL, 3),
(746, '050907', 'Paico', NULL, NULL, 842, '95.00', 76, 1, NULL, 3),
(747, '050908', 'San Pedro De Larcay', NULL, NULL, 1021, '316.00', 76, 1, NULL, 3),
(748, '050909', 'San Salvador De Quije', NULL, NULL, 1642, '126.00', 76, 1, NULL, 3),
(749, '050910', 'Santiago De Paucaray', NULL, NULL, 747, '62.00', 76, 1, NULL, 3),
(750, '050911', 'Soras', NULL, NULL, 1292, '352.00', 76, 1, NULL, 3),
(751, '051001', 'Huancapi', NULL, NULL, 1934, '240.00', 77, 1, NULL, 3),
(752, '051002', 'Alcamenca', NULL, NULL, 2414, '116.00', 77, 1, NULL, 3),
(753, '051003', 'Apongo', NULL, NULL, 1403, '176.00', 77, 1, NULL, 3),
(754, '051004', 'Asquipata', NULL, NULL, 447, '71.00', 77, 1, NULL, 3),
(755, '051005', 'Canaria', NULL, NULL, 3997, '264.00', 77, 1, NULL, 3),
(756, '051006', 'Cayara', NULL, NULL, 1165, '63.00', 77, 1, NULL, 3),
(757, '051007', 'Colca', NULL, NULL, 1014, '64.00', 77, 1, NULL, 3),
(758, '051008', 'Huamanquiquia', NULL, NULL, 1254, '72.00', 77, 1, NULL, 3),
(759, '051009', 'Huancaraylla', NULL, NULL, 1077, '163.00', 77, 1, NULL, 3),
(760, '051010', 'Huaya', NULL, NULL, 3241, '157.00', 77, 1, NULL, 3),
(761, '051011', 'Sarhua', NULL, NULL, 2763, '378.00', 77, 1, NULL, 3),
(762, '051012', 'Vilcanchos', NULL, NULL, 2674, '500.00', 77, 1, NULL, 3),
(763, '051101', 'Vilcas Huaman', NULL, NULL, 8369, '220.00', 78, 1, NULL, 3),
(764, '051102', 'Accomarca', NULL, NULL, 982, '87.00', 78, 1, NULL, 3),
(765, '051103', 'Carhuanca', NULL, NULL, 1012, '54.00', 78, 1, NULL, 3),
(766, '051104', 'Concepcion', NULL, NULL, 3100, '229.00', 78, 1, NULL, 3),
(767, '051105', 'Huambalpa', NULL, NULL, 2168, '162.00', 78, 1, NULL, 3),
(768, '051106', 'Independencia', NULL, NULL, 1593, '88.00', 78, 1, NULL, 3),
(769, '051107', 'Saurama', NULL, NULL, 1292, '89.00', 78, 1, NULL, 3),
(770, '051108', 'Vischongo', NULL, NULL, 4697, '278.00', 78, 1, NULL, 3),
(771, '060101', 'Cajamarca', NULL, NULL, 246536, '379.00', 79, 1, NULL, 3),
(772, '060102', 'Asuncion', NULL, NULL, 13365, '214.00', 79, 1, NULL, 3),
(773, '060103', 'Chetilla', NULL, NULL, 4294, '74.00', 79, 1, NULL, 3),
(774, '060104', 'Cospan', NULL, NULL, 7887, '552.00', 79, 1, NULL, 3),
(775, '060105', 'Encañada', NULL, NULL, 24190, '631.00', 79, 1, NULL, 3),
(776, '060106', 'Jesus', NULL, NULL, 14703, '296.00', 79, 1, NULL, 3),
(777, '060107', 'Llacanora', NULL, NULL, 5363, '50.00', 79, 1, NULL, 3),
(778, '060108', 'Los Baños Del Inca', NULL, NULL, 42753, '283.00', 79, 1, NULL, 3),
(779, '060109', 'Magdalena', NULL, NULL, 9650, '209.00', 79, 1, NULL, 3),
(780, '060110', 'Matara', NULL, NULL, 3567, '63.00', 79, 1, NULL, 3),
(781, '060111', 'Namora', NULL, NULL, 10637, '157.00', 79, 1, NULL, 3),
(782, '060112', 'San Juan', NULL, NULL, 5195, '72.00', 79, 1, NULL, 3),
(783, '060201', 'Cajabamba', NULL, NULL, 30603, '190.00', 80, 1, NULL, 3),
(784, '060202', 'Cachachi', NULL, NULL, 26794, '815.00', 80, 1, NULL, 3),
(785, '060203', 'Condebamba', NULL, NULL, 13954, '199.00', 80, 1, NULL, 3),
(786, '060204', 'Sitacocha', NULL, NULL, 8910, '584.00', 80, 1, NULL, 3),
(787, '060301', 'Celendin', NULL, NULL, 28030, '407.00', 81, 1, NULL, 3),
(788, '060302', 'Chumuch', NULL, NULL, 3196, '216.00', 81, 1, NULL, 3),
(789, '060303', 'Cortegana', NULL, NULL, 8819, '230.00', 81, 1, NULL, 3),
(790, '060304', 'Huasmin', NULL, NULL, 13611, '445.00', 81, 1, NULL, 3),
(791, '060305', 'Jorge Chavez', NULL, NULL, 597, '54.00', 81, 1, NULL, 3),
(792, '060306', 'Jose Galvez', NULL, NULL, 2545, '48.00', 81, 1, NULL, 3),
(793, '060307', 'Miguel Iglesias', NULL, NULL, 5556, '249.00', 81, 1, NULL, 3),
(794, '060308', 'Oxamarca', NULL, NULL, 6937, '285.00', 81, 1, NULL, 3),
(795, '060309', 'Sorochuco', NULL, NULL, 9892, '166.00', 81, 1, NULL, 3),
(796, '060310', 'Sucre', NULL, NULL, 6073, '260.00', 81, 1, NULL, 3),
(797, '060311', 'Utco', NULL, NULL, 1408, '100.00', 81, 1, NULL, 3),
(798, '060312', 'La Libertad De Pallan', NULL, NULL, 8988, '187.00', 81, 1, NULL, 3),
(799, '060401', 'Chota', NULL, NULL, 48698, '268.00', 82, 1, NULL, 3),
(800, '060402', 'Anguia', NULL, NULL, 4298, '150.00', 82, 1, NULL, 3),
(801, '060403', 'Chadin', NULL, NULL, 4111, '64.00', 82, 1, NULL, 3),
(802, '060404', 'Chiguirip', NULL, NULL, 4672, '49.00', 82, 1, NULL, 3),
(803, '060405', 'Chimban', NULL, NULL, 3663, '145.00', 82, 1, NULL, 3),
(804, '060406', 'Choropampa', NULL, NULL, 2663, '206.00', 82, 1, NULL, 3),
(805, '060407', 'Cochabamba', NULL, NULL, 6441, '119.00', 82, 1, NULL, 3),
(806, '060408', 'Conchan', NULL, NULL, 7015, '156.00', 82, 1, NULL, 3),
(807, '060409', 'Huambos', NULL, NULL, 9508, '246.00', 82, 1, NULL, 3),
(808, '060410', 'Lajas', NULL, NULL, 12552, '123.00', 82, 1, NULL, 3),
(809, '060411', 'Llama', NULL, NULL, 8061, '492.00', 82, 1, NULL, 3),
(810, '060412', 'Miracosta', NULL, NULL, 3910, '420.00', 82, 1, NULL, 3),
(811, '060413', 'Paccha', NULL, NULL, 5327, '106.00', 82, 1, NULL, 3),
(812, '060414', 'Pion', NULL, NULL, 1575, '139.00', 82, 1, NULL, 3),
(813, '060415', 'Querocoto', NULL, NULL, 8968, '300.00', 82, 1, NULL, 3),
(814, '060416', 'San Juan De Licupis', NULL, NULL, 986, '201.00', 82, 1, NULL, 3),
(815, '060417', 'Tacabamba', NULL, NULL, 20049, '191.00', 82, 1, NULL, 3),
(816, '060418', 'Tocmoche', NULL, NULL, 995, '216.00', 82, 1, NULL, 3),
(817, '060419', 'Chalamarca', NULL, NULL, 11222, '181.00', 82, 1, NULL, 3),
(818, '060501', 'Contumaza', NULL, NULL, 8499, '347.00', 83, 1, NULL, 3),
(819, '060502', 'Chilete', NULL, NULL, 2787, '131.00', 83, 1, NULL, 3),
(820, '060503', 'Cupisnique', NULL, NULL, 1471, '288.00', 83, 1, NULL, 3),
(821, '060504', 'Guzmango', NULL, NULL, 3130, '50.00', 83, 1, NULL, 3),
(822, '060505', 'San Benito', NULL, NULL, 3823, '486.00', 83, 1, NULL, 3),
(823, '060506', 'Santa Cruz De Toled', NULL, NULL, 1056, '66.00', 83, 1, NULL, 3),
(824, '060507', 'Tantarica', NULL, NULL, 3247, '147.00', 83, 1, NULL, 3),
(825, '060508', 'Yonan', NULL, NULL, 7899, '546.00', 83, 1, NULL, 3),
(826, '060601', 'Cutervo', NULL, NULL, 56157, '426.00', 84, 1, NULL, 3),
(827, '060602', 'Callayuc', NULL, NULL, 10321, '311.00', 84, 1, NULL, 3),
(828, '060603', 'Choros', NULL, NULL, 3599, '260.00', 84, 1, NULL, 3),
(829, '060604', 'Cujillo', NULL, NULL, 3033, '103.00', 84, 1, NULL, 3),
(830, '060605', 'La Ramada', NULL, NULL, 4855, '36.00', 84, 1, NULL, 3),
(831, '060606', 'Pimpingos', NULL, NULL, 5767, '169.00', 84, 1, NULL, 3),
(832, '060607', 'Querocotillo', NULL, NULL, 16988, '687.00', 84, 1, NULL, 3),
(833, '060608', 'San Andres De Cutervo', NULL, NULL, 5259, '124.00', 84, 1, NULL, 3),
(834, '060609', 'San Juan De Cutervo', NULL, NULL, 2005, '65.00', 84, 1, NULL, 3),
(835, '060610', 'San Luis De Lucma', NULL, NULL, 4041, '102.00', 84, 1, NULL, 3),
(836, '060611', 'Santa Cruz', NULL, NULL, 2936, '130.00', 84, 1, NULL, 3),
(837, '060612', 'Santo Domingo De La Capilla', NULL, NULL, 5643, '105.00', 84, 1, NULL, 3),
(838, '060613', 'Santo Tomas', NULL, NULL, 7988, '229.00', 84, 1, NULL, 3),
(839, '060614', 'Socota', NULL, NULL, 10747, '154.00', 84, 1, NULL, 3),
(840, '060615', 'Toribio Casanova', NULL, NULL, 1294, '127.00', 84, 1, NULL, 3),
(841, '060701', 'Bambamarca', NULL, NULL, 81731, '449.00', 85, 1, NULL, 3),
(842, '060702', 'Chugur', NULL, NULL, 3603, '106.00', 85, 1, NULL, 3),
(843, '060703', 'Hualgayoc', NULL, NULL, 16994, '229.00', 85, 1, NULL, 3),
(844, '060801', 'Jaen', NULL, NULL, 100450, '554.00', 86, 1, NULL, 3),
(845, '060802', 'Bellavista', NULL, NULL, 15361, '881.00', 86, 1, NULL, 3),
(846, '060803', 'Chontali', NULL, NULL, 10235, '427.00', 86, 1, NULL, 3),
(847, '060804', 'Colasay', NULL, NULL, 10577, '603.00', 86, 1, NULL, 3),
(848, '060805', 'Huabal', NULL, NULL, 7056, '83.00', 86, 1, NULL, 3),
(849, '060806', 'Las Pirias', NULL, NULL, 4054, '59.00', 86, 1, NULL, 3),
(850, '060807', 'Pomahuaca', NULL, NULL, 10078, '803.00', 86, 1, NULL, 3),
(851, '060808', 'Pucara', NULL, NULL, 7657, '228.00', 86, 1, NULL, 3),
(852, '060809', 'Sallique', NULL, NULL, 8656, '367.00', 86, 1, NULL, 3),
(853, '060810', 'San Felipe', NULL, NULL, 6218, '256.00', 86, 1, NULL, 3),
(854, '060811', 'San Jose Del Alto', NULL, NULL, 7192, '485.00', 86, 1, NULL, 3),
(855, '060812', 'Santa Rosa', NULL, NULL, 11466, '279.00', 86, 1, NULL, 3),
(856, '060901', 'San Ignacio', NULL, NULL, 37436, '325.00', 87, 1, NULL, 3),
(857, '060902', 'Chirinos', NULL, NULL, 14299, '352.00', 87, 1, NULL, 3),
(858, '060903', 'Huarango', NULL, NULL, 20614, '906.00', 87, 1, NULL, 3),
(859, '060904', 'La Coipa', NULL, NULL, 20882, '417.00', 87, 1, NULL, 3),
(860, '060905', 'Namballe', NULL, NULL, 11600, '693.00', 87, 1, NULL, 3),
(861, '060906', 'San Jose De Lourdes', NULL, NULL, 21847, '1359.00', 87, 1, NULL, 3),
(862, '060907', 'Tabaconas', NULL, NULL, 21686, '813.00', 87, 1, NULL, 3),
(863, '061001', 'Pedro Galvez', NULL, NULL, 21345, '243.00', 88, 1, NULL, 3),
(864, '061002', 'Chancay', NULL, NULL, 3337, '67.00', 88, 1, NULL, 3),
(865, '061003', 'Eduardo Villanueva', NULL, NULL, 2292, '62.00', 88, 1, NULL, 3),
(866, '061004', 'Gregorio Pita', NULL, NULL, 6711, '216.00', 88, 1, NULL, 3),
(867, '061005', 'Ichocan', NULL, NULL, 1698, '69.00', 88, 1, NULL, 3),
(868, '061006', 'Jose Manuel Quiroz', NULL, NULL, 3988, '109.00', 88, 1, NULL, 3),
(869, '061007', 'Jose Sabogal', NULL, NULL, 15115, '583.00', 88, 1, NULL, 3),
(870, '061101', 'San Miguel', NULL, NULL, 15885, '354.00', 89, 1, NULL, 3),
(871, '061102', 'Bolivar', NULL, NULL, 1488, '65.00', 89, 1, NULL, 3),
(872, '061103', 'Calquis', NULL, NULL, 4429, '336.00', 89, 1, NULL, 3),
(873, '061104', 'Catilluc', NULL, NULL, 3486, '203.00', 89, 1, NULL, 3),
(874, '061105', 'El Prado', NULL, NULL, 1402, '70.00', 89, 1, NULL, 3),
(875, '061106', 'La Florida', NULL, NULL, 2205, '58.00', 89, 1, NULL, 3),
(876, '061107', 'Llapa', NULL, NULL, 6035, '141.00', 89, 1, NULL, 3),
(877, '061108', 'Nanchoc', NULL, NULL, 1538, '373.00', 89, 1, NULL, 3),
(878, '061109', 'Niepos', NULL, NULL, 4058, '154.00', 89, 1, NULL, 3),
(879, '061110', 'San Gregorio', NULL, NULL, 2293, '310.00', 89, 1, NULL, 3),
(880, '061111', 'San Silvestre De Cochan', NULL, NULL, 4475, '135.00', 89, 1, NULL, 3),
(881, '061112', 'Tongod', NULL, NULL, 4857, '156.00', 89, 1, NULL, 3),
(882, '061113', 'Union Agua Blanca', NULL, NULL, 3594, '171.00', 89, 1, NULL, 3),
(883, '061201', 'San Pablo', NULL, NULL, 13591, '200.00', 90, 1, NULL, 3),
(884, '061202', 'San Bernardino', NULL, NULL, 4827, '166.00', 90, 1, NULL, 3),
(885, '061203', 'San Luis', NULL, NULL, 1276, '43.00', 90, 1, NULL, 3),
(886, '061204', 'Tumbaden', NULL, NULL, 3604, '257.00', 90, 1, NULL, 3),
(887, '061301', 'Santa Cruz', NULL, NULL, 12250, '104.00', 91, 1, NULL, 3),
(888, '061302', 'Andabamba', NULL, NULL, 1527, '8.00', 91, 1, NULL, 3),
(889, '061303', 'Catache', NULL, NULL, 10010, '567.00', 91, 1, NULL, 3),
(890, '061304', 'Chancaybaños', NULL, NULL, 3905, '126.00', 91, 1, NULL, 3),
(891, '061305', 'La Esperanza', NULL, NULL, 2601, '59.00', 91, 1, NULL, 3),
(892, '061306', 'Ninabamba', NULL, NULL, 2791, '57.00', 91, 1, NULL, 3),
(893, '061307', 'Pulan', NULL, NULL, 4492, '159.00', 91, 1, NULL, 3),
(894, '061308', 'Saucepampa', NULL, NULL, 1871, '31.00', 91, 1, NULL, 3),
(895, '061309', 'Sexi', NULL, NULL, 566, '192.00', 91, 1, NULL, 3),
(896, '061310', 'Uticyacu', NULL, NULL, 1614, '42.00', 91, 1, NULL, 3),
(897, '061311', 'Yauyucan', NULL, NULL, 3595, '38.00', 91, 1, NULL, 3),
(898, '070101', 'Callao', NULL, NULL, 406889, '50.00', 92, 1, NULL, 3),
(899, '070102', 'Bellavista', NULL, NULL, 71833, '5.00', 92, 1, NULL, 3),
(900, '070103', 'Carmen De La Legua Y Reynoso', NULL, NULL, 41100, '2.00', 92, 1, NULL, 3),
(901, '070104', 'La Perla', NULL, NULL, 58817, '3.00', 92, 1, NULL, 3),
(902, '070105', 'La Punta', NULL, NULL, 3392, '1.00', 92, 1, NULL, 3),
(903, '070106', 'Ventanilla', NULL, NULL, 428284, '77.00', 92, 1, NULL, 3),
(904, '070107', 'Mi Perú', NULL, NULL, 60977, '3.00', 92, 1, NULL, 3),
(905, '080101', 'Cusco', NULL, NULL, 118316, '101.00', 93, 1, NULL, 3),
(906, '080102', 'Ccorca', NULL, NULL, 2235, '162.00', 93, 1, NULL, 3),
(907, '080103', 'Poroy', NULL, NULL, 7817, '13.00', 93, 1, NULL, 3),
(908, '080104', 'San Jeronimo', NULL, NULL, 47101, '89.00', 93, 1, NULL, 3),
(909, '080105', 'San Sebastian', NULL, NULL, 115305, '77.00', 93, 1, NULL, 3),
(910, '080106', 'Santiago', NULL, NULL, 90154, '59.00', 93, 1, NULL, 3),
(911, '080107', 'Saylla', NULL, NULL, 5389, '24.00', 93, 1, NULL, 3),
(912, '080108', 'Wanchaq', NULL, NULL, 63778, '5.00', 93, 1, NULL, 3),
(913, '080201', 'Acomayo', NULL, NULL, 5552, '143.00', 94, 1, NULL, 3),
(914, '080202', 'Acopia', NULL, NULL, 2379, '70.00', 94, 1, NULL, 3),
(915, '080203', 'Acos', NULL, NULL, 2338, '136.00', 94, 1, NULL, 3),
(916, '080204', 'Mosoc Llacta', NULL, NULL, 2287, '44.00', 94, 1, NULL, 3),
(917, '080205', 'Pomacanchi', NULL, NULL, 9020, '268.00', 94, 1, NULL, 3),
(918, '080206', 'Rondocan', NULL, NULL, 2379, '185.00', 94, 1, NULL, 3),
(919, '080207', 'Sangarara', NULL, NULL, 3738, '86.00', 94, 1, NULL, 3),
(920, '080301', 'Anta', NULL, NULL, 16703, '187.00', 95, 1, NULL, 3),
(921, '080302', 'Ancahuasi', NULL, NULL, 6947, '124.00', 95, 1, NULL, 3),
(922, '080303', 'Cachimayo', NULL, NULL, 2285, '43.00', 95, 1, NULL, 3),
(923, '080304', 'Chinchaypujio', NULL, NULL, 4303, '395.00', 95, 1, NULL, 3),
(924, '080305', 'Huarocondo', NULL, NULL, 5762, '220.00', 95, 1, NULL, 3),
(925, '080306', 'Limatambo', NULL, NULL, 9801, '513.00', 95, 1, NULL, 3),
(926, '080307', 'Mollepata', NULL, NULL, 2600, '356.00', 95, 1, NULL, 3),
(927, '080308', 'Pucyura', NULL, NULL, 4258, '34.00', 95, 1, NULL, 3),
(928, '080309', 'Zurite', NULL, NULL, 3643, '60.00', 95, 1, NULL, 3),
(929, '080401', 'Calca', NULL, NULL, 23316, '336.00', 96, 1, NULL, 3),
(930, '080402', 'Coya', NULL, NULL, 4026, '71.00', 96, 1, NULL, 3),
(931, '080403', 'Lamay', NULL, NULL, 5768, '96.00', 96, 1, NULL, 3),
(932, '080404', 'Lares', NULL, NULL, 7210, '743.00', 96, 1, NULL, 3),
(933, '080405', 'Pisac', NULL, NULL, 10188, '147.00', 96, 1, NULL, 3),
(934, '080406', 'San Salvador', NULL, NULL, 5622, '128.00', 96, 1, NULL, 3),
(935, '080407', 'Taray', NULL, NULL, 4728, '54.00', 96, 1, NULL, 3),
(936, '080408', 'Yanatile', NULL, NULL, 13337, '1992.00', 96, 1, NULL, 3),
(937, '080501', 'Yanaoca', NULL, NULL, 9976, '288.00', 97, 1, NULL, 3),
(938, '080502', 'Checca', NULL, NULL, 6302, '505.00', 97, 1, NULL, 3),
(939, '080503', 'Kunturkanki', NULL, NULL, 5738, '393.00', 97, 1, NULL, 3),
(940, '080504', 'Langui', NULL, NULL, 2467, '170.00', 97, 1, NULL, 3),
(941, '080505', 'Layo', NULL, NULL, 6333, '481.00', 97, 1, NULL, 3),
(942, '080506', 'Pampamarca', NULL, NULL, 2003, '31.00', 97, 1, NULL, 3),
(943, '080507', 'Quehue', NULL, NULL, 3606, '148.00', 97, 1, NULL, 3),
(944, '080508', 'Tupac Amaru', NULL, NULL, 2868, '117.00', 97, 1, NULL, 3),
(945, '080601', 'Sicuani', NULL, NULL, 59894, '646.00', 98, 1, NULL, 3),
(946, '080602', 'Checacupe', NULL, NULL, 5000, '938.00', 98, 1, NULL, 3),
(947, '080603', 'Combapata', NULL, NULL, 5394, '173.00', 98, 1, NULL, 3),
(948, '080604', 'Marangani', NULL, NULL, 11247, '439.00', 98, 1, NULL, 3),
(949, '080605', 'Pitumarca', NULL, NULL, 7506, '1092.00', 98, 1, NULL, 3),
(950, '080606', 'San Pablo', NULL, NULL, 4680, '524.00', 98, 1, NULL, 3),
(951, '080607', 'San Pedro', NULL, NULL, 2804, '56.00', 98, 1, NULL, 3),
(952, '080608', 'Tinta', NULL, NULL, 5626, '82.00', 98, 1, NULL, 3),
(953, '080701', 'Santo Tomas', NULL, NULL, 26564, '1899.00', 99, 1, NULL, 3),
(954, '080702', 'Capacmarca', NULL, NULL, 4596, '268.00', 99, 1, NULL, 3),
(955, '080703', 'Chamaca', NULL, NULL, 8864, '672.00', 99, 1, NULL, 3),
(956, '080704', 'Colquemarca', NULL, NULL, 8579, '451.00', 99, 1, NULL, 3),
(957, '080705', 'Livitaca', NULL, NULL, 13357, '749.00', 99, 1, NULL, 3),
(958, '080706', 'Llusco', NULL, NULL, 7064, '312.00', 99, 1, NULL, 3),
(959, '080707', 'Quiñota', NULL, NULL, 4895, '224.00', 99, 1, NULL, 3),
(960, '080708', 'Velille', NULL, NULL, 8492, '761.00', 99, 1, NULL, 3),
(961, '080801', 'Espinar', NULL, NULL, 33242, '736.00', 100, 1, NULL, 3),
(962, '080802', 'Condoroma', NULL, NULL, 1400, '516.00', 100, 1, NULL, 3),
(963, '080803', 'Coporaque', NULL, NULL, 17846, '1544.00', 100, 1, NULL, 3),
(964, '080804', 'Ocoruro', NULL, NULL, 1606, '359.00', 100, 1, NULL, 3),
(965, '080805', 'Pallpata', NULL, NULL, 5542, '818.00', 100, 1, NULL, 3),
(966, '080806', 'Pichigua', NULL, NULL, 3603, '274.00', 100, 1, NULL, 3),
(967, '080807', 'Suyckutambo', NULL, NULL, 2768, '630.00', 100, 1, NULL, 3),
(968, '080808', 'Alto Pichigua', NULL, NULL, 3139, '359.00', 100, 1, NULL, 3),
(969, '080901', 'Santa Ana', NULL, NULL, 34434, '375.00', 101, 1, NULL, 3),
(970, '080902', 'Echarate', NULL, NULL, 44983, '22178.00', 101, 1, NULL, 3),
(971, '080903', 'Huayopata', NULL, NULL, 4698, '542.00', 101, 1, NULL, 3),
(972, '080904', 'Maranura', NULL, NULL, 6058, '177.00', 101, 1, NULL, 3),
(973, '080905', 'Ocobamba', NULL, NULL, 6767, '850.00', 101, 1, NULL, 3),
(974, '080906', 'Quellouno', NULL, NULL, 18089, '1731.00', 101, 1, NULL, 3),
(975, '080907', 'Kimbiri', NULL, NULL, 16865, '987.00', 101, 1, NULL, 3),
(976, '080908', 'Santa Teresa', NULL, NULL, 6476, '1335.00', 101, 1, NULL, 3),
(977, '080909', 'Vilcabamba', NULL, NULL, 21159, '2958.00', 101, 1, NULL, 3),
(978, '080910', 'Pichari', NULL, NULL, 20316, '832.00', 101, 1, NULL, 3),
(979, '081001', 'Paruro', NULL, NULL, 3338, '152.00', 102, 1, NULL, 3),
(980, '081002', 'Accha', NULL, NULL, 3787, '239.00', 102, 1, NULL, 3),
(981, '081003', 'Ccapi', NULL, NULL, 3682, '326.00', 102, 1, NULL, 3),
(982, '081004', 'Colcha', NULL, NULL, 1138, '141.00', 102, 1, NULL, 3),
(983, '081005', 'Huanoquite', NULL, NULL, 5648, '362.00', 102, 1, NULL, 3),
(984, '081006', 'Omacha', NULL, NULL, 7203, '427.00', 102, 1, NULL, 3),
(985, '081007', 'Paccaritambo', NULL, NULL, 2012, '119.00', 102, 1, NULL, 3),
(986, '081008', 'Pillpinto', NULL, NULL, 1220, '78.00', 102, 1, NULL, 3),
(987, '081009', 'Yaurisque', NULL, NULL, 2473, '124.00', 102, 1, NULL, 3),
(988, '081101', 'Paucartambo', NULL, NULL, 13190, '792.00', 103, 1, NULL, 3),
(989, '081102', 'Caicay', NULL, NULL, 2701, '107.00', 103, 1, NULL, 3),
(990, '081103', 'Challabamba', NULL, NULL, 11264, '725.00', 103, 1, NULL, 3),
(991, '081104', 'Colquepata', NULL, NULL, 10662, '463.00', 103, 1, NULL, 3),
(992, '081105', 'Huancarani', NULL, NULL, 7634, '143.00', 103, 1, NULL, 3),
(993, '081106', 'Kosñipata', NULL, NULL, 5609, '3670.00', 103, 1, NULL, 3),
(994, '081201', 'Urcos', NULL, NULL, 9254, '141.00', 104, 1, NULL, 3),
(995, '081202', 'Andahuaylillas', NULL, NULL, 5465, '85.00', 104, 1, NULL, 3),
(996, '081203', 'Camanti', NULL, NULL, 2094, '3183.00', 104, 1, NULL, 3),
(997, '081204', 'Ccarhuayo', NULL, NULL, 3129, '297.00', 104, 1, NULL, 3),
(998, '081205', 'Ccatca', NULL, NULL, 17944, '296.00', 104, 1, NULL, 3),
(999, '081206', 'Cusipata', NULL, NULL, 4770, '244.00', 104, 1, NULL, 3),
(1000, '081207', 'Huaro', NULL, NULL, 4491, '107.00', 104, 1, NULL, 3),
(1001, '081208', 'Lucre', NULL, NULL, 4000, '119.00', 104, 1, NULL, 3),
(1002, '081209', 'Marcapata', NULL, NULL, 4514, '1579.00', 104, 1, NULL, 3),
(1003, '081210', 'Ocongate', NULL, NULL, 15614, '948.00', 104, 1, NULL, 3),
(1004, '081211', 'Oropesa', NULL, NULL, 7280, '78.00', 104, 1, NULL, 3),
(1005, '081212', 'Quiquijana', NULL, NULL, 10962, '364.00', 104, 1, NULL, 3),
(1006, '081301', 'Urubamba', NULL, NULL, 20879, '162.00', 105, 1, NULL, 3),
(1007, '081302', 'Chinchero', NULL, NULL, 9763, '102.00', 105, 1, NULL, 3),
(1008, '081303', 'Huayllabamba', NULL, NULL, 5228, '74.00', 105, 1, NULL, 3),
(1009, '081304', 'Machupicchu', NULL, NULL, 8332, '374.00', 105, 1, NULL, 3),
(1010, '081305', 'Maras', NULL, NULL, 5794, '142.00', 105, 1, NULL, 3),
(1011, '081306', 'Ollantaytambo', NULL, NULL, 11225, '583.00', 105, 1, NULL, 3),
(1012, '081307', 'Yucay', NULL, NULL, 3299, '24.00', 105, 1, NULL, 3),
(1013, '090101', 'Huancavelica', NULL, NULL, 40345, '508.00', 106, 1, NULL, 3),
(1014, '090102', 'Acobambilla', NULL, NULL, 4593, '747.00', 106, 1, NULL, 3),
(1015, '090103', 'Acoria', NULL, NULL, 36373, '533.00', 106, 1, NULL, 3),
(1016, '090104', 'Conayca', NULL, NULL, 1219, '42.00', 106, 1, NULL, 3),
(1017, '090105', 'Cuenca', NULL, NULL, 1974, '55.00', 106, 1, NULL, 3),
(1018, '090106', 'Huachocolpa', NULL, NULL, 2883, '337.00', 106, 1, NULL, 3),
(1019, '090107', 'Huayllahuara', NULL, NULL, 752, '37.00', 106, 1, NULL, 3),
(1020, '090108', 'Izcuchaca', NULL, NULL, 879, '12.00', 106, 1, NULL, 3),
(1021, '090109', 'Laria', NULL, NULL, 1440, '64.00', 106, 1, NULL, 3),
(1022, '090110', 'Manta', NULL, NULL, 1848, '159.00', 106, 1, NULL, 3),
(1023, '090111', 'Mariscal Caceres', NULL, NULL, 1017, '10.00', 106, 1, NULL, 3),
(1024, '090112', 'Moya', NULL, NULL, 2479, '95.00', 106, 1, NULL, 3),
(1025, '090113', 'Nuevo Occoro', NULL, NULL, 2676, '218.00', 106, 1, NULL, 3),
(1026, '090114', 'Palca', NULL, NULL, 3225, '81.00', 106, 1, NULL, 3),
(1027, '090115', 'Pilchaca', NULL, NULL, 510, '40.00', 106, 1, NULL, 3),
(1028, '090116', 'Vilca', NULL, NULL, 3051, '309.00', 106, 1, NULL, 3),
(1029, '090117', 'Yauli', NULL, NULL, 33440, '320.00', 106, 1, NULL, 3),
(1030, '090118', 'Ascension', NULL, NULL, 12252, '428.00', 106, 1, NULL, 3),
(1031, '090119', 'Huando', NULL, NULL, 7638, '196.00', 106, 1, NULL, 3),
(1032, '090201', 'Acobamba', NULL, NULL, 10058, '124.00', 107, 1, NULL, 3),
(1033, '090202', 'Andabamba', NULL, NULL, 5569, '82.00', 107, 1, NULL, 3),
(1034, '090203', 'Anta', NULL, NULL, 9407, '92.00', 107, 1, NULL, 3),
(1035, '090204', 'Caja', NULL, NULL, 2818, '86.00', 107, 1, NULL, 3),
(1036, '090205', 'Marcas', NULL, NULL, 2381, '171.00', 107, 1, NULL, 3),
(1037, '090206', 'Paucara', NULL, NULL, 36713, '220.00', 107, 1, NULL, 3),
(1038, '090207', 'Pomacocha', NULL, NULL, 3936, '55.00', 107, 1, NULL, 3),
(1039, '090208', 'Rosario', NULL, NULL, 7752, '97.00', 107, 1, NULL, 3),
(1040, '090301', 'Lircay', NULL, NULL, 24699, '830.00', 108, 1, NULL, 3),
(1041, '090302', 'Anchonga', NULL, NULL, 7966, '75.00', 108, 1, NULL, 3),
(1042, '090303', 'Callanmarca', NULL, NULL, 764, '25.00', 108, 1, NULL, 3),
(1043, '090304', 'Ccochaccasa', NULL, NULL, 2737, '107.00', 108, 1, NULL, 3),
(1044, '090305', 'Chincho', NULL, NULL, 3438, '173.00', 108, 1, NULL, 3),
(1045, '090306', 'Congalla', NULL, NULL, 4109, '210.00', 108, 1, NULL, 3),
(1046, '090307', 'Huanca-huanca', NULL, NULL, 1746, '108.00', 108, 1, NULL, 3),
(1047, '090308', 'Huayllay Grande', NULL, NULL, 2168, '32.00', 108, 1, NULL, 3),
(1048, '090309', 'Julcamarca', NULL, NULL, 1753, '51.00', 108, 1, NULL, 3),
(1049, '090310', 'San Antonio De Antaparco', NULL, NULL, 7514, '34.00', 108, 1, NULL, 3),
(1050, '090311', 'Santo Tomas De Pata', NULL, NULL, 2610, '139.00', 108, 1, NULL, 3),
(1051, '090312', 'Secclla', NULL, NULL, 3751, '162.00', 108, 1, NULL, 3),
(1052, '090401', 'Castrovirreyna', NULL, NULL, 3248, '921.00', 109, 1, NULL, 3),
(1053, '090402', 'Arma', NULL, NULL, 1436, '301.00', 109, 1, NULL, 3),
(1054, '090403', 'Aurahua', NULL, NULL, 2234, '345.00', 109, 1, NULL, 3),
(1055, '090404', 'Capillas', NULL, NULL, 1440, '400.00', 109, 1, NULL, 3),
(1056, '090405', 'Chupamarca', NULL, NULL, 1207, '371.00', 109, 1, NULL, 3),
(1057, '090406', 'Cocas', NULL, NULL, 912, '81.00', 109, 1, NULL, 3),
(1058, '090407', 'Huachos', NULL, NULL, 1676, '175.00', 109, 1, NULL, 3),
(1059, '090408', 'Huamatambo', NULL, NULL, 388, '59.00', 109, 1, NULL, 3),
(1060, '090409', 'Mollepampa', NULL, NULL, 1659, '170.00', 109, 1, NULL, 3),
(1061, '090410', 'San Juan', NULL, NULL, 473, '197.00', 109, 1, NULL, 3),
(1062, '090411', 'Santa Ana', NULL, NULL, 2157, '646.00', 109, 1, NULL, 3),
(1063, '090412', 'Tantara', NULL, NULL, 722, '111.00', 109, 1, NULL, 3),
(1064, '090413', 'Ticrapo', NULL, NULL, 1617, '182.00', 109, 1, NULL, 3),
(1065, '090501', 'Churcampa', NULL, NULL, 5479, '133.00', 110, 1, NULL, 3),
(1066, '090502', 'Anco', NULL, NULL, 11420, '270.00', 110, 1, NULL, 3),
(1067, '090503', 'Chinchihuasi', NULL, NULL, 4786, '166.00', 110, 1, NULL, 3),
(1068, '090504', 'El Carmen', NULL, NULL, 2998, '76.00', 110, 1, NULL, 3),
(1069, '090505', 'La Merced', NULL, NULL, 1834, '72.00', 110, 1, NULL, 3),
(1070, '090506', 'Locroja', NULL, NULL, 4048, '81.00', 110, 1, NULL, 3),
(1071, '090507', 'Paucarbamba', NULL, NULL, 7232, '103.00', 110, 1, NULL, 3),
(1072, '090508', 'San Miguel De Mayocc', NULL, NULL, 1261, '26.00', 110, 1, NULL, 3),
(1073, '090509', 'San Pedro De Coris', NULL, NULL, 2823, '137.00', 110, 1, NULL, 3),
(1074, '090510', 'Pachamarca', NULL, NULL, 2701, '157.00', 110, 1, NULL, 3),
(1075, '090601', 'Huaytara', NULL, NULL, 2100, '393.00', 111, 1, NULL, 3),
(1076, '090602', 'Ayavi', NULL, NULL, 617, '180.00', 111, 1, NULL, 3),
(1077, '090603', 'Cordova', NULL, NULL, 2826, '103.00', 111, 1, NULL, 3),
(1078, '090604', 'Huayacundo Arma', NULL, NULL, 467, '38.00', 111, 1, NULL, 3),
(1079, '090605', 'Laramarca', NULL, NULL, 850, '208.00', 111, 1, NULL, 3),
(1080, '090606', 'Ocoyo', NULL, NULL, 2436, '154.00', 111, 1, NULL, 3),
(1081, '090607', 'Pilpichaca', NULL, NULL, 3688, '2176.00', 111, 1, NULL, 3),
(1082, '090608', 'Querco', NULL, NULL, 1006, '682.00', 111, 1, NULL, 3),
(1083, '090609', 'Quito-arma', NULL, NULL, 775, '238.00', 111, 1, NULL, 3),
(1084, '090610', 'San Antonio De Cusicancha', NULL, NULL, 1658, '240.00', 111, 1, NULL, 3),
(1085, '090611', 'San Francisco De Sangayaico', NULL, NULL, 580, '78.00', 111, 1, NULL, 3),
(1086, '090612', 'San Isidro', NULL, NULL, 1171, '169.00', 111, 1, NULL, 3),
(1087, '090613', 'Santiago De Chocorvos', NULL, NULL, 2887, '1146.00', 111, 1, NULL, 3),
(1088, '090614', 'Santiago De Quirahuara', NULL, NULL, 655, '177.00', 111, 1, NULL, 3),
(1089, '090615', 'Santo Domingo De Capillas', NULL, NULL, 983, '257.00', 111, 1, NULL, 3),
(1090, '090616', 'Tambo', NULL, NULL, 322, '227.00', 111, 1, NULL, 3),
(1091, '090701', 'Pampas', NULL, NULL, 11166, '109.00', 112, 1, NULL, 3),
(1092, '090702', 'Acostambo', NULL, NULL, 4131, '162.00', 112, 1, NULL, 3),
(1093, '090703', 'Acraquia', NULL, NULL, 4984, '109.00', 112, 1, NULL, 3),
(1094, '090704', 'Ahuaycha', NULL, NULL, 5497, '105.00', 112, 1, NULL, 3),
(1095, '090705', 'Colcabamba', NULL, NULL, 18802, '579.00', 112, 1, NULL, 3),
(1096, '090706', 'Daniel Hernandez', NULL, NULL, 10243, '105.00', 112, 1, NULL, 3),
(1097, '090707', 'Huachocolpa', NULL, NULL, 6286, '278.00', 112, 1, NULL, 3),
(1098, '090709', 'Huaribamba', NULL, NULL, 7850, '364.00', 112, 1, NULL, 3),
(1099, '090710', 'Ñahuimpuquio', NULL, NULL, 1904, '67.00', 112, 1, NULL, 3),
(1100, '090711', 'Pazos', NULL, NULL, 7230, '155.00', 112, 1, NULL, 3),
(1101, '090713', 'Quishuar', NULL, NULL, 899, '33.00', 112, 1, NULL, 3),
(1102, '090714', 'Salcabamba', NULL, NULL, 4619, '189.00', 112, 1, NULL, 3),
(1103, '090715', 'Salcahuasi', NULL, NULL, 3345, '117.00', 112, 1, NULL, 3),
(1104, '090716', 'San Marcos De Rocchac', NULL, NULL, 2880, '288.00', 112, 1, NULL, 3),
(1105, '090717', 'Surcubamba', NULL, NULL, 4897, '271.00', 112, 1, NULL, 3),
(1106, '090718', 'Tintay Puncu', NULL, NULL, 12975, '448.00', 112, 1, NULL, 3),
(1107, '100101', 'Huanuco', NULL, NULL, 76065, '98.00', 113, 1, NULL, 3),
(1108, '100102', 'Amarilis', NULL, NULL, 70286, '137.00', 113, 1, NULL, 3),
(1109, '100103', 'Chinchao', NULL, NULL, 25961, '1839.00', 113, 1, NULL, 3),
(1110, '100104', 'Churubamba', NULL, NULL, 29599, '558.00', 113, 1, NULL, 3),
(1111, '100105', 'Margos', NULL, NULL, 15029, '286.00', 113, 1, NULL, 3),
(1112, '100106', 'Quisqui', NULL, NULL, 7978, '160.00', 113, 1, NULL, 3),
(1113, '100107', 'San Francisco De Cayran', NULL, NULL, 5141, '99.00', 113, 1, NULL, 3),
(1114, '100108', 'San Pedro De Chaulan', NULL, NULL, 7745, '279.00', 113, 1, NULL, 3),
(1115, '100109', 'Santa Maria Del Valle', NULL, NULL, 18248, '490.00', 113, 1, NULL, 3),
(1116, '100110', 'Yarumayo', NULL, NULL, 2881, '63.00', 113, 1, NULL, 3),
(1117, '100111', 'Pillco Marca', NULL, NULL, 51515, '69.00', 113, 1, NULL, 3),
(1118, '100201', 'Ambo', NULL, NULL, 17138, '288.00', 114, 1, NULL, 3),
(1119, '100202', 'Cayna', NULL, NULL, 3528, '156.00', 114, 1, NULL, 3),
(1120, '100203', 'Colpas', NULL, NULL, 2513, '185.00', 114, 1, NULL, 3),
(1121, '100204', 'Conchamarca', NULL, NULL, 6822, '108.00', 114, 1, NULL, 3),
(1122, '100205', 'Huacar', NULL, NULL, 7643, '238.00', 114, 1, NULL, 3),
(1123, '100206', 'San Francisco', NULL, NULL, 3327, '118.00', 114, 1, NULL, 3),
(1124, '100207', 'San Rafael', NULL, NULL, 12186, '441.00', 114, 1, NULL, 3),
(1125, '100208', 'Tomay Kichwa', NULL, NULL, 4082, '43.00', 114, 1, NULL, 3),
(1126, '100301', 'La Union', NULL, NULL, 6332, '169.00', 115, 1, NULL, 3),
(1127, '100307', 'Chuquis', NULL, NULL, 5894, '153.00', 115, 1, NULL, 3),
(1128, '100311', 'Marias', NULL, NULL, 9538, '616.00', 115, 1, NULL, 3),
(1129, '100313', 'Pachas', NULL, NULL, 13039, '268.00', 115, 1, NULL, 3),
(1130, '100316', 'Quivilla', NULL, NULL, 3035, '34.00', 115, 1, NULL, 3),
(1131, '100317', 'Ripan', NULL, NULL, 7050, '76.00', 115, 1, NULL, 3),
(1132, '100321', 'Shunqui', NULL, NULL, 2520, '33.00', 115, 1, NULL, 3),
(1133, '100322', 'Sillapata', NULL, NULL, 2549, '71.00', 115, 1, NULL, 3),
(1134, '100323', 'Yanas', NULL, NULL, 3367, '37.00', 115, 1, NULL, 3),
(1135, '100401', 'Huacaybamba', NULL, NULL, 7245, '562.00', 116, 1, NULL, 3),
(1136, '100402', 'Canchabamba', NULL, NULL, 3301, '186.00', 116, 1, NULL, 3),
(1137, '100403', 'Cochabamba', NULL, NULL, 3478, '722.00', 116, 1, NULL, 3),
(1138, '100404', 'Pinra', NULL, NULL, 8819, '295.00', 116, 1, NULL, 3),
(1139, '100501', 'Llata', NULL, NULL, 15059, '412.00', 117, 1, NULL, 3),
(1140, '100502', 'Arancay', NULL, NULL, 1513, '118.00', 117, 1, NULL, 3),
(1141, '100503', 'Chavin De Pariarca', NULL, NULL, 3846, '92.00', 117, 1, NULL, 3),
(1142, '100504', 'Jacas Grande', NULL, NULL, 5916, '223.00', 117, 1, NULL, 3),
(1143, '100505', 'Jircan', NULL, NULL, 3569, '251.00', 117, 1, NULL, 3),
(1144, '100506', 'Miraflores', NULL, NULL, 3565, '97.00', 117, 1, NULL, 3),
(1145, '100507', 'Monzon', NULL, NULL, 28605, '1397.00', 117, 1, NULL, 3),
(1146, '100508', 'Punchao', NULL, NULL, 2536, '40.00', 117, 1, NULL, 3),
(1147, '100509', 'Puños', NULL, NULL, 4412, '97.00', 117, 1, NULL, 3),
(1148, '100510', 'Singa', NULL, NULL, 3482, '162.00', 117, 1, NULL, 3),
(1149, '100511', 'Tantamayo', NULL, NULL, 3002, '279.00', 117, 1, NULL, 3),
(1150, '100601', 'Rupa-rupa', NULL, NULL, 63764, '369.00', 118, 1, NULL, 3),
(1151, '100602', 'Daniel Alomias Robles', NULL, NULL, 7775, '780.00', 118, 1, NULL, 3),
(1152, '100603', 'Hermilio Valdizan', NULL, NULL, 4101, '129.00', 118, 1, NULL, 3),
(1153, '100604', 'Jose Crespo Y Castillo', NULL, NULL, 38423, '2791.00', 118, 1, NULL, 3),
(1154, '100605', 'Luyando', NULL, NULL, 9851, '113.00', 118, 1, NULL, 3),
(1155, '100606', 'Mariano Damaso Beraun', NULL, NULL, 9586, '742.00', 118, 1, NULL, 3),
(1156, '100701', 'Huacrachuco', NULL, NULL, 15793, '703.00', 119, 1, NULL, 3),
(1157, '100702', 'Cholon', NULL, NULL, 13643, '4087.00', 119, 1, NULL, 3),
(1158, '100703', 'San Buenaventura', NULL, NULL, 2682, '89.00', 119, 1, NULL, 3),
(1159, '100801', 'Panao', NULL, NULL, 24223, '1614.00', 120, 1, NULL, 3),
(1160, '100802', 'Chaglla', NULL, NULL, 11768, '638.00', 120, 1, NULL, 3),
(1161, '100803', 'Molino', NULL, NULL, 14840, '238.00', 120, 1, NULL, 3),
(1162, '100804', 'Umari', NULL, NULL, 21398, '157.00', 120, 1, NULL, 3),
(1163, '100901', 'Puerto Inca', NULL, NULL, 7784, '2227.00', 121, 1, NULL, 3),
(1164, '100902', 'Codo Del Pozuzo', NULL, NULL, 6603, '3222.00', 121, 1, NULL, 3),
(1165, '100903', 'Honoria', NULL, NULL, 6303, '813.00', 121, 1, NULL, 3),
(1166, '100904', 'Tournavista', NULL, NULL, 4585, '1621.00', 121, 1, NULL, 3),
(1167, '100905', 'Yuyapichis', NULL, NULL, 6154, '1932.00', 121, 1, NULL, 3),
(1168, '101001', 'Jesus', NULL, NULL, 5786, '456.00', 122, 1, NULL, 3),
(1169, '101002', 'Baños', NULL, NULL, 7035, '193.00', 122, 1, NULL, 3),
(1170, '101003', 'Jivia', NULL, NULL, 2795, '62.00', 122, 1, NULL, 3),
(1171, '101004', 'Queropalca', NULL, NULL, 2944, '133.00', 122, 1, NULL, 3),
(1172, '101005', 'Rondos', NULL, NULL, 7648, '172.00', 122, 1, NULL, 3),
(1173, '101006', 'San Francisco De Asis', NULL, NULL, 2173, '85.00', 122, 1, NULL, 3),
(1174, '101007', 'San Miguel De Cauri', NULL, NULL, 10286, '822.00', 122, 1, NULL, 3),
(1175, '101101', 'Chavinillo', NULL, NULL, 5992, '208.00', 123, 1, NULL, 3),
(1176, '101102', 'Cahuac', NULL, NULL, 4635, '30.00', 123, 1, NULL, 3),
(1177, '101103', 'Chacabamba', NULL, NULL, 3679, '17.00', 123, 1, NULL, 3),
(1178, '101104', 'Aparicio Pomares', NULL, NULL, 5594, '187.00', 123, 1, NULL, 3),
(1179, '101105', 'Jacas Chico', NULL, NULL, 2045, '69.00', 123, 1, NULL, 3),
(1180, '101106', 'Obas', NULL, NULL, 5528, '125.00', 123, 1, NULL, 3),
(1181, '101107', 'Pampamarca', NULL, NULL, 2071, '74.00', 123, 1, NULL, 3),
(1182, '101108', 'Choras', NULL, NULL, 3691, '62.00', 123, 1, NULL, 3),
(1183, '110101', 'Ica', NULL, NULL, 131003, '886.00', 124, 1, NULL, 3),
(1184, '110102', 'La Tinguiña', NULL, NULL, 35641, '81.00', 124, 1, NULL, 3),
(1185, '110103', 'Los Aquijes', NULL, NULL, 19259, '92.00', 124, 1, NULL, 3),
(1186, '110104', 'Ocucaje', NULL, NULL, 3745, '1402.00', 124, 1, NULL, 3),
(1187, '110105', 'Pachacutec', NULL, NULL, 6729, '41.00', 124, 1, NULL, 3),
(1188, '110106', 'Parcona', NULL, NULL, 54747, '18.00', 124, 1, NULL, 3),
(1189, '110107', 'Pueblo Nuevo', NULL, NULL, 4784, '29.00', 124, 1, NULL, 3),
(1190, '110108', 'Salas', NULL, NULL, 23504, '657.00', 124, 1, NULL, 3),
(1191, '110109', 'San Jose De Los Molinos', NULL, NULL, 6235, '362.00', 124, 1, NULL, 3),
(1192, '110110', 'San Juan Bautista', NULL, NULL, 14663, '31.00', 124, 1, NULL, 3),
(1193, '110111', 'Santiago', NULL, NULL, 29117, '2772.00', 124, 1, NULL, 3),
(1194, '110112', 'Subtanjalla', NULL, NULL, 27706, '186.00', 124, 1, NULL, 3),
(1195, '110113', 'Tate', NULL, NULL, 4574, '7.00', 124, 1, NULL, 3),
(1196, '110114', 'Yauca Del Rosario', NULL, NULL, 986, '1263.00', 124, 1, NULL, 3),
(1197, '110201', 'Chincha Alta', NULL, NULL, 63671, '226.00', 125, 1, NULL, 3),
(1198, '110202', 'Alto Laran', NULL, NULL, 7387, '364.00', 125, 1, NULL, 3),
(1199, '110203', 'Chavin', NULL, NULL, 1417, '426.00', 125, 1, NULL, 3),
(1200, '110204', 'Chincha Baja', NULL, NULL, 12323, '73.00', 125, 1, NULL, 3),
(1201, '110205', 'El Carmen', NULL, NULL, 13296, '752.00', 125, 1, NULL, 3),
(1202, '110206', 'Grocio Prado', NULL, NULL, 24049, '177.00', 125, 1, NULL, 3),
(1203, '110207', 'Pueblo Nuevo', NULL, NULL, 61078, '211.00', 125, 1, NULL, 3),
(1204, '110208', 'San Juan De Yanac', NULL, NULL, 316, '439.00', 125, 1, NULL, 3),
(1205, '110209', 'San Pedro De Huacarpana', NULL, NULL, 1660, '206.00', 125, 1, NULL, 3),
(1206, '110210', 'Sunampe', NULL, NULL, 27496, '16.00', 125, 1, NULL, 3),
(1207, '110211', 'Tambo De Mora', NULL, NULL, 4990, '11.00', 125, 1, NULL, 3),
(1208, '110301', 'Nazca', NULL, NULL, 26719, '1144.00', 126, 1, NULL, 3),
(1209, '110302', 'Changuillo', NULL, NULL, 1537, '957.00', 126, 1, NULL, 3),
(1210, '110303', 'El Ingenio', NULL, NULL, 2702, '612.00', 126, 1, NULL, 3),
(1211, '110304', 'Marcona', NULL, NULL, 12403, '1945.00', 126, 1, NULL, 3),
(1212, '110305', 'Vista Alegre', NULL, NULL, 15419, '534.00', 126, 1, NULL, 3),
(1213, '110401', 'Palpa', NULL, NULL, 7195, '132.00', 127, 1, NULL, 3),
(1214, '110402', 'Llipata', NULL, NULL, 1497, '182.00', 127, 1, NULL, 3),
(1215, '110403', 'Rio Grande', NULL, NULL, 2268, '315.00', 127, 1, NULL, 3),
(1216, '110404', 'Santa Cruz', NULL, NULL, 988, '254.00', 127, 1, NULL, 3),
(1217, '110405', 'Tibillo', NULL, NULL, 331, '337.00', 127, 1, NULL, 3),
(1218, '110501', 'Pisco', NULL, NULL, 53887, '23.00', 128, 1, NULL, 3),
(1219, '110502', 'Huancano', NULL, NULL, 1594, '900.00', 128, 1, NULL, 3),
(1220, '110503', 'Humay', NULL, NULL, 5869, '912.00', 128, 1, NULL, 3),
(1221, '110504', 'Independencia', NULL, NULL, 14390, '269.00', 128, 1, NULL, 3),
(1222, '110505', 'Paracas', NULL, NULL, 7009, '1427.00', 128, 1, NULL, 3),
(1223, '110506', 'San Andres', NULL, NULL, 13539, '232.00', 128, 1, NULL, 3),
(1224, '110507', 'San Clemente', NULL, NULL, 21796, '134.00', 128, 1, NULL, 3),
(1225, '110508', 'Tupac Amaru Inca', NULL, NULL, 17651, '56.00', 128, 1, NULL, 3),
(1226, '120101', 'Huancayo', NULL, NULL, 116953, '224.00', 129, 1, NULL, 3),
(1227, '120104', 'Carhuacallanga', NULL, NULL, 1337, '13.00', 129, 1, NULL, 3),
(1228, '120105', 'Chacapampa', NULL, NULL, 888, '126.00', 129, 1, NULL, 3),
(1229, '120106', 'Chicche', NULL, NULL, 968, '42.00', 129, 1, NULL, 3),
(1230, '120107', 'Chilca', NULL, NULL, 85628, '29.00', 129, 1, NULL, 3),
(1231, '120108', 'Chongos Alto', NULL, NULL, 1389, '702.00', 129, 1, NULL, 3),
(1232, '120111', 'Chupuro', NULL, NULL, 1778, '40.00', 129, 1, NULL, 3),
(1233, '120112', 'Colca', NULL, NULL, 2053, '111.00', 129, 1, NULL, 3),
(1234, '120113', 'Cullhuas', NULL, NULL, 2247, '111.00', 129, 1, NULL, 3),
(1235, '120114', 'El Tambo', NULL, NULL, 161429, '167.00', 129, 1, NULL, 3),
(1236, '120116', 'Huacrapuquio', NULL, NULL, 1284, '24.00', 129, 1, NULL, 3),
(1237, '120117', 'Hualhuas', NULL, NULL, 4488, '14.00', 129, 1, NULL, 3),
(1238, '120119', 'Huancan', NULL, NULL, 20835, '10.00', 129, 1, NULL, 3),
(1239, '120120', 'Huasicancha', NULL, NULL, 859, '52.00', 129, 1, NULL, 3),
(1240, '120121', 'Huayucachi', NULL, NULL, 8558, '14.00', 129, 1, NULL, 3),
(1241, '120122', 'Ingenio', NULL, NULL, 2503, '53.00', 129, 1, NULL, 3),
(1242, '120124', 'Pariahuanca', NULL, NULL, 5941, '581.00', 129, 1, NULL, 3),
(1243, '120125', 'Pilcomayo', NULL, NULL, 16443, '10.00', 129, 1, NULL, 3),
(1244, '120126', 'Pucara', NULL, NULL, 5063, '110.00', 129, 1, NULL, 3),
(1245, '120127', 'Quichuay', NULL, NULL, 1757, '31.00', 129, 1, NULL, 3),
(1246, '120128', 'Quilcas', NULL, NULL, 4186, '161.00', 129, 1, NULL, 3),
(1247, '120129', 'San Agustin', NULL, NULL, 11607, '26.00', 129, 1, NULL, 3),
(1248, '120130', 'San Jeronimo De Tunan', NULL, NULL, 10203, '22.00', 129, 1, NULL, 3),
(1249, '120132', 'Saño', NULL, NULL, 4026, '13.00', 129, 1, NULL, 3),
(1250, '120133', 'Sapallanga', NULL, NULL, 12769, '124.00', 129, 1, NULL, 3),
(1251, '120134', 'Sicaya', NULL, NULL, 7988, '41.00', 129, 1, NULL, 3),
(1252, '120135', 'Santo Domingo De Acobamba', NULL, NULL, 7737, '878.00', 129, 1, NULL, 3),
(1253, '120136', 'Viques', NULL, NULL, 2222, '6.00', 129, 1, NULL, 3),
(1254, '120201', 'Concepcion', NULL, NULL, 14756, '18.00', 130, 1, NULL, 3),
(1255, '120202', 'Aco', NULL, NULL, 1642, '38.00', 130, 1, NULL, 3),
(1256, '120203', 'Andamarca', NULL, NULL, 4638, '505.00', 130, 1, NULL, 3),
(1257, '120204', 'Chambara', NULL, NULL, 2868, '100.00', 130, 1, NULL, 3),
(1258, '120205', 'Cochas', NULL, NULL, 1829, '109.00', 130, 1, NULL, 3),
(1259, '120206', 'Comas', NULL, NULL, 6258, '951.00', 130, 1, NULL, 3),
(1260, '120207', 'Heroinas Toledo', NULL, NULL, 1222, '26.00', 130, 1, NULL, 3),
(1261, '120208', 'Manzanares', NULL, NULL, 1401, '17.00', 130, 1, NULL, 3),
(1262, '120209', 'Mariscal Castilla', NULL, NULL, 1633, '56.00', 130, 1, NULL, 3),
(1263, '120210', 'Matahuasi', NULL, NULL, 5114, '23.00', 130, 1, NULL, 3),
(1264, '120211', 'Mito', NULL, NULL, 1372, '25.00', 130, 1, NULL, 3),
(1265, '120212', 'Nueve De Julio', NULL, NULL, 1522, '7.00', 130, 1, NULL, 3),
(1266, '120213', 'Orcotuna', NULL, NULL, 4135, '45.00', 130, 1, NULL, 3),
(1267, '120214', 'San Jose De Quero', NULL, NULL, 6080, '312.00', 130, 1, NULL, 3),
(1268, '120215', 'Santa Rosa De Ocopa', NULL, NULL, 2025, '16.00', 130, 1, NULL, 3),
(1269, '120301', 'Chanchamayo', NULL, NULL, 24675, '761.00', 131, 1, NULL, 3),
(1270, '120302', 'Perene', NULL, NULL, 74699, '1520.00', 131, 1, NULL, 3),
(1271, '120303', 'Pichanaqui', NULL, NULL, 68551, '1236.00', 131, 1, NULL, 3),
(1272, '120304', 'San Luis De Shuaro', NULL, NULL, 7233, '161.00', 131, 1, NULL, 3),
(1273, '120305', 'San Ramon', NULL, NULL, 27011, '623.00', 131, 1, NULL, 3),
(1274, '120306', 'Vitoc', NULL, NULL, 1866, '372.00', 131, 1, NULL, 3),
(1275, '120401', 'Jauja', NULL, NULL, 14717, '9.00', 132, 1, NULL, 3),
(1276, '120402', 'Acolla', NULL, NULL, 7343, '121.00', 132, 1, NULL, 3),
(1277, '120403', 'Apata', NULL, NULL, 4198, '419.00', 132, 1, NULL, 3),
(1278, '120404', 'Ataura', NULL, NULL, 1161, '6.00', 132, 1, NULL, 3),
(1279, '120405', 'Canchayllo', NULL, NULL, 1658, '945.00', 132, 1, NULL, 3),
(1280, '120406', 'Curicaca', NULL, NULL, 1645, '66.00', 132, 1, NULL, 3),
(1281, '120407', 'El Mantaro', NULL, NULL, 2541, '21.00', 132, 1, NULL, 3),
(1282, '120408', 'Huamali', NULL, NULL, 1815, '20.00', 132, 1, NULL, 3),
(1283, '120409', 'Huaripampa', NULL, NULL, 867, '20.00', 132, 1, NULL, 3),
(1284, '120410', 'Huertas', NULL, NULL, 1664, '12.00', 132, 1, NULL, 3),
(1285, '120411', 'Janjaillo', NULL, NULL, 717, '32.00', 132, 1, NULL, 3),
(1286, '120412', 'Julcan', NULL, NULL, 697, '19.00', 132, 1, NULL, 3),
(1287, '120413', 'Leonor Ordoñez', NULL, NULL, 1492, '21.00', 132, 1, NULL, 3),
(1288, '120414', 'Llocllapampa', NULL, NULL, 1058, '110.00', 132, 1, NULL, 3),
(1289, '120415', 'Marco', NULL, NULL, 1659, '29.00', 132, 1, NULL, 3),
(1290, '120416', 'Masma', NULL, NULL, 2069, '15.00', 132, 1, NULL, 3),
(1291, '120417', 'Masma Chicche', NULL, NULL, 777, '34.00', 132, 1, NULL, 3),
(1292, '120418', 'Molinos', NULL, NULL, 1558, '315.00', 132, 1, NULL, 3),
(1293, '120419', 'Monobamba', NULL, NULL, 1101, '961.00', 132, 1, NULL, 3),
(1294, '120420', 'Muqui', NULL, NULL, 966, '11.00', 132, 1, NULL, 3),
(1295, '120421', 'Muquiyauyo', NULL, NULL, 2216, '21.00', 132, 1, NULL, 3),
(1296, '120422', 'Paca', NULL, NULL, 1027, '33.00', 132, 1, NULL, 3),
(1297, '120423', 'Paccha', NULL, NULL, 1841, '89.00', 132, 1, NULL, 3),
(1298, '120424', 'Pancan', NULL, NULL, 1285, '11.00', 132, 1, NULL, 3),
(1299, '120425', 'Parco', NULL, NULL, 1208, '34.00', 132, 1, NULL, 3),
(1300, '120426', 'Pomacancha', NULL, NULL, 1980, '284.00', 132, 1, NULL, 3),
(1301, '120427', 'Ricran', NULL, NULL, 1626, '320.00', 132, 1, NULL, 3),
(1302, '120428', 'San Lorenzo', NULL, NULL, 2447, '22.00', 132, 1, NULL, 3),
(1303, '120429', 'San Pedro De Chunan', NULL, NULL, 854, '9.00', 132, 1, NULL, 3),
(1304, '120430', 'Sausa', NULL, NULL, 3009, '5.00', 132, 1, NULL, 3),
(1305, '120431', 'Sincos', NULL, NULL, 4795, '233.00', 132, 1, NULL, 3),
(1306, '120432', 'Tunan Marca', NULL, NULL, 1196, '33.00', 132, 1, NULL, 3),
(1307, '120433', 'Yauli', NULL, NULL, 1353, '93.00', 132, 1, NULL, 3),
(1308, '120434', 'Yauyos', NULL, NULL, 9256, '14.00', 132, 1, NULL, 3),
(1309, '120501', 'Junin', NULL, NULL, 9893, '869.00', 133, 1, NULL, 3),
(1310, '120502', 'Carhuamayo', NULL, NULL, 7784, '205.00', 133, 1, NULL, 3),
(1311, '120503', 'Ondores', NULL, NULL, 1965, '558.00', 133, 1, NULL, 3),
(1312, '120504', 'Ulcumayo', NULL, NULL, 5840, '994.00', 133, 1, NULL, 3),
(1313, '120601', 'Satipo', NULL, NULL, 41939, '816.00', 134, 1, NULL, 3),
(1314, '120602', 'Coviriali', NULL, NULL, 6101, '97.00', 134, 1, NULL, 3),
(1315, '120603', 'Llaylla', NULL, NULL, 6168, '309.00', 134, 1, NULL, 3),
(1316, '120604', 'Mazamari', NULL, NULL, 63429, '2035.00', 134, 1, NULL, 3),
(1317, '120605', 'Pampa Hermosa', NULL, NULL, 10414, '947.00', 134, 1, NULL, 3),
(1318, '120606', 'Pangoa', NULL, NULL, 59841, '4244.00', 134, 1, NULL, 3),
(1319, '120607', 'Rio Negro', NULL, NULL, 28301, '488.00', 134, 1, NULL, 3),
(1320, '120608', 'Rio Tambo', NULL, NULL, 58417, '10284.00', 134, 1, NULL, 3),
(1321, '120701', 'Tarma', NULL, NULL, 46281, '454.00', 135, 1, NULL, 3),
(1322, '120702', 'Acobamba', NULL, NULL, 13419, '108.00', 135, 1, NULL, 3),
(1323, '120703', 'Huaricolca', NULL, NULL, 3212, '163.00', 135, 1, NULL, 3),
(1324, '120704', 'Huasahuasi', NULL, NULL, 15239, '634.00', 135, 1, NULL, 3),
(1325, '120705', 'La Union', NULL, NULL, 3225, '143.00', 135, 1, NULL, 3),
(1326, '120706', 'Palca', NULL, NULL, 5674, '328.00', 135, 1, NULL, 3),
(1327, '120707', 'Palcamayo', NULL, NULL, 9305, '169.00', 135, 1, NULL, 3),
(1328, '120708', 'San Pedro De Cajas', NULL, NULL, 5633, '512.00', 135, 1, NULL, 3),
(1329, '120709', 'Tapo', NULL, NULL, 5988, '153.00', 135, 1, NULL, 3),
(1330, '120801', 'La Oroya', NULL, NULL, 13637, '380.00', 136, 1, NULL, 3),
(1331, '120802', 'Chacapalpa', NULL, NULL, 737, '186.00', 136, 1, NULL, 3),
(1332, '120803', 'Huay-huay', NULL, NULL, 1494, '192.00', 136, 1, NULL, 3),
(1333, '120804', 'Marcapomacocha', NULL, NULL, 1287, '885.00', 136, 1, NULL, 3),
(1334, '120805', 'Morococha', NULL, NULL, 4432, '265.00', 136, 1, NULL, 3),
(1335, '120806', 'Paccha', NULL, NULL, 1669, '324.00', 136, 1, NULL, 3),
(1336, '120807', 'Santa Barbara De Carhuacayan', NULL, NULL, 2292, '680.00', 136, 1, NULL, 3),
(1337, '120808', 'Santa Rosa De Sacco', NULL, NULL, 10421, '100.00', 136, 1, NULL, 3),
(1338, '120809', 'Suitucancha', NULL, NULL, 990, '214.00', 136, 1, NULL, 3),
(1339, '120810', 'Yauli', NULL, NULL, 5211, '412.00', 136, 1, NULL, 3),
(1340, '120901', 'Chupaca', NULL, NULL, 21952, '23.00', 137, 1, NULL, 3),
(1341, '120902', 'Ahuac', NULL, NULL, 5968, '72.00', 137, 1, NULL, 3),
(1342, '120903', 'Chongos Bajo', NULL, NULL, 4031, '104.00', 137, 1, NULL, 3),
(1343, '120904', 'Huachac', NULL, NULL, 3946, '20.00', 137, 1, NULL, 3),
(1344, '120905', 'Huamancaca Chico', NULL, NULL, 5912, '13.00', 137, 1, NULL, 3),
(1345, '120906', 'San Juan De Yscos', NULL, NULL, 2135, '26.00', 137, 1, NULL, 3),
(1346, '120907', 'San Juan De Jarpa', NULL, NULL, 3569, '128.00', 137, 1, NULL, 3),
(1347, '120908', 'Tres De Diciembre', NULL, NULL, 2092, '16.00', 137, 1, NULL, 3),
(1348, '120909', 'Yanacancha', NULL, NULL, 3475, '762.00', 137, 1, NULL, 3),
(1349, '130101', 'Trujillo', NULL, NULL, 318914, '39.00', 138, 1, NULL, 3),
(1350, '130102', 'El Porvenir', NULL, NULL, 186127, '38.00', 138, 1, NULL, 3),
(1351, '130103', 'Florencia De Mora', NULL, NULL, 41914, '3.00', 138, 1, NULL, 3),
(1352, '130104', 'Huanchaco', NULL, NULL, 68104, '318.00', 138, 1, NULL, 3),
(1353, '130105', 'La Esperanza', NULL, NULL, 182494, '20.00', 138, 1, NULL, 3),
(1354, '130106', 'Laredo', NULL, NULL, 35289, '491.00', 138, 1, NULL, 3),
(1355, '130107', 'Moche', NULL, NULL, 34503, '28.00', 138, 1, NULL, 3),
(1356, '130108', 'Poroto', NULL, NULL, 3195, '126.00', 138, 1, NULL, 3),
(1357, '130109', 'Salaverry', NULL, NULL, 18129, '290.00', 138, 1, NULL, 3),
(1358, '130110', 'Simbal', NULL, NULL, 4317, '389.00', 138, 1, NULL, 3),
(1359, '130111', 'Victor Larco Herrera', NULL, NULL, 64024, '12.00', 138, 1, NULL, 3),
(1360, '130201', 'Ascope', NULL, NULL, 6677, '289.00', 139, 1, NULL, 3),
(1361, '130202', 'Chicama', NULL, NULL, 15492, '897.00', 139, 1, NULL, 3),
(1362, '130203', 'Chocope', NULL, NULL, 9413, '101.00', 139, 1, NULL, 3),
(1363, '130204', 'Magdalena De Cao', NULL, NULL, 3232, '156.00', 139, 1, NULL, 3),
(1364, '130205', 'Paijan', NULL, NULL, 25584, '82.00', 139, 1, NULL, 3),
(1365, '130206', 'Razuri', NULL, NULL, 9079, '313.00', 139, 1, NULL, 3),
(1366, '130207', 'Santiago De Cao', NULL, NULL, 19660, '130.00', 139, 1, NULL, 3),
(1367, '130208', 'Casa Grande', NULL, NULL, 31174, '674.00', 139, 1, NULL, 3),
(1368, '130301', 'Bolivar', NULL, NULL, 4838, '734.00', 140, 1, NULL, 3),
(1369, '130302', 'Bambamarca', NULL, NULL, 3868, '165.00', 140, 1, NULL, 3),
(1370, '130303', 'Condormarca', NULL, NULL, 2063, '324.00', 140, 1, NULL, 3),
(1371, '130304', 'Longotea', NULL, NULL, 2232, '189.00', 140, 1, NULL, 3),
(1372, '130305', 'Uchumarca', NULL, NULL, 2759, '234.00', 140, 1, NULL, 3),
(1373, '130306', 'Ucuncha', NULL, NULL, 815, '99.00', 140, 1, NULL, 3),
(1374, '130401', 'Chepen', NULL, NULL, 48563, '289.00', 141, 1, NULL, 3),
(1375, '130402', 'Pacanga', NULL, NULL, 23643, '586.00', 141, 1, NULL, 3),
(1376, '130403', 'Pueblo Nuevo', NULL, NULL, 14805, '271.00', 141, 1, NULL, 3),
(1377, '130501', 'Julcan', NULL, NULL, 11662, '184.00', 142, 1, NULL, 3),
(1378, '130502', 'Calamarca', NULL, NULL, 5657, '208.00', 142, 1, NULL, 3),
(1379, '130503', 'Carabamba', NULL, NULL, 6518, '280.00', 142, 1, NULL, 3),
(1380, '130504', 'Huaso', NULL, NULL, 7253, '439.00', 142, 1, NULL, 3),
(1381, '130601', 'Otuzco', NULL, NULL, 27257, '444.00', 143, 1, NULL, 3),
(1382, '130602', 'Agallpampa', NULL, NULL, 9859, '257.00', 143, 1, NULL, 3),
(1383, '130604', 'Charat', NULL, NULL, 2847, '65.00', 143, 1, NULL, 3),
(1384, '130605', 'Huaranchal', NULL, NULL, 5077, '126.00', 143, 1, NULL, 3),
(1385, '130606', 'La Cuesta', NULL, NULL, 687, '41.00', 143, 1, NULL, 3),
(1386, '130608', 'Mache', NULL, NULL, 3112, '38.00', 143, 1, NULL, 3),
(1387, '130610', 'Paranday', NULL, NULL, 730, '22.00', 143, 1, NULL, 3),
(1388, '130611', 'Salpo', NULL, NULL, 6142, '196.00', 143, 1, NULL, 3),
(1389, '130613', 'Sinsicap', NULL, NULL, 8619, '453.00', 143, 1, NULL, 3),
(1390, '130614', 'Usquil', NULL, NULL, 27383, '445.00', 143, 1, NULL, 3),
(1391, '130701', 'San Pedro De Lloc', NULL, NULL, 16519, '679.00', 144, 1, NULL, 3),
(1392, '130702', 'Guadalupe', NULL, NULL, 43965, '166.00', 144, 1, NULL, 3);
INSERT INTO `comun_ubigeo` (`id`, `codigo`, `nombre`, `codigo_telefono`, `capital`, `poblacion`, `area_km2`, `padre_id`, `pais_id`, `region_natural_id`, `tipo_ubigeo_id`) VALUES
(1393, '130703', 'Jequetepeque', NULL, NULL, 3808, '49.00', 144, 1, NULL, 3),
(1394, '130704', 'Pacasmayo', NULL, NULL, 27434, '35.00', 144, 1, NULL, 3),
(1395, '130705', 'San Jose', NULL, NULL, 12259, '179.00', 144, 1, NULL, 3),
(1396, '130801', 'Tayabamba', NULL, NULL, 14586, '338.00', 145, 1, NULL, 3),
(1397, '130802', 'Buldibuyo', NULL, NULL, 3763, '229.00', 145, 1, NULL, 3),
(1398, '130803', 'Chillia', NULL, NULL, 13402, '306.00', 145, 1, NULL, 3),
(1399, '130804', 'Huancaspata', NULL, NULL, 6390, '242.00', 145, 1, NULL, 3),
(1400, '130805', 'Huaylillas', NULL, NULL, 3518, '92.00', 145, 1, NULL, 3),
(1401, '130806', 'Huayo', NULL, NULL, 4373, '126.00', 145, 1, NULL, 3),
(1402, '130807', 'Ongon', NULL, NULL, 1761, '1308.00', 145, 1, NULL, 3),
(1403, '130808', 'Parcoy', NULL, NULL, 21784, '323.00', 145, 1, NULL, 3),
(1404, '130809', 'Pataz', NULL, NULL, 8804, '394.00', 145, 1, NULL, 3),
(1405, '130810', 'Pias', NULL, NULL, 1316, '270.00', 145, 1, NULL, 3),
(1406, '130811', 'Santiago De Challas', NULL, NULL, 2533, '129.00', 145, 1, NULL, 3),
(1407, '130812', 'Taurija', NULL, NULL, 3004, '130.00', 145, 1, NULL, 3),
(1408, '130813', 'Urpay', NULL, NULL, 2804, '102.00', 145, 1, NULL, 3),
(1409, '130901', 'Huamachuco', NULL, NULL, 62424, '415.00', 146, 1, NULL, 3),
(1410, '130902', 'Chugay', NULL, NULL, 18753, '417.00', 146, 1, NULL, 3),
(1411, '130903', 'Cochorco', NULL, NULL, 9340, '262.00', 146, 1, NULL, 3),
(1412, '130904', 'Curgos', NULL, NULL, 8526, '98.00', 146, 1, NULL, 3),
(1413, '130905', 'Marcabal', NULL, NULL, 16698, '234.00', 146, 1, NULL, 3),
(1414, '130906', 'Sanagoran', NULL, NULL, 14859, '338.00', 146, 1, NULL, 3),
(1415, '130907', 'Sarin', NULL, NULL, 9945, '334.00', 146, 1, NULL, 3),
(1416, '130908', 'Sartimbamba', NULL, NULL, 13691, '397.00', 146, 1, NULL, 3),
(1417, '131001', 'Santiago De Chuco', NULL, NULL, 20372, '1078.00', 147, 1, NULL, 3),
(1418, '131002', 'Angasmarca', NULL, NULL, 7266, '152.00', 147, 1, NULL, 3),
(1419, '131003', 'Cachicadan', NULL, NULL, 7964, '269.00', 147, 1, NULL, 3),
(1420, '131004', 'Mollebamba', NULL, NULL, 2312, '69.00', 147, 1, NULL, 3),
(1421, '131005', 'Mollepata', NULL, NULL, 2666, '69.00', 147, 1, NULL, 3),
(1422, '131006', 'Quiruvilca', NULL, NULL, 14295, '552.00', 147, 1, NULL, 3),
(1423, '131007', 'Santa Cruz De Chuca', NULL, NULL, 3187, '165.00', 147, 1, NULL, 3),
(1424, '131008', 'Sitabamba', NULL, NULL, 3412, '319.00', 147, 1, NULL, 3),
(1425, '131101', 'Cascas', NULL, NULL, 14187, '475.00', 148, 1, NULL, 3),
(1426, '131102', 'Lucma', NULL, NULL, 6896, '295.00', 148, 1, NULL, 3),
(1427, '131103', 'Compin', NULL, NULL, 2118, '301.00', 148, 1, NULL, 3),
(1428, '131104', 'Sayapullo', NULL, NULL, 7908, '236.00', 148, 1, NULL, 3),
(1429, '131201', 'Viru', NULL, NULL, 67228, '1084.00', 149, 1, NULL, 3),
(1430, '131202', 'Chao', NULL, NULL, 40272, '1672.00', 149, 1, NULL, 3),
(1431, '131203', 'Guadalupito', NULL, NULL, 9588, '426.00', 149, 1, NULL, 3),
(1432, '140101', 'Chiclayo', NULL, NULL, 291777, '50.00', 150, 1, NULL, 3),
(1433, '140102', 'Chongoyape', NULL, NULL, 17940, '723.00', 150, 1, NULL, 3),
(1434, '140103', 'Eten', NULL, NULL, 10571, '84.00', 150, 1, NULL, 3),
(1435, '140104', 'Eten Puerto', NULL, NULL, 2167, '13.00', 150, 1, NULL, 3),
(1436, '140105', 'Jose Leonardo Ortiz', NULL, NULL, 193232, '29.00', 150, 1, NULL, 3),
(1437, '140106', 'La Victoria', NULL, NULL, 90546, '41.00', 150, 1, NULL, 3),
(1438, '140107', 'Lagunas', NULL, NULL, 10234, '429.00', 150, 1, NULL, 3),
(1439, '140108', 'Monsefu', NULL, NULL, 31847, '45.00', 150, 1, NULL, 3),
(1440, '140109', 'Nueva Arica', NULL, NULL, 2338, '212.00', 150, 1, NULL, 3),
(1441, '140110', 'Oyotun', NULL, NULL, 9854, '486.00', 150, 1, NULL, 3),
(1442, '140111', 'Picsi', NULL, NULL, 9782, '53.00', 150, 1, NULL, 3),
(1443, '140112', 'Pimentel', NULL, NULL, 44285, '65.00', 150, 1, NULL, 3),
(1444, '140113', 'Reque', NULL, NULL, 14942, '46.00', 150, 1, NULL, 3),
(1445, '140114', 'Santa Rosa', NULL, NULL, 12687, '13.00', 150, 1, NULL, 3),
(1446, '140115', 'Saña', NULL, NULL, 12288, '311.00', 150, 1, NULL, 3),
(1447, '140116', 'Cayalti', NULL, NULL, 15967, '163.00', 150, 1, NULL, 3),
(1448, '140117', 'Patapo', NULL, NULL, 22452, '181.00', 150, 1, NULL, 3),
(1449, '140118', 'Pomalca', NULL, NULL, 25323, '78.00', 150, 1, NULL, 3),
(1450, '140119', 'Pucala', NULL, NULL, 8979, '177.00', 150, 1, NULL, 3),
(1451, '140120', 'Tuman', NULL, NULL, 30194, '124.00', 150, 1, NULL, 3),
(1452, '140201', 'Ferreñafe', NULL, NULL, 35360, '61.00', 151, 1, NULL, 3),
(1453, '140202', 'Cañaris', NULL, NULL, 14516, '287.00', 151, 1, NULL, 3),
(1454, '140203', 'Incahuasi', NULL, NULL, 15518, '438.00', 151, 1, NULL, 3),
(1455, '140204', 'Manuel Antonio Mesones Muro', NULL, NULL, 4230, '211.00', 151, 1, NULL, 3),
(1456, '140205', 'Pitipo', NULL, NULL, 23572, '550.00', 151, 1, NULL, 3),
(1457, '140206', 'Pueblo Nuevo', NULL, NULL, 13404, '31.00', 151, 1, NULL, 3),
(1458, '140301', 'Lambayeque', NULL, NULL, 77234, '328.00', 152, 1, NULL, 3),
(1459, '140302', 'Chochope', NULL, NULL, 1139, '78.00', 152, 1, NULL, 3),
(1460, '140303', 'Illimo', NULL, NULL, 9328, '25.00', 152, 1, NULL, 3),
(1461, '140304', 'Jayanca', NULL, NULL, 17523, '685.00', 152, 1, NULL, 3),
(1462, '140305', 'Mochumi', NULL, NULL, 19158, '103.00', 152, 1, NULL, 3),
(1463, '140306', 'Morrope', NULL, NULL, 46046, '1046.00', 152, 1, NULL, 3),
(1464, '140307', 'Motupe', NULL, NULL, 26409, '556.00', 152, 1, NULL, 3),
(1465, '140308', 'Olmos', NULL, NULL, 40642, '5326.00', 152, 1, NULL, 3),
(1466, '140309', 'Pacora', NULL, NULL, 7190, '89.00', 152, 1, NULL, 3),
(1467, '140310', 'Salas', NULL, NULL, 12999, '998.00', 152, 1, NULL, 3),
(1468, '140311', 'San Jose', NULL, NULL, 16172, '45.00', 152, 1, NULL, 3),
(1469, '140312', 'Tucume', NULL, NULL, 22805, '63.00', 152, 1, NULL, 3),
(1470, '150101', 'Lima', NULL, NULL, 271814, '21.00', 153, 1, NULL, 3),
(1471, '150102', 'Ancon', NULL, NULL, 43382, '317.00', 153, 1, NULL, 3),
(1472, '150103', 'Ate', NULL, NULL, 630086, '87.00', 153, 1, NULL, 3),
(1473, '150104', 'Barranco', NULL, NULL, 29984, '3.00', 153, 1, NULL, 3),
(1474, '150105', 'Breña', NULL, NULL, 75925, '3.00', 153, 1, NULL, 3),
(1475, '150106', 'Carabayllo', NULL, NULL, 301978, '416.00', 153, 1, NULL, 3),
(1476, '150107', 'Chaclacayo', NULL, NULL, 43428, '46.00', 153, 1, NULL, 3),
(1477, '150108', 'Chorrillos', NULL, NULL, 325547, '37.00', 153, 1, NULL, 3),
(1478, '150109', 'Cieneguilla', NULL, NULL, 47080, '228.00', 153, 1, NULL, 3),
(1479, '150110', 'Comas', NULL, NULL, 524894, '49.00', 153, 1, NULL, 3),
(1480, '150111', 'El Agustino', NULL, NULL, 191365, '15.00', 153, 1, NULL, 3),
(1481, '150112', 'Independencia', NULL, NULL, 216822, '16.00', 153, 1, NULL, 3),
(1482, '150113', 'Jesus Maria', NULL, NULL, 71589, '4.00', 153, 1, NULL, 3),
(1483, '150114', 'La Molina', NULL, NULL, 171646, '52.00', 153, 1, NULL, 3),
(1484, '150115', 'La Victoria', NULL, NULL, 171779, '9.00', 153, 1, NULL, 3),
(1485, '150116', 'Lince', NULL, NULL, 50228, '3.00', 153, 1, NULL, 3),
(1486, '150117', 'Los Olivos', NULL, NULL, 371229, '18.00', 153, 1, NULL, 3),
(1487, '150118', 'Lurigancho', NULL, NULL, 218976, '293.00', 153, 1, NULL, 3),
(1488, '150119', 'Lurin', NULL, NULL, 85132, '157.00', 153, 1, NULL, 3),
(1489, '150120', 'Magdalena Del Mar', NULL, NULL, 54656, '3.00', 153, 1, NULL, 3),
(1490, '150121', 'Pueblo Libre', NULL, NULL, 76114, '5.00', 153, 1, NULL, 3),
(1491, '150122', 'Miraflores', NULL, NULL, 81932, '9.00', 153, 1, NULL, 3),
(1492, '150123', 'Pachacamac', NULL, NULL, 129653, '197.00', 153, 1, NULL, 3),
(1493, '150124', 'Pucusana', NULL, NULL, 17044, '90.00', 153, 1, NULL, 3),
(1494, '150125', 'Puente Piedra', NULL, NULL, 353327, '57.00', 153, 1, NULL, 3),
(1495, '150126', 'Punta Hermosa', NULL, NULL, 7609, '3.00', 153, 1, NULL, 3),
(1496, '150127', 'Punta Negra', NULL, NULL, 7934, '3.00', 153, 1, NULL, 3),
(1497, '150128', 'Rimac', NULL, NULL, 164911, '12.00', 153, 1, NULL, 3),
(1498, '150129', 'San Bartolo', NULL, NULL, 7699, '280.00', 153, 1, NULL, 3),
(1499, '150130', 'San Borja', NULL, NULL, 111928, '10.00', 153, 1, NULL, 3),
(1500, '150131', 'San Isidro', NULL, NULL, 54206, '10.00', 153, 1, NULL, 3),
(1501, '150132', 'San Juan De Lurigancho', NULL, NULL, 1091303, '144.00', 153, 1, NULL, 3),
(1502, '150133', 'San Juan De Miraflores', NULL, NULL, 404001, '20.00', 153, 1, NULL, 3),
(1503, '150134', 'San Luis', NULL, NULL, 57600, '3.00', 153, 1, NULL, 3),
(1504, '150135', 'San Martin De Porres', NULL, NULL, 700177, '36.00', 153, 1, NULL, 3),
(1505, '150136', 'San Miguel', NULL, NULL, 135506, '10.00', 153, 1, NULL, 3),
(1506, '150137', 'Santa Anita', NULL, NULL, 228422, '10.00', 153, 1, NULL, 3),
(1507, '150138', 'Santa Maria Del Mar', NULL, NULL, 1608, '11.00', 153, 1, NULL, 3),
(1508, '150139', 'Santa Rosa', NULL, NULL, 18751, '23.00', 153, 1, NULL, 3),
(1509, '150140', 'Santiago De Surco', NULL, NULL, 344242, '33.00', 153, 1, NULL, 3),
(1510, '150141', 'Surquillo', NULL, NULL, 91346, '5.00', 153, 1, NULL, 3),
(1511, '150142', 'Villa El Salvador', NULL, NULL, 463014, '35.00', 153, 1, NULL, 3),
(1512, '150143', 'Villa Maria Del Triunfo', NULL, NULL, 448545, '66.00', 153, 1, NULL, 3),
(1513, '150201', 'Barranca', NULL, NULL, 70430, '160.00', 154, 1, NULL, 3),
(1514, '150202', 'Paramonga', NULL, NULL, 22387, '414.00', 154, 1, NULL, 3),
(1515, '150203', 'Pativilca', NULL, NULL, 19272, '268.00', 154, 1, NULL, 3),
(1516, '150204', 'Supe', NULL, NULL, 22543, '15.00', 154, 1, NULL, 3),
(1517, '150205', 'Supe Puerto', NULL, NULL, 11609, '519.00', 154, 1, NULL, 3),
(1518, '150301', 'Cajatambo', NULL, NULL, 2281, '582.00', 155, 1, NULL, 3),
(1519, '150302', 'Copa', NULL, NULL, 841, '204.00', 155, 1, NULL, 3),
(1520, '150303', 'Gorgor', NULL, NULL, 2683, '308.00', 155, 1, NULL, 3),
(1521, '150304', 'Huancapon', NULL, NULL, 1030, '151.00', 155, 1, NULL, 3),
(1522, '150305', 'Manas', NULL, NULL, 993, '282.00', 155, 1, NULL, 3),
(1523, '150401', 'Canta', NULL, NULL, 2794, '136.00', 156, 1, NULL, 3),
(1524, '150402', 'Arahuay', NULL, NULL, 750, '136.00', 156, 1, NULL, 3),
(1525, '150403', 'Huamantanga', NULL, NULL, 1300, '494.00', 156, 1, NULL, 3),
(1526, '150404', 'Huaros', NULL, NULL, 776, '327.00', 156, 1, NULL, 3),
(1527, '150405', 'Lachaqui', NULL, NULL, 878, '129.00', 156, 1, NULL, 3),
(1528, '150406', 'San Buenaventura', NULL, NULL, 526, '109.00', 156, 1, NULL, 3),
(1529, '150407', 'Santa Rosa De Quives', NULL, NULL, 8098, '311.00', 156, 1, NULL, 3),
(1530, '150501', 'San Vicente De Cañete', NULL, NULL, 55824, '519.00', 157, 1, NULL, 3),
(1531, '150502', 'Asia', NULL, NULL, 9321, '282.00', 157, 1, NULL, 3),
(1532, '150503', 'Calango', NULL, NULL, 2377, '526.00', 157, 1, NULL, 3),
(1533, '150504', 'Cerro Azul', NULL, NULL, 8053, '119.00', 157, 1, NULL, 3),
(1534, '150505', 'Chilca', NULL, NULL, 15801, '449.00', 157, 1, NULL, 3),
(1535, '150506', 'Coayllo', NULL, NULL, 1077, '600.00', 157, 1, NULL, 3),
(1536, '150507', 'Imperial', NULL, NULL, 39628, '52.00', 157, 1, NULL, 3),
(1537, '150508', 'Lunahuana', NULL, NULL, 4812, '494.00', 157, 1, NULL, 3),
(1538, '150509', 'Mala', NULL, NULL, 34386, '131.00', 157, 1, NULL, 3),
(1539, '150510', 'Nuevo Imperial', NULL, NULL, 23130, '320.00', 157, 1, NULL, 3),
(1540, '150511', 'Pacaran', NULL, NULL, 1791, '255.00', 157, 1, NULL, 3),
(1541, '150512', 'Quilmana', NULL, NULL, 15200, '447.00', 157, 1, NULL, 3),
(1542, '150513', 'San Antonio', NULL, NULL, 4169, '38.00', 157, 1, NULL, 3),
(1543, '150514', 'San Luis', NULL, NULL, 12971, '37.00', 157, 1, NULL, 3),
(1544, '150515', 'Santa Cruz De Flores', NULL, NULL, 2793, '96.00', 157, 1, NULL, 3),
(1545, '150516', 'Zuñiga', NULL, NULL, 1818, '193.00', 157, 1, NULL, 3),
(1546, '150601', 'Huaral', NULL, NULL, 100436, '645.00', 158, 1, NULL, 3),
(1547, '150602', 'Atavillos Alto', NULL, NULL, 712, '347.00', 158, 1, NULL, 3),
(1548, '150603', 'Atavillos Bajo', NULL, NULL, 1173, '175.00', 158, 1, NULL, 3),
(1549, '150604', 'Aucallama', NULL, NULL, 19502, '708.00', 158, 1, NULL, 3),
(1550, '150605', 'Chancay', NULL, NULL, 61790, '155.00', 158, 1, NULL, 3),
(1551, '150606', 'Ihuari', NULL, NULL, 2381, '473.00', 158, 1, NULL, 3),
(1552, '150607', 'Lampian', NULL, NULL, 416, '147.00', 158, 1, NULL, 3),
(1553, '150608', 'Pacaraos', NULL, NULL, 490, '305.00', 158, 1, NULL, 3),
(1554, '150609', 'San Miguel De Acos', NULL, NULL, 768, '42.00', 158, 1, NULL, 3),
(1555, '150610', 'Santa Cruz De Andamarca', NULL, NULL, 1407, '216.00', 158, 1, NULL, 3),
(1556, '150611', 'Sumbilca', NULL, NULL, 986, '238.00', 158, 1, NULL, 3),
(1557, '150612', 'Veintisiete De Noviembre', NULL, NULL, 440, '207.00', 158, 1, NULL, 3),
(1558, '150701', 'Matucana', NULL, NULL, 3680, '181.00', 159, 1, NULL, 3),
(1559, '150702', 'Antioquia', NULL, NULL, 1238, '415.00', 159, 1, NULL, 3),
(1560, '150703', 'Callahuanca', NULL, NULL, 4080, '51.00', 159, 1, NULL, 3),
(1561, '150704', 'Carampoma', NULL, NULL, 1788, '231.00', 159, 1, NULL, 3),
(1562, '150705', 'Chicla', NULL, NULL, 7632, '235.00', 159, 1, NULL, 3),
(1563, '150706', 'Cuenca', NULL, NULL, 395, '69.00', 159, 1, NULL, 3),
(1564, '150707', 'Huachupampa', NULL, NULL, 2814, '79.00', 159, 1, NULL, 3),
(1565, '150708', 'Huanza', NULL, NULL, 2674, '233.00', 159, 1, NULL, 3),
(1566, '150709', 'Huarochiri', NULL, NULL, 1291, '240.00', 159, 1, NULL, 3),
(1567, '150710', 'Lahuaytambo', NULL, NULL, 674, '82.00', 159, 1, NULL, 3),
(1568, '150711', 'Langa', NULL, NULL, 851, '76.00', 159, 1, NULL, 3),
(1569, '150712', 'Laraos', NULL, NULL, 2298, '120.00', 159, 1, NULL, 3),
(1570, '150713', 'Mariatana', NULL, NULL, 1309, '171.00', 159, 1, NULL, 3),
(1571, '150714', 'Ricardo Palma', NULL, NULL, 6103, '34.00', 159, 1, NULL, 3),
(1572, '150715', 'San Andres De Tupicocha', NULL, NULL, 1268, '97.00', 159, 1, NULL, 3),
(1573, '150716', 'San Antonio', NULL, NULL, 5469, '525.00', 159, 1, NULL, 3),
(1574, '150717', 'San Bartolome', NULL, NULL, 2271, '42.00', 159, 1, NULL, 3),
(1575, '150718', 'San Damian', NULL, NULL, 1183, '333.00', 159, 1, NULL, 3),
(1576, '150719', 'San Juan De Iris', NULL, NULL, 1772, '127.00', 159, 1, NULL, 3),
(1577, '150720', 'San Juan De Tantaranche', NULL, NULL, 471, '137.00', 159, 1, NULL, 3),
(1578, '150721', 'San Lorenzo De Quinti', NULL, NULL, 1532, '452.00', 159, 1, NULL, 3),
(1579, '150722', 'San Mateo', NULL, NULL, 5017, '419.00', 159, 1, NULL, 3),
(1580, '150723', 'San Mateo De Otao', NULL, NULL, 1603, '136.00', 159, 1, NULL, 3),
(1581, '150724', 'San Pedro De Casta', NULL, NULL, 1303, '82.00', 159, 1, NULL, 3),
(1582, '150725', 'San Pedro De Huancayre', NULL, NULL, 246, '40.00', 159, 1, NULL, 3),
(1583, '150726', 'Sangallaya', NULL, NULL, 576, '91.00', 159, 1, NULL, 3),
(1584, '150727', 'Santa Cruz De Cocachacra', NULL, NULL, 2477, '33.00', 159, 1, NULL, 3),
(1585, '150728', 'Santa Eulalia', NULL, NULL, 11787, '109.00', 159, 1, NULL, 3),
(1586, '150729', 'Santiago De Anchucaya', NULL, NULL, 522, '95.00', 159, 1, NULL, 3),
(1587, '150730', 'Santiago De Tuna', NULL, NULL, 729, '63.00', 159, 1, NULL, 3),
(1588, '150731', 'Santo Domingo De Los Olleros', NULL, NULL, 4705, '577.00', 159, 1, NULL, 3),
(1589, '150732', 'Surco', NULL, NULL, 1938, '107.00', 159, 1, NULL, 3),
(1590, '150801', 'Huacho', NULL, NULL, 58532, '711.00', 160, 1, NULL, 3),
(1591, '150802', 'Ambar', NULL, NULL, 2737, '941.00', 160, 1, NULL, 3),
(1592, '150803', 'Caleta De Carquin', NULL, NULL, 6801, '4.00', 160, 1, NULL, 3),
(1593, '150804', 'Checras', NULL, NULL, 1781, '167.00', 160, 1, NULL, 3),
(1594, '150805', 'Hualmay', NULL, NULL, 28589, '7.00', 160, 1, NULL, 3),
(1595, '150806', 'Huaura', NULL, NULL, 35373, '481.00', 160, 1, NULL, 3),
(1596, '150807', 'Leoncio Prado', NULL, NULL, 1980, '294.00', 160, 1, NULL, 3),
(1597, '150808', 'Paccho', NULL, NULL, 2189, '237.00', 160, 1, NULL, 3),
(1598, '150809', 'Santa Leonor', NULL, NULL, 1455, '375.00', 160, 1, NULL, 3),
(1599, '150810', 'Santa Maria', NULL, NULL, 33496, '135.00', 160, 1, NULL, 3),
(1600, '150811', 'Sayan', NULL, NULL, 24095, '1314.00', 160, 1, NULL, 3),
(1601, '150812', 'Vegueta', NULL, NULL, 22031, '257.00', 160, 1, NULL, 3),
(1602, '150901', 'Oyon', NULL, NULL, 14479, '873.00', 161, 1, NULL, 3),
(1603, '150902', 'Andajes', NULL, NULL, 1045, '158.00', 161, 1, NULL, 3),
(1604, '150903', 'Caujul', NULL, NULL, 1036, '97.00', 161, 1, NULL, 3),
(1605, '150904', 'Cochamarca', NULL, NULL, 1607, '258.00', 161, 1, NULL, 3),
(1606, '150905', 'Navan', NULL, NULL, 1192, '231.00', 161, 1, NULL, 3),
(1607, '150906', 'Pachangara', NULL, NULL, 3423, '254.00', 161, 1, NULL, 3),
(1608, '151001', 'Yauyos', NULL, NULL, 2791, '332.00', 162, 1, NULL, 3),
(1609, '151002', 'Alis', NULL, NULL, 1203, '141.00', 162, 1, NULL, 3),
(1610, '151003', 'Ayauca', NULL, NULL, 2203, '441.00', 162, 1, NULL, 3),
(1611, '151004', 'Ayaviri', NULL, NULL, 690, '244.00', 162, 1, NULL, 3),
(1612, '151005', 'Azangaro', NULL, NULL, 532, '78.00', 162, 1, NULL, 3),
(1613, '151006', 'Cacra', NULL, NULL, 384, '212.00', 162, 1, NULL, 3),
(1614, '151007', 'Carania', NULL, NULL, 367, '122.00', 162, 1, NULL, 3),
(1615, '151008', 'Catahuasi', NULL, NULL, 951, '126.00', 162, 1, NULL, 3),
(1616, '151009', 'Chocos', NULL, NULL, 1194, '211.00', 162, 1, NULL, 3),
(1617, '151010', 'Cochas', NULL, NULL, 419, '38.00', 162, 1, NULL, 3),
(1618, '151011', 'Colonia', NULL, NULL, 1315, '319.00', 162, 1, NULL, 3),
(1619, '151012', 'Hongos', NULL, NULL, 396, '122.00', 162, 1, NULL, 3),
(1620, '151013', 'Huampara', NULL, NULL, 188, '55.00', 162, 1, NULL, 3),
(1621, '151014', 'Huancaya', NULL, NULL, 1330, '279.00', 162, 1, NULL, 3),
(1622, '151015', 'Huangascar', NULL, NULL, 570, '50.00', 162, 1, NULL, 3),
(1623, '151016', 'Huantan', NULL, NULL, 941, '512.00', 162, 1, NULL, 3),
(1624, '151017', 'Huañec', NULL, NULL, 484, '42.00', 162, 1, NULL, 3),
(1625, '151018', 'Laraos', NULL, NULL, 762, '411.00', 162, 1, NULL, 3),
(1626, '151019', 'Lincha', NULL, NULL, 917, '217.00', 162, 1, NULL, 3),
(1627, '151020', 'Madean', NULL, NULL, 808, '208.00', 162, 1, NULL, 3),
(1628, '151021', 'Miraflores', NULL, NULL, 448, '202.00', 162, 1, NULL, 3),
(1629, '151022', 'Omas', NULL, NULL, 578, '294.00', 162, 1, NULL, 3),
(1630, '151023', 'Putinza', NULL, NULL, 481, '61.00', 162, 1, NULL, 3),
(1631, '151024', 'Quinches', NULL, NULL, 978, '116.00', 162, 1, NULL, 3),
(1632, '151025', 'Quinocay', NULL, NULL, 540, '158.00', 162, 1, NULL, 3),
(1633, '151026', 'San Joaquin', NULL, NULL, 428, '76.00', 162, 1, NULL, 3),
(1634, '151027', 'San Pedro De Pilas', NULL, NULL, 374, '99.00', 162, 1, NULL, 3),
(1635, '151028', 'Tanta', NULL, NULL, 505, '343.00', 162, 1, NULL, 3),
(1636, '151029', 'Tauripampa', NULL, NULL, 428, '531.00', 162, 1, NULL, 3),
(1637, '151030', 'Tomas', NULL, NULL, 1127, '294.00', 162, 1, NULL, 3),
(1638, '151031', 'Tupe', NULL, NULL, 649, '318.00', 162, 1, NULL, 3),
(1639, '151032', 'Viñac', NULL, NULL, 1848, '157.00', 162, 1, NULL, 3),
(1640, '151033', 'Vitis', NULL, NULL, 630, '104.00', 162, 1, NULL, 3),
(1641, '160101', 'Iquitos', NULL, NULL, 150484, '361.00', 163, 1, NULL, 3),
(1642, '160102', 'Alto Nanay', NULL, NULL, 2784, '14526.00', 163, 1, NULL, 3),
(1643, '160103', 'Fernando Lores', NULL, NULL, 20225, '4546.00', 163, 1, NULL, 3),
(1644, '160104', 'Indiana', NULL, NULL, 11301, '3348.00', 163, 1, NULL, 3),
(1645, '160105', 'Las Amazonas', NULL, NULL, 9885, '6690.00', 163, 1, NULL, 3),
(1646, '160106', 'Mazan', NULL, NULL, 13779, '10082.00', 163, 1, NULL, 3),
(1647, '160107', 'Napo', NULL, NULL, 16286, '24643.00', 163, 1, NULL, 3),
(1648, '160108', 'Punchana', NULL, NULL, 91128, '1598.00', 163, 1, NULL, 3),
(1649, '160109', 'Putumayo', NULL, NULL, 6187, '34972.00', 163, 1, NULL, 3),
(1650, '160110', 'Torres Causana', NULL, NULL, 5152, '7550.00', 163, 1, NULL, 3),
(1651, '160112', 'Belen', NULL, NULL, 75685, '647.00', 163, 1, NULL, 3),
(1652, '160113', 'San Juan Bautista', NULL, NULL, 154696, '3166.00', 163, 1, NULL, 3),
(1653, '160114', 'Teniente Manuel Clavero', NULL, NULL, 5657, '9580.00', 163, 1, NULL, 3),
(1654, '160201', 'Yurimaguas', NULL, NULL, 72170, '2718.00', 164, 1, NULL, 3),
(1655, '160202', 'Balsapuerto', NULL, NULL, 17436, '2939.00', 164, 1, NULL, 3),
(1656, '160205', 'Jeberos', NULL, NULL, 5271, '5282.00', 164, 1, NULL, 3),
(1657, '160206', 'Lagunas', NULL, NULL, 14308, '6241.00', 164, 1, NULL, 3),
(1658, '160210', 'Santa Cruz', NULL, NULL, 4449, '1112.00', 164, 1, NULL, 3),
(1659, '160211', 'Teniente Cesar Lopez Rojas', NULL, NULL, 6587, '1521.00', 164, 1, NULL, 3),
(1660, '160301', 'Nauta', NULL, NULL, 30086, '6782.00', 165, 1, NULL, 3),
(1661, '160302', 'Parinari', NULL, NULL, 7264, '13167.00', 165, 1, NULL, 3),
(1662, '160303', 'Tigre', NULL, NULL, 8421, '20174.00', 165, 1, NULL, 3),
(1663, '160304', 'Trompeteros', NULL, NULL, 10745, '12472.00', 165, 1, NULL, 3),
(1664, '160305', 'Urarinas', NULL, NULL, 14716, '16040.00', 165, 1, NULL, 3),
(1665, '160401', 'Ramon Castilla', NULL, NULL, 24141, '7104.00', 166, 1, NULL, 3),
(1666, '160402', 'Pebas', NULL, NULL, 17061, '11595.00', 166, 1, NULL, 3),
(1667, '160403', 'Yavari', NULL, NULL, 15638, '14061.00', 166, 1, NULL, 3),
(1668, '160404', 'San Pablo', NULL, NULL, 16069, '5109.00', 166, 1, NULL, 3),
(1669, '160501', 'Requena', NULL, NULL, 30156, '3088.00', 167, 1, NULL, 3),
(1670, '160502', 'Alto Tapiche', NULL, NULL, 2106, '8862.00', 167, 1, NULL, 3),
(1671, '160503', 'Capelo', NULL, NULL, 4454, '856.00', 167, 1, NULL, 3),
(1672, '160504', 'Emilio San Martin', NULL, NULL, 7488, '4648.00', 167, 1, NULL, 3),
(1673, '160505', 'Maquia', NULL, NULL, 8371, '4872.00', 167, 1, NULL, 3),
(1674, '160506', 'Puinahua', NULL, NULL, 6017, '6046.00', 167, 1, NULL, 3),
(1675, '160507', 'Saquena', NULL, NULL, 4927, '2114.00', 167, 1, NULL, 3),
(1676, '160508', 'Soplin', NULL, NULL, 690, '4788.00', 167, 1, NULL, 3),
(1677, '160509', 'Tapiche', NULL, NULL, 1211, '2047.00', 167, 1, NULL, 3),
(1678, '160510', 'Jenaro Herrera', NULL, NULL, 5632, '1542.00', 167, 1, NULL, 3),
(1679, '160511', 'Yaquerana', NULL, NULL, 2989, '11205.00', 167, 1, NULL, 3),
(1680, '160601', 'Contamana', NULL, NULL, 27273, '10852.00', 168, 1, NULL, 3),
(1681, '160602', 'Inahuaya', NULL, NULL, 2659, '657.00', 168, 1, NULL, 3),
(1682, '160603', 'Padre Marquez', NULL, NULL, 7597, '2517.00', 168, 1, NULL, 3),
(1683, '160604', 'Pampa Hermosa', NULL, NULL, 10630, '7468.00', 168, 1, NULL, 3),
(1684, '160605', 'Sarayacu', NULL, NULL, 16569, '6408.00', 168, 1, NULL, 3),
(1685, '160606', 'Vargas Guerra', NULL, NULL, 8932, '1877.00', 168, 1, NULL, 3),
(1686, '160701', 'Barranca', NULL, NULL, 13608, '6803.00', 169, 1, NULL, 3),
(1687, '160702', 'Cahuapanas', NULL, NULL, 8331, '4957.00', 169, 1, NULL, 3),
(1688, '160703', 'Manseriche', NULL, NULL, 10370, '4315.00', 169, 1, NULL, 3),
(1689, '160704', 'Morona', NULL, NULL, 13024, '9653.00', 169, 1, NULL, 3),
(1690, '160705', 'Pastaza', NULL, NULL, 6363, '8444.00', 169, 1, NULL, 3),
(1691, '160706', 'Andoas', NULL, NULL, 12364, '12519.00', 169, 1, NULL, 3),
(1692, '170101', 'Tambopata', NULL, NULL, 78378, '20677.00', 170, 1, NULL, 3),
(1693, '170102', 'Inambari', NULL, NULL, 10110, '4878.00', 170, 1, NULL, 3),
(1694, '170103', 'Las Piedras', NULL, NULL, 5826, '7492.00', 170, 1, NULL, 3),
(1695, '170104', 'Laberinto', NULL, NULL, 5091, '2815.00', 170, 1, NULL, 3),
(1696, '170201', 'Manu', NULL, NULL, 3118, '8609.00', 171, 1, NULL, 3),
(1697, '170202', 'Fitzcarrald', NULL, NULL, 1536, '10526.00', 171, 1, NULL, 3),
(1698, '170203', 'Madre De Dios', NULL, NULL, 12810, '7754.00', 171, 1, NULL, 3),
(1699, '170204', 'Huepetuhe', NULL, NULL, 6633, '1499.00', 171, 1, NULL, 3),
(1700, '170301', 'Iñapari', NULL, NULL, 1555, '14343.00', 172, 1, NULL, 3),
(1701, '170302', 'Iberia', NULL, NULL, 8836, '2556.00', 172, 1, NULL, 3),
(1702, '170303', 'Tahuamanu', NULL, NULL, 3423, '3430.00', 172, 1, NULL, 3),
(1703, '180101', 'Moquegua', NULL, NULL, 57243, '3948.00', 173, 1, NULL, 3),
(1704, '180102', 'Carumas', NULL, NULL, 5602, '2263.00', 173, 1, NULL, 3),
(1705, '180103', 'Cuchumbaya', NULL, NULL, 2177, '69.00', 173, 1, NULL, 3),
(1706, '180104', 'Samegua', NULL, NULL, 6496, '64.00', 173, 1, NULL, 3),
(1707, '180105', 'San Cristobal', NULL, NULL, 4058, '542.00', 173, 1, NULL, 3),
(1708, '180106', 'Torata', NULL, NULL, 5874, '1772.00', 173, 1, NULL, 3),
(1709, '180201', 'Omate', NULL, NULL, 4477, '255.00', 174, 1, NULL, 3),
(1710, '180202', 'Chojata', NULL, NULL, 2573, '858.00', 174, 1, NULL, 3),
(1711, '180203', 'Coalaque', NULL, NULL, 1125, '247.00', 174, 1, NULL, 3),
(1712, '180204', 'Ichuña', NULL, NULL, 4826, '999.00', 174, 1, NULL, 3),
(1713, '180205', 'La Capilla', NULL, NULL, 2213, '775.00', 174, 1, NULL, 3),
(1714, '180206', 'Lloque', NULL, NULL, 1975, '255.00', 174, 1, NULL, 3),
(1715, '180207', 'Matalaque', NULL, NULL, 1187, '562.00', 174, 1, NULL, 3),
(1716, '180208', 'Puquina', NULL, NULL, 2521, '593.00', 174, 1, NULL, 3),
(1717, '180209', 'Quinistaquillas', NULL, NULL, 1410, '194.00', 174, 1, NULL, 3),
(1718, '180210', 'Ubinas', NULL, NULL, 3649, '877.00', 174, 1, NULL, 3),
(1719, '180211', 'Yunga', NULL, NULL, 2377, '112.00', 174, 1, NULL, 3),
(1720, '180301', 'Ilo', NULL, NULL, 66876, '294.00', 175, 1, NULL, 3),
(1721, '180302', 'El Algarrobal', NULL, NULL, 320, '737.00', 175, 1, NULL, 3),
(1722, '180303', 'Pacocha', NULL, NULL, 3498, '334.00', 175, 1, NULL, 3),
(1723, '190101', 'Chaupimarca', NULL, NULL, 26085, '15.00', 176, 1, NULL, 3),
(1724, '190102', 'Huachon', NULL, NULL, 4722, '466.00', 176, 1, NULL, 3),
(1725, '190103', 'Huariaca', NULL, NULL, 8257, '123.00', 176, 1, NULL, 3),
(1726, '190104', 'Huayllay', NULL, NULL, 11412, '1018.00', 176, 1, NULL, 3),
(1727, '190105', 'Ninacaca', NULL, NULL, 3418, '541.00', 176, 1, NULL, 3),
(1728, '190106', 'Pallanchacra', NULL, NULL, 4866, '72.00', 176, 1, NULL, 3),
(1729, '190107', 'Paucartambo', NULL, NULL, 24303, '721.00', 176, 1, NULL, 3),
(1730, '190108', 'San Francisco De Asis De Yarusyacan', NULL, NULL, 9901, '122.00', 176, 1, NULL, 3),
(1731, '190109', 'Simon Bolivar', NULL, NULL, 11913, '689.00', 176, 1, NULL, 3),
(1732, '190110', 'Ticlacayan', NULL, NULL, 13285, '558.00', 176, 1, NULL, 3),
(1733, '190111', 'Tinyahuarco', NULL, NULL, 6286, '96.00', 176, 1, NULL, 3),
(1734, '190112', 'Vicco', NULL, NULL, 2292, '173.00', 176, 1, NULL, 3),
(1735, '190113', 'Yanacancha', NULL, NULL, 30570, '156.00', 176, 1, NULL, 3),
(1736, '190201', 'Yanahuanca', NULL, NULL, 12922, '745.00', 177, 1, NULL, 3),
(1737, '190202', 'Chacayan', NULL, NULL, 4295, '163.00', 177, 1, NULL, 3),
(1738, '190203', 'Goyllarisquizga', NULL, NULL, 3896, '16.00', 177, 1, NULL, 3),
(1739, '190204', 'Paucar', NULL, NULL, 1797, '108.00', 177, 1, NULL, 3),
(1740, '190205', 'San Pedro De Pillao', NULL, NULL, 1823, '74.00', 177, 1, NULL, 3),
(1741, '190206', 'Santa Ana De Tusi', NULL, NULL, 22945, '285.00', 177, 1, NULL, 3),
(1742, '190207', 'Tapuc', NULL, NULL, 4360, '49.00', 177, 1, NULL, 3),
(1743, '190208', 'Vilcabamba', NULL, NULL, 1609, '83.00', 177, 1, NULL, 3),
(1744, '190301', 'Oxapampa', NULL, NULL, 14257, '414.00', 178, 1, NULL, 3),
(1745, '190302', 'Chontabamba', NULL, NULL, 3504, '442.00', 178, 1, NULL, 3),
(1746, '190303', 'Huancabamba', NULL, NULL, 6536, '1245.00', 178, 1, NULL, 3),
(1747, '190304', 'Palcazu', NULL, NULL, 10710, '2868.00', 178, 1, NULL, 3),
(1748, '190305', 'Pozuzo', NULL, NULL, 9342, '1249.00', 178, 1, NULL, 3),
(1749, '190306', 'Puerto Bermudez', NULL, NULL, 28669, '10692.00', 178, 1, NULL, 3),
(1750, '190307', 'Villa Rica', NULL, NULL, 20183, '786.00', 178, 1, NULL, 3),
(1751, '200101', 'Piura', NULL, NULL, 301311, '318.00', 179, 1, NULL, 3),
(1752, '200104', 'Castilla', NULL, NULL, 143203, '660.00', 179, 1, NULL, 3),
(1753, '200105', 'Catacaos', NULL, NULL, 72779, '2535.00', 179, 1, NULL, 3),
(1754, '200107', 'Cura Mori', NULL, NULL, 18639, '196.00', 179, 1, NULL, 3),
(1755, '200108', 'El Tallan', NULL, NULL, 4962, '106.00', 179, 1, NULL, 3),
(1756, '200109', 'La Arena', NULL, NULL, 37607, '168.00', 179, 1, NULL, 3),
(1757, '200110', 'La Union', NULL, NULL, 40613, '209.00', 179, 1, NULL, 3),
(1758, '200111', 'Las Lomas', NULL, NULL, 26768, '510.00', 179, 1, NULL, 3),
(1759, '200114', 'Tambo Grande', NULL, NULL, 119086, '1447.00', 179, 1, NULL, 3),
(1760, '200201', 'Ayabaca', NULL, NULL, 38339, '1544.00', 180, 1, NULL, 3),
(1761, '200202', 'Frias', NULL, NULL, 24203, '559.00', 180, 1, NULL, 3),
(1762, '200203', 'Jilili', NULL, NULL, 2775, '99.00', 180, 1, NULL, 3),
(1763, '200204', 'Lagunas', NULL, NULL, 7251, '196.00', 180, 1, NULL, 3),
(1764, '200205', 'Montero', NULL, NULL, 6683, '130.00', 180, 1, NULL, 3),
(1765, '200206', 'Pacaipampa', NULL, NULL, 24796, '970.00', 180, 1, NULL, 3),
(1766, '200207', 'Paimas', NULL, NULL, 10332, '325.00', 180, 1, NULL, 3),
(1767, '200208', 'Sapillica', NULL, NULL, 12194, '271.00', 180, 1, NULL, 3),
(1768, '200209', 'Sicchez', NULL, NULL, 1897, '34.00', 180, 1, NULL, 3),
(1769, '200210', 'Suyo', NULL, NULL, 12287, '1069.00', 180, 1, NULL, 3),
(1770, '200301', 'Huancabamba', NULL, NULL, 30404, '445.00', 181, 1, NULL, 3),
(1771, '200302', 'Canchaque', NULL, NULL, 8235, '313.00', 181, 1, NULL, 3),
(1772, '200303', 'El Carmen De La Frontera', NULL, NULL, 13864, '653.00', 181, 1, NULL, 3),
(1773, '200304', 'Huarmaca', NULL, NULL, 41238, '1938.00', 181, 1, NULL, 3),
(1774, '200305', 'Lalaquiz', NULL, NULL, 4626, '146.00', 181, 1, NULL, 3),
(1775, '200306', 'San Miguel De El Faique', NULL, NULL, 8994, '208.00', 181, 1, NULL, 3),
(1776, '200307', 'Sondor', NULL, NULL, 8564, '344.00', 181, 1, NULL, 3),
(1777, '200308', 'Sondorillo', NULL, NULL, 10758, '230.00', 181, 1, NULL, 3),
(1778, '200401', 'Chulucanas', NULL, NULL, 76214, '862.00', 182, 1, NULL, 3),
(1779, '200402', 'Buenos Aires', NULL, NULL, 7985, '246.00', 182, 1, NULL, 3),
(1780, '200403', 'Chalaco', NULL, NULL, 8992, '149.00', 182, 1, NULL, 3),
(1781, '200404', 'La Matanza', NULL, NULL, 12761, '1036.00', 182, 1, NULL, 3),
(1782, '200405', 'Morropon', NULL, NULL, 14099, '171.00', 182, 1, NULL, 3),
(1783, '200406', 'Salitral', NULL, NULL, 8409, '610.00', 182, 1, NULL, 3),
(1784, '200407', 'San Juan De Bigote', NULL, NULL, 6566, '250.00', 182, 1, NULL, 3),
(1785, '200408', 'Santa Catalina De Mossa', NULL, NULL, 4095, '80.00', 182, 1, NULL, 3),
(1786, '200409', 'Santo Domingo', NULL, NULL, 7207, '190.00', 182, 1, NULL, 3),
(1787, '200410', 'Yamango', NULL, NULL, 9567, '217.00', 182, 1, NULL, 3),
(1788, '200501', 'Paita', NULL, NULL, 93147, '731.00', 183, 1, NULL, 3),
(1789, '200502', 'Amotape', NULL, NULL, 2310, '61.00', 183, 1, NULL, 3),
(1790, '200503', 'Arenal', NULL, NULL, 1006, '8.00', 183, 1, NULL, 3),
(1791, '200504', 'Colan', NULL, NULL, 12429, '123.00', 183, 1, NULL, 3),
(1792, '200505', 'La Huaca', NULL, NULL, 11696, '596.00', 183, 1, NULL, 3),
(1793, '200506', 'Tamarindo', NULL, NULL, 4555, '66.00', 183, 1, NULL, 3),
(1794, '200507', 'Vichayal', NULL, NULL, 4761, '158.00', 183, 1, NULL, 3),
(1795, '200601', 'Sullana', NULL, NULL, 176804, '487.00', 184, 1, NULL, 3),
(1796, '200602', 'Bellavista', NULL, NULL, 38071, '2.00', 184, 1, NULL, 3),
(1797, '200603', 'Ignacio Escudero', NULL, NULL, 19987, '183.00', 184, 1, NULL, 3),
(1798, '200604', 'Lancones', NULL, NULL, 13245, '2164.00', 184, 1, NULL, 3),
(1799, '200605', 'Marcavelica', NULL, NULL, 28876, '1647.00', 184, 1, NULL, 3),
(1800, '200606', 'Miguel Checa', NULL, NULL, 8639, '456.00', 184, 1, NULL, 3),
(1801, '200607', 'Querecotillo', NULL, NULL, 25290, '282.00', 184, 1, NULL, 3),
(1802, '200608', 'Salitral', NULL, NULL, 6663, '31.00', 184, 1, NULL, 3),
(1803, '200701', 'Pariñas', NULL, NULL, 89877, '1122.00', 185, 1, NULL, 3),
(1804, '200702', 'El Alto', NULL, NULL, 7056, '479.00', 185, 1, NULL, 3),
(1805, '200703', 'La Brea', NULL, NULL, 11817, '830.00', 185, 1, NULL, 3),
(1806, '200704', 'Lobitos', NULL, NULL, 1646, '237.00', 185, 1, NULL, 3),
(1807, '200705', 'Los Organos', NULL, NULL, 9411, '163.00', 185, 1, NULL, 3),
(1808, '200706', 'Mancora', NULL, NULL, 12888, '97.00', 185, 1, NULL, 3),
(1809, '200801', 'Sechura', NULL, NULL, 42974, '5731.00', 186, 1, NULL, 3),
(1810, '200802', 'Bellavista De La Union', NULL, NULL, 4303, '15.00', 186, 1, NULL, 3),
(1811, '200803', 'Bernal', NULL, NULL, 7276, '70.00', 186, 1, NULL, 3),
(1812, '200804', 'Cristo Nos Valga', NULL, NULL, 3878, '262.00', 186, 1, NULL, 3),
(1813, '200805', 'Vice', NULL, NULL, 14108, '334.00', 186, 1, NULL, 3),
(1814, '200806', 'Rinconada Llicuar', NULL, NULL, 3113, '19.00', 186, 1, NULL, 3),
(1815, '210101', 'Puno', NULL, NULL, 141064, '460.00', 187, 1, NULL, 3),
(1816, '210102', 'Acora', NULL, NULL, 28189, '1937.00', 187, 1, NULL, 3),
(1817, '210103', 'Amantani', NULL, NULL, 4447, '163.00', 187, 1, NULL, 3),
(1818, '210104', 'Atuncolla', NULL, NULL, 5653, '159.00', 187, 1, NULL, 3),
(1819, '210105', 'Capachica', NULL, NULL, 11336, '99.00', 187, 1, NULL, 3),
(1820, '210106', 'Chucuito', NULL, NULL, 7012, '89.00', 187, 1, NULL, 3),
(1821, '210107', 'Coata', NULL, NULL, 8034, '104.00', 187, 1, NULL, 3),
(1822, '210108', 'Huata', NULL, NULL, 10353, '129.00', 187, 1, NULL, 3),
(1823, '210109', 'Mañazo', NULL, NULL, 5369, '402.00', 187, 1, NULL, 3),
(1824, '210110', 'Paucarcolla', NULL, NULL, 5135, '175.00', 187, 1, NULL, 3),
(1825, '210111', 'Pichacani', NULL, NULL, 5324, '1639.00', 187, 1, NULL, 3),
(1826, '210112', 'Plateria', NULL, NULL, 7743, '249.00', 187, 1, NULL, 3),
(1827, '210113', 'San Antonio', NULL, NULL, 3799, '337.00', 187, 1, NULL, 3),
(1828, '210114', 'Tiquillaca', NULL, NULL, 1790, '489.00', 187, 1, NULL, 3),
(1829, '210115', 'Vilque', NULL, NULL, 3129, '193.00', 187, 1, NULL, 3),
(1830, '210201', 'Azangaro', NULL, NULL, 28195, '721.00', 188, 1, NULL, 3),
(1831, '210202', 'Achaya', NULL, NULL, 4479, '126.00', 188, 1, NULL, 3),
(1832, '210203', 'Arapa', NULL, NULL, 7483, '337.00', 188, 1, NULL, 3),
(1833, '210204', 'Asillo', NULL, NULL, 17407, '404.00', 188, 1, NULL, 3),
(1834, '210205', 'Caminaca', NULL, NULL, 3564, '147.00', 188, 1, NULL, 3),
(1835, '210206', 'Chupa', NULL, NULL, 13045, '151.00', 188, 1, NULL, 3),
(1836, '210207', 'Jose Domingo Choquehuanca', NULL, NULL, 5458, '67.00', 188, 1, NULL, 3),
(1837, '210208', 'Muñani', NULL, NULL, 8180, '785.00', 188, 1, NULL, 3),
(1838, '210209', 'Potoni', NULL, NULL, 6456, '623.00', 188, 1, NULL, 3),
(1839, '210210', 'Saman', NULL, NULL, 14249, '337.00', 188, 1, NULL, 3),
(1840, '210211', 'San Anton', NULL, NULL, 9978, '513.00', 188, 1, NULL, 3),
(1841, '210212', 'San Jose', NULL, NULL, 5751, '397.00', 188, 1, NULL, 3),
(1842, '210213', 'San Juan De Salinas', NULL, NULL, 4325, '104.00', 188, 1, NULL, 3),
(1843, '210214', 'Santiago De Pupuja', NULL, NULL, 5172, '319.00', 188, 1, NULL, 3),
(1844, '210215', 'Tirapata', NULL, NULL, 3077, '200.00', 188, 1, NULL, 3),
(1845, '210301', 'Macusani', NULL, NULL, 12869, '1017.00', 189, 1, NULL, 3),
(1846, '210302', 'Ajoyani', NULL, NULL, 2079, '427.00', 189, 1, NULL, 3),
(1847, '210303', 'Ayapata', NULL, NULL, 11975, '776.00', 189, 1, NULL, 3),
(1848, '210304', 'Coasa', NULL, NULL, 15879, '3071.00', 189, 1, NULL, 3),
(1849, '210305', 'Corani', NULL, NULL, 3916, '890.00', 189, 1, NULL, 3),
(1850, '210306', 'Crucero', NULL, NULL, 9208, '854.00', 189, 1, NULL, 3),
(1851, '210307', 'Ituata', NULL, NULL, 6341, '1251.00', 189, 1, NULL, 3),
(1852, '210308', 'Ollachea', NULL, NULL, 5566, '709.00', 189, 1, NULL, 3),
(1853, '210309', 'San Gaban', NULL, NULL, 4109, '2042.00', 189, 1, NULL, 3),
(1854, '210310', 'Usicayos', NULL, NULL, 23448, '654.00', 189, 1, NULL, 3),
(1855, '210401', 'Juli', NULL, NULL, 21462, '772.00', 190, 1, NULL, 3),
(1856, '210402', 'Desaguadero', NULL, NULL, 31524, '177.00', 190, 1, NULL, 3),
(1857, '210403', 'Huacullani', NULL, NULL, 23188, '627.00', 190, 1, NULL, 3),
(1858, '210404', 'Kelluyo', NULL, NULL, 25415, '487.00', 190, 1, NULL, 3),
(1859, '210405', 'Pisacoma', NULL, NULL, 13608, '957.00', 190, 1, NULL, 3),
(1860, '210406', 'Pomata', NULL, NULL, 16094, '401.00', 190, 1, NULL, 3),
(1861, '210407', 'Zepita', NULL, NULL, 18948, '525.00', 190, 1, NULL, 3),
(1862, '210501', 'Ilave', NULL, NULL, 57905, '917.00', 191, 1, NULL, 3),
(1863, '210502', 'Capazo', NULL, NULL, 2203, '1043.00', 191, 1, NULL, 3),
(1864, '210503', 'Pilcuyo', NULL, NULL, 12850, '153.00', 191, 1, NULL, 3),
(1865, '210504', 'Santa Rosa', NULL, NULL, 7735, '2698.00', 191, 1, NULL, 3),
(1866, '210505', 'Conduriri', NULL, NULL, 4387, '841.00', 191, 1, NULL, 3),
(1867, '210601', 'Huancane', NULL, NULL, 18253, '387.00', 192, 1, NULL, 3),
(1868, '210602', 'Cojata', NULL, NULL, 4239, '880.00', 192, 1, NULL, 3),
(1869, '210603', 'Huatasani', NULL, NULL, 5371, '107.00', 192, 1, NULL, 3),
(1870, '210604', 'Inchupalla', NULL, NULL, 3275, '297.00', 192, 1, NULL, 3),
(1871, '210605', 'Pusi', NULL, NULL, 6278, '147.00', 192, 1, NULL, 3),
(1872, '210606', 'Rosaspata', NULL, NULL, 5106, '305.00', 192, 1, NULL, 3),
(1873, '210607', 'Taraco', NULL, NULL, 14014, '197.00', 192, 1, NULL, 3),
(1874, '210608', 'Vilque Chico', NULL, NULL, 8290, '507.00', 192, 1, NULL, 3),
(1875, '210701', 'Lampa', NULL, NULL, 10420, '660.00', 193, 1, NULL, 3),
(1876, '210702', 'Cabanilla', NULL, NULL, 5325, '385.00', 193, 1, NULL, 3),
(1877, '210703', 'Calapuja', NULL, NULL, 1473, '141.00', 193, 1, NULL, 3),
(1878, '210704', 'Nicasio', NULL, NULL, 2666, '134.00', 193, 1, NULL, 3),
(1879, '210705', 'Ocuviri', NULL, NULL, 3059, '877.00', 193, 1, NULL, 3),
(1880, '210706', 'Palca', NULL, NULL, 2855, '497.00', 193, 1, NULL, 3),
(1881, '210707', 'Paratia', NULL, NULL, 8778, '746.00', 193, 1, NULL, 3),
(1882, '210708', 'Pucara', NULL, NULL, 5342, '526.00', 193, 1, NULL, 3),
(1883, '210709', 'Santa Lucia', NULL, NULL, 7485, '1590.00', 193, 1, NULL, 3),
(1884, '210710', 'Vilavila', NULL, NULL, 4125, '162.00', 193, 1, NULL, 3),
(1885, '210801', 'Ayaviri', NULL, NULL, 22397, '1017.00', 194, 1, NULL, 3),
(1886, '210802', 'Antauta', NULL, NULL, 4516, '655.00', 194, 1, NULL, 3),
(1887, '210803', 'Cupi', NULL, NULL, 3274, '217.00', 194, 1, NULL, 3),
(1888, '210804', 'Llalli', NULL, NULL, 4719, '229.00', 194, 1, NULL, 3),
(1889, '210805', 'Macari', NULL, NULL, 8532, '692.00', 194, 1, NULL, 3),
(1890, '210806', 'Nuñoa', NULL, NULL, 11017, '2199.00', 194, 1, NULL, 3),
(1891, '210807', 'Orurillo', NULL, NULL, 10805, '397.00', 194, 1, NULL, 3),
(1892, '210808', 'Santa Rosa', NULL, NULL, 7342, '804.00', 194, 1, NULL, 3),
(1893, '210809', 'Umachiri', NULL, NULL, 4384, '331.00', 194, 1, NULL, 3),
(1894, '210901', 'Moho', NULL, NULL, 15656, '506.00', 195, 1, NULL, 3),
(1895, '210902', 'Conima', NULL, NULL, 2909, '68.00', 195, 1, NULL, 3),
(1896, '210903', 'Huayrapata', NULL, NULL, 4258, '401.00', 195, 1, NULL, 3),
(1897, '210904', 'Tilali', NULL, NULL, 2649, '52.00', 195, 1, NULL, 3),
(1898, '211001', 'Putina', NULL, NULL, 26628, '1034.00', 196, 1, NULL, 3),
(1899, '211002', 'Ananea', NULL, NULL, 32285, '979.00', 196, 1, NULL, 3),
(1900, '211003', 'Pedro Vilca Apaza', NULL, NULL, 2934, '143.00', 196, 1, NULL, 3),
(1901, '211004', 'Quilcapuncu', NULL, NULL, 5743, '525.00', 196, 1, NULL, 3),
(1902, '211005', 'Sina', NULL, NULL, 1660, '465.00', 196, 1, NULL, 3),
(1903, '211101', 'Juliaca', NULL, NULL, 278444, '526.00', 197, 1, NULL, 3),
(1904, '211102', 'Cabana', NULL, NULL, 4224, '193.00', 197, 1, NULL, 3),
(1905, '211103', 'Cabanillas', NULL, NULL, 5374, '1275.00', 197, 1, NULL, 3),
(1906, '211104', 'Caracoto', NULL, NULL, 5655, '287.00', 197, 1, NULL, 3),
(1907, '211201', 'Sandia', NULL, NULL, 12191, '696.00', 198, 1, NULL, 3),
(1908, '211202', 'Cuyocuyo', NULL, NULL, 4707, '503.00', 198, 1, NULL, 3),
(1909, '211203', 'Limbani', NULL, NULL, 4274, '2422.00', 198, 1, NULL, 3),
(1910, '211204', 'Patambuco', NULL, NULL, 3960, '474.00', 198, 1, NULL, 3),
(1911, '211205', 'Phara', NULL, NULL, 4844, '428.00', 198, 1, NULL, 3),
(1912, '211206', 'Quiaca', NULL, NULL, 2374, '413.00', 198, 1, NULL, 3),
(1913, '211207', 'San Juan Del Oro', NULL, NULL, 13111, '196.00', 198, 1, NULL, 3),
(1914, '211208', 'Yanahuaya', NULL, NULL, 2269, '648.00', 198, 1, NULL, 3),
(1915, '211209', 'Alto Inambari', NULL, NULL, 9241, '1360.00', 198, 1, NULL, 3),
(1916, '211210', 'San Pedro De Putina Punco', NULL, NULL, 13577, '5415.00', 198, 1, NULL, 3),
(1917, '211301', 'Yunguyo', NULL, NULL, 27074, '175.00', 199, 1, NULL, 3),
(1918, '211302', 'Anapia', NULL, NULL, 3334, '121.00', 199, 1, NULL, 3),
(1919, '211303', 'Copani', NULL, NULL, 5021, '60.00', 199, 1, NULL, 3),
(1920, '211304', 'Cuturapi', NULL, NULL, 1214, '24.00', 199, 1, NULL, 3),
(1921, '211305', 'Ollaraya', NULL, NULL, 5336, '27.00', 199, 1, NULL, 3),
(1922, '211306', 'Tinicachi', NULL, NULL, 1593, '4.00', 199, 1, NULL, 3),
(1923, '211307', 'Unicachi', NULL, NULL, 3824, '6.00', 199, 1, NULL, 3),
(1924, '220101', 'Moyobamba', NULL, NULL, 83475, '2805.00', 200, 1, NULL, 3),
(1925, '220102', 'Calzada', NULL, NULL, 4302, '116.00', 200, 1, NULL, 3),
(1926, '220103', 'Habana', NULL, NULL, 1993, '70.00', 200, 1, NULL, 3),
(1927, '220104', 'Jepelacio', NULL, NULL, 21164, '245.00', 200, 1, NULL, 3),
(1928, '220105', 'Soritor', NULL, NULL, 33851, '483.00', 200, 1, NULL, 3),
(1929, '220106', 'Yantalo', NULL, NULL, 3375, '72.00', 200, 1, NULL, 3),
(1930, '220201', 'Bellavista', NULL, NULL, 13395, '299.00', 201, 1, NULL, 3),
(1931, '220202', 'Alto Biavo', NULL, NULL, 7015, '5780.00', 201, 1, NULL, 3),
(1932, '220203', 'Bajo Biavo', NULL, NULL, 19335, '1004.00', 201, 1, NULL, 3),
(1933, '220204', 'Huallaga', NULL, NULL, 3003, '114.00', 201, 1, NULL, 3),
(1934, '220205', 'San Pablo', NULL, NULL, 8916, '351.00', 201, 1, NULL, 3),
(1935, '220206', 'San Rafael', NULL, NULL, 7290, '99.00', 201, 1, NULL, 3),
(1936, '220301', 'San Jose De Sisa', NULL, NULL, 11796, '275.00', 202, 1, NULL, 3),
(1937, '220302', 'Agua Blanca', NULL, NULL, 2359, '177.00', 202, 1, NULL, 3),
(1938, '220303', 'San Martin', NULL, NULL, 13022, '548.00', 202, 1, NULL, 3),
(1939, '220304', 'Santa Rosa', NULL, NULL, 10052, '244.00', 202, 1, NULL, 3),
(1940, '220305', 'Shatoja', NULL, NULL, 3120, '75.00', 202, 1, NULL, 3),
(1941, '220401', 'Saposoa', NULL, NULL, 11341, '513.00', 203, 1, NULL, 3),
(1942, '220402', 'Alto Saposoa', NULL, NULL, 3148, '1346.00', 203, 1, NULL, 3),
(1943, '220403', 'El Eslabon', NULL, NULL, 3753, '126.00', 203, 1, NULL, 3),
(1944, '220404', 'Piscoyacu', NULL, NULL, 3830, '184.00', 203, 1, NULL, 3),
(1945, '220405', 'Sacanche', NULL, NULL, 2584, '150.00', 203, 1, NULL, 3),
(1946, '220406', 'Tingo De Saposoa', NULL, NULL, 672, '41.00', 203, 1, NULL, 3),
(1947, '220501', 'Lamas', NULL, NULL, 12434, '96.00', 204, 1, NULL, 3),
(1948, '220502', 'Alonso De Alvarado', NULL, NULL, 18862, '247.00', 204, 1, NULL, 3),
(1949, '220503', 'Barranquita', NULL, NULL, 5085, '1075.00', 204, 1, NULL, 3),
(1950, '220504', 'Caynarachi', NULL, NULL, 7899, '1287.00', 204, 1, NULL, 3),
(1951, '220505', 'Cuñumbuqui', NULL, NULL, 4681, '193.00', 204, 1, NULL, 3),
(1952, '220506', 'Pinto Recodo', NULL, NULL, 10663, '819.00', 204, 1, NULL, 3),
(1953, '220507', 'Rumisapa', NULL, NULL, 2481, '41.00', 204, 1, NULL, 3),
(1954, '220508', 'San Roque De Cumbaza', NULL, NULL, 1450, '645.00', 204, 1, NULL, 3),
(1955, '220509', 'Shanao', NULL, NULL, 3460, '25.00', 204, 1, NULL, 3),
(1956, '220510', 'Tabalosos', NULL, NULL, 13130, '397.00', 204, 1, NULL, 3),
(1957, '220511', 'Zapatero', NULL, NULL, 4776, '178.00', 204, 1, NULL, 3),
(1958, '220601', 'Juanjui', NULL, NULL, 26364, '353.00', 205, 1, NULL, 3),
(1959, '220602', 'Campanilla', NULL, NULL, 7642, '2160.00', 205, 1, NULL, 3),
(1960, '220603', 'Huicungo', NULL, NULL, 6481, '9799.00', 205, 1, NULL, 3),
(1961, '220604', 'Pachiza', NULL, NULL, 4180, '1767.00', 205, 1, NULL, 3),
(1962, '220605', 'Pajarillo', NULL, NULL, 5941, '357.00', 205, 1, NULL, 3),
(1963, '220701', 'Picota', NULL, NULL, 8094, '185.00', 206, 1, NULL, 3),
(1964, '220702', 'Buenos Aires', NULL, NULL, 3202, '291.00', 206, 1, NULL, 3),
(1965, '220703', 'Caspisapa', NULL, NULL, 2052, '89.00', 206, 1, NULL, 3),
(1966, '220704', 'Pilluana', NULL, NULL, 713, '69.00', 206, 1, NULL, 3),
(1967, '220705', 'Pucacaca', NULL, NULL, 2456, '222.00', 206, 1, NULL, 3),
(1968, '220706', 'San Cristobal', NULL, NULL, 1375, '24.00', 206, 1, NULL, 3),
(1969, '220707', 'San Hilarion', NULL, NULL, 5458, '96.00', 206, 1, NULL, 3),
(1970, '220708', 'Shamboyacu', NULL, NULL, 11449, '311.00', 206, 1, NULL, 3),
(1971, '220709', 'Tingo De Ponasa', NULL, NULL, 4659, '416.00', 206, 1, NULL, 3),
(1972, '220710', 'Tres Unidos', NULL, NULL, 5075, '352.00', 206, 1, NULL, 3),
(1973, '220801', 'Rioja', NULL, NULL, 23472, '205.00', 207, 1, NULL, 3),
(1974, '220802', 'Awajun', NULL, NULL, 11630, '475.00', 207, 1, NULL, 3),
(1975, '220803', 'Elias Soplin Vargas', NULL, NULL, 13156, '159.00', 207, 1, NULL, 3),
(1976, '220804', 'Nueva Cajamarca', NULL, NULL, 45241, '332.00', 207, 1, NULL, 3),
(1977, '220805', 'Pardo Miguel', NULL, NULL, 22345, '1151.00', 207, 1, NULL, 3),
(1978, '220806', 'Posic', NULL, NULL, 1633, '61.00', 207, 1, NULL, 3),
(1979, '220807', 'San Fernando', NULL, NULL, 3389, '75.00', 207, 1, NULL, 3),
(1980, '220808', 'Yorongos', NULL, NULL, 3587, '81.00', 207, 1, NULL, 3),
(1981, '220809', 'Yuracyacu', NULL, NULL, 3914, '60.00', 207, 1, NULL, 3),
(1982, '220901', 'Tarapoto', NULL, NULL, 73015, '45.00', 208, 1, NULL, 3),
(1983, '220902', 'Alberto Leveau', NULL, NULL, 673, '51.00', 208, 1, NULL, 3),
(1984, '220903', 'Cacatachi', NULL, NULL, 3327, '45.00', 208, 1, NULL, 3),
(1985, '220904', 'Chazuta', NULL, NULL, 8111, '1415.00', 208, 1, NULL, 3),
(1986, '220905', 'Chipurana', NULL, NULL, 1794, '280.00', 208, 1, NULL, 3),
(1987, '220906', 'El Porvenir', NULL, NULL, 2692, '452.00', 208, 1, NULL, 3),
(1988, '220907', 'Huimbayoc', NULL, NULL, 3444, '1228.00', 208, 1, NULL, 3),
(1989, '220908', 'Juan Guerra', NULL, NULL, 3117, '208.00', 208, 1, NULL, 3),
(1990, '220909', 'La Banda De Shilcayo', NULL, NULL, 41114, '273.00', 208, 1, NULL, 3),
(1991, '220910', 'Morales', NULL, NULL, 29302, '53.00', 208, 1, NULL, 3),
(1992, '220911', 'Papaplaya', NULL, NULL, 2062, '888.00', 208, 1, NULL, 3),
(1993, '220912', 'San Antonio', NULL, NULL, 1340, '125.00', 208, 1, NULL, 3),
(1994, '220913', 'Sauce', NULL, NULL, 15840, '98.00', 208, 1, NULL, 3),
(1995, '220914', 'Shapaja', NULL, NULL, 1489, '235.00', 208, 1, NULL, 3),
(1996, '221001', 'Tocache', NULL, NULL, 25271, '1117.00', 209, 1, NULL, 3),
(1997, '221002', 'Nuevo Progreso', NULL, NULL, 11971, '819.00', 209, 1, NULL, 3),
(1998, '221003', 'Polvora', NULL, NULL, 13684, '2311.00', 209, 1, NULL, 3),
(1999, '221004', 'Shunte', NULL, NULL, 1006, '1083.00', 209, 1, NULL, 3),
(2000, '221005', 'Uchiza', NULL, NULL, 20318, '773.00', 209, 1, NULL, 3),
(2001, '230101', 'Tacna', NULL, NULL, 85228, '2429.00', 210, 1, NULL, 3),
(2002, '230102', 'Alto De La Alianza', NULL, NULL, 39180, '375.00', 210, 1, NULL, 3),
(2003, '230103', 'Calana', NULL, NULL, 3189, '117.00', 210, 1, NULL, 3),
(2004, '230104', 'Ciudad Nueva', NULL, NULL, 37671, '175.00', 210, 1, NULL, 3),
(2005, '230105', 'Inclan', NULL, NULL, 7684, '1440.00', 210, 1, NULL, 3),
(2006, '230106', 'Pachia', NULL, NULL, 1964, '614.00', 210, 1, NULL, 3),
(2007, '230107', 'Palca', NULL, NULL, 1669, '1433.00', 210, 1, NULL, 3),
(2008, '230108', 'Pocollay', NULL, NULL, 21278, '293.00', 210, 1, NULL, 3),
(2009, '230109', 'Sama', NULL, NULL, 2604, '1135.00', 210, 1, NULL, 3),
(2010, '230110', 'Coronel Gregorio Albarracin Lanchipa', NULL, NULL, 116497, '168.00', 210, 1, NULL, 3),
(2011, '230201', 'Candarave', NULL, NULL, 3001, '1378.00', 211, 1, NULL, 3),
(2012, '230202', 'Cairani', NULL, NULL, 1301, '176.00', 211, 1, NULL, 3),
(2013, '230203', 'Camilaca', NULL, NULL, 1514, '491.00', 211, 1, NULL, 3),
(2014, '230204', 'Curibaya', NULL, NULL, 180, '113.00', 211, 1, NULL, 3),
(2015, '230205', 'Huanuara', NULL, NULL, 898, '90.00', 211, 1, NULL, 3),
(2016, '230206', 'Quilahuani', NULL, NULL, 1201, '55.00', 211, 1, NULL, 3),
(2017, '230301', 'Locumba', NULL, NULL, 2601, '843.00', 212, 1, NULL, 3),
(2018, '230302', 'Ilabaya', NULL, NULL, 3008, '1056.00', 212, 1, NULL, 3),
(2019, '230303', 'Ite', NULL, NULL, 3425, '826.00', 212, 1, NULL, 3),
(2020, '230401', 'Tarata', NULL, NULL, 3252, '883.00', 213, 1, NULL, 3),
(2021, '230402', 'Heroes Albarracin', NULL, NULL, 655, '378.00', 213, 1, NULL, 3),
(2022, '230403', 'Estique', NULL, NULL, 710, '300.00', 213, 1, NULL, 3),
(2023, '230404', 'Estique-pampa', NULL, NULL, 666, '146.00', 213, 1, NULL, 3),
(2024, '230405', 'Sitajara', NULL, NULL, 697, '234.00', 213, 1, NULL, 3),
(2025, '230406', 'Susapaya', NULL, NULL, 768, '360.00', 213, 1, NULL, 3),
(2026, '230407', 'Tarucachi', NULL, NULL, 410, '106.00', 213, 1, NULL, 3),
(2027, '230408', 'Ticaco', NULL, NULL, 587, '355.00', 213, 1, NULL, 3),
(2028, '240101', 'Tumbes', NULL, NULL, 111683, '161.00', 214, 1, NULL, 3),
(2029, '240102', 'Corrales', NULL, NULL, 23868, '127.00', 214, 1, NULL, 3),
(2030, '240103', 'La Cruz', NULL, NULL, 9173, '66.00', 214, 1, NULL, 3),
(2031, '240104', 'Pampas De Hospital', NULL, NULL, 7050, '724.00', 214, 1, NULL, 3),
(2032, '240105', 'San Jacinto', NULL, NULL, 8541, '585.00', 214, 1, NULL, 3),
(2033, '240106', 'San Juan De La Virgen', NULL, NULL, 4089, '116.00', 214, 1, NULL, 3),
(2034, '240201', 'Zorritos', NULL, NULL, 12313, '648.00', 215, 1, NULL, 3),
(2035, '240202', 'Casitas', NULL, NULL, 2109, '855.00', 215, 1, NULL, 3),
(2036, '240203', 'Canoas De Punta Sal', NULL, NULL, 5474, '624.00', 215, 1, NULL, 3),
(2037, '240301', 'Zarumilla', NULL, NULL, 22257, '99.00', 216, 1, NULL, 3),
(2038, '240302', 'Aguas Verdes', NULL, NULL, 23480, '47.00', 216, 1, NULL, 3),
(2039, '240303', 'Matapalo', NULL, NULL, 2395, '389.00', 216, 1, NULL, 3),
(2040, '240304', 'Papayal', NULL, NULL, 5253, '206.00', 216, 1, NULL, 3),
(2041, '250101', 'Calleria', NULL, NULL, 154082, '11926.00', 217, 1, NULL, 3),
(2042, '250102', 'Campoverde', NULL, NULL, 15743, '1725.00', 217, 1, NULL, 3),
(2043, '250103', 'Iparia', NULL, NULL, 11826, '6735.00', 217, 1, NULL, 3),
(2044, '250104', 'Masisea', NULL, NULL, 12758, '15157.00', 217, 1, NULL, 3),
(2045, '250105', 'Yarinacocha', NULL, NULL, 97678, '243.00', 217, 1, NULL, 3),
(2046, '250106', 'Nueva Requena', NULL, NULL, 5538, '2249.00', 217, 1, NULL, 3),
(2047, '250107', 'Manantay', NULL, NULL, 80250, '1177.00', 217, 1, NULL, 3),
(2048, '250201', 'Raymondi', NULL, NULL, 34419, '14876.00', 218, 1, NULL, 3),
(2049, '250202', 'Sepahua', NULL, NULL, 8793, '7715.00', 218, 1, NULL, 3),
(2050, '250203', 'Tahuania', NULL, NULL, 8020, '7236.00', 218, 1, NULL, 3),
(2051, '250204', 'Yurua', NULL, NULL, 2587, '8758.00', 218, 1, NULL, 3),
(2052, '250301', 'Padre Abad', NULL, NULL, 25971, '4691.00', 219, 1, NULL, 3),
(2053, '250302', 'Irazola', NULL, NULL, 24833, '2736.00', 219, 1, NULL, 3),
(2054, '250303', 'Curimana', NULL, NULL, 8543, '1952.00', 219, 1, NULL, 3),
(2055, '250401', 'Purus', NULL, NULL, 4481, '18147.00', 220, 1, NULL, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` char(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `django_admin_log`
--

INSERT INTO `django_admin_log` (`id`, `action_time`, `object_id`, `object_repr`, `action_flag`, `change_message`, `content_type_id`, `user_id`) VALUES
(1, '2018-07-08 16:19:38.670472', 'ecff37b6-3985-4cc1-874a-2198fa019530', 'admin2', 1, '[{\"added\": {}}]', 15, 'd66e5064c6024126859be51a7e5b8e31'),
(2, '2018-07-08 16:20:00.001227', 'ecff37b6-3985-4cc1-874a-2198fa019530', 'admin2', 2, '[{\"changed\": {\"fields\": [\"password\"]}}]', 15, 'd66e5064c6024126859be51a7e5b8e31'),
(3, '2018-07-08 16:25:31.843941', 'ecff37b6-3985-4cc1-874a-2198fa019530', 'admin2', 2, '[{\"changed\": {\"fields\": [\"user_permissions\"]}}]', 15, 'd66e5064c6024126859be51a7e5b8e31'),
(4, '2018-07-08 16:31:18.219334', 'ecff37b6-3985-4cc1-874a-2198fa019530', 'admin2', 2, '[{\"changed\": {\"fields\": [\"user_permissions\"]}}]', 15, 'd66e5064c6024126859be51a7e5b8e31'),
(5, '2018-07-08 21:54:56.727850', 'ecff37b6-3985-4cc1-874a-2198fa019530', 'admin2', 2, '[{\"changed\": {\"fields\": [\"user_permissions\"]}}]', 15, 'd66e5064c6024126859be51a7e5b8e31'),
(6, '2018-07-08 22:06:33.664089', 'ecff37b6-3985-4cc1-874a-2198fa019530', 'admin2', 2, '[{\"changed\": {\"fields\": [\"user_permissions\"]}}]', 15, 'd66e5064c6024126859be51a7e5b8e31'),
(7, '2018-07-09 01:25:56.447277', 'ecff37b6-3985-4cc1-874a-2198fa019530', 'admin2', 2, '[{\"changed\": {\"fields\": [\"user_permissions\"]}}]', 15, 'd66e5064c6024126859be51a7e5b8e31'),
(8, '2018-07-09 01:31:34.997027', 'ecff37b6-3985-4cc1-874a-2198fa019530', 'admin2', 2, '[{\"changed\": {\"fields\": [\"user_permissions\"]}}]', 15, 'd66e5064c6024126859be51a7e5b8e31'),
(9, '2018-07-09 01:32:19.351805', 'ecff37b6-3985-4cc1-874a-2198fa019530', 'admin2', 2, '[{\"changed\": {\"fields\": [\"user_permissions\"]}}]', 15, 'd66e5064c6024126859be51a7e5b8e31'),
(10, '2018-07-09 01:32:57.230237', '1', 'Invitado', 1, '[{\"added\": {}}]', 3, 'd66e5064c6024126859be51a7e5b8e31'),
(11, '2018-07-09 01:33:08.644629', 'ecff37b6-3985-4cc1-874a-2198fa019530', 'admin2', 2, '[{\"changed\": {\"fields\": [\"groups\", \"user_permissions\"]}}]', 15, 'd66e5064c6024126859be51a7e5b8e31'),
(12, '2018-07-09 01:33:26.830326', 'ecff37b6-3985-4cc1-874a-2198fa019530', 'admin2', 2, '[{\"changed\": {\"fields\": [\"groups\"]}}]', 15, 'd66e5064c6024126859be51a7e5b8e31'),
(13, '2018-07-09 01:33:39.721149', 'ecff37b6-3985-4cc1-874a-2198fa019530', 'admin2', 2, '[{\"changed\": {\"fields\": [\"groups\"]}}]', 15, 'd66e5064c6024126859be51a7e5b8e31'),
(14, '2018-07-10 17:00:18.829834', 'ecff37b6-3985-4cc1-874a-2198fa019530', 'admin2', 2, '[{\"changed\": {\"fields\": [\"user_permissions\"]}}]', 15, 'd66e5064c6024126859be51a7e5b8e31'),
(15, '2018-07-10 17:01:47.718567', 'ecff37b6-3985-4cc1-874a-2198fa019530', 'admin2', 2, '[{\"changed\": {\"fields\": [\"groups\"]}}]', 15, 'd66e5064c6024126859be51a7e5b8e31'),
(16, '2018-07-10 17:02:42.336353', 'ecff37b6-3985-4cc1-874a-2198fa019530', 'admin2', 2, '[{\"changed\": {\"fields\": [\"user_permissions\"]}}]', 15, 'd66e5064c6024126859be51a7e5b8e31'),
(17, '2018-07-16 02:01:51.108808', '18', 'Persy Quiroz Menor', 2, '[{\"changed\": {\"fields\": [\"nombre_completo\", \"foto_logo\", \"foto_logo_rec\"]}}]', 14, 'd66e5064c6024126859be51a7e5b8e31'),
(18, '2018-07-16 02:09:27.199204', '18', 'Persy Quiroz Menor', 2, '[{\"changed\": {\"fields\": [\"foto_logo\", \"foto_logo_rec\"]}}]', 14, 'd66e5064c6024126859be51a7e5b8e31'),
(19, '2018-07-16 02:15:47.418945', '15', 'María Solano Flores', 2, '[{\"changed\": {\"fields\": [\"nombre_completo\", \"foto_logo\", \"foto_logo_rec\"]}}]', 14, 'd66e5064c6024126859be51a7e5b8e31'),
(20, '2018-07-16 02:16:33.052951', '16', 'Henry Goodman Solano', 2, '[{\"changed\": {\"fields\": [\"nombre_completo\", \"foto_logo\", \"foto_logo_rec\"]}}]', 14, 'd66e5064c6024126859be51a7e5b8e31'),
(21, '2018-07-16 02:17:09.972484', '26', 'Luis Alberto Lavado Llaro', 2, '[{\"changed\": {\"fields\": [\"nombre_completo\", \"foto_logo\", \"foto_logo_rec\"]}}]', 14, 'd66e5064c6024126859be51a7e5b8e31'),
(22, '2018-07-17 18:27:02.958009', '42', 'Universidad Peruana Unión', 2, '[{\"changed\": {\"fields\": [\"foto_logo\", \"foto_logo_rec\"]}}]', 14, 'd66e5064c6024126859be51a7e5b8e31'),
(23, '2018-07-20 09:40:15.023812', '2', 'Presidente', 2, '[{\"changed\": {\"fields\": [\"permissions\"]}}]', 3, 'd66e5064c6024126859be51a7e5b8e31'),
(24, '2018-07-20 10:46:34.565965', 'ecff37b6-3985-4cc1-874a-2198fa019530', 'admin2', 2, '[{\"changed\": {\"fields\": [\"user_permissions\"]}}]', 15, 'd66e5064c6024126859be51a7e5b8e31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1, 'admin', 'logentry'),
(3, 'auth', 'group'),
(2, 'auth', 'permission'),
(70, 'caja', 'accesocuenta'),
(71, 'caja', 'amortizacion'),
(72, 'caja', 'cuentaentidad'),
(73, 'caja', 'entidadfinanciera'),
(74, 'caja', 'estadocuenta'),
(75, 'caja', 'movimientocuenta'),
(76, 'caja', 'personacuenta'),
(77, 'caja', 'tipocuenta'),
(78, 'caja', 'tipocuentafinan'),
(79, 'caja', 'tipoentfinan'),
(80, 'caja', 'tipomediopago'),
(9, 'comun', 'clasificacioniiu'),
(35, 'comun', 'configdirecciontipopersona'),
(36, 'comun', 'configdocumentotipopersona'),
(7, 'comun', 'contador'),
(6, 'comun', 'continente'),
(26, 'comun', 'cuentacontacto'),
(38, 'comun', 'entidad'),
(37, 'comun', 'etiquetacontacto'),
(10, 'comun', 'mes'),
(8, 'comun', 'moneda'),
(11, 'comun', 'pais'),
(29, 'comun', 'paistipoubigeo'),
(14, 'comun', 'persona'),
(54, 'comun', 'personacontacto'),
(43, 'comun', 'personadocumento'),
(45, 'comun', 'personafecha'),
(60, 'comun', 'personajuridica'),
(61, 'comun', 'personajuridicaactividad'),
(42, 'comun', 'personanatural'),
(59, 'comun', 'personanaturaloficio'),
(20, 'comun', 'profesionoficio'),
(31, 'comun', 'regionnatural'),
(32, 'comun', 'tipoambito'),
(27, 'comun', 'tipocontacto'),
(13, 'comun', 'tipocontribuyente'),
(33, 'comun', 'tipodireccion'),
(25, 'comun', 'tipodocumento'),
(24, 'comun', 'tipoentidad'),
(34, 'comun', 'tipofecha'),
(21, 'comun', 'tiponumerovia'),
(12, 'comun', 'tipopersona'),
(30, 'comun', 'tipoubigeo'),
(22, 'comun', 'tipovia'),
(23, 'comun', 'tipozona'),
(28, 'comun', 'ubigeo'),
(4, 'contenttypes', 'contenttype'),
(17, 'oauth2_provider', 'accesstoken'),
(16, 'oauth2_provider', 'application'),
(18, 'oauth2_provider', 'grant'),
(19, 'oauth2_provider', 'refreshtoken'),
(44, 'procesos', 'comprobante'),
(46, 'procesos', 'comprobconfig'),
(51, 'procesos', 'contrato'),
(52, 'procesos', 'contratoservicio'),
(63, 'procesos', 'cronogramapago'),
(64, 'procesos', 'direccion'),
(65, 'procesos', 'estadocronograma'),
(49, 'procesos', 'estadopredio'),
(68, 'procesos', 'mesrecibo'),
(66, 'procesos', 'motivomovimientocuenta'),
(50, 'procesos', 'predio'),
(69, 'procesos', 'recibo'),
(48, 'procesos', 'servicio'),
(67, 'procesos', 'tipomovimientocuenta'),
(47, 'procesos', 'tipousoservicio'),
(53, 'procesos', 'zona'),
(41, 'segur', 'menu'),
(40, 'segur', 'modulo'),
(39, 'segur', 'tipomenu'),
(15, 'segur', 'usuario'),
(5, 'sessions', 'session');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'comun', '0001_initial', '2018-07-03 15:18:52.871184'),
(2, 'comun', '0002_auto_20180702_1603', '2018-07-03 15:18:53.788425'),
(3, 'comun', '0003_contador', '2018-07-03 15:18:54.443434'),
(4, 'comun', '0004_auto_20180702_1614', '2018-07-03 15:18:55.501538'),
(5, 'comun', '0005_moneda', '2018-07-03 15:18:56.947915'),
(6, 'comun', '0006_clasificacioniiu', '2018-07-03 15:18:57.388964'),
(7, 'comun', '0007_mes', '2018-07-03 15:18:57.820948'),
(8, 'comun', '0008_pais', '2018-07-03 15:19:00.112365'),
(9, 'comun', '0009_tipopersona', '2018-07-03 15:19:00.470079'),
(10, 'comun', '0010_tipocontribuyente', '2018-07-03 15:19:01.308068'),
(11, 'comun', '0011_persona', '2018-07-03 15:19:06.835667'),
(12, 'contenttypes', '0001_initial', '2018-07-03 15:19:08.065630'),
(13, 'contenttypes', '0002_remove_content_type_name', '2018-07-03 15:19:09.747761'),
(14, 'auth', '0001_initial', '2018-07-03 15:19:20.670374'),
(15, 'auth', '0002_alter_permission_name_max_length', '2018-07-03 15:19:23.107036'),
(16, 'auth', '0003_alter_user_email_max_length', '2018-07-03 15:19:23.395713'),
(17, 'auth', '0004_alter_user_username_opts', '2018-07-03 15:19:23.693241'),
(18, 'auth', '0005_alter_user_last_login_null', '2018-07-03 15:19:23.831277'),
(19, 'auth', '0006_require_contenttypes_0002', '2018-07-03 15:19:23.937743'),
(20, 'auth', '0007_alter_validators_add_error_messages', '2018-07-03 15:19:24.080597'),
(21, 'auth', '0008_alter_user_username_max_length', '2018-07-03 15:19:24.196069'),
(22, 'auth', '0009_alter_user_last_name_max_length', '2018-07-03 15:19:24.299491'),
(23, 'segur', '0001_initial', '2018-07-03 15:19:38.697515'),
(24, 'admin', '0001_initial', '2018-07-03 15:19:43.463511'),
(25, 'admin', '0002_logentry_remove_auto_add', '2018-07-03 15:19:43.926414'),
(26, 'sessions', '0001_initial', '2018-07-03 15:19:46.890805'),
(27, 'oauth2_provider', '0001_initial', '2018-07-03 15:21:44.592234'),
(28, 'oauth2_provider', '0002_08_updates', '2018-07-03 15:21:50.642574'),
(29, 'oauth2_provider', '0003_auto_20160316_1503', '2018-07-03 15:21:53.300025'),
(30, 'oauth2_provider', '0004_auto_20160525_1623', '2018-07-03 15:21:55.792944'),
(31, 'oauth2_provider', '0005_auto_20170514_1141', '2018-07-03 15:22:42.085892'),
(32, 'oauth2_provider', '0006_auto_20171214_2232', '2018-07-03 15:22:51.683097'),
(33, 'comun', '0012_regionnatural', '2018-07-04 13:20:14.681550'),
(34, 'comun', '0013_profesionoficio', '2018-07-04 13:20:14.930689'),
(35, 'comun', '0014_tipoubigeo', '2018-07-04 13:20:15.300516'),
(36, 'comun', '0015_tiponumerovia', '2018-07-04 13:20:15.746760'),
(37, 'comun', '0016_tipovia', '2018-07-04 13:20:16.083610'),
(38, 'comun', '0017_tipozona', '2018-07-04 13:20:16.624746'),
(39, 'comun', '0018_tipoentidad', '2018-07-04 13:20:16.931751'),
(40, 'comun', '0019_tipodocumento', '2018-07-04 13:20:17.255886'),
(41, 'comun', '0020_ubigeo', '2018-07-04 13:20:22.146109'),
(42, 'comun', '0021_tipocontacto', '2018-07-04 13:20:22.808643'),
(43, 'comun', '0022_cuentacontacto', '2018-07-04 13:20:24.731793'),
(44, 'comun', '0023_paistipoubigeo', '2018-07-04 13:20:30.671759'),
(45, 'comun', '0024_tipoambito', '2018-07-04 13:20:31.827366'),
(46, 'comun', '0025_tipodireccion', '2018-07-04 13:20:32.701457'),
(47, 'comun', '0026_auto_20180704_0914', '2018-07-04 14:15:22.642617'),
(48, 'comun', '0027_auto_20180704_0918', '2018-07-04 14:19:09.714751'),
(49, 'comun', '0028_tipofecha', '2018-07-04 15:49:00.904764'),
(50, 'comun', '0029_auto_20180704_1139', '2018-07-04 16:39:13.367177'),
(51, 'comun', '0030_auto_20180704_1150', '2018-07-04 16:50:35.656428'),
(52, 'comun', '0031_auto_20180704_1154', '2018-07-04 16:54:27.743704'),
(53, 'comun', '0032_auto_20180704_1158', '2018-07-04 16:58:56.829904'),
(54, 'comun', '0033_auto_20180704_1216', '2018-07-04 17:17:01.617816'),
(55, 'comun', '0034_auto_20180708_2026', '2018-07-09 01:27:11.806725'),
(57, 'segur', '0002_tipomenu', '2018-07-09 20:48:27.555806'),
(58, 'segur', '0003_modulo', '2018-07-09 21:06:21.507091'),
(60, 'segur', '0004_menu', '2018-07-09 21:26:29.161206'),
(61, 'comun', '0035_entidad', '2018-07-09 21:28:01.489717'),
(63, 'comun', '0036_auto_20180709_1707', '2018-07-09 22:08:08.295469'),
(64, 'comun', '0037_auto_20180710_0949', '2018-07-10 14:49:58.988755'),
(76, 'comun', '0038_auto_20180710_1255', '2018-07-10 17:56:07.172950'),
(77, 'comun', '0039_auto_20180710_1305', '2018-07-10 18:05:22.113585'),
(78, 'comun', '0040_auto_20180710_1309', '2018-07-10 18:09:26.486263'),
(79, 'comun', '0041_auto_20180710_1312', '2018-07-10 18:12:20.321040'),
(80, 'comun', '0042_auto_20180710_1316', '2018-07-10 18:16:51.999552'),
(81, 'comun', '0043_auto_20180710_1320', '2018-07-10 18:21:01.499690'),
(82, 'comun', '0044_auto_20180710_1326', '2018-07-10 18:26:05.845439'),
(83, 'comun', '0045_auto_20180710_1328', '2018-07-10 18:28:24.214692'),
(84, 'comun', '0046_auto_20180710_1330', '2018-07-10 18:30:45.328856'),
(85, 'comun', '0047_auto_20180710_1334', '2018-07-10 18:34:31.965627'),
(86, 'comun', '0048_auto_20180710_1337', '2018-07-10 18:37:16.174241'),
(87, 'comun', '0049_tipofecha', '2018-07-10 18:44:28.044399'),
(88, 'comun', '0050_auto_20180710_1350', '2018-07-10 18:51:42.407734'),
(89, 'comun', '0051_auto_20180710_1359', '2018-07-10 18:59:48.803531'),
(91, 'comun', '0052_auto_20180710_1521', '2018-07-10 20:22:59.320553'),
(92, 'comun', '0053_entidad', '2018-07-10 20:27:14.306262'),
(94, 'comun', '0054_personanatural', '2018-07-11 01:05:41.606915'),
(95, 'comun', '0055_auto_20180711_1005', '2018-07-11 15:05:56.405701'),
(97, 'comun', '0056_auto_20180711_1620', '2018-07-11 21:20:45.810639'),
(98, 'comun', '0057_auto_20180711_1731', '2018-07-11 22:31:32.560169'),
(107, 'comun', '0058_auto_20180712_0847', '2018-07-12 13:48:14.145011'),
(117, 'procesos', '0001_initial', '2018-07-12 15:59:22.526093'),
(118, 'procesos', '0002_comprobconfig', '2018-07-12 16:00:05.453535'),
(119, 'procesos', '0003_tipousoservicio', '2018-07-12 16:00:41.187756'),
(120, 'procesos', '0004_servicio', '2018-07-12 16:01:20.881276'),
(121, 'procesos', '0005_estadopredio', '2018-07-12 16:01:49.280591'),
(122, 'procesos', '0006_zona', '2018-07-12 16:02:26.610099'),
(123, 'procesos', '0007_predio', '2018-07-12 16:03:20.936344'),
(124, 'procesos', '0008_contrato', '2018-07-12 16:04:05.653658'),
(125, 'procesos', '0009_contratoservicio', '2018-07-12 16:04:38.356704'),
(127, 'comun', '0059_auto_20180712_1718', '2018-07-12 22:19:53.449248'),
(134, 'comun', '0060_personanaturaloficio', '2018-07-15 22:58:26.307812'),
(135, 'comun', '0061_personajuridica', '2018-07-15 23:41:12.860357'),
(136, 'comun', '0062_auto_20180715_1841', '2018-07-15 23:41:39.171060'),
(137, 'comun', '0063_auto_20180715_1943', '2018-07-16 00:43:35.758009'),
(138, 'comun', '0064_auto_20180715_2108', '2018-07-16 02:08:45.692576'),
(140, 'procesos', '0011_auto_20180719_1659', '2018-07-19 17:00:27.087123'),
(144, 'procesos', '0012_auto_20180719_1723', '2018-07-19 17:24:09.669164'),
(145, 'procesos', '0013_auto_20180720_1103', '2018-07-20 11:03:26.708776'),
(146, 'procesos', '0014_auto_20180720_1107', '2018-07-20 11:07:37.555115'),
(147, 'procesos', '0015_auto_20180720_1115', '2018-07-20 11:16:02.619854'),
(148, 'procesos', '0016_auto_20180720_1211', '2018-07-20 12:11:57.593824'),
(149, 'procesos', '0017_auto_20180720_1235', '2018-07-20 12:35:49.633628'),
(150, 'procesos', '0018_auto_20180720_1242', '2018-07-20 12:42:13.384130'),
(151, 'procesos', '0019_auto_20180720_1245', '2018-07-20 12:45:56.810928'),
(152, 'procesos', '0020_auto_20180720_1248', '2018-07-20 12:49:00.002313'),
(153, 'segur', '0005_auto_20180722_1035', '2018-07-22 10:36:11.404502'),
(154, 'segur', '0006_auto_20180722_1115', '2018-07-22 11:15:48.896671'),
(156, 'procesos', '0021_auto_20180724_1717', '2018-07-24 17:18:07.954178'),
(157, 'procesos', '0022_recibo', '2018-07-24 17:42:41.048758'),
(158, 'procesos', '0023_servicio_padre', '2018-07-24 18:11:55.030291'),
(159, 'procesos', '0024_auto_20180724_1820', '2018-07-24 18:24:22.667484'),
(160, 'caja', '0001_initial', '2018-07-25 16:47:47.714438'),
(161, 'procesos', '0025_contrato_forma_cobranza', '2018-07-25 17:25:40.064018'),
(162, 'segur', '0007_usuario_entidades', '2018-07-26 09:09:30.907543');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('2shdgupqdycru3dk4lqezln3bdbpeiuw', 'OTJlYmEwNjc2NmQ1NDFkOWVlOTI4YzQ1OGZlZDg0NjI3YzhiNzk1Yzp7Il9hdXRoX3VzZXJfaWQiOiJkNjZlNTA2NC1jNjAyLTQxMjYtODU5Yi1lNTFhN2U1YjhlMzEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaGFzaCI6IjhjODZlMWFkNTlmOTQ1ODIwYzU0MGQ2MjE4NGFhNmY3ZjNkM2RmYWUifQ==', '2018-08-07 18:15:05.353694'),
('d3xth922gci08qd5gnezcqgyldrhgop8', 'NzlmMjI3MDc3MmNmODkwYTc5NDEzODcyZThiM2EyNDAwODhkM2U3ZTp7Il9hdXRoX3VzZXJfaWQiOiJkNjZlNTA2NC1jNjAyLTQxMjYtODU5Yi1lNTFhN2U1YjhlMzEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaGFzaCI6ImMyYzhhNDliZmM1MTA5MGM4NDkyYzQ0YjBlZjJkYzM1MDQ4N2I0NGUifQ==', '2018-08-02 20:07:33.958256'),
('gfmtoger8484osbzpfqj5olepth8u7kt', 'NzAyOGJjOGNjNGI5MjcwMWU2YjgzYWVmMDdhMGY0ZmNiMGQ3NDRmNDp7Il9hdXRoX3VzZXJfaWQiOiJkNjZlNTA2NC1jNjAyLTQxMjYtODU5Yi1lNTFhN2U1YjhlMzEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaGFzaCI6IjdjNTc5YTg0OWNhMjQ1NjFkZGQwZjRhZjM3NDk4NmZmYThjODBhMmUifQ==', '2018-07-23 15:09:03.342134'),
('hfm6o28r5316kmdylju32swhq1w4t36b', 'OTJlYmEwNjc2NmQ1NDFkOWVlOTI4YzQ1OGZlZDg0NjI3YzhiNzk1Yzp7Il9hdXRoX3VzZXJfaWQiOiJkNjZlNTA2NC1jNjAyLTQxMjYtODU5Yi1lNTFhN2U1YjhlMzEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaGFzaCI6IjhjODZlMWFkNTlmOTQ1ODIwYzU0MGQ2MjE4NGFhNmY3ZjNkM2RmYWUifQ==', '2018-08-06 12:21:06.908847'),
('ik3bvbdqx7u1sgz1g1xec7nqcbt6pyfw', 'NzAyOGJjOGNjNGI5MjcwMWU2YjgzYWVmMDdhMGY0ZmNiMGQ3NDRmNDp7Il9hdXRoX3VzZXJfaWQiOiJkNjZlNTA2NC1jNjAyLTQxMjYtODU5Yi1lNTFhN2U1YjhlMzEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaGFzaCI6IjdjNTc5YTg0OWNhMjQ1NjFkZGQwZjRhZjM3NDk4NmZmYThjODBhMmUifQ==', '2018-07-17 17:22:46.242009'),
('mdtvgm0g7czo0fjbfmjrw9p3nkldds42', 'NzAyOGJjOGNjNGI5MjcwMWU2YjgzYWVmMDdhMGY0ZmNiMGQ3NDRmNDp7Il9hdXRoX3VzZXJfaWQiOiJkNjZlNTA2NC1jNjAyLTQxMjYtODU5Yi1lNTFhN2U1YjhlMzEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaGFzaCI6IjdjNTc5YTg0OWNhMjQ1NjFkZGQwZjRhZjM3NDk4NmZmYThjODBhMmUifQ==', '2018-07-17 23:58:36.201845'),
('pc7qiim2lh1hmeqszmuuo8trt1gw8c16', 'NzAyOGJjOGNjNGI5MjcwMWU2YjgzYWVmMDdhMGY0ZmNiMGQ3NDRmNDp7Il9hdXRoX3VzZXJfaWQiOiJkNjZlNTA2NC1jNjAyLTQxMjYtODU5Yi1lNTFhN2U1YjhlMzEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaGFzaCI6IjdjNTc5YTg0OWNhMjQ1NjFkZGQwZjRhZjM3NDk4NmZmYThjODBhMmUifQ==', '2018-07-17 23:58:11.592479'),
('tfvme12642lvl5qsxuqkqan9ykaasx3x', 'NzAyOGJjOGNjNGI5MjcwMWU2YjgzYWVmMDdhMGY0ZmNiMGQ3NDRmNDp7Il9hdXRoX3VzZXJfaWQiOiJkNjZlNTA2NC1jNjAyLTQxMjYtODU5Yi1lNTFhN2U1YjhlMzEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaGFzaCI6IjdjNTc5YTg0OWNhMjQ1NjFkZGQwZjRhZjM3NDk4NmZmYThjODBhMmUifQ==', '2018-07-17 23:58:33.177308'),
('v3kbl6r3fud3fupegiexlk77t6rl9r1l', 'NzAyOGJjOGNjNGI5MjcwMWU2YjgzYWVmMDdhMGY0ZmNiMGQ3NDRmNDp7Il9hdXRoX3VzZXJfaWQiOiJkNjZlNTA2NC1jNjAyLTQxMjYtODU5Yi1lNTFhN2U1YjhlMzEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaGFzaCI6IjdjNTc5YTg0OWNhMjQ1NjFkZGQwZjRhZjM3NDk4NmZmYThjODBhMmUifQ==', '2018-07-17 23:58:10.924460'),
('vojm4mss1xbzz32gzk830i78ddaobmtr', 'NzAyOGJjOGNjNGI5MjcwMWU2YjgzYWVmMDdhMGY0ZmNiMGQ3NDRmNDp7Il9hdXRoX3VzZXJfaWQiOiJkNjZlNTA2NC1jNjAyLTQxMjYtODU5Yi1lNTFhN2U1YjhlMzEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaGFzaCI6IjdjNTc5YTg0OWNhMjQ1NjFkZGQwZjRhZjM3NDk4NmZmYThjODBhMmUifQ==', '2018-07-17 23:58:12.550250');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entidad`
--

CREATE TABLE `entidad` (
  `id` int(11) NOT NULL,
  `logo` varchar(120) DEFAULT NULL,
  `codigo` varchar(60) DEFAULT NULL,
  `estado` varchar(1) NOT NULL,
  `moneda_id` int(11) DEFAULT NULL,
  `padre_id` int(11) DEFAULT NULL,
  `persona_id` int(11) NOT NULL,
  `tipo_entidad_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `entidad`
--

INSERT INTO `entidad` (`id`, `logo`, `codigo`, `estado`, `moneda_id`, `padre_id`, `persona_id`, `tipo_entidad_id`) VALUES
(1, NULL, '00001', '1', 1, NULL, 48, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth2_provider_accesstoken`
--

CREATE TABLE `oauth2_provider_accesstoken` (
  `id` bigint(20) NOT NULL,
  `token` varchar(255) NOT NULL,
  `expires` datetime(6) NOT NULL,
  `scope` longtext NOT NULL,
  `application_id` bigint(20) DEFAULT NULL,
  `user_id` char(32) DEFAULT NULL,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `source_refresh_token_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `oauth2_provider_accesstoken`
--

INSERT INTO `oauth2_provider_accesstoken` (`id`, `token`, `expires`, `scope`, `application_id`, `user_id`, `created`, `updated`, `source_refresh_token_id`) VALUES
(1, 'qP2S8qo5WBzIAWXuV5T2kM0A0JIBFV', '2018-07-04 02:33:57.350329', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-03 16:33:57.350329', '2018-07-03 16:33:57.350329', NULL),
(2, 'AUStIjYdBlIolSFZLUbdrBVgekNLto', '2018-07-04 03:42:08.811305', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-03 17:42:08.811305', '2018-07-03 17:42:08.811305', NULL),
(3, 'kiZk2uAKNmY5cujHO4vDIWP4tfOdRx', '2018-07-04 08:02:18.237862', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-03 22:02:18.238862', '2018-07-03 22:02:18.238862', NULL),
(4, 'omdKeuM8s4DK5emBx7MRCjpDmkdVd2', '2018-07-05 01:02:09.808771', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-04 15:02:09.808771', '2018-07-04 15:02:09.808771', NULL),
(5, 'xMGk2hcHv6wvonVajKRpklHbQQTKlQ', '2018-07-05 02:17:16.097075', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-04 16:17:16.097075', '2018-07-04 16:17:16.097075', NULL),
(6, 'UAjNlLWCDPtBWK2UJAwzbMOgq0N3Dz', '2018-07-05 02:30:18.947241', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-04 16:30:18.948241', '2018-07-04 16:30:18.948241', NULL),
(7, 'NsWps0BcisAJaAbUjnPIxrYjOlA7U2', '2018-07-05 02:34:06.001854', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-04 16:34:06.002877', '2018-07-04 16:34:06.002877', NULL),
(8, 'NgxQGnHociWBDg5voh4M2VTajgMKJR', '2018-07-05 03:42:23.960044', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-04 17:42:23.960044', '2018-07-04 17:42:23.960044', NULL),
(9, '9xXFqMl257TMwns0TXcTw67uDDKidD', '2018-07-05 03:52:59.705183', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-04 17:52:59.705183', '2018-07-04 17:52:59.705183', NULL),
(10, 'yrtZrdAOCOC03Xu9WLyqEfSKAObR3w', '2018-07-05 07:42:23.987377', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-04 21:42:23.987377', '2018-07-04 21:42:23.987377', NULL),
(11, 'xWFd1BdpDkWPvxhIi1SrLBywCu5kXB', '2018-07-05 23:15:57.198688', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-05 13:15:57.198688', '2018-07-05 13:15:57.198688', NULL),
(12, '5SijZRBPefi2TxWeaYBokCGcBfDG6Z', '2018-07-09 00:20:06.910852', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-08 14:20:06.910852', '2018-07-08 14:20:06.910852', NULL),
(13, 'w3KLKm8BiCmLu20kwOvNpwxFBrn67z', '2018-07-09 01:12:05.741127', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-08 15:12:05.741127', '2018-07-08 15:12:05.741127', NULL),
(14, 'KkfqTLvyd2K9unUQVZjUKkUOVK1osP', '2018-07-09 01:13:29.975158', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-08 15:13:29.975158', '2018-07-08 15:13:29.975158', NULL),
(15, 'wQk6tyD3UfI8l7l50GIVAK6WCYB3yi', '2018-07-09 01:58:05.879367', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-08 15:58:05.879367', '2018-07-08 15:58:05.879367', NULL),
(16, 'f391RkxjCs2gWNUl3oaEjW3HDwGPUE', '2018-07-09 02:20:17.562579', 'read write groups', 1, 'ecff37b639854cc1874a2198fa019530', '2018-07-08 16:20:17.562579', '2018-07-08 16:20:17.562579', NULL),
(17, 'yDJACS2OA4A6b42lx1NNh3DusxALeS', '2018-07-09 02:26:01.871744', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-08 16:26:01.871744', '2018-07-08 16:26:01.871744', NULL),
(18, 'lswkb5ypsw6Q8h3e5HtmYYHP1nbUgl', '2018-07-09 02:29:23.349270', 'read write groups', 1, 'ecff37b639854cc1874a2198fa019530', '2018-07-08 16:29:23.349270', '2018-07-08 16:29:23.349270', NULL),
(19, 'X86puv6OPS96Lvi3rDQj1rr3mj3ONx', '2018-07-09 07:50:54.209614', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-08 21:50:54.210616', '2018-07-08 21:50:54.210616', NULL),
(20, 'ZFnx2obfTxdSYW3vaUSfZIN357UJQM', '2018-07-09 07:55:22.052218', 'read write groups', 1, 'ecff37b639854cc1874a2198fa019530', '2018-07-08 21:55:22.052218', '2018-07-08 21:55:22.052218', NULL),
(21, 'uqCBmosSbAglq8WQfwn2B2e4ni4mGs', '2018-07-09 08:03:30.084470', 'read write groups', 1, 'ecff37b639854cc1874a2198fa019530', '2018-07-08 22:03:30.084470', '2018-07-08 22:03:30.084470', NULL),
(22, 'Oz7tTuFbWSawLYwZsSp6NGEj6WHZhI', '2018-07-10 01:08:17.318821', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-09 15:08:17.318821', '2018-07-09 15:08:17.318821', NULL),
(23, 'EyogVOVlSDwY61wpV2nQAYtrppvmHc', '2018-07-10 06:52:42.359511', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-09 20:52:42.359511', '2018-07-09 20:52:42.359511', NULL),
(24, 't78bdZlep0qMCqj9nsrvS0G9ol0oGr', '2018-07-10 08:37:01.007544', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-09 22:37:01.007544', '2018-07-09 22:37:01.007544', NULL),
(25, 'TIfZylT2JGE48YhbiFjLCmlYSjsEt2', '2018-07-11 01:23:03.849121', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-10 15:23:03.850123', '2018-07-10 15:23:03.850123', NULL),
(26, 'RzHYum5rNgR9q6Xv1ao47pnqSFHPP1', '2018-07-11 02:57:23.325715', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-10 16:57:23.325715', '2018-07-10 16:57:23.325715', NULL),
(27, '9B9D2jo0ydlhV91J8hijLWyCc9NSEy', '2018-07-11 03:00:44.341034', 'read write groups', 1, 'ecff37b639854cc1874a2198fa019530', '2018-07-10 17:00:44.341034', '2018-07-10 17:00:44.341034', NULL),
(28, 'OjJfk3s3vv6WyOdovksczNfPdonKkl', '2018-07-11 03:36:28.471404', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-10 17:36:28.472406', '2018-07-10 17:36:28.472406', NULL),
(29, '8DPZtLPIViv76Wyd98mTxX9At0AaXr', '2018-07-11 06:34:08.764804', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-10 20:34:08.764804', '2018-07-10 20:34:08.764804', NULL),
(30, 'HEaAEKY2j3Kf78TZDD25uWbFCcTsT4', '2018-07-11 06:41:47.990460', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-10 20:41:47.991460', '2018-07-10 20:41:47.991460', NULL),
(31, 'xEKAUj7NQnfGJLtS6p3D0149n1PoSJ', '2018-07-11 07:01:17.485127', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-10 21:01:17.485127', '2018-07-10 21:01:17.485127', NULL),
(32, '5rMnnxYzuWBL9EynvnVpoUhF91n27A', '2018-07-11 07:24:27.729382', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-10 21:24:27.729382', '2018-07-10 21:24:27.729382', NULL),
(33, 'vjYMyV2uPy0EcTTStnww4Hi3QATCZ7', '2018-07-11 11:11:48.518971', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-11 01:11:48.518971', '2018-07-11 01:11:48.518971', NULL),
(34, 'cumzxkq5JLNjJETAtbMcGjyW9iq3xG', '2018-07-12 00:03:17.721364', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-11 14:03:17.721364', '2018-07-11 14:03:17.721364', NULL),
(35, 't6iADsAf5Oo8XZcZ99KQvBwB4P349A', '2018-07-12 05:50:25.919752', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-11 19:50:25.920728', '2018-07-11 19:50:25.920728', NULL),
(36, 'UboBkx9ZdYozdAXTM5jLS7XkrgYxYd', '2018-07-12 07:55:12.764806', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-11 21:55:12.764806', '2018-07-11 21:55:12.764806', NULL),
(37, '8cexopKMcpY8qmbqTONaRsVEE22wKy', '2018-07-12 12:11:38.711279', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-12 02:11:38.715279', '2018-07-12 02:11:38.715279', NULL),
(38, 'mWS4ktUPXV6V9wlB6Sc2AJ29hDHWDf', '2018-07-12 12:18:36.993220', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-12 02:18:36.993220', '2018-07-12 02:18:36.993220', NULL),
(39, 'rn6jiRfIAqiVo5U3Za5d0IPJRIaz1E', '2018-07-12 23:49:38.860769', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-12 13:49:38.860769', '2018-07-12 13:49:38.876367', NULL),
(40, '0paMUSpW8azOaCnGi6R90V8jbslTmG', '2018-07-13 03:36:17.655371', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-12 17:36:17.655371', '2018-07-12 17:36:17.655371', NULL),
(41, 'MgM3xbqL2QPGoXxOkmAPukWmyF8e3J', '2018-07-13 07:48:55.005803', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-12 21:48:55.005803', '2018-07-12 21:48:55.005803', NULL),
(42, 'XzQbBhCr2sWv9nwc3XuV50dNj1EQkC', '2018-07-13 23:37:28.781850', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-13 13:37:28.781850', '2018-07-13 13:37:28.781850', NULL),
(43, 'F1XpDcNfm2Y7pavXeeARNGWvg13Yq8', '2018-07-13 23:49:13.232626', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-13 13:49:13.248270', '2018-07-13 13:49:13.248270', NULL),
(44, 'hnxoY0pt2uFPnPgzsjknNgT1xYQL8l', '2018-07-14 00:00:10.676833', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-13 14:00:10.676833', '2018-07-13 14:00:10.676833', NULL),
(45, 'd41pyFlnNfbCiullnaC4gjKQV6rQ85', '2018-07-14 02:30:50.451560', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-13 16:30:50.451560', '2018-07-13 16:30:50.451560', NULL),
(46, 'h3Re5GVsVfuueIlmuVBiq0PEoQoYR1', '2018-07-15 23:44:29.278604', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-15 13:44:29.279581', '2018-07-15 13:44:29.279581', NULL),
(47, 'y1Gtb0rEPqWhDbvkoscIjwseHnnZdW', '2018-07-15 23:50:58.384971', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-15 13:50:58.384971', '2018-07-15 13:50:58.384971', NULL),
(48, '2z7K6B5EcQtwTSfXqJEEsjVLI1FCTz', '2018-07-16 00:27:53.913228', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-15 14:27:53.917247', '2018-07-15 14:27:53.917247', NULL),
(49, 'OWWMhlYp82QBjFIRmhSWovhJw5jE29', '2018-07-16 00:32:22.081040', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-15 14:32:22.091041', '2018-07-15 14:32:22.091041', NULL),
(50, 'ncgDUYUVLqPepYayLsaiIbC2NsL0GX', '2018-07-16 00:50:25.147388', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-15 14:50:25.147388', '2018-07-15 14:50:25.147388', NULL),
(51, '7Sfp7wnc5cBpHB8gLLFZykHUMY4pUU', '2018-07-16 00:52:18.117085', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-15 14:52:18.117085', '2018-07-15 14:52:18.117085', NULL),
(52, 'wU50tCwV0BQluuMBRug7YdQpuxEBTi', '2018-07-16 10:33:23.956705', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-16 00:33:23.956705', '2018-07-16 00:33:23.956705', NULL),
(53, 'hROI2ZzFD86tq4r2eFyRuWx2pZdlr5', '2018-07-16 23:55:44.548802', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-16 13:55:44.548802', '2018-07-16 13:55:44.548802', NULL),
(54, 'cML016flPx86brzHXY4gIHvktSuw1T', '2018-07-16 19:20:22.026439', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-16 09:20:22.026439', '2018-07-16 09:20:22.026439', NULL),
(55, 'OnsCVn8Jd1DYTHlo1Py1FSjVd1gL0t', '2018-07-17 01:44:47.693990', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-16 15:44:47.693990', '2018-07-16 15:44:47.693990', NULL),
(56, 'pmpiINzAbIr6vwM8AaPMgvnAZgdYzT', '2018-07-17 04:06:36.554760', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-16 18:06:36.555765', '2018-07-16 18:06:36.555765', NULL),
(57, 'zp8xKDlmfkMeW5vBhF6XzA8iWv36WO', '2018-07-17 19:04:07.604813', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-17 09:04:07.604813', '2018-07-17 09:04:07.604813', NULL),
(58, 'idZGMiBx9Z6LsUOQwQjYRRig0dFjNr', '2018-07-17 19:05:37.468036', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-17 09:05:37.469013', '2018-07-17 09:05:37.469013', NULL),
(59, 'LGsxDZNQdqgFb4kKChIcqISyrou1Ts', '2018-07-17 19:28:12.756717', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-17 09:28:12.756717', '2018-07-17 09:28:12.756717', NULL),
(60, 'xcLjnC6wdYnhL85y0RndTTUutoOdRM', '2018-07-18 05:10:55.261734', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-17 19:10:55.261734', '2018-07-17 19:10:55.261734', NULL),
(61, 'KEPugrUKN5HnfzeHUAO0D9M6IupYy6', '2018-07-18 05:12:15.898763', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-17 19:12:15.898763', '2018-07-17 19:12:15.898763', NULL),
(62, 'UyDSK15b1GWnXrA8X3EDlW8kJC4f1Y', '2018-07-18 07:30:34.084095', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-17 21:30:34.088095', '2018-07-17 21:30:34.088095', NULL),
(63, '1YdjjDydtDg2HhAVJTpflOaoxWdsuj', '2018-07-18 18:49:05.356036', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-18 08:49:05.357035', '2018-07-18 08:49:05.357035', NULL),
(64, '8iTAKGZd1KUhtfgasC7gpMh7sFRzLN', '2018-07-18 18:50:48.006780', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-18 08:50:48.006780', '2018-07-18 08:50:48.006780', NULL),
(65, 'uKCRkXJtGarnsIO5I37nVk38pKs5Ai', '2018-07-19 01:44:18.847221', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-18 15:44:18.847221', '2018-07-18 15:44:18.847221', NULL),
(66, 'ipbJ8aX9f1lLhTY3zghLxaDwxR1apW', '2018-07-19 01:48:38.438654', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-18 15:48:38.438654', '2018-07-18 15:48:38.438654', NULL),
(71, 'I5OHfzKODCdQTQaQrFhWS1EBEz6qwp', '2018-07-19 03:12:50.559753', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-18 17:12:50.559753', '2018-07-18 17:12:50.559753', NULL),
(76, 's8jR7SrB17N5LaK6j9vO6DBzY4mCHd', '2018-07-19 04:07:13.059619', 'read write groups', 1, 'ecff37b639854cc1874a2198fa019530', '2018-07-18 18:07:13.059619', '2018-07-18 18:07:13.059619', NULL),
(77, 'tEndNVKVI49Pbod5PapuL4M8AW8lto', '2018-07-19 04:07:34.590449', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-18 18:07:34.590449', '2018-07-18 18:07:34.590449', NULL),
(78, '5B8rwQTvncMDRG6sFI9eMU9t4TiRIh', '2018-07-19 04:39:04.750007', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-18 18:39:04.750007', '2018-07-18 18:39:04.750007', NULL),
(79, 'Ha03o9N3zUruXxub1h9MEho68e9eAe', '2018-07-19 18:52:11.061611', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-19 08:52:11.063610', '2018-07-19 08:52:11.063610', NULL),
(83, 'AQd1xeeAIsK8tnxRoPt1YRNPqxwbVM', '2018-07-19 21:28:30.534257', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-19 11:28:30.534257', '2018-07-19 11:28:30.534257', NULL),
(84, 'rER92phV7u9HpQgJoz0rZG0Sg3Q4OH', '2018-07-19 22:12:04.745639', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-19 12:12:04.745639', '2018-07-19 12:12:04.745639', NULL),
(91, 'SSULYy6oHfGLBNSsvA7cxhxPrR1BV7', '2018-07-20 04:49:41.000551', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-19 18:49:41.000551', '2018-07-19 18:49:41.000551', NULL),
(92, 'l4Ikq32SSt3pTnXHak3VmK0h96J5iV', '2018-07-20 04:53:00.199676', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-19 18:53:00.199676', '2018-07-19 18:53:00.199676', NULL),
(93, 'R60DyYyURvCRLt7qL8zJUV1W47oxFt', '2018-07-20 04:53:21.759524', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-19 18:53:21.760525', '2018-07-19 18:53:21.760525', NULL),
(95, 'NP6BqSJq3yjQDlWtiBc3GZstQIeyra', '2018-07-20 19:25:19.501939', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-20 09:25:19.501939', '2018-07-20 09:25:19.501939', NULL),
(96, '2taMB4ukfeGq1pOPqZQW5frxgL5Lmo', '2018-07-20 19:25:57.221863', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-20 09:25:57.221863', '2018-07-20 09:25:57.221863', NULL),
(97, 'TtccsgF9apULz5VLtIYEEmnelt0KO3', '2018-07-20 20:20:56.117915', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-20 10:20:56.117915', '2018-07-20 10:20:56.117915', NULL),
(98, 'jxAyUKni7JwhIF2eGp4NOY7CSaGy8q', '2018-07-22 20:17:02.558939', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-22 10:17:02.574565', '2018-07-22 10:17:02.574565', NULL),
(99, 'J6CY0XwXqUt3YQheyKUu6Di2De8kMx', '2018-07-22 21:11:20.628968', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-22 11:11:20.628968', '2018-07-22 11:11:20.628968', NULL),
(101, 'Mdnk6f15OpZRfUJabVr8HH0CcH1vEA', '2018-07-23 20:11:14.933699', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-23 10:11:14.933699', '2018-07-23 10:11:14.933699', NULL),
(105, '7KM1psftj4FfTWfs9494k6zkONAMNJ', '2018-07-23 22:02:14.459548', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-23 12:02:14.459548', '2018-07-23 12:02:14.459548', NULL),
(107, '8zPMFsJ9E13s6fvUWkdAHj8lzKtVtQ', '2018-07-24 00:12:17.829466', 'read write groups', 1, 'ecff37b639854cc1874a2198fa019530', '2018-07-23 14:12:17.829466', '2018-07-23 14:12:17.829466', NULL),
(108, 'gKBSalsXGXKp34XwfDH3rKd5G4n6dP', '2018-07-24 00:24:11.803070', 'read write groups', 1, 'ecff37b639854cc1874a2198fa019530', '2018-07-23 14:24:11.803070', '2018-07-23 14:24:11.803070', NULL),
(109, 'e37AWJMxU8ID4PoOfPU0LoxzCQKMtp', '2018-07-24 00:24:46.350789', 'read write groups', 1, 'ecff37b639854cc1874a2198fa019530', '2018-07-23 14:24:46.350789', '2018-07-23 14:24:46.350789', NULL),
(110, 'vfWcrevBYlTEgUHtnmTGy8rt5D10ey', '2018-07-24 00:40:44.062473', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-23 14:40:44.062473', '2018-07-23 14:40:44.062473', NULL),
(111, 'HjZzklLQHHhUQHb3Bp280SfHmyD14h', '2018-07-24 01:16:17.459896', 'read write groups', 1, 'ecff37b639854cc1874a2198fa019530', '2018-07-23 15:16:17.459896', '2018-07-23 15:16:17.459896', NULL),
(112, 'USNRnCRmofbnQ1VJuXLb3vqBaBOfCx', '2018-07-24 01:16:55.317865', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-23 15:16:55.317865', '2018-07-23 15:16:55.317865', NULL),
(113, 'fQhHikM9Nyp7kR89Qn2fqfMsVXlCpg', '2018-07-24 07:24:48.173082', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-23 21:24:48.173082', '2018-07-23 21:24:48.173082', NULL),
(114, 'Y3twglLTiiSBMy5lRRYTOpwlspKuwj', '2018-07-24 18:38:46.672812', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-24 08:38:46.672812', '2018-07-24 08:38:46.672812', NULL),
(115, 'oa2orXmCVUhSqJGLkq9HFMw9Jgu4QQ', '2018-07-24 18:42:49.248173', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-24 08:42:49.248173', '2018-07-24 08:42:49.248173', NULL),
(116, 'PYriLRPCnuSFHxvZ9956W1f9x6RYRQ', '2018-07-24 23:56:37.839179', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-24 13:56:37.840155', '2018-07-24 13:56:37.840155', NULL),
(117, '3JGnjq9x9H7nKPBGw6994aPJb5bTEf', '2018-07-25 04:43:20.561463', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-24 18:43:20.561463', '2018-07-24 18:43:20.561463', NULL),
(118, '95vuSLvynig5FLR0CaoQX8vueOYTNS', '2018-07-25 18:47:52.728617', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-25 08:47:52.728617', '2018-07-25 08:47:52.728617', NULL),
(119, 'KxQHyhACIrnjrCghasIJIXS6zK5E4M', '2018-07-26 01:27:40.522525', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-25 15:27:40.522525', '2018-07-25 15:27:40.522525', NULL),
(121, 'cIYhXM5calaswfwrYzOis4jSlnwzQC', '2018-07-26 03:00:41.274355', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-25 17:00:41.274355', '2018-07-25 17:00:41.274355', NULL),
(122, '27VxdOgc9cBeIbdkS2XVU1aopeVbP8', '2018-07-26 04:19:15.040394', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-25 18:19:15.041395', '2018-07-25 18:19:15.041395', NULL),
(124, 'DHSN6wXPmFceAwZVCtfq7v2JFydCop', '2018-07-26 10:39:51.269428', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-26 00:39:51.269428', '2018-07-26 00:39:51.269428', NULL),
(127, 'T2ZcuhL1d7gP4umABHAOcC0NEbskYA', '2018-07-26 20:55:26.971196', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-26 10:55:27.018068', '2018-07-26 10:55:27.018068', NULL),
(128, 'QWV1ofMcw8K7ok2ix6GfAGNgcmgGMN', '2018-07-26 21:44:59.705772', 'read write groups', 1, 'ecff37b639854cc1874a2198fa019530', '2018-07-26 11:44:59.705772', '2018-07-26 11:44:59.705772', NULL),
(129, 'GaaVEtd0YCUmosdaY22d4pMK8Nqztq', '2018-07-26 21:45:37.085288', 'read write groups', 1, 'ecff37b639854cc1874a2198fa019530', '2018-07-26 11:45:37.085288', '2018-07-26 11:45:37.085288', NULL),
(137, 'V9STmXebY3Di451ZqsEAsLUif6cVV1', '2018-07-27 01:51:23.301864', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-26 15:51:23.301864', '2018-07-26 15:51:23.301864', NULL),
(138, 'WCHILFbzMZOWEm9tXjMJ5emrQRWksg', '2018-07-27 01:52:26.758803', 'read write groups', 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-26 15:52:26.758803', '2018-07-26 15:52:26.758803', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth2_provider_application`
--

CREATE TABLE `oauth2_provider_application` (
  `id` bigint(20) NOT NULL,
  `client_id` varchar(100) NOT NULL,
  `redirect_uris` longtext NOT NULL,
  `client_type` varchar(32) NOT NULL,
  `authorization_grant_type` varchar(32) NOT NULL,
  `client_secret` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `user_id` char(32) DEFAULT NULL,
  `skip_authorization` tinyint(1) NOT NULL,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `oauth2_provider_application`
--

INSERT INTO `oauth2_provider_application` (`id`, `client_id`, `redirect_uris`, `client_type`, `authorization_grant_type`, `client_secret`, `name`, `user_id`, `skip_authorization`, `created`, `updated`) VALUES
(1, 'oyO6sVO5qk2rcu1clYlGnW4WjJ5Opi2jv8AhaGA2', 'http://localhost', 'confidential', 'password', 'WmFn2FB37sfzGQPXFLbaBHHRPkvEfqtcKSTjvAOEEjURkc9MV5I7ie7wswjRICrUFOvWabTfG608MWtkPIl2CWB2MSJ7VqDhKw51Jl8faod2fl9H5FKklpsNlluRr5Mo', 'WebAngulajs', 'd66e5064c6024126859be51a7e5b8e31', 0, '2018-07-03 16:15:33.981502', '2018-07-03 16:15:33.981502');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth2_provider_grant`
--

CREATE TABLE `oauth2_provider_grant` (
  `id` bigint(20) NOT NULL,
  `code` varchar(255) NOT NULL,
  `expires` datetime(6) NOT NULL,
  `redirect_uri` varchar(255) NOT NULL,
  `scope` longtext NOT NULL,
  `application_id` bigint(20) NOT NULL,
  `user_id` char(32) NOT NULL,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth2_provider_refreshtoken`
--

CREATE TABLE `oauth2_provider_refreshtoken` (
  `id` bigint(20) NOT NULL,
  `token` varchar(255) NOT NULL,
  `access_token_id` bigint(20) DEFAULT NULL,
  `application_id` bigint(20) NOT NULL,
  `user_id` char(32) NOT NULL,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `revoked` datetime(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `oauth2_provider_refreshtoken`
--

INSERT INTO `oauth2_provider_refreshtoken` (`id`, `token`, `access_token_id`, `application_id`, `user_id`, `created`, `updated`, `revoked`) VALUES
(1, 'GhNqfqDuR8oH66GZ9VQx8yxrpmiDPw', 1, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-03 16:33:57.459701', '2018-07-03 16:33:57.459701', NULL),
(2, '8w9TiEgrkNANpcWiRdw96wKvhjsMgr', 2, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-03 17:42:08.942646', '2018-07-03 17:42:08.942646', NULL),
(3, 'Hm6tQncgQE61xkeAQEEHb6HXCE0cKY', 3, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-03 22:02:18.312910', '2018-07-03 22:02:18.312910', NULL),
(4, 'NDZLnX2Lzb3aTh62aLVLvan2Kh801W', 4, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-04 15:02:09.871298', '2018-07-04 15:02:09.871298', NULL),
(5, 'wH4rsNMuxxw5lp1YjSFUIz7uCFoP3i', 5, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-04 16:17:16.174378', '2018-07-04 16:17:16.174378', NULL),
(6, 'Qem94hzf69mb2OhywUj9KzKK2pLCof', 6, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-04 16:30:18.983910', '2018-07-04 16:30:18.983910', NULL),
(7, 'OOoteB2Lhjs74IL2tj8nW5tqtEAK46', 7, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-04 16:34:06.033817', '2018-07-04 16:34:06.049415', NULL),
(8, 'CoUUY6qzFmvhjeqNtxJ0DZTpuZRw9v', 8, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-04 17:42:24.100679', '2018-07-04 17:42:24.100679', NULL),
(9, 'TIc6M0oMlUcWKJ3VDaeMAEnMpZRZ97', 9, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-04 17:52:59.788988', '2018-07-04 17:52:59.788988', NULL),
(10, 'CyqYtmtoVZiz2T282scLsphlselqaG', 10, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-04 21:42:24.003008', '2018-07-04 21:42:24.003008', NULL),
(11, 'J8meAjU6VQhJ7Of6P7VrkLewzF4kW7', 11, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-05 13:15:57.261198', '2018-07-05 13:15:57.261198', NULL),
(12, '9IRG4nR2LiIG6jYQ0vGRUl6C6PLBkQ', 12, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-08 14:20:06.926481', '2018-07-08 14:20:06.926481', NULL),
(13, 'q5EjIgZ68qGKb9D1upXrq3nyisX539', 13, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-08 15:12:05.772405', '2018-07-08 15:12:05.772405', NULL),
(14, 'XFeoEhA5H58MtkZwDHO898KUDCVjN8', 14, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-08 15:13:29.990787', '2018-07-08 15:13:29.990787', NULL),
(15, 'OdCP54mTTutjkYacPCvgSjEMscUc9W', 15, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-08 15:58:05.910618', '2018-07-08 15:58:05.910618', NULL),
(16, 'yr9xhk6oUG2W29Iscnl3zg6RBwnHLS', 16, 1, 'ecff37b639854cc1874a2198fa019530', '2018-07-08 16:20:17.562579', '2018-07-08 16:20:17.562579', NULL),
(17, 'G9THBUxjN5veGllGgRDKfFuJpELVYA', 17, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-08 16:26:01.903012', '2018-07-08 16:26:01.903012', NULL),
(18, 'IAYSq03tOwVSMS60v3NlnJ6YX5xUbm', 18, 1, 'ecff37b639854cc1874a2198fa019530', '2018-07-08 16:29:23.364899', '2018-07-08 16:29:23.364899', NULL),
(19, 'IEyrn5wrxqKCUxrEnSECLsUXAXMLE4', 19, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-08 21:50:54.276658', '2018-07-08 21:50:54.276658', NULL),
(20, 'OHHjLiFkQ68SOeemabhiGTIPqHPh0h', 20, 1, 'ecff37b639854cc1874a2198fa019530', '2018-07-08 21:55:22.083470', '2018-07-08 21:55:22.083470', NULL),
(21, 'pvxTHoh4sJTHWv7lj9NT9lxkV7BWVr', 21, 1, 'ecff37b639854cc1874a2198fa019530', '2018-07-08 22:03:30.112963', '2018-07-08 22:03:30.112963', NULL),
(22, 'qTDMJ3vOrExTHU4ZjGLrA50zwyPhFC', 22, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-09 15:08:17.428219', '2018-07-09 15:08:17.428219', NULL),
(23, 'y78LpfnUhViZ0QT08QLZ9ule4BeGu9', 23, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-09 20:52:42.390798', '2018-07-09 20:52:42.390798', NULL),
(24, 'rJie0hDIGxzFui8iKN1NzLntytFTnB', 24, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-09 22:37:01.094590', '2018-07-09 22:37:01.094590', NULL),
(25, 'jXunaU3hf8NbdUJvDnmIJGIVzVWMLj', 25, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-10 15:23:03.915379', '2018-07-10 15:23:03.915379', NULL),
(26, 'Qgp3Jm6GBdksp1kf7XYrqBiv8t7ctU', 26, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-10 16:57:23.439765', '2018-07-10 16:57:23.439765', NULL),
(27, 'djIOfic9HPD96JU9l8sRKZ5oBxJOOQ', 27, 1, 'ecff37b639854cc1874a2198fa019530', '2018-07-10 17:00:44.372284', '2018-07-10 17:00:44.372284', NULL),
(28, 'BP3HCkn8IGOhZ5RAD0L0iGVdi0qqOD', 28, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-10 17:36:28.604226', '2018-07-10 17:36:28.604226', NULL),
(29, 'UJdpgG7EOsyXCXfUCy3f2EpnLVsuHq', 29, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-10 20:34:08.820805', '2018-07-10 20:34:08.820805', NULL),
(30, 'iJn6lc7r9CcMoH7fcr23X9UyYm9eHN', 30, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-10 20:41:48.024440', '2018-07-10 20:41:48.025440', NULL),
(31, 'n6VFxhx4Y4aWphketRO7hv7WOle0kd', 31, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-10 21:01:17.532029', '2018-07-10 21:01:17.532029', NULL),
(32, 'ABRUnzr3GM9l1wKxuk29PRp5gP0vI0', 32, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-10 21:24:27.772501', '2018-07-10 21:24:27.773503', NULL),
(33, 'dQ6w8lC50ivLHq4XlQh2oQYgEML6Qc', 33, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-11 01:11:48.581509', '2018-07-11 01:11:48.581509', NULL),
(34, 'f5Rhha4zTiDFr1ziGFaD9s7YBCzmtF', 34, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-11 14:03:17.815151', '2018-07-11 14:03:17.815151', NULL),
(35, 'mkMWyOOqttHcOTAZPIZHzkNOPmjF4a', 35, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-11 19:50:25.992961', '2018-07-11 19:50:25.992961', NULL),
(36, 'ZEOitM5WlfWZFiTpUh6JAwSkiER7o6', 36, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-11 21:55:12.811706', '2018-07-11 21:55:12.811706', NULL),
(37, 'FOz3gMqD2Yy1ikbFL1SqBvChbZVsIF', 37, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-12 02:11:38.831221', '2018-07-12 02:11:38.831221', NULL),
(38, 'WkVna4mul1mvdQVE7IF0mv371Ieysh', 38, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-12 02:18:37.024467', '2018-07-12 02:18:37.024467', NULL),
(39, 'OaqaFKn0PbIsCQ2fpp7BZErRKHPCjv', 39, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-12 13:49:38.923267', '2018-07-12 13:49:38.923267', NULL),
(40, 'GSnrzZNUEYQdYdPvqBxl35R2fPR1Tn', 40, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-12 17:36:17.717893', '2018-07-12 17:36:17.717893', NULL),
(41, 'ZxkOwafhoOIA79ghDiFD9jLRI2UVcM', 41, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-12 21:48:55.083941', '2018-07-12 21:48:55.083941', NULL),
(42, 'geqtmBPGM7LScBVYnU3i5AHuGW1KNy', 42, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-13 13:37:28.828727', '2018-07-13 13:37:28.828727', NULL),
(43, 'rVbZQln4s8mUkkRNIpJIEtNywlf4Su', 43, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-13 13:49:13.289658', '2018-07-13 13:49:13.289658', NULL),
(44, '3EXairAJ93p4TfoH9YmYEsaSYwK0Zm', 44, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-13 14:00:10.697808', '2018-07-13 14:00:10.697808', NULL),
(45, '9fAT9XmkeYsdqSMRXIRmQ7qFzPIX1j', 45, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-13 16:30:50.498440', '2018-07-13 16:30:50.498440', NULL),
(46, 'INOG0is9INe0emLrXUXD5RDsHkyQnM', 46, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-15 13:44:29.319576', '2018-07-15 13:44:29.319576', NULL),
(47, 'Nq7RObpuLT73hqfrLklXGE7fIObiXM', 47, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-15 13:50:58.416220', '2018-07-15 13:50:58.416220', NULL),
(48, 'LdRBl8J3JgCruQIefZfyr5eaVILzr5', 48, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-15 14:27:53.957231', '2018-07-15 14:27:53.957231', NULL),
(49, '0AhDpS3TibBxiE4JXP5zmDmqaVe7Pq', 49, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-15 14:32:22.246564', '2018-07-15 14:32:22.246564', NULL),
(50, 'ZXnXOkp600pF5ARNdPeDsWNhuSXQnW', 50, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-15 14:50:25.178638', '2018-07-15 14:50:25.178638', NULL),
(51, '5j1P997uLP4H3ic3K90SK06J7Adtyy', 51, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-15 14:52:18.132731', '2018-07-15 14:52:18.132731', NULL),
(52, 'i8fYivhuYnqYS4DOGsl4D5Y0iuF6XX', 52, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-16 00:33:24.004603', '2018-07-16 00:33:24.004603', NULL),
(53, 'NDOhQtzcvpgiwlxS4YH3SFjEDUUNld', 53, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-16 13:55:44.580064', '2018-07-16 13:55:44.580064', NULL),
(54, 'rCo3B8emLW3sV2W4BJfpImphiQeEud', 54, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-16 09:20:22.120185', '2018-07-16 09:20:22.120185', NULL),
(55, 'GK9gKi8ykR5lyWFshPXDL1F9CADLqx', 55, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-16 15:44:47.739379', '2018-07-16 15:44:47.739379', NULL),
(56, 'lgTIeFQzuKqLkJOWh3DnsaptNyRWO8', 56, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-16 18:06:36.603900', '2018-07-16 18:06:36.603900', NULL),
(57, 'eVVcac7ZJoqDT8z4JvQwcHvBIVTc8P', 57, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-17 09:04:07.651693', '2018-07-17 09:04:07.651693', NULL),
(58, '4UzOcyfD0IVtjAphyep4qxMt7fglTh', 58, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-17 09:05:37.529791', '2018-07-17 09:05:37.529791', NULL),
(59, 'fvlDX5k7Hfi1BfYyQ50gnA5a0ibNf7', 59, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-17 09:28:12.776719', '2018-07-17 09:28:12.776719', NULL),
(60, 'Q1JjYTbIVQaT83dexrdq5vgX32CnHI', 60, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-17 19:10:55.346520', '2018-07-17 19:10:55.346520', NULL),
(61, 'mCFu0UcPsq0BHIUjaXsuXTki8TSSN0', 61, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-17 19:12:15.914759', '2018-07-17 19:12:15.914759', NULL),
(62, '8JQ5b1fLaqyr0RJuO5DLnbXTSRdU58', 62, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-17 21:30:34.168358', '2018-07-17 21:30:34.168358', NULL),
(63, 'edZcqzfhufcllxei4aeMqpFgwD27ME', 63, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-18 08:49:05.425319', '2018-07-18 08:49:05.425319', NULL),
(64, '3TvarEuqajL10F3C4ntRphE1NuNNGV', 64, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-18 08:50:48.022407', '2018-07-18 08:50:48.022407', NULL),
(65, 'L0yZK9gnjekmzl7xfxRBilBbMdVgiX', 65, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-18 15:44:18.908699', '2018-07-18 15:44:18.908699', NULL),
(66, 'mkrmmmu5tO5BZoYNVhHhJNW0jEwNHW', 66, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-18 15:48:38.468499', '2018-07-18 15:48:38.468499', NULL),
(67, 'X0T0pasO9oveoXgxTvSFmeocJ9vz7S', NULL, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-18 16:02:44.166652', '2018-07-18 16:02:44.166652', NULL),
(68, 'WNFoNo4n3EOAx9gp8wvJSrnSdrII7b', NULL, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-18 17:02:23.132120', '2018-07-18 17:02:23.132120', NULL),
(69, 'TzIF0wt5Eo2QCZukm7rlMm8qMNtWHu', NULL, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-18 17:11:17.339489', '2018-07-18 17:11:17.339489', NULL),
(70, 'Zz58ZiEspEHxaYYXE4ROD65lzQqmcj', NULL, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-18 17:11:51.353407', '2018-07-18 17:11:51.353407', NULL),
(71, 'wqpX7Zjg7NIyjuyLUx6NdGmUXdsLj8', 71, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-18 17:12:50.576582', '2018-07-18 17:12:50.576582', NULL),
(72, 'MVA03ZwsmJErLa4p5WGig3MHM2a9MY', NULL, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-18 17:17:06.808517', '2018-07-18 17:17:06.808517', NULL),
(73, 'wPq4wnhgmCZfsqyKhiaD86kfQz9R9X', NULL, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-18 17:23:09.076527', '2018-07-18 17:23:09.076527', NULL),
(74, 'mv0iHyHHYe6updnYRVAOiy091SJN8e', NULL, 1, 'ecff37b639854cc1874a2198fa019530', '2018-07-18 18:06:35.579353', '2018-07-18 18:06:35.579353', NULL),
(75, 'bE2STX5tcxNdEnNOq8iYJPtUaulrvF', NULL, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-18 18:07:01.973795', '2018-07-18 18:07:01.973795', NULL),
(76, 'yxDumKEgq4ukP6aloIFuzZnUw7K4gQ', 76, 1, 'ecff37b639854cc1874a2198fa019530', '2018-07-18 18:07:13.075245', '2018-07-18 18:07:13.075245', NULL),
(77, 'dEfiJMdPUpRPZoyWqrUFfDjV628saJ', 77, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-18 18:07:34.621699', '2018-07-18 18:07:34.621699', NULL),
(78, 'LCAQsGLtUHJMSUI9hZDzvzdFmBW6zx', 78, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-18 18:39:04.782903', '2018-07-18 18:39:04.782903', NULL),
(79, 'j5JXx72S2tQdky9NcLvy6gAmGzZZ3z', 79, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-19 08:52:11.107584', '2018-07-19 08:52:11.107584', NULL),
(80, 'QiahXUjoTlDN4pcKxmWZ8BUudaPXVA', NULL, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-19 09:33:11.877760', '2018-07-19 09:33:11.877760', NULL),
(81, 'OJrhuFMA62pJ63iYJ2zqeIGqkQEImZ', NULL, 1, 'ecff37b639854cc1874a2198fa019530', '2018-07-19 10:08:58.869862', '2018-07-19 10:08:58.869862', NULL),
(82, 'KVrcWtgg7SByweW29bo6uWwW6DotwC', NULL, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-19 10:09:39.229535', '2018-07-19 10:09:39.229535', NULL),
(83, 'sLyTVTnzFI0oDWg4reEOJpRYdXkIC1', 83, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-19 11:28:30.549885', '2018-07-19 11:28:30.549885', NULL),
(84, 'MgKnaC9nolWQAUhBOVnswhdLRmuoS1', 84, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-19 12:12:04.792516', '2018-07-19 12:12:04.792516', NULL),
(85, 'rj1UTIJOmL60RjGXcsGk5FdiurcCZv', NULL, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-19 17:32:23.126147', '2018-07-19 17:32:23.126147', NULL),
(86, 'mpw9WeRWpYMUCInQWIAZrDKtcK289n', NULL, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-19 17:44:50.434196', '2018-07-19 17:44:50.434196', NULL),
(87, 'Iq0bvKJL22qxoCyyPZoMcNsjJdkqZf', NULL, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-19 18:25:47.770666', '2018-07-19 18:25:47.770666', NULL),
(88, 'CUtJt2Pe8WyHWCel0fWSK9quyDZyzH', NULL, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-19 18:26:42.143598', '2018-07-19 18:26:42.143598', NULL),
(89, '3cvxTQDxUZNhWiYMbVyPil1mP1NJF0', NULL, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-19 18:28:40.044867', '2018-07-19 18:28:40.044867', NULL),
(90, 'RwA9SRDWWNcPWYx2dovnoKEunQ8PET', NULL, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-19 18:31:17.535393', '2018-07-19 18:31:17.535393', NULL),
(91, '6k3lYF304ks8r1lEwGYnWGvg24LDMY', 91, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-19 18:49:41.064792', '2018-07-19 18:49:41.064792', NULL),
(92, 'PlnsIN7Z4sJyFDSxX3O4ebJlqswUxX', 92, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-19 18:53:00.221663', '2018-07-19 18:53:00.221663', NULL),
(93, 'XDs09l2581c6IuV1JmHhNvQSuynPvj', 93, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-19 18:53:21.790936', '2018-07-19 18:53:21.790936', NULL),
(94, 'TzaVAvrXyk2A5sk6b8zcIy9NQjk11Q', NULL, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-20 09:18:42.764048', '2018-07-20 09:18:42.764048', NULL),
(95, 'oBxIp2cY4UPJnRVFBlkDBUpDtYA4At', 95, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-20 09:25:19.517937', '2018-07-20 09:25:19.517937', NULL),
(96, 'laWhFaD7nlyoJrK6rvFzbiebhG9509', 96, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-20 09:25:57.248752', '2018-07-20 09:25:57.248752', NULL),
(97, 'PTQLRqres0AYKSQrSGFQ9rOI931b7d', 97, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-20 10:20:56.133552', '2018-07-20 10:20:56.133552', NULL),
(98, 'WAMgQQSHgZDNXUNVZ3ricBRk8ZZ1Uv', 98, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-22 10:17:02.605818', '2018-07-22 10:17:02.605818', NULL),
(99, '1DhnnPoK2qyaY8sS5Uinvsj9rebrrG', 99, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-22 11:11:20.660230', '2018-07-22 11:11:20.660230', NULL),
(100, 'oDwMwngoCAvu8noMS2M7dBbM0baBWn', NULL, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-23 08:49:42.826989', '2018-07-23 08:49:42.826989', NULL),
(101, 'Rz4oJayPo6UZC8dtjrq6CIjn6bMo5R', 101, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-23 10:11:14.945692', '2018-07-23 10:11:14.945692', NULL),
(102, 'bJnbnczwTkN4F29CKRmDxk2lRCVP8G', NULL, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-23 11:34:00.474462', '2018-07-23 11:34:00.474462', NULL),
(104, 'Rtm9lDzSbVjoNYE0HlMQ7PXqGC3bke', NULL, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-23 11:38:38.500594', '2018-07-23 11:38:38.500594', NULL),
(105, 'lmemq2WWmkVK4Q3DorQjZg6xJPQw2f', 105, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-23 12:02:14.475203', '2018-07-23 12:02:14.475203', NULL),
(106, 'TANH4nES2dzofHsBoqKpQudkXx16ZD', NULL, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-23 14:07:47.437871', '2018-07-23 14:07:47.437871', NULL),
(107, 'PnffyF2iXQvvW3G7v335Jvl7pQMt6G', 107, 1, 'ecff37b639854cc1874a2198fa019530', '2018-07-23 14:12:17.860717', '2018-07-23 14:12:17.860717', NULL),
(108, 'zSadshK9UZW5eKqtJ7L2djsfKUCY9E', 108, 1, 'ecff37b639854cc1874a2198fa019530', '2018-07-23 14:24:11.834321', '2018-07-23 14:24:11.834321', NULL),
(109, 'olLMLDx5hGfi8S0LNlzZl2QSayMPCb', 109, 1, 'ecff37b639854cc1874a2198fa019530', '2018-07-23 14:24:46.382047', '2018-07-23 14:24:46.382047', NULL),
(110, 'R1X2bBLPgXxuB8ERvOIHB7FvuGtnuw', 110, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-23 14:40:44.107582', '2018-07-23 14:40:44.107582', NULL),
(111, 'ijF1tAU3LThdJ3ok33NtyV0YWyvh53', 111, 1, 'ecff37b639854cc1874a2198fa019530', '2018-07-23 15:16:17.475526', '2018-07-23 15:16:17.475526', NULL),
(112, 'jjPd99nUrT0lV5Qu8sNbfpbostNQ9O', 112, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-23 15:16:55.361691', '2018-07-23 15:16:55.361691', NULL),
(113, 'Tlr0dN5qsiMiASQV9M7VVmAe29m3a9', 113, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-23 21:24:48.204329', '2018-07-23 21:24:48.204329', NULL),
(114, 'RgdetxfdbZX58qHCPgYeR7orGFcX9h', 114, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-24 08:38:46.721950', '2018-07-24 08:38:46.721950', NULL),
(115, '3qrAOXiNgTfrCixZHLAi34hH5RsLvV', 115, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-24 08:42:49.279424', '2018-07-24 08:42:49.279424', NULL),
(116, '9xZ18mh1k66AyL0dwBq9cAUswH2WTW', 116, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-24 13:56:37.855778', '2018-07-24 13:56:37.855778', NULL),
(117, 'KZtRkNweU2Fp1N3U5t2sm7dSrNQp5u', 117, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-24 18:43:20.592714', '2018-07-24 18:43:20.592714', NULL),
(118, '78MRZz8nbJYesYZ8m1PoTXndyZxyhO', 118, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-25 08:47:52.786102', '2018-07-25 08:47:52.786102', NULL),
(119, 'utNWce71GUrqINZxlyaPe2a51nqfNC', 119, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-25 15:27:40.612303', '2018-07-25 15:27:40.612303', NULL),
(120, 'xRH0f3fTudvklEZ4kDLplEoQNkVR0V', NULL, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-25 16:57:03.614783', '2018-07-25 16:57:03.614783', NULL),
(121, 'wO6IaBGVMBc26tsjDF565GG238bgMs', 121, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-25 17:00:41.289999', '2018-07-25 17:00:41.289999', NULL),
(122, 'pP9lJajQVVHClBmc4xeAvhtvLIGmTC', 122, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-25 18:19:15.061580', '2018-07-25 18:19:15.061580', NULL),
(123, 'sc2ar8a1zjNmr9dFFwyW9bMeEzVvpZ', NULL, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-25 22:58:25.822518', '2018-07-25 22:58:25.822518', NULL),
(124, 'fQ8HdiZknseq66zwUOKzGHFgNuf9ky', 124, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-26 00:39:51.300411', '2018-07-26 00:39:51.300411', NULL),
(125, 'BflXlWEYazOIHhrLgaabHgrgstbUxt', NULL, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-26 08:38:23.737413', '2018-07-26 08:38:23.737413', NULL),
(126, 'ozUYuPDRU5UAxwyIHqEofkPvHNodou', NULL, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-26 10:27:10.816231', '2018-07-26 10:27:10.816231', NULL),
(127, 'h5HunOJCr5He5gcqN6C9IF2dWZ1YU0', 127, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-26 10:55:27.053995', '2018-07-26 10:55:27.053995', NULL),
(128, 'qse09krTdbH5HOxXcN1BoyULqCO0tL', 128, 1, 'ecff37b639854cc1874a2198fa019530', '2018-07-26 11:44:59.752650', '2018-07-26 11:44:59.752650', NULL),
(129, 'HWILNjrg0GEVWKCu6fMt1I15N0Xsb0', 129, 1, 'ecff37b639854cc1874a2198fa019530', '2018-07-26 11:45:37.125164', '2018-07-26 11:45:37.125164', NULL),
(130, 'DIebMZazy2ASvP9TF30Z2Siuad0070', NULL, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-26 11:46:14.444744', '2018-07-26 11:46:14.444744', NULL),
(131, 'thFvvaRJQNwbHrGtiIShsUTUFjxvyU', NULL, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-26 13:57:40.903344', '2018-07-26 13:57:40.903344', NULL),
(132, '8JDmFxFmjK89tDLab5VVpPdnd6u6w1', NULL, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-26 14:59:23.864822', '2018-07-26 14:59:23.864822', NULL),
(133, 'Cy2RzfV4bwatKqPHOkUY92us6LVBUw', NULL, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-26 15:41:23.006522', '2018-07-26 15:41:23.006522', NULL),
(134, '5F99O1VLrfurkhy3OXK8Q22VU0943d', NULL, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-26 15:41:58.602271', '2018-07-26 15:41:58.602271', NULL),
(135, 'W0ybYtAprSMmDVHxQvzBOzlgC7JfYJ', NULL, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-26 15:44:13.329442', '2018-07-26 15:44:13.329442', NULL),
(136, 'BOPtw4hMsQbkQ6e06gOof50MZt7WX2', NULL, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-26 15:45:27.171954', '2018-07-26 15:45:27.171954', NULL),
(137, 'KMb2iiinlCSczVbj4zGusmrWceCjA0', 137, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-26 15:51:23.405943', '2018-07-26 15:51:23.405943', NULL),
(138, '8dNWsTJq4Cz3YF3do8t19Lap0LEiPC', 138, 1, 'd66e5064c6024126859be51a7e5b8e31', '2018-07-26 15:52:26.790062', '2018-07-26 15:52:26.790062', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `procesos_comprobante`
--

CREATE TABLE `procesos_comprobante` (
  `id` int(11) NOT NULL,
  `codigo` varchar(10) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `nombre_plural` varchar(70) NOT NULL,
  `abreviatura` varchar(40) NOT NULL,
  `codigo_une` varchar(10) DEFAULT NULL,
  `orden` int(11) NOT NULL,
  `url_entrada` varchar(120) DEFAULT NULL,
  `url_salida` varchar(120) DEFAULT NULL,
  `estado` varchar(1) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `procesos_comprobante`
--

INSERT INTO `procesos_comprobante` (`id`, `codigo`, `nombre`, `nombre_plural`, `abreviatura`, `codigo_une`, `orden`, `url_entrada`, `url_salida`, `estado`, `created_at`, `updated_at`) VALUES
(2, 'B01', 'Boleta', 'Boletas', 'Bol.', '', 2, NULL, NULL, '1', '2018-07-15 17:10:51.357102', '2018-07-15 17:10:51.357102'),
(3, 'F01', 'Factura', 'Facturas', 'Fac.', '', 3, NULL, NULL, '1', '2018-07-15 17:12:14.568016', '2018-07-15 17:12:26.771032'),
(4, '3255', 'Recibo de pago', 'Recibos de pago', 'R. de pago', 'recpagosss', 4, NULL, NULL, '1', '2018-07-18 17:28:32.145147', '2018-07-18 17:30:20.965325'),
(5, 'COPRESAP', 'CONTRATO DE PRESTACIÓN DE SERVICIOS DE AGUA POTABLE', 'CONTRATOS DE PRESTACIÓN DE SERVICIOS DE AGUA POTABLE', 'Cont. Prest. Serv. de Agua Potable', 'trt', 3, NULL, NULL, '1', '2018-07-20 12:38:44.129846', '2018-07-20 12:50:30.028466');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `procesos_comprob_config`
--

CREATE TABLE `procesos_comprob_config` (
  `id` int(11) NOT NULL,
  `serie` varchar(8) NOT NULL,
  `nro_inicial` int(11) NOT NULL,
  `nro_limite` int(11) DEFAULT NULL,
  `correlativo` int(11) NOT NULL,
  `nro_autoriz` varchar(40) DEFAULT NULL,
  `completa_ceros` varchar(10) DEFAULT NULL,
  `orden` int(11) NOT NULL,
  `estado` varchar(1) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `comprobante_id` int(11) NOT NULL,
  `entidad_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `procesos_contrato`
--

CREATE TABLE `procesos_contrato` (
  `id` int(11) NOT NULL,
  `numero` varchar(10) DEFAULT NULL,
  `nro_cuotas` int(11) NOT NULL,
  `estado` varchar(1) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `persona_id` int(11) NOT NULL,
  `predio_id` int(11) DEFAULT NULL,
  `tipo_uso_servicio_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `comprob_config_id` int(11) DEFAULT NULL,
  `entidad_id` int(11) NOT NULL,
  `forma_cobranza` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `procesos_contrato_servicio`
--

CREATE TABLE `procesos_contrato_servicio` (
  `id` int(11) NOT NULL,
  `precio` decimal(12,2) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `contrato_id` int(11) NOT NULL,
  `servicio_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `procesos_cronograma_pago`
--

CREATE TABLE `procesos_cronograma_pago` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(120) DEFAULT NULL,
  `monto` decimal(12,2) NOT NULL,
  `porc_interes` decimal(12,2) DEFAULT NULL,
  `fecha_cobranza` date DEFAULT NULL,
  `contrato_id` int(11) DEFAULT NULL,
  `entidad_id` int(11) DEFAULT NULL,
  `estado_cronograma_id` int(11) NOT NULL,
  `motivo_movimiento_cuenta_id` int(11) NOT NULL,
  `padre_id` int(11) DEFAULT NULL,
  `persona_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `procesos_cronograma_pago`
--

INSERT INTO `procesos_cronograma_pago` (`id`, `descripcion`, `monto`, `porc_interes`, `fecha_cobranza`, `contrato_id`, `entidad_id`, `estado_cronograma_id`, `motivo_movimiento_cuenta_id`, `padre_id`, `persona_id`, `usuario_id`, `created_at`, `updated_at`) VALUES
(2, 'ggdd', '46.00', '4.00', '2018-07-20', NULL, 1, 4, 1, NULL, 18, 18, '2018-07-20 11:17:17.641587', '2018-07-20 11:17:17.641587'),
(3, 'camilo', '45.00', '4.00', '2018-07-20', NULL, 1, 1, 2, NULL, 16, 18, '2018-07-20 11:54:21.273609', '2018-07-20 11:54:21.273609'),
(4, 'fghghgf', '50.00', '4.00', '2018-07-29', NULL, 1, 1, 1, NULL, 43, 18, '2018-07-20 11:58:18.767939', '2018-07-20 11:58:18.767939'),
(5, 'contrato', '56.00', '4.00', '2018-07-21', NULL, 1, 1, 2, NULL, 47, 18, '2018-07-20 12:47:53.356482', '2018-07-20 12:47:53.356482'),
(6, 'Recibo 0299883', '24.00', '0.00', '2018-08-31', NULL, 1, 1, 2, NULL, 18, 18, '2018-07-26 16:12:37.539525', '2018-07-26 16:12:37.539525'),
(7, 'Trabajo secretaría Julio 2018', '1100.00', NULL, '2018-07-31', NULL, 1, 4, 5, NULL, 15, 18, '2018-07-26 16:30:20.428425', '2018-07-26 16:30:20.428425');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `procesos_direccion`
--

CREATE TABLE `procesos_direccion` (
  `id` int(11) NOT NULL,
  `completa` varchar(2) NOT NULL,
  `direccion_detalle` longtext NOT NULL,
  `nombre_via` varchar(90) DEFAULT NULL,
  `numero_via` varchar(20) DEFAULT NULL,
  `manzana` varchar(20) DEFAULT NULL,
  `lote` varchar(20) DEFAULT NULL,
  `piso` varchar(12) DEFAULT NULL,
  `dpto_interior` varchar(30) DEFAULT NULL,
  `nombre_zona` varchar(60) DEFAULT NULL,
  `referencia1` varchar(150) DEFAULT NULL,
  `referencia2` varchar(150) DEFAULT NULL,
  `punto_lat` varchar(30) DEFAULT NULL,
  `punto_long` varchar(30) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `pais_id` int(11) NOT NULL,
  `persona_id` int(11) DEFAULT NULL,
  `predio_id` int(11) DEFAULT NULL,
  `tipo_direccion_id` int(11) NOT NULL,
  `tipo_numero_via_id` int(11) DEFAULT NULL,
  `tipo_via_id` int(11) DEFAULT NULL,
  `tipo_zona_id` int(11) DEFAULT NULL,
  `ubigeo_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `procesos_direccion`
--

INSERT INTO `procesos_direccion` (`id`, `completa`, `direccion_detalle`, `nombre_via`, `numero_via`, `manzana`, `lote`, `piso`, `dpto_interior`, `nombre_zona`, `referencia1`, `referencia2`, `punto_lat`, `punto_long`, `created_at`, `updated_at`, `pais_id`, `persona_id`, `predio_id`, `tipo_direccion_id`, `tipo_numero_via_id`, `tipo_via_id`, `tipo_zona_id`, `ubigeo_id`) VALUES
(1, 'SI', 'Call. Integración Nro. 100 Mz. A Lt. L Piso 2 Dep.Int. 1 - Urb. Santa Lucía', 'Integración', '100', 'A', 'L', '2', '1', 'Santa Lucía', NULL, NULL, '-6.477107019862376', '-76.39566552458848', '2018-07-19 17:27:38.494645', '2018-07-19 17:27:38.494645', 1, 18, NULL, 3, 1, 3, 1, 1991),
(2, 'SI', 'Call. Los Mártires Km. 4 Mz. M Lt. 4 Piso 5 Dep.Int. 6 - P.J. Zonita', 'Los Mártires', '4', 'M', '4', '5', '6', 'Zonita', NULL, NULL, NULL, NULL, '2018-07-20 12:21:34.572035', '2018-07-20 12:21:34.572035', 1, NULL, 39, 1, 2, 3, 2, 227),
(3, 'SI', 'Male. José C. Mariategui Km. 5 - Cjto. Hab. nueva zona', 'José C. Mariategui', '5', NULL, NULL, NULL, NULL, 'nueva zona', NULL, NULL, NULL, NULL, '2018-07-20 12:23:33.112210', '2018-07-20 12:23:33.112210', 1, NULL, 40, 1, 2, 6, 4, 227),
(4, 'NO', 'Jr. Integración 200 - Urb. San Alejandro', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Residencia Doña Esther', 'Detrás de la UPEU y el Molino de Santa Lucía', '-6.471709358233679', '-76.39614872362768', '2018-07-24 14:54:59.279759', '2018-07-24 14:54:59.279759', 1, 26, NULL, 3, NULL, NULL, NULL, 1991),
(5, 'SI', 'Pje. Enarte Torres s/n - Urb. Santa Lucía', 'Enarte Torres', NULL, NULL, NULL, NULL, NULL, 'Santa Lucía', NULL, NULL, '-6.473575516683684', '-76.39510491608519', '2018-07-26 14:34:13.509880', '2018-07-26 14:34:13.509880', 1, 48, NULL, 1, 3, 4, 1, 1991);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `procesos_estado_cronograma`
--

CREATE TABLE `procesos_estado_cronograma` (
  `id` int(11) NOT NULL,
  `codigo` varchar(12) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `estado` varchar(1) NOT NULL,
  `color` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `procesos_estado_cronograma`
--

INSERT INTO `procesos_estado_cronograma` (`id`, `codigo`, `nombre`, `estado`, `color`) VALUES
(1, 'REG', 'Registrado', '1', 'estado_registrado'),
(2, 'PAG', 'Pagado', '1', 'estado_pagado'),
(3, 'ANU', 'Anulado', '1', 'estado_anulado'),
(4, 'HAB', 'Habilitado', '1', 'estado_habilitado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `procesos_estado_predio`
--

CREATE TABLE `procesos_estado_predio` (
  `id` int(11) NOT NULL,
  `codigo` varchar(10) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `estado` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `procesos_estado_predio`
--

INSERT INTO `procesos_estado_predio` (`id`, `codigo`, `nombre`, `estado`) VALUES
(1, 'EnConstr', 'En Construcción', '1'),
(2, 'Habit', 'Habitado', '1'),
(3, 'Otros', 'Otros', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `procesos_mes_recibo`
--

CREATE TABLE `procesos_mes_recibo` (
  `id` int(11) NOT NULL,
  `anio` int(11) NOT NULL,
  `fecha_emision` date NOT NULL,
  `fecha_vencimiento` date NOT NULL,
  `fecha_corte` date DEFAULT NULL,
  `texto_html` longtext,
  `estado` varchar(3) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `entidad_id` int(11) NOT NULL,
  `mes_id` varchar(2) NOT NULL,
  `usuario_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `procesos_mes_recibo`
--

INSERT INTO `procesos_mes_recibo` (`id`, `anio`, `fecha_emision`, `fecha_vencimiento`, `fecha_corte`, `texto_html`, `estado`, `created_at`, `updated_at`, `entidad_id`, `mes_id`, `usuario_id`) VALUES
(1, 2018, '2018-08-01', '2018-08-30', '2018-08-31', NULL, 'REG', '2018-07-24 04:00:00.000000', '2018-07-24 09:00:00.000000', 1, '07', 18),
(2, 2018, '2018-09-01', '2018-09-29', '2018-09-30', NULL, 'REG', '2018-07-24 04:00:00.000000', '2018-07-24 09:00:00.000000', 1, '08', 18);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `procesos_motivo_movimiento_cuenta`
--

CREATE TABLE `procesos_motivo_movimiento_cuenta` (
  `id` int(11) NOT NULL,
  `codigo` varchar(12) DEFAULT NULL,
  `nombre` varchar(120) NOT NULL,
  `nombre_plural` varchar(120) DEFAULT NULL,
  `abreviatura` varchar(80) DEFAULT NULL,
  `estado` varchar(1) NOT NULL,
  `tipo_movimiento_cuenta_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `procesos_motivo_movimiento_cuenta`
--

INSERT INTO `procesos_motivo_movimiento_cuenta` (`id`, `codigo`, `nombre`, `nombre_plural`, `abreviatura`, `estado`, `tipo_movimiento_cuenta_id`) VALUES
(1, '00001', 'Inscripción al servicio', 'Inscripciones al servicio', 'INSCRIP', '1', 1),
(2, '00002', 'Recibo de agua', 'Recibos de agua', 'RECAGUA', '1', 1),
(3, '00006', 'Multa de reunión', 'Multas de reuniones', 'Multas', '1', 1),
(4, '00007', 'Servicio de re-instalación', 'Servicio de re-instalaciones', 'Reinstalación', '1', 1),
(5, '00008', 'Pago de personal', 'Pagos de personal', 'Personal', '1', 2),
(6, '00009', 'Servicios básicos local', 'Servicios básicos local', 'Serv. Basicos', '1', 2),
(7, '00010', 'Mantenimiento local', 'Mantenimiento local', 'Mant. Local', '1', 2),
(8, '00011', 'Construcción', 'Construcción', 'Construcción', '1', 2),
(9, '00012', 'Mantenimiento del servicio', 'Mantenimiento del servicio', 'Mantenimiento', '1', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `procesos_predio`
--

CREATE TABLE `procesos_predio` (
  `id` int(11) NOT NULL,
  `partida_registral` varchar(20) DEFAULT NULL,
  `estado_descripcion` varchar(60) DEFAULT NULL,
  `estado` varchar(1) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `duenio_id` int(11) DEFAULT NULL,
  `estado_predio_id` int(11) NOT NULL,
  `representante_id` int(11) DEFAULT NULL,
  `zona_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `procesos_predio`
--

INSERT INTO `procesos_predio` (`id`, `partida_registral`, `estado_descripcion`, `estado`, `created_at`, `updated_at`, `duenio_id`, `estado_predio_id`, `representante_id`, `zona_id`, `usuario_id`) VALUES
(39, '34325', NULL, '1', '2018-07-20 12:21:34.452062', '2018-07-20 12:21:34.452062', 16, 1, 18, 14, 18),
(40, '543', NULL, '1', '2018-07-20 12:23:33.040213', '2018-07-20 12:23:33.040213', 18, 2, 21, 15, 18);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `procesos_recibo`
--

CREATE TABLE `procesos_recibo` (
  `id` int(11) NOT NULL,
  `numero` varchar(10) NOT NULL,
  `fecha_emision` date NOT NULL,
  `monto_total` decimal(12,2) NOT NULL,
  `estado` varchar(3) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `comprob_config_id` int(11) DEFAULT NULL,
  `contrato_id` int(11) NOT NULL,
  `mes_recibo_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `procesos_servicio`
--

CREATE TABLE `procesos_servicio` (
  `id` int(11) NOT NULL,
  `codigo` varchar(10) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `nombre_plural` varchar(80) NOT NULL,
  `precio` decimal(12,2) DEFAULT NULL,
  `estado` varchar(1) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `padre_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `procesos_servicio`
--

INSERT INTO `procesos_servicio` (`id`, `codigo`, `nombre`, `nombre_plural`, `precio`, `estado`, `created_at`, `updated_at`, `padre_id`) VALUES
(1, 'Inscrip', 'Inscripción', 'Inscripciones', '550.00', '1', '0000-00-00 00:00:00.000000', '2018-07-23 15:16:26.229947', NULL),
(2, 'ServAguaPo', 'Servicio de Agua Potable', 'Servicios de Agua Potable', '25.00', '1', '0000-00-00 00:00:00.000000', '2018-07-16 00:00:36.950208', NULL),
(3, 'Electrobom', 'Electrobomba', 'Electrobombas', '5.00', '1', '0000-00-00 00:00:00.000000', '0000-00-00 00:00:00.000000', NULL),
(4, 'AlqLocal', 'Alquiler de local', 'Alquileres de local', '0.00', '1', '0000-00-00 00:00:00.000000', '2018-07-23 15:16:29.119750', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `procesos_tipo_movimiento_cuenta`
--

CREATE TABLE `procesos_tipo_movimiento_cuenta` (
  `id` int(11) NOT NULL,
  `codigo` varchar(12) DEFAULT NULL,
  `nombre` varchar(120) NOT NULL,
  `nombre_plural` varchar(120) DEFAULT NULL,
  `estado` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `procesos_tipo_movimiento_cuenta`
--

INSERT INTO `procesos_tipo_movimiento_cuenta` (`id`, `codigo`, `nombre`, `nombre_plural`, `estado`) VALUES
(1, 'ING', 'Ingreso', 'Ingresos', '1'),
(2, 'EGR', 'Egreso', 'Egresos', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `procesos_tipo_uso_servicio`
--

CREATE TABLE `procesos_tipo_uso_servicio` (
  `id` int(11) NOT NULL,
  `codigo` varchar(10) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `estado` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `procesos_tipo_uso_servicio`
--

INSERT INTO `procesos_tipo_uso_servicio` (`id`, `codigo`, `nombre`, `estado`) VALUES
(8, 'Domést', 'Doméstico', '1'),
(9, 'Comerc', 'Comercial', '1'),
(10, 'Indust', 'Industrial', '1'),
(11, 'Estat', 'Estatal', '1'),
(12, 'Social', 'Social', '1'),
(13, 'Tempor', 'Temporal', '1'),
(14, 'Pil. públi', 'Pileta pública', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `procesos_zona`
--

CREATE TABLE `procesos_zona` (
  `id` int(11) NOT NULL,
  `codigo` varchar(10) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `estado` varchar(1) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `tipo_zona_id` int(11) NOT NULL,
  `ubigeo_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `procesos_zona`
--

INSERT INTO `procesos_zona` (`id`, `codigo`, `nombre`, `estado`, `created_at`, `updated_at`, `tipo_zona_id`, `ubigeo_id`) VALUES
(11, '0105', 'San Alejandro I Etapa', '1', '2018-07-16 09:30:01.266595', '2018-07-16 09:30:52.344105', 1, 1991),
(12, '0106', 'San Alejandro II Etapa', '1', '2018-07-16 09:44:24.477368', '2018-07-16 09:44:24.477368', 1, 1991),
(13, '0107', 'Santa Lucia I Etapa', '1', '2018-07-16 09:49:02.609426', '2018-07-16 09:49:02.609426', 1, 1991),
(14, '0109', 'Zonita', '1', '2018-07-16 11:18:52.674584', '2018-07-16 11:18:52.674584', 2, 227),
(15, '0110', 'errebeb', '1', '2018-07-16 11:21:11.119494', '2018-07-16 11:21:11.119494', 1, 227),
(17, '0112', 'San Alejandro III Etapa', '1', '2018-07-16 12:33:47.891304', '2018-07-16 12:33:47.891304', 2, 1991),
(18, '0115', 'placinie III etapa', '1', '2018-07-18 22:36:39.800149', '2018-07-18 22:36:39.800149', 3, 1991);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `segur_menu`
--

CREATE TABLE `segur_menu` (
  `id` int(11) NOT NULL,
  `codigo` varchar(30) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `nombre_plural` varchar(70) NOT NULL,
  `estado_router` varchar(60) NOT NULL,
  `icono` varchar(20) DEFAULT NULL,
  `orden` int(11) NOT NULL,
  `estado` varchar(1) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `modulo_id` int(11) NOT NULL,
  `padre_id` int(11) DEFAULT NULL,
  `permiso_id` int(11) DEFAULT NULL,
  `tipo_menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `segur_menu`
--

INSERT INTO `segur_menu` (`id`, `codigo`, `nombre`, `nombre_plural`, `estado_router`, `icono`, `orden`, `estado`, `created_at`, `updated_at`, `modulo_id`, `padre_id`, `permiso_id`, `tipo_menu_id`) VALUES
(1, 'GENERAL', 'GENERAL', 'GENERALIDADES', '#', 'style', 1, '1', '2018-03-23 05:18:10.000000', '2018-03-23 05:18:10.000000', 1, NULL, NULL, 1),
(2, 'CONFIGURAC', 'CONFIGURACIÓN', 'CONFIGURACIONES', '#', 'settings', 2, '1', '2018-03-23 05:18:10.000000', '2018-03-23 05:18:10.000000', 1, NULL, NULL, 1),
(3, 'SEGURIDAD', 'SEGURIDAD', 'SEGURIDAD', '#', 'security', 5, '1', '2018-05-14 23:03:54.000000', '2018-05-14 23:03:54.000000', 1, NULL, NULL, 1),
(4, 'CONTINENTE', 'Continente', 'Continentes', 'app.continentes', NULL, 2, '1', '2018-03-23 05:27:38.000000', '2018-05-17 14:54:22.000000', 1, 1, 118, 2),
(5, 'PAIS', 'País', 'Países', 'app.paises', NULL, 12, '1', '2018-03-23 05:27:38.000000', '2018-05-17 14:54:40.000000', 1, 1, 150, 2),
(6, 'UBIGEO', 'Ubigeo', 'Ubigeos', 'app.ubigeos', NULL, 13, '1', '2018-03-23 05:27:38.000000', '2018-05-17 14:54:54.000000', 1, 1, 250, 2),
(7, 'PERSONAS', 'Persona', 'Personas', 'app.personas', NULL, 14, '1', '2018-03-23 05:27:38.000000', '2018-05-17 14:55:12.000000', 1, 1, 230, 2),
(8, 'MONEDAS', 'Moneda', 'Tipos de moneda', 'app.monedas', NULL, 15, '1', '2018-03-26 01:40:13.000000', '2018-05-17 14:55:28.000000', 1, 1, 113, 2),
(9, 'TIPOENTID', 'Tipo de entidad', 'Tipos de entidad', 'app.tiposentidad', NULL, 1, '1', '2018-03-23 21:34:09.000000', '2018-05-17 14:55:57.000000', 1, 2, 263, 2),
(10, 'ENTIDADES', 'Entidad', 'Entidades', 'app.entidades', NULL, 2, '1', '2018-03-23 21:34:09.000000', '2018-05-17 14:56:07.000000', 1, 2, 326, 2),
(11, 'PERMISOS', 'Permiso', 'Permisos', 'app.permisos', NULL, 1, '1', '2018-05-14 23:06:20.000000', '2018-05-17 15:01:44.000000', 1, 3, 482, 2),
(12, 'ROLES', 'Rol', 'Roles', 'app.roles', NULL, 2, '1', '2018-05-14 23:08:51.000000', '2018-05-17 15:01:53.000000', 1, 3, 484, 2),
(13, 'USUARIOS', 'Usuario', 'Usuarios', 'app.users', NULL, 3, '1', '2018-05-14 23:10:19.000000', '2018-05-17 15:02:08.000000', 1, 3, 486, 2),
(14, 'MODULOS', 'Módulo', 'Módulos', 'app.modulos', NULL, 4, '1', '2018-05-15 01:55:13.000000', '2018-05-17 15:02:20.000000', 1, 3, 132, 2),
(15, 'TIPOSMENU', 'Tipo de menú', 'Tipos de menú', 'app.tiposmenu', NULL, 5, '1', '2018-05-15 01:55:13.000000', '2018-05-17 15:02:58.000000', 1, 3, 488, 2),
(16, 'MENU', 'Menú', 'Menús', 'app.menus', NULL, 6, '1', '2018-05-15 01:55:13.000000', '2018-05-17 15:03:14.000000', 1, 3, 137, 2),
(17, 'PROCESOS', 'PROCESOS', 'PROCESOS', '#', 'send', 3, '1', '2018-03-23 05:18:10.000000', '2018-03-23 05:18:10.000000', 1, NULL, NULL, 1),
(18, 'MOTIVOMOVC', 'Motivo movimiento cuenta', 'Motivos movimiento cuenta', 'app.motivosmovimientocuenta', NULL, 7, '1', '2018-03-23 05:27:38.000000', '2018-05-17 14:54:40.000000', 1, 2, 472, 2),
(19, 'SERVICIOS', 'Servicio', 'Servicios', 'app.servicios', NULL, 5, '1', '2018-03-23 05:27:38.000000', '2018-05-17 14:54:40.000000', 1, 2, 412, 2),
(20, 'COMPROBANTES', 'Comprobante', 'Comprobantes', 'app.comprobantes', NULL, 3, '1', '2018-03-23 21:34:09.000000', '2018-05-17 14:56:07.000000', 1, 2, 397, 2),
(21, 'COMPROBANTESC', 'Comprobante Config', 'Config. de comprobantes', 'app.comprobantesconfig', NULL, 4, '1', '2018-03-23 21:34:09.000000', '2018-05-17 14:56:07.000000', 1, 2, 402, 2),
(22, 'PREDIOS', 'Predio', 'Predios', 'app.predios', NULL, 7, '1', '2018-03-23 05:27:38.000000', '2018-05-17 14:54:40.000000', 1, 2, 427, 2),
(23, 'CONTRATOS', 'Contrato', 'Contratos', 'app.contratos', NULL, 1, '1', '2018-03-23 05:27:38.000000', '2018-05-17 14:54:40.000000', 1, 17, 432, 2),
(24, 'RECIBOS', 'Recibo', 'Recibos', 'app.recibos', NULL, 2, '1', '2018-03-23 05:27:38.000000', '2018-05-17 14:54:40.000000', 1, 17, NULL, 2),
(25, 'PAGOS', 'Registrar pago', 'Registrar pagos', 'app.caja.registropagos', NULL, 5, '1', '2018-03-23 05:27:38.000000', '2018-05-17 14:54:40.000000', 1, 30, NULL, 2),
(26, 'ZONAS', 'Zona', 'Zonas', 'app.zonas', NULL, 6, '1', '2018-03-23 05:27:38.000000', '2018-05-17 14:54:40.000000', 1, 2, 422, 2),
(27, 'CRONCOB', 'Cronograma de cobranza', 'Cronogramas de cobranza', 'app.cronogramascobranza', NULL, 3, '1', '2018-03-23 05:27:38.000000', '2018-05-17 14:54:40.000000', 1, 17, 462, 2),
(29, 'CRONPAG', 'Cronograma de pago', 'Cronogramas de pago', 'app.cronogramaspago', NULL, 4, '1', '2018-03-23 05:27:38.000000', '2018-05-17 14:54:40.000000', 1, 17, 462, 2),
(30, 'CAJA', 'CAJA', 'CAJA', '#', 'payment', 4, '1', '2018-07-23 15:21:25.613412', '2018-07-23 15:21:25.613412', 1, NULL, NULL, 1),
(31, 'CUENTAENTIDAD', 'Cuenta entidad', 'Cuentas entidad', 'app.caja.cuentasentidad', NULL, 1, '1', '2018-07-25 15:05:05.585509', '2018-07-25 15:05:05.585509', 1, 30, 508, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `segur_modulo`
--

CREATE TABLE `segur_modulo` (
  `id` int(11) NOT NULL,
  `codigo` varchar(10) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `logo` varchar(100) DEFAULT NULL,
  `logo_rec` varchar(100) DEFAULT NULL,
  `orden` int(11) NOT NULL,
  `estado` varchar(1) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `segur_modulo`
--

INSERT INTO `segur_modulo` (`id`, `codigo`, `nombre`, `logo`, `logo_rec`, `orden`, `estado`, `created_at`, `updated_at`) VALUES
(1, 'comercio', 'JASS PROCESOS', 'storage/segur/modulos/logos/e7ff8a0f66db4a289c63442fdbf63cd2.png', 'storage/segur/modulos/logos/e458a0a2466e44d58034929cd8d93460.png', 1, '1', '2018-07-09 16:07:32.000000', '2018-07-09 16:07:32.000000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `segur_tipo_menu`
--

CREATE TABLE `segur_tipo_menu` (
  `id` int(11) NOT NULL,
  `codigo` varchar(10) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `orden` int(11) NOT NULL,
  `estado` varchar(1) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `segur_tipo_menu`
--

INSERT INTO `segur_tipo_menu` (`id`, `codigo`, `nombre`, `orden`, `estado`, `created_at`, `updated_at`) VALUES
(1, 'toggle', 'Principal', 1, '1', '2018-07-09 15:51:05.000000', '2018-07-22 11:32:56.343800'),
(2, 'menu_list', 'Lista Menú', 2, '1', '2018-07-09 15:51:05.000000', '2018-07-22 11:32:46.948203');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `segur_usuario`
--

CREATE TABLE `segur_usuario` (
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  `id` char(32) NOT NULL,
  `persona_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `segur_usuario`
--

INSERT INTO `segur_usuario` (`password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`, `id`, `persona_id`) VALUES
('pbkdf2_sha256$100000$DtxKVgDqKMsw$sdaZWfeUDuzi8hOMwKi7eRoOPhZ0+FAuBniKBhukQNg=', '2018-07-24 18:15:05.298141', 1, 'admin', '', '', '', 1, 1, '2018-07-03 15:27:14.795459', 'd66e5064c6024126859be51a7e5b8e31', 18),
('pbkdf2_sha256$100000$iZyJkfnOhOTw$3fWiabRQNtv6xnO/7kMeO0Tb0yzReRty7/vjCUw2yMw=', '2018-07-23 14:27:17.558995', 0, 'luis.lavado', 'Admin 2', 'Admin 2', '', 0, 1, '2018-07-08 16:18:29.000000', 'ecff37b639854cc1874a2198fa019530', 26);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `segur_usuario_entidades`
--

CREATE TABLE `segur_usuario_entidades` (
  `id` int(11) NOT NULL,
  `usuario_id` char(32) NOT NULL,
  `entidad_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `segur_usuario_entidades`
--

INSERT INTO `segur_usuario_entidades` (`id`, `usuario_id`, `entidad_id`) VALUES
(3, 'd66e5064c6024126859be51a7e5b8e31', 1),
(2, 'ecff37b639854cc1874a2198fa019530', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `segur_usuario_groups`
--

CREATE TABLE `segur_usuario_groups` (
  `id` int(11) NOT NULL,
  `usuario_id` char(32) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `segur_usuario_user_permissions`
--

CREATE TABLE `segur_usuario_user_permissions` (
  `id` int(11) NOT NULL,
  `usuario_id` char(32) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `segur_usuario_user_permissions`
--

INSERT INTO `segur_usuario_user_permissions` (`id`, `usuario_id`, `permission_id`) VALUES
(14, 'ecff37b639854cc1874a2198fa019530', 230),
(13, 'ecff37b639854cc1874a2198fa019530', 234);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indices de la tabla `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indices de la tabla `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indices de la tabla `caja_acceso_cuenta`
--
ALTER TABLE `caja_acceso_cuenta`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `caja_acceso_cuenta_cuenta_entidad_id_persona_id_d603ceeb_uniq` (`cuenta_entidad_id`,`persona_id`),
  ADD KEY `caja_acceso_cuenta_persona_id_8189acdf_fk_comun_persona_id` (`persona_id`),
  ADD KEY `caja_acceso_cuenta_usuario_id_4d228542_fk_comun_persona_id` (`usuario_id`);

--
-- Indices de la tabla `caja_amortizacion`
--
ALTER TABLE `caja_amortizacion`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `caja_amortizacion_movimiento_cuenta_id_cro_26d70481_uniq` (`movimiento_cuenta_id`,`cronograma_pago_id`),
  ADD KEY `caja_amortizacion_cronograma_pago_id_7fa2910e_fk_procesos_` (`cronograma_pago_id`);

--
-- Indices de la tabla `caja_cuenta_entidad`
--
ALTER TABLE `caja_cuenta_entidad`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `caja_cuenta_entidad_entidad_id_tipo_cuenta_i_88101d0e_uniq` (`entidad_id`,`tipo_cuenta_id`,`persona_cuenta_id`,`nombre`),
  ADD KEY `caja_cuenta_entidad_persona_cuenta_id_86c2dd2c_fk_caja_pers` (`persona_cuenta_id`),
  ADD KEY `caja_cuenta_entidad_tipo_cuenta_id_4959a42d_fk_caja_tipo` (`tipo_cuenta_id`),
  ADD KEY `caja_cuenta_entidad_usuario_id_2fab641c_fk_comun_persona_id` (`usuario_id`);

--
-- Indices de la tabla `caja_entidad_financiera`
--
ALTER TABLE `caja_entidad_financiera`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `caja_entidad_financiera_tipo_ent_finan_id_nombre_14c529fc_uniq` (`tipo_ent_finan_id`,`nombre`);

--
-- Indices de la tabla `caja_estado_cuenta`
--
ALTER TABLE `caja_estado_cuenta`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `caja_estado_cuenta_entidad_id_persona_id_110c53c2_uniq` (`entidad_id`,`persona_id`),
  ADD KEY `caja_estado_cuenta_persona_id_2b0cdb98_fk_comun_persona_id` (`persona_id`);

--
-- Indices de la tabla `caja_movimiento_cuenta`
--
ALTER TABLE `caja_movimiento_cuenta`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `numero` (`numero`),
  ADD KEY `caja_movimiento_cuen_cuenta_entidad_id_5323d847_fk_caja_cuen` (`cuenta_entidad_id`),
  ADD KEY `caja_movimiento_cuenta_moneda_id_5a42d262_fk_comun_moneda_id` (`moneda_id`),
  ADD KEY `caja_movimiento_cuen_moneda_cambio_id_60fb20f2_fk_comun_mon` (`moneda_cambio_id`),
  ADD KEY `caja_movimiento_cuen_motivo_mov_cuenta_id_817156c3_fk_procesos_` (`motivo_mov_cuenta_id`),
  ADD KEY `caja_movimiento_cuenta_persona_id_32f1e087_fk_comun_persona_id` (`persona_id`),
  ADD KEY `caja_movimiento_cuen_tipo_medio_pago_id_1c18a503_fk_caja_tipo` (`tipo_medio_pago_id`),
  ADD KEY `caja_movimiento_cuenta_usuario_id_e5809a78_fk_comun_persona_id` (`usuario_id`);

--
-- Indices de la tabla `caja_persona_cuenta`
--
ALTER TABLE `caja_persona_cuenta`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `caja_persona_cuenta_entidad_financiera_id_numero_a2feadf9_uniq` (`entidad_financiera_id`,`numero`),
  ADD KEY `caja_persona_cuenta_persona_id_6296a6f3_fk_comun_persona_id` (`persona_id`),
  ADD KEY `caja_persona_cuenta_tipo_cuenta_finan_id_36d92787_fk_caja_tipo` (`tipo_cuenta_finan_id`);

--
-- Indices de la tabla `caja_tipo_cuenta`
--
ALTER TABLE `caja_tipo_cuenta`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `caja_tipo_cuenta_finan`
--
ALTER TABLE `caja_tipo_cuenta_finan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `caja_tipo_ent_finan`
--
ALTER TABLE `caja_tipo_ent_finan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `caja_tipo_medio_pago`
--
ALTER TABLE `caja_tipo_medio_pago`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD UNIQUE KEY `nombre` (`nombre`),
  ADD KEY `caja_tipo_medio_pago_tipo_cuenta_id_51adb091_fk_caja_tipo` (`tipo_cuenta_id`);

--
-- Indices de la tabla `comun_clasificacion_iiu`
--
ALTER TABLE `comun_clasificacion_iiu`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`);

--
-- Indices de la tabla `comun_config_direccion_tipo_persona`
--
ALTER TABLE `comun_config_direccion_tipo_persona`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `comun_config_direccion_t_tipo_persona_id_tipo_dir_0a98d1d6_uniq` (`tipo_persona_id`,`tipo_direccion_id`),
  ADD KEY `comun_config_direcci_tipo_direccion_id_1f1b76dc_fk_comun_tip` (`tipo_direccion_id`);

--
-- Indices de la tabla `comun_config_documento_tipo_persona`
--
ALTER TABLE `comun_config_documento_tipo_persona`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `comun_config_documento_t_tipo_persona_id_tipo_doc_28a32515_uniq` (`tipo_persona_id`,`tipo_documento_id`),
  ADD KEY `comun_config_documen_tipo_documento_id_0a7244f3_fk_comun_tip` (`tipo_documento_id`);

--
-- Indices de la tabla `comun_contador`
--
ALTER TABLE `comun_contador`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`);

--
-- Indices de la tabla `comun_continente`
--
ALTER TABLE `comun_continente`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`),
  ADD UNIQUE KEY `codigo` (`codigo`);

--
-- Indices de la tabla `comun_cuenta_contacto`
--
ALTER TABLE `comun_cuenta_contacto`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD KEY `comun_cuenta_contact_tipo_contacto_id_49be3982_fk_comun_tip` (`tipo_contacto_id`);

--
-- Indices de la tabla `comun_etiqueta_contacto`
--
ALTER TABLE `comun_etiqueta_contacto`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD UNIQUE KEY `comun_etiqueta_contacto_tipo_contacto_id_nombre_93149e4f_uniq` (`tipo_contacto_id`,`nombre`);

--
-- Indices de la tabla `comun_mes`
--
ALTER TABLE `comun_mes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `comun_moneda`
--
ALTER TABLE `comun_moneda`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `comun_monedap`
--
ALTER TABLE `comun_monedap`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comun_monedap_fk_comun_moneda_id` (`moneda_id`);

--
-- Indices de la tabla `comun_pais`
--
ALTER TABLE `comun_pais`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD KEY `comun_pais_continente_id_2ebb8b4d_fk_comun_continente_id` (`continente_id`);

--
-- Indices de la tabla `comun_pais_tipo_ubigeo`
--
ALTER TABLE `comun_pais_tipo_ubigeo`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `comun_pais_tipo_ubigeo_pais_id_tipo_ubigeo_id_95c04eb4_uniq` (`pais_id`,`tipo_ubigeo_id`),
  ADD KEY `comun_pais_tipo_ubig_tipo_ubigeo_id_1e3efe4d_fk_comun_tip` (`tipo_ubigeo_id`);

--
-- Indices de la tabla `comun_persona`
--
ALTER TABLE `comun_persona`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD KEY `comun_persona_nacionalidad_id_ac032fa4_fk_comun_pais_id` (`nacionalidad_id`),
  ADD KEY `comun_persona_tipo_contribuyente_i_ca2fe139_fk_comun_tip` (`tipo_contribuyente_id`),
  ADD KEY `comun_persona_tipo_persona_id_77523f88_fk_comun_tipo_persona_id` (`tipo_persona_id`);

--
-- Indices de la tabla `comun_persona_contacto`
--
ALTER TABLE `comun_persona_contacto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comun_persona_contac_cuenta_contacto_id_8fd44b7e_fk_comun_cue` (`cuenta_contacto_id`),
  ADD KEY `comun_persona_contac_etiqueta_contacto_id_9c4dd1c5_fk_comun_eti` (`etiqueta_contacto_id`),
  ADD KEY `comun_persona_contacto_pais_id_20e93f29_fk_comun_pais_id` (`pais_id`),
  ADD KEY `comun_persona_contacto_persona_id_02335016_fk_comun_persona_id` (`persona_id`),
  ADD KEY `comun_persona_contac_tipo_contacto_id_3aa47a2b_fk_comun_tip` (`tipo_contacto_id`),
  ADD KEY `comun_persona_contacto_ubigeo_id_db28dd22_fk_comun_ubigeo_id` (`ubigeo_id`);

--
-- Indices de la tabla `comun_persona_documento`
--
ALTER TABLE `comun_persona_documento`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `comun_persona_documento_persona_id_tipo_document_37a31a74_uniq` (`persona_id`,`tipo_documento_id`),
  ADD KEY `comun_persona_docume_tipo_documento_id_1c04deb0_fk_comun_tip` (`tipo_documento_id`);

--
-- Indices de la tabla `comun_persona_fecha`
--
ALTER TABLE `comun_persona_fecha`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `comun_persona_fecha_persona_id_tipo_fecha_id_64c5213a_uniq` (`persona_id`,`tipo_fecha_id`),
  ADD KEY `comun_persona_fecha_mes_id_74d4f19c_fk_comun_mes_id` (`mes_id`),
  ADD KEY `comun_persona_fecha_tipo_fecha_id_09ef1c3c_fk_comun_tip` (`tipo_fecha_id`);

--
-- Indices de la tabla `comun_persona_juridica`
--
ALTER TABLE `comun_persona_juridica`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comun_persona_juridi_repres_legal_id_5f28206a_fk_comun_per` (`repres_legal_id`),
  ADD KEY `comun_persona_juridi_tipo_ambito_id_51fd5eda_fk_comun_tip` (`tipo_ambito_id`);

--
-- Indices de la tabla `comun_persona_juridica_actividad`
--
ALTER TABLE `comun_persona_juridica_actividad`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `comun_persona_juridica_a_persona_natural_id_clasi_04d2c6a7_uniq` (`persona_natural_id`,`clasificacion_iiu_id`),
  ADD KEY `comun_persona_juridi_clasificacion_iiu_id_9c3fad9a_fk_comun_cla` (`clasificacion_iiu_id`);

--
-- Indices de la tabla `comun_persona_natural`
--
ALTER TABLE `comun_persona_natural`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `comun_persona_natural_oficio`
--
ALTER TABLE `comun_persona_natural_oficio`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comun_persona_natura_persona_natural_id_81a7d67e_fk_comun_per` (`persona_natural_id`),
  ADD KEY `comun_persona_natura_profesion_oficio_id_eac1a92a_fk_comun_pro` (`profesion_oficio_id`);

--
-- Indices de la tabla `comun_profesion_oficio`
--
ALTER TABLE `comun_profesion_oficio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `comun_region_natural`
--
ALTER TABLE `comun_region_natural`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `comun_tipo_ambito`
--
ALTER TABLE `comun_tipo_ambito`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `comun_tipo_contacto`
--
ALTER TABLE `comun_tipo_contacto`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `comun_tipo_contribuyente`
--
ALTER TABLE `comun_tipo_contribuyente`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `comun_tipo_direccion`
--
ALTER TABLE `comun_tipo_direccion`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `comun_tipo_documento`
--
ALTER TABLE `comun_tipo_documento`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `comun_tipo_entidad`
--
ALTER TABLE `comun_tipo_entidad`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `comun_tipo_fecha`
--
ALTER TABLE `comun_tipo_fecha`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD UNIQUE KEY `nombre` (`nombre`),
  ADD KEY `comun_tipo_fecha_tipo_persona_id_edac9520_fk_comun_tip` (`tipo_persona_id`);

--
-- Indices de la tabla `comun_tipo_numero_via`
--
ALTER TABLE `comun_tipo_numero_via`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `comun_tipo_persona`
--
ALTER TABLE `comun_tipo_persona`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `comun_tipo_ubigeo`
--
ALTER TABLE `comun_tipo_ubigeo`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `comun_tipo_via`
--
ALTER TABLE `comun_tipo_via`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `comun_tipo_zona`
--
ALTER TABLE `comun_tipo_zona`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `comun_ubigeo`
--
ALTER TABLE `comun_ubigeo`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `comun_ubigeo_pais_id_nombre_tipo_ubig_2f268b02_uniq` (`pais_id`,`nombre`,`tipo_ubigeo_id`,`padre_id`),
  ADD KEY `comun_ubigeo_padre_id_1287eba7_fk_comun_ubigeo_id` (`padre_id`),
  ADD KEY `comun_ubigeo_region_natural_id_050dc43a_fk_comun_reg` (`region_natural_id`),
  ADD KEY `comun_ubigeo_tipo_ubigeo_id_8a17adb1_fk_comun_tipo_ubigeo_id` (`tipo_ubigeo_id`);

--
-- Indices de la tabla `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_segur_usuario_id` (`user_id`);

--
-- Indices de la tabla `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indices de la tabla `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Indices de la tabla `entidad`
--
ALTER TABLE `entidad`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `persona_id` (`persona_id`),
  ADD KEY `entidad_moneda_id_e46fe142_fk_comun_moneda_id` (`moneda_id`),
  ADD KEY `entidad_padre_id_cc65f5d3_fk_entidad_id` (`padre_id`),
  ADD KEY `entidad_tipo_entidad_id_88845b9c_fk_comun_tipo_entidad_id` (`tipo_entidad_id`);

--
-- Indices de la tabla `oauth2_provider_accesstoken`
--
ALTER TABLE `oauth2_provider_accesstoken`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `oauth2_provider_accesstoken_token_8af090f8_uniq` (`token`),
  ADD UNIQUE KEY `source_refresh_token_id` (`source_refresh_token_id`),
  ADD KEY `oauth2_provider_accesstoken_user_id_6e4c9a65_fk_segur_usuario_id` (`user_id`),
  ADD KEY `oauth2_provider_accesstoken_application_id_b22886e1_fk` (`application_id`);

--
-- Indices de la tabla `oauth2_provider_application`
--
ALTER TABLE `oauth2_provider_application`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `client_id` (`client_id`),
  ADD KEY `oauth2_provider_application_client_secret_53133678` (`client_secret`),
  ADD KEY `oauth2_provider_application_user_id_79829054_fk_segur_usuario_id` (`user_id`);

--
-- Indices de la tabla `oauth2_provider_grant`
--
ALTER TABLE `oauth2_provider_grant`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `oauth2_provider_grant_code_49ab4ddf_uniq` (`code`),
  ADD KEY `oauth2_provider_grant_application_id_81923564_fk` (`application_id`),
  ADD KEY `oauth2_provider_grant_user_id_e8f62af8_fk_segur_usuario_id` (`user_id`);

--
-- Indices de la tabla `oauth2_provider_refreshtoken`
--
ALTER TABLE `oauth2_provider_refreshtoken`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `access_token_id` (`access_token_id`),
  ADD UNIQUE KEY `oauth2_provider_refreshtoken_token_revoked_af8a5134_uniq` (`token`,`revoked`),
  ADD KEY `oauth2_provider_refreshtoken_application_id_2d1c311b_fk` (`application_id`),
  ADD KEY `oauth2_provider_refr_user_id_da837fce_fk_segur_usu` (`user_id`);

--
-- Indices de la tabla `procesos_comprobante`
--
ALTER TABLE `procesos_comprobante`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `procesos_comprob_config`
--
ALTER TABLE `procesos_comprob_config`
  ADD PRIMARY KEY (`id`),
  ADD KEY `procesos_comprob_con_comprobante_id_76065044_fk_procesos_` (`comprobante_id`),
  ADD KEY `procesos_comprob_config_entidad_id_940b5400_fk_entidad_id` (`entidad_id`);

--
-- Indices de la tabla `procesos_contrato`
--
ALTER TABLE `procesos_contrato`
  ADD PRIMARY KEY (`id`),
  ADD KEY `procesos_contrato_persona_id_5276601a_fk_comun_persona_id` (`persona_id`),
  ADD KEY `procesos_contrato_predio_id_3e8946c9_fk_procesos_predio_id` (`predio_id`),
  ADD KEY `procesos_contrato_tipo_uso_servicio_id_ea701348_fk_procesos_` (`tipo_uso_servicio_id`),
  ADD KEY `procesos_contrato_usuario_id_5df515b0_fk_segur_usuario_id` (`usuario_id`),
  ADD KEY `procesos_contrato_comprob_config_id_affb8da4_fk_procesos_` (`comprob_config_id`),
  ADD KEY `procesos_contrato_entidad_id_4b940e27_fk_entidad_id` (`entidad_id`);

--
-- Indices de la tabla `procesos_contrato_servicio`
--
ALTER TABLE `procesos_contrato_servicio`
  ADD PRIMARY KEY (`id`),
  ADD KEY `procesos_contrato_se_contrato_id_e601b4fb_fk_procesos_` (`contrato_id`),
  ADD KEY `procesos_contrato_se_servicio_id_41a8701c_fk_procesos_` (`servicio_id`);

--
-- Indices de la tabla `procesos_cronograma_pago`
--
ALTER TABLE `procesos_cronograma_pago`
  ADD PRIMARY KEY (`id`),
  ADD KEY `procesos_cronograma__contrato_id_71c244cc_fk_procesos_` (`contrato_id`),
  ADD KEY `procesos_cronograma__padre_id_b1d47d9f_fk_procesos_` (`padre_id`),
  ADD KEY `procesos_cronograma_pago_persona_id_80068302_fk_comun_persona_id` (`persona_id`),
  ADD KEY `procesos_cronograma__estado_cronograma_id_1ebc1b2e_fk_procesos_` (`estado_cronograma_id`),
  ADD KEY `procesos_cronograma_pago_usuario_id_c28baec8_fk_comun_persona_id` (`usuario_id`),
  ADD KEY `procesos_cronograma_pago_entidad_id_34a4f4aa` (`entidad_id`),
  ADD KEY `procesos_cronograma__motivo_movimiento_cu_67d45e9a_fk_procesos_` (`motivo_movimiento_cuenta_id`);

--
-- Indices de la tabla `procesos_direccion`
--
ALTER TABLE `procesos_direccion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `procesos_direccion_pais_id_651fdd6f_fk_comun_pais_id` (`pais_id`),
  ADD KEY `procesos_direccion_persona_id_fa2ba981_fk_comun_persona_id` (`persona_id`),
  ADD KEY `procesos_direccion_predio_id_14a0b8e9_fk_procesos_predio_id` (`predio_id`),
  ADD KEY `procesos_direccion_tipo_direccion_id_e70760aa_fk_comun_tip` (`tipo_direccion_id`),
  ADD KEY `procesos_direccion_tipo_numero_via_id_5c64d0ab_fk_comun_tip` (`tipo_numero_via_id`),
  ADD KEY `procesos_direccion_tipo_via_id_ff0333df_fk_comun_tipo_via_id` (`tipo_via_id`),
  ADD KEY `procesos_direccion_tipo_zona_id_cc172c0f_fk_comun_tipo_zona_id` (`tipo_zona_id`),
  ADD KEY `procesos_direccion_ubigeo_id_235be796_fk_comun_ubigeo_id` (`ubigeo_id`);

--
-- Indices de la tabla `procesos_estado_cronograma`
--
ALTER TABLE `procesos_estado_cronograma`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `procesos_estado_predio`
--
ALTER TABLE `procesos_estado_predio`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `procesos_mes_recibo`
--
ALTER TABLE `procesos_mes_recibo`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `procesos_mes_recibo_entidad_id_mes_id_anio_aab0bf36_uniq` (`entidad_id`,`mes_id`,`anio`),
  ADD KEY `procesos_mes_recibo_mes_id_ac5457e2_fk_comun_mes_id` (`mes_id`),
  ADD KEY `procesos_mes_recibo_usuario_id_509984cd_fk_comun_persona_id` (`usuario_id`);

--
-- Indices de la tabla `procesos_motivo_movimiento_cuenta`
--
ALTER TABLE `procesos_motivo_movimiento_cuenta`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD KEY `procesos_motivo_movi_tipo_movimiento_cuen_0aeba605_fk_procesos_` (`tipo_movimiento_cuenta_id`);

--
-- Indices de la tabla `procesos_predio`
--
ALTER TABLE `procesos_predio`
  ADD PRIMARY KEY (`id`),
  ADD KEY `procesos_predio_duenio_id_12df9c25_fk_comun_persona_id` (`duenio_id`),
  ADD KEY `procesos_predio_estado_predio_id_b5cf80cc_fk_procesos_` (`estado_predio_id`),
  ADD KEY `procesos_predio_representante_id_ea92a73f_fk_comun_persona_id` (`representante_id`),
  ADD KEY `procesos_predio_zona_id_9d59ed36_fk_procesos_zona_id` (`zona_id`),
  ADD KEY `procesos_predio_usuario_id_54525d61_fk_comun_persona_id` (`usuario_id`);

--
-- Indices de la tabla `procesos_recibo`
--
ALTER TABLE `procesos_recibo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `procesos_recibo_comprob_config_id_be1d8ddf_fk_procesos_` (`comprob_config_id`),
  ADD KEY `procesos_recibo_contrato_id_82ae379a_fk_procesos_contrato_id` (`contrato_id`),
  ADD KEY `procesos_recibo_mes_recibo_id_221b3992_fk_procesos_mes_recibo_id` (`mes_recibo_id`),
  ADD KEY `procesos_recibo_usuario_id_8dcf4f66_fk_comun_persona_id` (`usuario_id`);

--
-- Indices de la tabla `procesos_servicio`
--
ALTER TABLE `procesos_servicio`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD UNIQUE KEY `nombre` (`nombre`),
  ADD KEY `procesos_servicio_padre_id_81a7eca9_fk_procesos_servicio_id` (`padre_id`);

--
-- Indices de la tabla `procesos_tipo_movimiento_cuenta`
--
ALTER TABLE `procesos_tipo_movimiento_cuenta`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`),
  ADD UNIQUE KEY `codigo` (`codigo`);

--
-- Indices de la tabla `procesos_tipo_uso_servicio`
--
ALTER TABLE `procesos_tipo_uso_servicio`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `procesos_zona`
--
ALTER TABLE `procesos_zona`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD UNIQUE KEY `procesos_zona_nombre_ubigeo_id_tipo_zona_id_174dfa94_uniq` (`nombre`,`ubigeo_id`,`tipo_zona_id`),
  ADD KEY `procesos_zona_tipo_zona_id_8f066139_fk_comun_tipo_zona_id` (`tipo_zona_id`),
  ADD KEY `procesos_zona_ubigeo_id_7f5766dd_fk_comun_ubigeo_id` (`ubigeo_id`);

--
-- Indices de la tabla `segur_menu`
--
ALTER TABLE `segur_menu`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD KEY `segur_menu_modulo_id_1fa185be_fk_segur_modulo_id` (`modulo_id`),
  ADD KEY `segur_menu_padre_id_e2af8cc4_fk_segur_menu_id` (`padre_id`),
  ADD KEY `segur_menu_permiso_id_0caaddda_fk_auth_permission_id` (`permiso_id`),
  ADD KEY `segur_menu_tipo_menu_id_bf299ab5_fk_segur_tipo_menu_id` (`tipo_menu_id`);

--
-- Indices de la tabla `segur_modulo`
--
ALTER TABLE `segur_modulo`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `segur_tipo_menu`
--
ALTER TABLE `segur_tipo_menu`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `segur_usuario`
--
ALTER TABLE `segur_usuario`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `persona_id` (`persona_id`);

--
-- Indices de la tabla `segur_usuario_entidades`
--
ALTER TABLE `segur_usuario_entidades`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `segur_usuario_entidades_usuario_id_entidad_id_7d50edd5_uniq` (`usuario_id`,`entidad_id`),
  ADD KEY `segur_usuario_entidades_entidad_id_dc7c5281_fk_entidad_id` (`entidad_id`);

--
-- Indices de la tabla `segur_usuario_groups`
--
ALTER TABLE `segur_usuario_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `segur_usuario_groups_usuario_id_group_id_e42793e8_uniq` (`usuario_id`,`group_id`),
  ADD KEY `segur_usuario_groups_group_id_895125cd_fk_auth_group_id` (`group_id`);

--
-- Indices de la tabla `segur_usuario_user_permissions`
--
ALTER TABLE `segur_usuario_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `segur_usuario_user_permi_usuario_id_permission_id_476eb84a_uniq` (`usuario_id`,`permission_id`),
  ADD KEY `segur_usuario_user_p_permission_id_b2b070ad_fk_auth_perm` (`permission_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de la tabla `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=553;

--
-- AUTO_INCREMENT de la tabla `caja_acceso_cuenta`
--
ALTER TABLE `caja_acceso_cuenta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `caja_amortizacion`
--
ALTER TABLE `caja_amortizacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `caja_cuenta_entidad`
--
ALTER TABLE `caja_cuenta_entidad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `caja_entidad_financiera`
--
ALTER TABLE `caja_entidad_financiera`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `caja_estado_cuenta`
--
ALTER TABLE `caja_estado_cuenta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `caja_movimiento_cuenta`
--
ALTER TABLE `caja_movimiento_cuenta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `caja_persona_cuenta`
--
ALTER TABLE `caja_persona_cuenta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `caja_tipo_cuenta`
--
ALTER TABLE `caja_tipo_cuenta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `caja_tipo_cuenta_finan`
--
ALTER TABLE `caja_tipo_cuenta_finan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `caja_tipo_ent_finan`
--
ALTER TABLE `caja_tipo_ent_finan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `caja_tipo_medio_pago`
--
ALTER TABLE `caja_tipo_medio_pago`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `comun_clasificacion_iiu`
--
ALTER TABLE `comun_clasificacion_iiu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT de la tabla `comun_config_direccion_tipo_persona`
--
ALTER TABLE `comun_config_direccion_tipo_persona`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `comun_config_documento_tipo_persona`
--
ALTER TABLE `comun_config_documento_tipo_persona`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `comun_contador`
--
ALTER TABLE `comun_contador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `comun_continente`
--
ALTER TABLE `comun_continente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `comun_cuenta_contacto`
--
ALTER TABLE `comun_cuenta_contacto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `comun_etiqueta_contacto`
--
ALTER TABLE `comun_etiqueta_contacto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `comun_moneda`
--
ALTER TABLE `comun_moneda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `comun_pais`
--
ALTER TABLE `comun_pais`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `comun_pais_tipo_ubigeo`
--
ALTER TABLE `comun_pais_tipo_ubigeo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `comun_persona`
--
ALTER TABLE `comun_persona`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT de la tabla `comun_persona_contacto`
--
ALTER TABLE `comun_persona_contacto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `comun_persona_documento`
--
ALTER TABLE `comun_persona_documento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `comun_persona_fecha`
--
ALTER TABLE `comun_persona_fecha`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `comun_persona_juridica_actividad`
--
ALTER TABLE `comun_persona_juridica_actividad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `comun_persona_natural_oficio`
--
ALTER TABLE `comun_persona_natural_oficio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `comun_profesion_oficio`
--
ALTER TABLE `comun_profesion_oficio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT de la tabla `comun_region_natural`
--
ALTER TABLE `comun_region_natural`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `comun_tipo_ambito`
--
ALTER TABLE `comun_tipo_ambito`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `comun_tipo_contacto`
--
ALTER TABLE `comun_tipo_contacto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `comun_tipo_contribuyente`
--
ALTER TABLE `comun_tipo_contribuyente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT de la tabla `comun_tipo_direccion`
--
ALTER TABLE `comun_tipo_direccion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `comun_tipo_documento`
--
ALTER TABLE `comun_tipo_documento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `comun_tipo_entidad`
--
ALTER TABLE `comun_tipo_entidad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `comun_tipo_fecha`
--
ALTER TABLE `comun_tipo_fecha`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `comun_tipo_numero_via`
--
ALTER TABLE `comun_tipo_numero_via`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `comun_tipo_persona`
--
ALTER TABLE `comun_tipo_persona`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `comun_tipo_ubigeo`
--
ALTER TABLE `comun_tipo_ubigeo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `comun_tipo_via`
--
ALTER TABLE `comun_tipo_via`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `comun_tipo_zona`
--
ALTER TABLE `comun_tipo_zona`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `comun_ubigeo`
--
ALTER TABLE `comun_ubigeo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2056;

--
-- AUTO_INCREMENT de la tabla `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT de la tabla `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT de la tabla `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=163;

--
-- AUTO_INCREMENT de la tabla `entidad`
--
ALTER TABLE `entidad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `oauth2_provider_accesstoken`
--
ALTER TABLE `oauth2_provider_accesstoken`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;

--
-- AUTO_INCREMENT de la tabla `oauth2_provider_application`
--
ALTER TABLE `oauth2_provider_application`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `oauth2_provider_grant`
--
ALTER TABLE `oauth2_provider_grant`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `oauth2_provider_refreshtoken`
--
ALTER TABLE `oauth2_provider_refreshtoken`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;

--
-- AUTO_INCREMENT de la tabla `procesos_comprobante`
--
ALTER TABLE `procesos_comprobante`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `procesos_comprob_config`
--
ALTER TABLE `procesos_comprob_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `procesos_contrato`
--
ALTER TABLE `procesos_contrato`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `procesos_contrato_servicio`
--
ALTER TABLE `procesos_contrato_servicio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `procesos_cronograma_pago`
--
ALTER TABLE `procesos_cronograma_pago`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `procesos_direccion`
--
ALTER TABLE `procesos_direccion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `procesos_estado_cronograma`
--
ALTER TABLE `procesos_estado_cronograma`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `procesos_estado_predio`
--
ALTER TABLE `procesos_estado_predio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `procesos_mes_recibo`
--
ALTER TABLE `procesos_mes_recibo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `procesos_motivo_movimiento_cuenta`
--
ALTER TABLE `procesos_motivo_movimiento_cuenta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `procesos_predio`
--
ALTER TABLE `procesos_predio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT de la tabla `procesos_recibo`
--
ALTER TABLE `procesos_recibo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `procesos_servicio`
--
ALTER TABLE `procesos_servicio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `procesos_tipo_movimiento_cuenta`
--
ALTER TABLE `procesos_tipo_movimiento_cuenta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `procesos_tipo_uso_servicio`
--
ALTER TABLE `procesos_tipo_uso_servicio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `procesos_zona`
--
ALTER TABLE `procesos_zona`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `segur_menu`
--
ALTER TABLE `segur_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT de la tabla `segur_modulo`
--
ALTER TABLE `segur_modulo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `segur_tipo_menu`
--
ALTER TABLE `segur_tipo_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `segur_usuario_entidades`
--
ALTER TABLE `segur_usuario_entidades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `segur_usuario_groups`
--
ALTER TABLE `segur_usuario_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `segur_usuario_user_permissions`
--
ALTER TABLE `segur_usuario_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Filtros para la tabla `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Filtros para la tabla `caja_acceso_cuenta`
--
ALTER TABLE `caja_acceso_cuenta`
  ADD CONSTRAINT `caja_acceso_cuenta_cuenta_entidad_id_9ae96e90_fk_caja_cuen` FOREIGN KEY (`cuenta_entidad_id`) REFERENCES `caja_cuenta_entidad` (`id`),
  ADD CONSTRAINT `caja_acceso_cuenta_persona_id_8189acdf_fk_comun_persona_id` FOREIGN KEY (`persona_id`) REFERENCES `comun_persona` (`id`),
  ADD CONSTRAINT `caja_acceso_cuenta_usuario_id_4d228542_fk_comun_persona_id` FOREIGN KEY (`usuario_id`) REFERENCES `comun_persona` (`id`);

--
-- Filtros para la tabla `caja_amortizacion`
--
ALTER TABLE `caja_amortizacion`
  ADD CONSTRAINT `caja_amortizacion_cronograma_pago_id_7fa2910e_fk_procesos_` FOREIGN KEY (`cronograma_pago_id`) REFERENCES `procesos_cronograma_pago` (`id`),
  ADD CONSTRAINT `caja_amortizacion_movimiento_cuenta_id_0a447e11_fk_caja_movi` FOREIGN KEY (`movimiento_cuenta_id`) REFERENCES `caja_movimiento_cuenta` (`id`);

--
-- Filtros para la tabla `caja_cuenta_entidad`
--
ALTER TABLE `caja_cuenta_entidad`
  ADD CONSTRAINT `caja_cuenta_entidad_entidad_id_40393d22_fk_entidad_id` FOREIGN KEY (`entidad_id`) REFERENCES `entidad` (`id`),
  ADD CONSTRAINT `caja_cuenta_entidad_persona_cuenta_id_86c2dd2c_fk_caja_pers` FOREIGN KEY (`persona_cuenta_id`) REFERENCES `caja_persona_cuenta` (`id`),
  ADD CONSTRAINT `caja_cuenta_entidad_tipo_cuenta_id_4959a42d_fk_caja_tipo` FOREIGN KEY (`tipo_cuenta_id`) REFERENCES `caja_tipo_cuenta` (`id`),
  ADD CONSTRAINT `caja_cuenta_entidad_usuario_id_2fab641c_fk_comun_persona_id` FOREIGN KEY (`usuario_id`) REFERENCES `comun_persona` (`id`);

--
-- Filtros para la tabla `caja_entidad_financiera`
--
ALTER TABLE `caja_entidad_financiera`
  ADD CONSTRAINT `caja_entidad_financi_tipo_ent_finan_id_5c57a49c_fk_caja_tipo` FOREIGN KEY (`tipo_ent_finan_id`) REFERENCES `caja_tipo_ent_finan` (`id`);

--
-- Filtros para la tabla `caja_estado_cuenta`
--
ALTER TABLE `caja_estado_cuenta`
  ADD CONSTRAINT `caja_estado_cuenta_entidad_id_0117a879_fk_entidad_id` FOREIGN KEY (`entidad_id`) REFERENCES `entidad` (`id`),
  ADD CONSTRAINT `caja_estado_cuenta_persona_id_2b0cdb98_fk_comun_persona_id` FOREIGN KEY (`persona_id`) REFERENCES `comun_persona` (`id`);

--
-- Filtros para la tabla `caja_movimiento_cuenta`
--
ALTER TABLE `caja_movimiento_cuenta`
  ADD CONSTRAINT `caja_movimiento_cuen_cuenta_entidad_id_5323d847_fk_caja_cuen` FOREIGN KEY (`cuenta_entidad_id`) REFERENCES `caja_cuenta_entidad` (`id`),
  ADD CONSTRAINT `caja_movimiento_cuen_moneda_cambio_id_60fb20f2_fk_comun_mon` FOREIGN KEY (`moneda_cambio_id`) REFERENCES `comun_moneda` (`id`),
  ADD CONSTRAINT `caja_movimiento_cuen_motivo_mov_cuenta_id_817156c3_fk_procesos_` FOREIGN KEY (`motivo_mov_cuenta_id`) REFERENCES `procesos_motivo_movimiento_cuenta` (`id`),
  ADD CONSTRAINT `caja_movimiento_cuen_tipo_medio_pago_id_1c18a503_fk_caja_tipo` FOREIGN KEY (`tipo_medio_pago_id`) REFERENCES `caja_tipo_medio_pago` (`id`),
  ADD CONSTRAINT `caja_movimiento_cuenta_moneda_id_5a42d262_fk_comun_moneda_id` FOREIGN KEY (`moneda_id`) REFERENCES `comun_moneda` (`id`),
  ADD CONSTRAINT `caja_movimiento_cuenta_persona_id_32f1e087_fk_comun_persona_id` FOREIGN KEY (`persona_id`) REFERENCES `comun_persona` (`id`),
  ADD CONSTRAINT `caja_movimiento_cuenta_usuario_id_e5809a78_fk_comun_persona_id` FOREIGN KEY (`usuario_id`) REFERENCES `comun_persona` (`id`);

--
-- Filtros para la tabla `caja_persona_cuenta`
--
ALTER TABLE `caja_persona_cuenta`
  ADD CONSTRAINT `caja_persona_cuenta_entidad_financiera_i_1859bcf2_fk_caja_enti` FOREIGN KEY (`entidad_financiera_id`) REFERENCES `caja_entidad_financiera` (`id`),
  ADD CONSTRAINT `caja_persona_cuenta_persona_id_6296a6f3_fk_comun_persona_id` FOREIGN KEY (`persona_id`) REFERENCES `comun_persona` (`id`),
  ADD CONSTRAINT `caja_persona_cuenta_tipo_cuenta_finan_id_36d92787_fk_caja_tipo` FOREIGN KEY (`tipo_cuenta_finan_id`) REFERENCES `caja_tipo_cuenta_finan` (`id`);

--
-- Filtros para la tabla `caja_tipo_medio_pago`
--
ALTER TABLE `caja_tipo_medio_pago`
  ADD CONSTRAINT `caja_tipo_medio_pago_tipo_cuenta_id_51adb091_fk_caja_tipo` FOREIGN KEY (`tipo_cuenta_id`) REFERENCES `caja_tipo_cuenta` (`id`);

--
-- Filtros para la tabla `comun_config_direccion_tipo_persona`
--
ALTER TABLE `comun_config_direccion_tipo_persona`
  ADD CONSTRAINT `comun_config_direcci_tipo_direccion_id_1f1b76dc_fk_comun_tip` FOREIGN KEY (`tipo_direccion_id`) REFERENCES `comun_tipo_direccion` (`id`),
  ADD CONSTRAINT `comun_config_direcci_tipo_persona_id_320e936d_fk_comun_tip` FOREIGN KEY (`tipo_persona_id`) REFERENCES `comun_tipo_persona` (`id`);

--
-- Filtros para la tabla `comun_config_documento_tipo_persona`
--
ALTER TABLE `comun_config_documento_tipo_persona`
  ADD CONSTRAINT `comun_config_documen_tipo_documento_id_0a7244f3_fk_comun_tip` FOREIGN KEY (`tipo_documento_id`) REFERENCES `comun_tipo_documento` (`id`),
  ADD CONSTRAINT `comun_config_documen_tipo_persona_id_5d1b882f_fk_comun_tip` FOREIGN KEY (`tipo_persona_id`) REFERENCES `comun_tipo_persona` (`id`);

--
-- Filtros para la tabla `comun_cuenta_contacto`
--
ALTER TABLE `comun_cuenta_contacto`
  ADD CONSTRAINT `comun_cuenta_contact_tipo_contacto_id_49be3982_fk_comun_tip` FOREIGN KEY (`tipo_contacto_id`) REFERENCES `comun_tipo_contacto` (`id`);

--
-- Filtros para la tabla `comun_etiqueta_contacto`
--
ALTER TABLE `comun_etiqueta_contacto`
  ADD CONSTRAINT `comun_etiqueta_conta_tipo_contacto_id_f2f2d271_fk_comun_tip` FOREIGN KEY (`tipo_contacto_id`) REFERENCES `comun_tipo_contacto` (`id`);

--
-- Filtros para la tabla `comun_monedap`
--
ALTER TABLE `comun_monedap`
  ADD CONSTRAINT `comun_monedap_fk_comun_moneda_id` FOREIGN KEY (`moneda_id`) REFERENCES `comun_moneda` (`id`);

--
-- Filtros para la tabla `comun_pais`
--
ALTER TABLE `comun_pais`
  ADD CONSTRAINT `comun_pais_continente_id_2ebb8b4d_fk_comun_continente_id` FOREIGN KEY (`continente_id`) REFERENCES `comun_continente` (`id`);

--
-- Filtros para la tabla `comun_pais_tipo_ubigeo`
--
ALTER TABLE `comun_pais_tipo_ubigeo`
  ADD CONSTRAINT `comun_pais_tipo_ubig_tipo_ubigeo_id_1e3efe4d_fk_comun_tip` FOREIGN KEY (`tipo_ubigeo_id`) REFERENCES `comun_tipo_ubigeo` (`id`),
  ADD CONSTRAINT `comun_pais_tipo_ubigeo_pais_id_1c6c4b30_fk_comun_pais_id` FOREIGN KEY (`pais_id`) REFERENCES `comun_pais` (`id`);

--
-- Filtros para la tabla `comun_persona`
--
ALTER TABLE `comun_persona`
  ADD CONSTRAINT `comun_persona_nacionalidad_id_ac032fa4_fk_comun_pais_id` FOREIGN KEY (`nacionalidad_id`) REFERENCES `comun_pais` (`id`),
  ADD CONSTRAINT `comun_persona_tipo_contribuyente_i_ca2fe139_fk_comun_tip` FOREIGN KEY (`tipo_contribuyente_id`) REFERENCES `comun_tipo_contribuyente` (`id`),
  ADD CONSTRAINT `comun_persona_tipo_persona_id_77523f88_fk_comun_tipo_persona_id` FOREIGN KEY (`tipo_persona_id`) REFERENCES `comun_tipo_persona` (`id`);

--
-- Filtros para la tabla `comun_persona_contacto`
--
ALTER TABLE `comun_persona_contacto`
  ADD CONSTRAINT `comun_persona_contac_cuenta_contacto_id_8fd44b7e_fk_comun_cue` FOREIGN KEY (`cuenta_contacto_id`) REFERENCES `comun_cuenta_contacto` (`id`),
  ADD CONSTRAINT `comun_persona_contac_etiqueta_contacto_id_9c4dd1c5_fk_comun_eti` FOREIGN KEY (`etiqueta_contacto_id`) REFERENCES `comun_etiqueta_contacto` (`id`),
  ADD CONSTRAINT `comun_persona_contac_tipo_contacto_id_3aa47a2b_fk_comun_tip` FOREIGN KEY (`tipo_contacto_id`) REFERENCES `comun_tipo_contacto` (`id`),
  ADD CONSTRAINT `comun_persona_contacto_pais_id_20e93f29_fk_comun_pais_id` FOREIGN KEY (`pais_id`) REFERENCES `comun_pais` (`id`),
  ADD CONSTRAINT `comun_persona_contacto_persona_id_02335016_fk_comun_persona_id` FOREIGN KEY (`persona_id`) REFERENCES `comun_persona` (`id`),
  ADD CONSTRAINT `comun_persona_contacto_ubigeo_id_db28dd22_fk_comun_ubigeo_id` FOREIGN KEY (`ubigeo_id`) REFERENCES `comun_ubigeo` (`id`);

--
-- Filtros para la tabla `comun_persona_documento`
--
ALTER TABLE `comun_persona_documento`
  ADD CONSTRAINT `comun_persona_docume_tipo_documento_id_1c04deb0_fk_comun_tip` FOREIGN KEY (`tipo_documento_id`) REFERENCES `comun_tipo_documento` (`id`),
  ADD CONSTRAINT `comun_persona_documento_persona_id_a120f807_fk_comun_persona_id` FOREIGN KEY (`persona_id`) REFERENCES `comun_persona` (`id`);

--
-- Filtros para la tabla `comun_persona_fecha`
--
ALTER TABLE `comun_persona_fecha`
  ADD CONSTRAINT `comun_persona_fecha_mes_id_74d4f19c_fk_comun_mes_id` FOREIGN KEY (`mes_id`) REFERENCES `comun_mes` (`id`),
  ADD CONSTRAINT `comun_persona_fecha_persona_id_3270f8c9_fk_comun_persona_id` FOREIGN KEY (`persona_id`) REFERENCES `comun_persona` (`id`),
  ADD CONSTRAINT `comun_persona_fecha_tipo_fecha_id_09ef1c3c_fk_comun_tip` FOREIGN KEY (`tipo_fecha_id`) REFERENCES `comun_tipo_fecha` (`id`);

--
-- Filtros para la tabla `comun_persona_juridica`
--
ALTER TABLE `comun_persona_juridica`
  ADD CONSTRAINT `comun_persona_juridi_repres_legal_id_5f28206a_fk_comun_per` FOREIGN KEY (`repres_legal_id`) REFERENCES `comun_persona` (`id`),
  ADD CONSTRAINT `comun_persona_juridi_tipo_ambito_id_51fd5eda_fk_comun_tip` FOREIGN KEY (`tipo_ambito_id`) REFERENCES `comun_tipo_ambito` (`id`),
  ADD CONSTRAINT `comun_persona_juridica_id_0f94ee14_fk_comun_persona_id` FOREIGN KEY (`id`) REFERENCES `comun_persona` (`id`);

--
-- Filtros para la tabla `comun_persona_juridica_actividad`
--
ALTER TABLE `comun_persona_juridica_actividad`
  ADD CONSTRAINT `comun_persona_juridi_clasificacion_iiu_id_9c3fad9a_fk_comun_cla` FOREIGN KEY (`clasificacion_iiu_id`) REFERENCES `comun_clasificacion_iiu` (`id`),
  ADD CONSTRAINT `comun_persona_juridi_persona_natural_id_e2ea7330_fk_comun_per` FOREIGN KEY (`persona_natural_id`) REFERENCES `comun_persona_juridica` (`id`);

--
-- Filtros para la tabla `comun_persona_natural`
--
ALTER TABLE `comun_persona_natural`
  ADD CONSTRAINT `comun_persona_natural_id_58c7e993_fk_comun_persona_id` FOREIGN KEY (`id`) REFERENCES `comun_persona` (`id`);

--
-- Filtros para la tabla `comun_persona_natural_oficio`
--
ALTER TABLE `comun_persona_natural_oficio`
  ADD CONSTRAINT `comun_persona_natura_persona_natural_id_81a7d67e_fk_comun_per` FOREIGN KEY (`persona_natural_id`) REFERENCES `comun_persona_natural` (`id`),
  ADD CONSTRAINT `comun_persona_natura_profesion_oficio_id_eac1a92a_fk_comun_pro` FOREIGN KEY (`profesion_oficio_id`) REFERENCES `comun_profesion_oficio` (`id`);

--
-- Filtros para la tabla `comun_tipo_fecha`
--
ALTER TABLE `comun_tipo_fecha`
  ADD CONSTRAINT `comun_tipo_fecha_tipo_persona_id_edac9520_fk_comun_tip` FOREIGN KEY (`tipo_persona_id`) REFERENCES `comun_tipo_persona` (`id`);

--
-- Filtros para la tabla `comun_ubigeo`
--
ALTER TABLE `comun_ubigeo`
  ADD CONSTRAINT `comun_ubigeo_padre_id_1287eba7_fk_comun_ubigeo_id` FOREIGN KEY (`padre_id`) REFERENCES `comun_ubigeo` (`id`),
  ADD CONSTRAINT `comun_ubigeo_pais_id_62f4df96_fk_comun_pais_id` FOREIGN KEY (`pais_id`) REFERENCES `comun_pais` (`id`),
  ADD CONSTRAINT `comun_ubigeo_region_natural_id_050dc43a_fk_comun_reg` FOREIGN KEY (`region_natural_id`) REFERENCES `comun_region_natural` (`id`),
  ADD CONSTRAINT `comun_ubigeo_tipo_ubigeo_id_8a17adb1_fk_comun_tipo_ubigeo_id` FOREIGN KEY (`tipo_ubigeo_id`) REFERENCES `comun_tipo_ubigeo` (`id`);

--
-- Filtros para la tabla `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_segur_usuario_id` FOREIGN KEY (`user_id`) REFERENCES `segur_usuario` (`id`);

--
-- Filtros para la tabla `entidad`
--
ALTER TABLE `entidad`
  ADD CONSTRAINT `entidad_moneda_id_e46fe142_fk_comun_moneda_id` FOREIGN KEY (`moneda_id`) REFERENCES `comun_moneda` (`id`),
  ADD CONSTRAINT `entidad_padre_id_cc65f5d3_fk_entidad_id` FOREIGN KEY (`padre_id`) REFERENCES `entidad` (`id`),
  ADD CONSTRAINT `entidad_persona_id_6a51c905_fk_comun_persona_id` FOREIGN KEY (`persona_id`) REFERENCES `comun_persona` (`id`),
  ADD CONSTRAINT `entidad_tipo_entidad_id_88845b9c_fk_comun_tipo_entidad_id` FOREIGN KEY (`tipo_entidad_id`) REFERENCES `comun_tipo_entidad` (`id`);

--
-- Filtros para la tabla `oauth2_provider_accesstoken`
--
ALTER TABLE `oauth2_provider_accesstoken`
  ADD CONSTRAINT `oauth2_provider_acce_source_refresh_token_e66fbc72_fk_oauth2_pr` FOREIGN KEY (`source_refresh_token_id`) REFERENCES `oauth2_provider_refreshtoken` (`id`),
  ADD CONSTRAINT `oauth2_provider_accesstoken_application_id_b22886e1_fk` FOREIGN KEY (`application_id`) REFERENCES `oauth2_provider_application` (`id`),
  ADD CONSTRAINT `oauth2_provider_accesstoken_user_id_6e4c9a65_fk_segur_usuario_id` FOREIGN KEY (`user_id`) REFERENCES `segur_usuario` (`id`);

--
-- Filtros para la tabla `oauth2_provider_application`
--
ALTER TABLE `oauth2_provider_application`
  ADD CONSTRAINT `oauth2_provider_application_user_id_79829054_fk_segur_usuario_id` FOREIGN KEY (`user_id`) REFERENCES `segur_usuario` (`id`);

--
-- Filtros para la tabla `oauth2_provider_grant`
--
ALTER TABLE `oauth2_provider_grant`
  ADD CONSTRAINT `oauth2_provider_grant_application_id_81923564_fk` FOREIGN KEY (`application_id`) REFERENCES `oauth2_provider_application` (`id`),
  ADD CONSTRAINT `oauth2_provider_grant_user_id_e8f62af8_fk_segur_usuario_id` FOREIGN KEY (`user_id`) REFERENCES `segur_usuario` (`id`);

--
-- Filtros para la tabla `oauth2_provider_refreshtoken`
--
ALTER TABLE `oauth2_provider_refreshtoken`
  ADD CONSTRAINT `oauth2_provider_refr_access_token_id_775e84e8_fk_oauth2_pr` FOREIGN KEY (`access_token_id`) REFERENCES `oauth2_provider_accesstoken` (`id`),
  ADD CONSTRAINT `oauth2_provider_refr_user_id_da837fce_fk_segur_usu` FOREIGN KEY (`user_id`) REFERENCES `segur_usuario` (`id`),
  ADD CONSTRAINT `oauth2_provider_refreshtoken_application_id_2d1c311b_fk` FOREIGN KEY (`application_id`) REFERENCES `oauth2_provider_application` (`id`);

--
-- Filtros para la tabla `procesos_comprob_config`
--
ALTER TABLE `procesos_comprob_config`
  ADD CONSTRAINT `procesos_comprob_con_comprobante_id_76065044_fk_procesos_` FOREIGN KEY (`comprobante_id`) REFERENCES `procesos_comprobante` (`id`),
  ADD CONSTRAINT `procesos_comprob_config_entidad_id_940b5400_fk_entidad_id` FOREIGN KEY (`entidad_id`) REFERENCES `entidad` (`id`);

--
-- Filtros para la tabla `procesos_contrato`
--
ALTER TABLE `procesos_contrato`
  ADD CONSTRAINT `procesos_contrato_comprob_config_id_affb8da4_fk_procesos_` FOREIGN KEY (`comprob_config_id`) REFERENCES `procesos_comprob_config` (`id`),
  ADD CONSTRAINT `procesos_contrato_entidad_id_4b940e27_fk_entidad_id` FOREIGN KEY (`entidad_id`) REFERENCES `entidad` (`id`),
  ADD CONSTRAINT `procesos_contrato_persona_id_5276601a_fk_comun_persona_id` FOREIGN KEY (`persona_id`) REFERENCES `comun_persona` (`id`),
  ADD CONSTRAINT `procesos_contrato_predio_id_3e8946c9_fk_procesos_predio_id` FOREIGN KEY (`predio_id`) REFERENCES `procesos_predio` (`id`),
  ADD CONSTRAINT `procesos_contrato_tipo_uso_servicio_id_ea701348_fk_procesos_` FOREIGN KEY (`tipo_uso_servicio_id`) REFERENCES `procesos_tipo_uso_servicio` (`id`),
  ADD CONSTRAINT `procesos_contrato_usuario_id_5df515b0_fk_comun_persona_id` FOREIGN KEY (`usuario_id`) REFERENCES `comun_persona` (`id`);

--
-- Filtros para la tabla `procesos_contrato_servicio`
--
ALTER TABLE `procesos_contrato_servicio`
  ADD CONSTRAINT `procesos_contrato_se_contrato_id_e601b4fb_fk_procesos_` FOREIGN KEY (`contrato_id`) REFERENCES `procesos_contrato` (`id`),
  ADD CONSTRAINT `procesos_contrato_se_servicio_id_41a8701c_fk_procesos_` FOREIGN KEY (`servicio_id`) REFERENCES `procesos_servicio` (`id`);

--
-- Filtros para la tabla `procesos_cronograma_pago`
--
ALTER TABLE `procesos_cronograma_pago`
  ADD CONSTRAINT `procesos_cronograma__contrato_id_71c244cc_fk_procesos_` FOREIGN KEY (`contrato_id`) REFERENCES `procesos_contrato` (`id`),
  ADD CONSTRAINT `procesos_cronograma__estado_cronograma_id_1ebc1b2e_fk_procesos_` FOREIGN KEY (`estado_cronograma_id`) REFERENCES `procesos_estado_cronograma` (`id`),
  ADD CONSTRAINT `procesos_cronograma__motivo_movimiento_cu_67d45e9a_fk_procesos_` FOREIGN KEY (`motivo_movimiento_cuenta_id`) REFERENCES `procesos_motivo_movimiento_cuenta` (`id`),
  ADD CONSTRAINT `procesos_cronograma__padre_id_b1d47d9f_fk_procesos_` FOREIGN KEY (`padre_id`) REFERENCES `procesos_cronograma_pago` (`id`),
  ADD CONSTRAINT `procesos_cronograma_pago_entidad_id_34a4f4aa_fk_entidad_id` FOREIGN KEY (`entidad_id`) REFERENCES `entidad` (`id`),
  ADD CONSTRAINT `procesos_cronograma_pago_persona_id_80068302_fk_comun_persona_id` FOREIGN KEY (`persona_id`) REFERENCES `comun_persona` (`id`),
  ADD CONSTRAINT `procesos_cronograma_pago_usuario_id_c28baec8_fk_comun_persona_id` FOREIGN KEY (`usuario_id`) REFERENCES `comun_persona` (`id`);

--
-- Filtros para la tabla `procesos_direccion`
--
ALTER TABLE `procesos_direccion`
  ADD CONSTRAINT `procesos_direccion_pais_id_651fdd6f_fk_comun_pais_id` FOREIGN KEY (`pais_id`) REFERENCES `comun_pais` (`id`),
  ADD CONSTRAINT `procesos_direccion_persona_id_fa2ba981_fk_comun_persona_id` FOREIGN KEY (`persona_id`) REFERENCES `comun_persona` (`id`),
  ADD CONSTRAINT `procesos_direccion_predio_id_14a0b8e9_fk_procesos_predio_id` FOREIGN KEY (`predio_id`) REFERENCES `procesos_predio` (`id`),
  ADD CONSTRAINT `procesos_direccion_tipo_direccion_id_e70760aa_fk_comun_tip` FOREIGN KEY (`tipo_direccion_id`) REFERENCES `comun_tipo_direccion` (`id`),
  ADD CONSTRAINT `procesos_direccion_tipo_numero_via_id_5c64d0ab_fk_comun_tip` FOREIGN KEY (`tipo_numero_via_id`) REFERENCES `comun_tipo_numero_via` (`id`),
  ADD CONSTRAINT `procesos_direccion_tipo_via_id_ff0333df_fk_comun_tipo_via_id` FOREIGN KEY (`tipo_via_id`) REFERENCES `comun_tipo_via` (`id`),
  ADD CONSTRAINT `procesos_direccion_tipo_zona_id_cc172c0f_fk_comun_tipo_zona_id` FOREIGN KEY (`tipo_zona_id`) REFERENCES `comun_tipo_zona` (`id`),
  ADD CONSTRAINT `procesos_direccion_ubigeo_id_235be796_fk_comun_ubigeo_id` FOREIGN KEY (`ubigeo_id`) REFERENCES `comun_ubigeo` (`id`);

--
-- Filtros para la tabla `procesos_mes_recibo`
--
ALTER TABLE `procesos_mes_recibo`
  ADD CONSTRAINT `procesos_mes_recibo_entidad_id_4061c534_fk_entidad_id` FOREIGN KEY (`entidad_id`) REFERENCES `entidad` (`id`),
  ADD CONSTRAINT `procesos_mes_recibo_mes_id_ac5457e2_fk_comun_mes_id` FOREIGN KEY (`mes_id`) REFERENCES `comun_mes` (`id`),
  ADD CONSTRAINT `procesos_mes_recibo_usuario_id_509984cd_fk_comun_persona_id` FOREIGN KEY (`usuario_id`) REFERENCES `comun_persona` (`id`);

--
-- Filtros para la tabla `procesos_motivo_movimiento_cuenta`
--
ALTER TABLE `procesos_motivo_movimiento_cuenta`
  ADD CONSTRAINT `procesos_motivo_movi_tipo_movimiento_cuen_0aeba605_fk_procesos_` FOREIGN KEY (`tipo_movimiento_cuenta_id`) REFERENCES `procesos_tipo_movimiento_cuenta` (`id`);

--
-- Filtros para la tabla `procesos_predio`
--
ALTER TABLE `procesos_predio`
  ADD CONSTRAINT `procesos_predio_duenio_id_12df9c25_fk_comun_persona_id` FOREIGN KEY (`duenio_id`) REFERENCES `comun_persona` (`id`),
  ADD CONSTRAINT `procesos_predio_estado_predio_id_b5cf80cc_fk_procesos_` FOREIGN KEY (`estado_predio_id`) REFERENCES `procesos_estado_predio` (`id`),
  ADD CONSTRAINT `procesos_predio_representante_id_ea92a73f_fk_comun_persona_id` FOREIGN KEY (`representante_id`) REFERENCES `comun_persona` (`id`),
  ADD CONSTRAINT `procesos_predio_usuario_id_54525d61_fk_comun_persona_id` FOREIGN KEY (`usuario_id`) REFERENCES `comun_persona` (`id`),
  ADD CONSTRAINT `procesos_predio_zona_id_9d59ed36_fk_procesos_zona_id` FOREIGN KEY (`zona_id`) REFERENCES `procesos_zona` (`id`);

--
-- Filtros para la tabla `procesos_recibo`
--
ALTER TABLE `procesos_recibo`
  ADD CONSTRAINT `procesos_recibo_comprob_config_id_be1d8ddf_fk_procesos_` FOREIGN KEY (`comprob_config_id`) REFERENCES `procesos_comprob_config` (`id`),
  ADD CONSTRAINT `procesos_recibo_contrato_id_82ae379a_fk_procesos_contrato_id` FOREIGN KEY (`contrato_id`) REFERENCES `procesos_contrato` (`id`),
  ADD CONSTRAINT `procesos_recibo_mes_recibo_id_221b3992_fk_procesos_mes_recibo_id` FOREIGN KEY (`mes_recibo_id`) REFERENCES `procesos_mes_recibo` (`id`),
  ADD CONSTRAINT `procesos_recibo_usuario_id_8dcf4f66_fk_comun_persona_id` FOREIGN KEY (`usuario_id`) REFERENCES `comun_persona` (`id`);

--
-- Filtros para la tabla `procesos_servicio`
--
ALTER TABLE `procesos_servicio`
  ADD CONSTRAINT `procesos_servicio_padre_id_81a7eca9_fk_procesos_servicio_id` FOREIGN KEY (`padre_id`) REFERENCES `procesos_servicio` (`id`);

--
-- Filtros para la tabla `procesos_zona`
--
ALTER TABLE `procesos_zona`
  ADD CONSTRAINT `procesos_zona_tipo_zona_id_8f066139_fk_comun_tipo_zona_id` FOREIGN KEY (`tipo_zona_id`) REFERENCES `comun_tipo_zona` (`id`),
  ADD CONSTRAINT `procesos_zona_ubigeo_id_7f5766dd_fk_comun_ubigeo_id` FOREIGN KEY (`ubigeo_id`) REFERENCES `comun_ubigeo` (`id`);

--
-- Filtros para la tabla `segur_menu`
--
ALTER TABLE `segur_menu`
  ADD CONSTRAINT `segur_menu_modulo_id_1fa185be_fk_segur_modulo_id` FOREIGN KEY (`modulo_id`) REFERENCES `segur_modulo` (`id`),
  ADD CONSTRAINT `segur_menu_padre_id_e2af8cc4_fk_segur_menu_id` FOREIGN KEY (`padre_id`) REFERENCES `segur_menu` (`id`),
  ADD CONSTRAINT `segur_menu_permiso_id_0caaddda_fk_auth_permission_id` FOREIGN KEY (`permiso_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `segur_menu_tipo_menu_id_bf299ab5_fk_segur_tipo_menu_id` FOREIGN KEY (`tipo_menu_id`) REFERENCES `segur_tipo_menu` (`id`);

--
-- Filtros para la tabla `segur_usuario`
--
ALTER TABLE `segur_usuario`
  ADD CONSTRAINT `segur_usuario_persona_id_bdc7191d_fk_comun_persona_id` FOREIGN KEY (`persona_id`) REFERENCES `comun_persona` (`id`);

--
-- Filtros para la tabla `segur_usuario_entidades`
--
ALTER TABLE `segur_usuario_entidades`
  ADD CONSTRAINT `segur_usuario_entidades_entidad_id_dc7c5281_fk_entidad_id` FOREIGN KEY (`entidad_id`) REFERENCES `entidad` (`id`),
  ADD CONSTRAINT `segur_usuario_entidades_usuario_id_4340b0b7_fk_segur_usuario_id` FOREIGN KEY (`usuario_id`) REFERENCES `segur_usuario` (`id`);

--
-- Filtros para la tabla `segur_usuario_groups`
--
ALTER TABLE `segur_usuario_groups`
  ADD CONSTRAINT `segur_usuario_groups_group_id_895125cd_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `segur_usuario_groups_usuario_id_5e0ee4a8_fk_segur_usuario_id` FOREIGN KEY (`usuario_id`) REFERENCES `segur_usuario` (`id`);

--
-- Filtros para la tabla `segur_usuario_user_permissions`
--
ALTER TABLE `segur_usuario_user_permissions`
  ADD CONSTRAINT `segur_usuario_user_p_permission_id_b2b070ad_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `segur_usuario_user_p_usuario_id_b248c9ff_fk_segur_usu` FOREIGN KEY (`usuario_id`) REFERENCES `segur_usuario` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
