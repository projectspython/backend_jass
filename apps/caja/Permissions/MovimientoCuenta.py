from apps.comun.Permissions.Base import BasePermission

class MovimientoCuentaPermissions(BasePermission):
	perms_map = {
				'registrarpago': 'caja.add_movimientocuenta',
				'edit': 'caja.view_movimientocuenta',
				'update': 'caja.update_movimientocuenta',
				'delete':'caja.delete_movimientocuenta',
				'searchform':'caja.view_form_movimientocuenta',
				'anularpago':'caja.anular_movimientocuenta',

				}

	permission_from_user = None