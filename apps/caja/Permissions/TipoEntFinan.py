from apps.comun.Permissions.Base import BasePermission

class TipoEntFinanPermissions(BasePermission):
	perms_map = {
				'add': 'caja.add_tipoentfinan',
				'edit': 'caja.view_tipoentfinan',
				'update': 'caja.update_tipoentfinan',
				'delete':'caja.delete_tipoentfinan',
				'searchform':'caja.view_form_tipoentfinan',}

	permission_from_user = None