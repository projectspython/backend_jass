from apps.comun.Permissions.Base import BasePermission

class CuentaEntidadPermissions(BasePermission):
	perms_map = {
				'add': 'caja.add_cuentaentidad',
				'edit': 'caja.view_cuentaentidad',
				'update': 'caja.update_cuentaentidad',
				'delete':'caja.delete_cuentaentidad',
				'searchform':'caja.view_form_cuentaentidad',
				'searchformcaja':'caja.view_form_cuentaentidad',}

	permission_from_user = None