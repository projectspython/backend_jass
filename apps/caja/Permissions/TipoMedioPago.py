from apps.comun.Permissions.Base import BasePermission

class TipoMedioPagoPermissions(BasePermission):
	perms_map = {
				'add': 'caja.add_tipomediopago',
				'edit': 'caja.view_tipomediopago',
				'update': 'caja.update_tipomediopago',
				'delete':'caja.delete_tipomediopago',
				'searchform':'caja.view_form_tipomediopago',
				'searchformcaja':'caja.view_form_tipomediopago',}

	permission_from_user = None