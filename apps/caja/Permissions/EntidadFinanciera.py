from apps.comun.Permissions.Base import BasePermission

class EntidadFinancieraPermissions(BasePermission):
	perms_map = {
				'add': 'caja.add_entidadfinanciera',
				'edit': 'caja.view_entidadfinanciera',
				'update': 'caja.update_entidadfinanciera',
				'delete':'caja.delete_entidadfinanciera',
				'searchform':'caja.view_form_entidadfinanciera',}

	permission_from_user = None