from apps.comun.Permissions.Base import BasePermission

class EstadoCuentaPermissions(BasePermission):
	perms_map = {
				'add': 'caja.add_estadocuenta',
				'edit': 'caja.view_estadocuenta',
				'update': 'caja.update_estadocuenta',
				'delete':'caja.delete_estadocuenta',
				'searchform':'caja.view_form_estadocuenta',}

	permission_from_user = None