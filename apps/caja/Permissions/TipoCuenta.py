from apps.comun.Permissions.Base import BasePermission

class TipoCuentaPermissions(BasePermission):
	perms_map = {
				'add': 'caja.add_tipocuenta',
				'edit': 'caja.view_tipocuenta',
				'update': 'caja.update_tipocuenta',
				'delete':'caja.delete_tipocuenta',
				'searchform':'caja.view_form_tipocuenta',}

	permission_from_user = None