from apps.comun.Permissions.Base import BasePermission

class TipoCuentaFinanPermissions(BasePermission):
	perms_map = {
				'add': 'caja.add_tipocuentafinan',
				'edit': 'caja.view_tipocuentafinan',
				'update': 'caja.update_tipocuentafinan',
				'delete':'caja.delete_tipocuentafinan',
				'searchform':'caja.view_form_tipocuentafinan',}

	permission_from_user = None