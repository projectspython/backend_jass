from apps.comun.Permissions.Base import BasePermission

class PersonaCuentaPermissions(BasePermission):
	perms_map = {
				'add': 'caja.add_personacuenta',
				'edit': 'caja.view_personacuenta',
				'update': 'caja.update_personacuenta',
				'delete':'caja.delete_personacuenta',
				'searchform':'caja.view_form_personacuenta',}

	permission_from_user = None