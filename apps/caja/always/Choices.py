from django.utils.text import capfirst
from django.utils.translation import ugettext_lazy as _

ESTADO_MOVIMIENTO_CUENTA = (
    ('1', capfirst(_('Activo'))),
    ('0', capfirst(_('Anulado'))),
)