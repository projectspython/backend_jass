
from apps.caja.models.EstadoCuenta import EstadoCuenta
from apps.caja.models.CuentaEntidad import CuentaEntidad

class Generales():
	"""docstring for ClassName"""
	def update_estado_cuenta(entidad_id, persona_id, importe, tipo):

		importe = float(importe)

		est_cuenta = EstadoCuenta.objects.filter(persona_id=persona_id, 
			entidad_id=entidad_id).values('id').first()
		if est_cuenta:
			model = EstadoCuenta.objects.get(pk=est_cuenta['id'])
			if tipo == 'deuda':
				model.deuda = float(model.deuda) + importe
			if tipo == 'saldo':
				model.saldo = float(model.saldo) + importe
			model.save()
		else:
			e_={}
			e_['entidad_id'] = entidad_id
			e_['persona_id'] = persona_id
			e_['estado'] = '1'
			if tipo == 'deuda':
				e_['deuda'] = importe
			if tipo == 'saldo':
				e_['saldo'] = importe

			ec = EstadoCuenta.objects.create(**e_)

		return ''

	def update_cuenta_empresa(cuenta_entidad_id, importe):
		cuen = CuentaEntidad.objects.get(id=cuenta_entidad_id)
		cuen.saldo = float(cuen.saldo) + importe
		cuen.save()
		return ''