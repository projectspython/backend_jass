
from apps.comun.models.Contador import Contador

def correlativo(codigo):
	model = Contador.objects.get(codigo=codigo)
	contador = model.contador + 1
	model.contador = contador
	model.save()
	return contador


class GenerarCodigo():
	"""docstring for ClassName"""
	def movimientocuenta():
		codigo = 'MCDEPO'
		contador = correlativo(codigo)
		return '%07d' % contador