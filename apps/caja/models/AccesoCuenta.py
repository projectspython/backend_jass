from apps.comun.models.Persona import Persona
from .CuentaEntidad import CuentaEntidad
from django.db import models

class AccesoCuenta(models.Model):

	cuenta_entidad = models.ForeignKey(CuentaEntidad, related_name='cuenta_entidad_acceso_cuentas',
		db_column='cuenta_entidad_id', on_delete=models.CASCADE)
	persona = models.ForeignKey(Persona, related_name='persona_acceso_cuentas',
		db_column='persona_id', on_delete=models.PROTECT)
	fecha_inicio = models.DateField()
	fecha_fin = models.DateField(blank=True, null=True)
	estado = models.CharField(max_length=1, default='1')
	usuario = models.ForeignKey(Persona, related_name='usuario_acceso_cuentas',
		db_column='usuario_id', on_delete=models.PROTECT)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		db_table ='caja_acceso_cuenta'
		verbose_name = 'Acceso Cuenta'
		verbose_name_plural = 'Acceso Cuentas'
		unique_together = ('cuenta_entidad', 'persona')
		default_permissions = ()
		permissions = (
			('view_accesocuenta','Listar Acceso Cuenta'),
			('add_accesocuenta','Agregar Acceso Cuenta'),
			('update_accesocuenta','Actualizar Acceso Cuenta'),
			('delete_accesocuenta','Eliminar Acceso Cuenta'),
			('view_form_accesocuenta','Listar Acceso Cuenta en formulario'),
		)


	def __str__(self):
		return self.persona.nombre_completo