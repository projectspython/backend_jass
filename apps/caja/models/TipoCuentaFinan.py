from django.db import models

class TipoCuentaFinan(models.Model):
	nombre = models.CharField(max_length=120, unique=True)
	estado = models.CharField(max_length=1, default='1')

	class Meta:
		db_table ='caja_tipo_cuenta_finan'
		verbose_name = 'Tipo cuenta financiera'
		verbose_name_plural = 'Tipo cuentas financieras'
		default_permissions = ()
		permissions = (
			('view_tipocuentafinan','Listar Tipo cuenta finan'),
			('add_tipocuentafinan','Agregar Tipo cuenta finan'),
			('update_tipocuentafinan','Actualizar Tipo cuenta finan'),
			('delete_tipocuentafinan','Eliminar Tipo cuenta finan'),
			('view_form_tipocuentafinan','Listar Tipo cuenta finan en formulario'),
		)

	def __str__(self):
		return self.nombre