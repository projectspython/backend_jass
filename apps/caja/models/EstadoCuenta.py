# Modelo que permite ver los saldos
# de la persona: deuda y saldo según entidad (empresa)

from apps.comun.models.Entidad import Entidad
from apps.comun.models.Persona import Persona
from django.db import models


class EstadoCuenta(models.Model):

	entidad = models.ForeignKey(Entidad, related_name='entidad_estado_cuentas',
		db_column='entidad_id', on_delete=models.CASCADE)
	persona = models.ForeignKey(Persona, related_name='persona_estado_cuentas',
		db_column='persona_id', on_delete=models.PROTECT)
	saldo = models.DecimalField(max_digits=12, decimal_places=2, default=0)
	deuda = models.DecimalField(max_digits=12, decimal_places=2, default=0)
	estado = models.CharField(max_length=1, default='1')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		db_table ='caja_estado_cuenta'
		verbose_name = 'Estado cuenta'
		verbose_name_plural = 'Estados Cuentas'
		unique_together = ('entidad','persona')
		default_permissions = ()
		permissions = (
			('view_estadocuenta','Listar Estado Cuenta'),
			('add_estadocuenta','Agregar Estado Cuenta'),
			('update_estadocuenta','Actualizar Estado Cuenta'),
			('delete_estadocuenta','Eliminar Estado Cuenta'),
			('view_form_estadocuenta','Listar Estado Cuenta en formulario'),
		)

	def __str__(self):
		return self.deuda