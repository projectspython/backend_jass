from apps.comun.models.Persona import Persona
from .TipoCuentaFinan import TipoCuentaFinan
from .EntidadFinanciera import EntidadFinanciera
from django.db import models

class PersonaCuenta(models.Model):

	persona = models.ForeignKey(Persona, related_name='persona_persona_cuentas',
		db_column='persona_id', on_delete=models.CASCADE)
	entidad_financiera = models.ForeignKey(EntidadFinanciera, related_name='entidad_financiera_persona_cuentas',
		db_column='entidad_financiera_id', on_delete=models.PROTECT)
	numero = models.CharField(max_length=60)
	numero_cci = models.CharField(max_length=70, blank=True, null=True)
	tipo_cuenta_finan = models.ForeignKey(TipoCuentaFinan, related_name='tipo_ent_finan_persona_cuentas',
		db_column='tipo_cuenta_finan_id', blank=True, null=True, on_delete=models.PROTECT)
	estado = models.CharField(max_length=1, default='1')

	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		db_table ='caja_persona_cuenta'
		verbose_name = 'Persona cuenta'
		verbose_name_plural = 'Personas cuentas'
		unique_together = ('entidad_financiera', 'numero')
		default_permissions = ()
		permissions = (
			('view_personacuenta','Listar Persona Cuentas'),
			('add_personacuenta','Agregar Persona Cuentas'),
			('update_personacuenta','Actualizar Persona Cuentas'),
			('delete_personacuenta','Eliminar Persona Cuentas'),
			('view_form_personacuenta','Listar Persona Cuentas en formulario'),
		)

	def __str__(self):
		return self.nombre
