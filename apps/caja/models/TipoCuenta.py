from django.db import models

class TipoCuenta(models.Model):
	codigo = models.CharField(max_length=10, unique=True)
	nombre = models.CharField(max_length=40, unique=True)
	estado = models.CharField(max_length=1, default='1')

	class Meta:
		db_table ='caja_tipo_cuenta'
		verbose_name = 'Tipo cuenta'
		verbose_name_plural = 'Tipos cuentas'
		default_permissions = ()
		permissions = (
			('view_tipocuenta','Listar Tipo cuenta'),
			('add_tipocuenta','Agregar Tipo cuenta'),
			('update_tipocuenta','Actualizar Tipo cuenta'),
			('delete_tipocuenta','Eliminar Tipo cuenta'),
			('view_form_tipocuenta','Listar Tipo cuenta en formulario'),
		)

	def __str__(self):
		return self.nombre