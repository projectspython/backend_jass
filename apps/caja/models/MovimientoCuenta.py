# from apps.comun.models.Entidad import Entidad
from .TipoMedioPago import TipoMedioPago
from .CuentaEntidad import CuentaEntidad
from django.db import models
from apps.comun.models.Persona import Persona
from apps.procesos.models.MotivoMovimientoCuenta import MotivoMovimientoCuenta
from apps.comun.models.Moneda import Moneda
from ..always.Choices import ESTADO_MOVIMIENTO_CUENTA

class MovimientoCuenta(models.Model):

	cuenta_entidad = models.ForeignKey(CuentaEntidad, related_name='cuenta_entidad_movim_cuenta',
		db_column='cuenta_entidad_id', on_delete=models.PROTECT)
	motivo_mov_cuenta = models.ForeignKey(MotivoMovimientoCuenta, related_name='motivo_mov_cuenta_movim_cuenta',
		db_column='motivo_mov_cuenta_id', on_delete=models.PROTECT)
	tipo_medio_pago = models.ForeignKey(TipoMedioPago, related_name='tipo_medio_pago_movim_cuenta',
		db_column='tipo_medio_pago_id', on_delete=models.PROTECT)
	numero = models.CharField(max_length=10, unique=True)
	persona = models.ForeignKey(Persona, related_name='persona_movim_cuenta',
		db_column='persona_id', on_delete=models.PROTECT)
	detalle = models.CharField(max_length=120, blank=True, null=True)
	moneda = models.ForeignKey(Moneda, related_name='moneda_movim_cuenta',
		db_column='moneda_id', on_delete=models.PROTECT)
	moneda_cambio = models.ForeignKey(Moneda, related_name='moneda_cambio_movim_cuenta',
		db_column='moneda_cambio_id', blank=True, null=True, on_delete=models.PROTECT)
	tipo_cambio = models.DecimalField(max_digits=12, decimal_places=4, default=1)
	importe = models.DecimalField(max_digits=12, decimal_places=2)
	comisiones = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
	recibido = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
	vuelto = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
	fecha = models.DateField()
	hora = models.TimeField()

	estado = models.CharField(max_length=1, choices=ESTADO_MOVIMIENTO_CUENTA, default='1')
	usuario = models.ForeignKey(Persona, related_name='usuario_movim_cuenta', 
		db_column='usuario_id', on_delete=models.PROTECT)
	detalle_anulado = models.CharField(max_length=120, blank=True, null=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	anulado_por = models.ForeignKey(Persona, related_name='anulado_movim_cuenta', 
		db_column='anulado_por_id', on_delete=models.PROTECT, blank=True, null=True)

	class Meta:
		db_table ='caja_movimiento_cuenta'
		verbose_name = 'Movimiento cuenta'
		verbose_name_plural = 'Movimiento cuenta'
		default_permissions = ()
		permissions = (
			('view_movimientocuenta','Listar Movimiento Cuenta'),
			('add_movimientocuenta','Agregar Movimiento Cuenta'),
			('update_movimientocuenta','Actualizar Movimiento Cuenta'),
			('delete_movimientocuenta','Eliminar Movimiento Cuenta'),
			('view_form_movimientocuenta','Listar Movimiento Cuenta en formulario'),
			('anular_movimientocuenta','Anular Movimiento Cuenta'),
		)

	def __str__(self):
		return self.detalle