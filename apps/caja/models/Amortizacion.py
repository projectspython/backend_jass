from apps.procesos.models.CronogramaPago import CronogramaPago
from .MovimientoCuenta import MovimientoCuenta
from django.db import models
from ..always.Choices import ESTADO_MOVIMIENTO_CUENTA

class Amortizacion(models.Model):

	movimiento_cuenta = models.ForeignKey(MovimientoCuenta, related_name='movimiento_cuenta_amortizaciones',
		db_column='movimiento_cuenta_id', on_delete=models.PROTECT)
	cronograma_pago = models.ForeignKey(CronogramaPago, related_name='cronograma_pago_amortizaciones',
		db_column='cronograma_pago_id', on_delete=models.PROTECT)
	importe = models.DecimalField(max_digits=12, decimal_places=2)
	estado = models.CharField(max_length=1, choices=ESTADO_MOVIMIENTO_CUENTA, default='1')

	class Meta:
		verbose_name = 'Amortizacion'
		verbose_name_plural = 'Amortizaciones'
		unique_together = ('movimiento_cuenta', 'cronograma_pago')
		default_permissions = ()

	def __str__(self):
		return self.importe