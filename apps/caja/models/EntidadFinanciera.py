from .TipoEntFinan import TipoEntFinan
from django.db import models
# from django.core.files.storage import FileSystemStorage
# fs = FileSystemStorage(location='/storage/storage/comun/entidadfinancieras/fotos')
dir_storage = "storage/caja/entidadesfinancieras/logos"

class EntidadFinanciera(models.Model):
	logo = models.ImageField(upload_to=dir_storage, blank=True, null=True)
	tipo_ent_finan = models.ForeignKey(TipoEntFinan, related_name='tipo_entidades_financieras',
		db_column='tipo_ent_finan_id', blank=True, null=True, on_delete=models.PROTECT)
	nombre = models.CharField(max_length=80)
	abreviatura = models.CharField(max_length=20)
	pagina_web = models.CharField(max_length=200, blank=True, null=True)
	estado = models.CharField(max_length=1, default='1')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		db_table ='caja_entidad_financiera'
		verbose_name = 'Entidad Financiera'
		verbose_name_plural = 'Entidades Financieras'
		unique_together = ('tipo_ent_finan', 'nombre')
		default_permissions = ()
		permissions = (
			('view_entidadfinanciera',
			 'Listar Entidades Financieras'),
			('add_entidadfinanciera',
			 'Agregar Entidades Financieras'),
			('update_entidadfinanciera',
			 'Actualizar Entidades Financieras'),
			('delete_entidadfinanciera',
			 'Eliminar Entidades Financieras'),
			('view_form_entidadfinanciera',
			 'Listar Entidades Financieras en formulario'),
		)

	def __str__(self):
		return self.nombre
