from apps.comun.models.Entidad import Entidad
from .TipoCuenta import TipoCuenta
from .PersonaCuenta import PersonaCuenta
from django.db import models
from apps.comun.models.Persona import Persona

class CuentaEntidad(models.Model):

	entidad = models.ForeignKey(Entidad, related_name='entidad_cuenta_entidades',
		db_column='entidad_id', on_delete=models.CASCADE)
	tipo_cuenta = models.ForeignKey(TipoCuenta, related_name='tipo_cuenta_cuenta_entidades',
		db_column='tipo_cuenta_id', on_delete=models.PROTECT)
	persona_cuenta = models.ForeignKey(PersonaCuenta, related_name='persona_cuenta_cuenta_entidades',
		db_column='persona_cuenta_id', blank=True, null=True, on_delete=models.PROTECT)

	nombre = models.CharField(max_length=60, blank=True, null=True)
	saldo = models.DecimalField(max_digits=12, decimal_places=2, default=0)
	usuario = models.ForeignKey(Persona, related_name='usuario_cuenta_entidades', 
		db_column='usuario_id', on_delete=models.PROTECT)
	estado = models.CharField(max_length=1, default='1')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		db_table ='caja_cuenta_entidad'
		verbose_name = 'Cuenta entidad'
		verbose_name_plural = 'Cuenta entidades'
		unique_together = ('entidad', 'tipo_cuenta','persona_cuenta','nombre')
		default_permissions = ()
		permissions = (
			('view_cuentaentidad','Listar Cuenta Entidad'),
			('add_cuentaentidad','Agregar Cuenta Entidad'),
			('update_cuentaentidad','Actualizar Cuenta Entidad'),
			('delete_cuentaentidad','Eliminar Cuenta Entidad'),
			('view_form_cuentaentidad','Listar Cuenta Entidad en formulario'),
		)

	def __str__(self):
		return self.nombre