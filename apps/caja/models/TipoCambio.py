from django.db import models
from apps.comun.models.Moneda import Moneda

class TipoCambio(models.Model):

	moneda = models.ForeignKey(Moneda, related_name='moneda_tipo_cambio',
		db_column='moneda_id', on_delete=models.PROTECT)
	moneda_cambio = models.ForeignKey(Moneda, related_name='moneda_cambio_tipo_cambio',
		db_column='moneda_cambio_id', on_delete=models.PROTECT)
	fecha = models.DateField()
	compra = models.DecimalField(max_digits=12, decimal_places=4, default=1)
	venta = models.DecimalField(max_digits=12, decimal_places=4, default=1)

	class Meta:
		db_table ='caja_tipo_cambio'
		verbose_name = 'Tipo cambio'
		verbose_name_plural = 'Tipos cambio'
		unique_together = ('moneda','moneda_cambio','fecha')
		default_permissions = ()
		permissions = (
			('view_tipocambio','Listar Tipo cambio'),
			('add_tipocambio','Agregar Tipo cambio'),
			('update_tipocambio','Actualizar Tipo cambio'),
			('delete_tipocambio','Eliminar Tipo cambio'),
			('view_form_tipocambio','Listar Tipo cambio en formulario'),
		)

	def __str__(self):
		return self.fecha