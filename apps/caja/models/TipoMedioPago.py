from .TipoCuenta import TipoCuenta
from django.db import models

class TipoMedioPago(models.Model):
	codigo = models.CharField(max_length=10, unique=True)
	nombre = models.CharField(max_length=60, unique=True)
	descripcion = models.CharField(max_length=120, blank=True, null=True)
	tipo_cuenta = models.ForeignKey(TipoCuenta, related_name='tipo_cuenta_tipo_medio_pagos',
		db_column='tipo_cuenta_id', blank=True, null=True, on_delete=models.PROTECT)
	estado = models.CharField(max_length=1, default='1')

	class Meta:
		db_table ='caja_tipo_medio_pago'
		verbose_name = 'Tipo medio pago'
		verbose_name_plural = 'Tipo medio pagos'
		default_permissions = ()
		permissions = (
			('view_tipomediopago','Listar Tipo Medio Pago'),
			('add_tipomediopago','Agregar Tipo Medio Pago'),
			('update_tipomediopago','Actualizar Tipo Medio Pago'),
			('delete_tipomediopago','Eliminar Tipo Medio Pago'),
			('view_form_tipomediopago','Listar Tipo Medio Pago en formulario'),
		)

	def __str__(self):
		return self.nombre