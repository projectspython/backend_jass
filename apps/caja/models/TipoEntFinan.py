from django.db import models

class TipoEntFinan(models.Model):
	nombre = models.CharField(max_length=120, unique=True)
	abreviatura = models.CharField(max_length=40)
	concatenar = models.CharField(max_length=60, blank=True, null=True)
	estado = models.CharField(max_length=1, default='1')

	class Meta:
		db_table ='caja_tipo_ent_finan'
		verbose_name = 'Tipo entidad financiera'
		verbose_name_plural = 'Tipo entidades financieras'
		default_permissions = ()
		permissions = (
			('view_tipoentfinan','Listar Tipo entidad finan'),
			('add_tipoentfinan','Agregar Tipo entidad finan'),
			('update_tipoentfinan','Actualizar Tipo entidad finan'),
			('delete_tipoentfinan','Eliminar Tipo entidad finan'),
			('view_form_tipoentfinan','Listar Tipo entidad finan en formulario'),
		)

	def __str__(self):
		return self.nombre