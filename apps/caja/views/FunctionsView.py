from rest_framework.response import Response
from rest_framework.decorators import list_route
from rest_framework import viewsets
import requests
from bs4 import BeautifulSoup
import time
from apps.caja.models.TipoCambio import TipoCambio
from apps.comun.models.Moneda import Moneda
from apps.comun.models.Entidad import Entidad
import urllib.request
import json


def contizacion_mes():
	page = requests.get("http://www.sunat.gob.pe/cl-at-ittipcam/tcS01Alias")
	soup = BeautifulSoup(page.content, 'html.parser')
	listado = soup.find_all('td', class_='tne10')
	lista_new = []
	for p in listado:
		lista_new.append(p.get_text().replace('\r','').replace('\n','').replace('\t','').strip())

	listado_td = soup.find_all('td', class_='H3')
	lista_new_td = []
	for p in listado_td:
		lista_new_td.append(p.get_text().replace('\r','').replace('\n','').replace('\t','').strip())

	final = []
	numero = 0
	numero2 = 1 
	for p in lista_new_td:
		d_fin = {}
		d_fin['dia'] = int(p)
		d_fin['compra'] = float(lista_new[numero])
		d_fin['venta'] = float(lista_new[numero2])
		final.append(d_fin)
		numero = numero + 2
		numero2 = numero2 + 2

	return final


class FunctionsViewSet(viewsets.ViewSet):

	@list_route(url_path='cotizaciondolar',  permission_classes=[], methods=['get'])
	def get_cotizaciondolar(self, request, format=None):

		entidad_id = request.query_params.get('entidad_id', None)
		result = {}
		dia_actual = int(time.strftime("%d"))
		fecha_actual = time.strftime("%Y-%m-%d")
		
		dolar = Moneda.objects.get(codigo='USD')

		if dolar.id:
			entidad = Entidad.objects.filter(pk=entidad_id).values('moneda_id').first()
			tipcam = TipoCambio.objects.filter(
				moneda_id=entidad['moneda_id'],
				moneda_cambio_id=dolar.id,
				fecha=fecha_actual
				).values('venta','compra').first()

			if tipcam:
				tipcam['venta'] = float(tipcam['venta'])
				tipcam['compra'] = float(tipcam['compra'])
				return Response(tipcam)
			else:
				listado = contizacion_mes()
				for p in listado:
					if p['dia'] == dia_actual:
						result = p

				if 'dia' not in result and len(listado)>0:
					result = listado[len(listado)-1]

				if 'dia' in result and dia_actual == result['dia']:
					reg = TipoCambio.objects.create(
						moneda_id=entidad['moneda_id'],
						moneda_cambio_id=dolar.id,
						fecha=fecha_actual,
						compra=result['compra'],
						venta=result['venta']
						)

				return Response(result)

	@list_route(url_path='imagebase64',  permission_classes=[], methods=['get'])
	def get_imagebase64(self, request, format=None):
		import base64
		import requests
		from apps.comun.always.FileUpload import Conversiones

		url = 'storage/comun/personas/fotos/tijera.jpg'
		base64out = Conversiones.to_base64(url)
		return Response(base64out)

	@list_route(url_path='datosdni',  permission_classes=[], methods=['get'])
	def get_datosdni(self, request, format=None):
		# import urllib2
		dni = request.query_params.get('numero', None)
		# token = 'eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZ_I6'
		# url = 'http://apis.misionlive.com/php/apidni/?token=%s&dni=%s'%(token,dni)
		url = 'http://apis.misionlive.com/consulta/dni/api.php?dni=%s'%(dni)
		response = urllib.request.urlopen(url)
		data = json.load(response)
		return Response(data)

	@list_route(url_path='datosruc',  permission_classes=[], methods=['get'])
	def get_datosruc(self, request, format=None):

		ruc = request.query_params.get('numero', None)
		url = 'http://apis.misionlive.com/consulta/ruc/api.php?ruc=%s'%(ruc)
		response = urllib.request.urlopen(url)
		data = json.load(response)
		return Response(data)
		

		