from rest_framework import serializers, viewsets
from apps.caja.models.MovimientoCuenta import MovimientoCuenta
from rest_framework import permissions
from apps.caja.serializers.MovimientoCuenta import MovimientoCuentaSerializer
from apps.comun.always.Pagination import LimonPagination
from rest_framework.filters import SearchFilter, OrderingFilter
from django.db.models.deletion import ProtectedError

from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.caja.Permissions.MovimientoCuenta import MovimientoCuentaPermissions as LimonPermission
from apps.segur.always.Permission import PermLimonList
from apps.comun.always.SearchFilter import search_filter
from apps.comun.always.SearchFilter import keys_add_none, extaer_de_column
from apps.caja.models.AccesoCuenta import AccesoCuenta
from apps.caja.serializers.AccesoCuenta import AccesoCuentaSerializer
from datetime import datetime
from django.db.models import Q
import time
from apps.caja.models.Amortizacion import Amortizacion
from apps.procesos.models.CronogramaPago import CronogramaPago
from apps.procesos.serializers.CronogramaPago import CronogramaPagoCajaVerifSerializer
from apps.procesos.models.EstadoCronograma import EstadoCronograma
from apps.caja.models.EstadoCuenta import EstadoCuenta
from apps.caja.models.CuentaEntidad import CuentaEntidad
from apps.caja.always.GenerarCodigo import GenerarCodigo
from apps.procesos.models.MotivoMovimientoCuenta import MotivoMovimientoCuenta
from apps.caja.always.Generales import Generales
from apps.procesos.models.Contrato import Contrato
from django_filters.rest_framework import DjangoFilterBackend

class MovimientoCuentaViewSet(LimonPagination, viewsets.ModelViewSet):
	queryset = MovimientoCuenta.objects.all()
	permission_classes = [permissions.IsAuthenticated, PermLimonList, ]
	serializer_class = MovimientoCuentaSerializer
	filter_backends = (SearchFilter,OrderingFilter,DjangoFilterBackend,)
	filter_fields = ('estado',)
	search_fields = ('numero','detalle','tipo_medio_pago__nombre','importe',)
	ordering_fields = '__all__'

	@list_route(url_path='registrarpago', methods=['post'], permission_classes=[LimonPermission,])
	def get_registrarpago(self, request, *args, **kwargs):
		data = request.data
		data['usuario_id'] = request.user.persona_id
		data['numero'] = GenerarCodigo.movimientocuenta()
		data['estado'] = '1'
		data['fecha'] = time.strftime("%Y-%m-%d")
		data['hora'] = time.strftime("%H:%M:%S")

		tiene_cronog = 'SI'
		if 'cronograma_pago_id' not in data:
			general = MotivoMovimientoCuenta.objects.get(codigo='PGSER')
			data['motivo_mov_cuenta_id'] = general.id
			tiene_cronog = 'NO'

		data_ = keys_add_none(data,'cuenta_entidad_id,motivo_mov_cuenta_id,tipo_medio_pago_id,numero,persona_id,'+
				'detalle,moneda_id,moneda_cambio_id,tipo_cambio,importe,comisiones,recibido,vuelto,fecha,'+
				'hora,estado,usuario_id')
		movc = MovimientoCuenta.objects.create(**data_)

		estadoc = EstadoCronograma.objects.filter(codigo='PAG').values('id').first()

		if movc and tiene_cronog == 'SI':
			data_amor = {'cronograma_pago_id':data['cronograma_pago_id'], 
						'movimiento_cuenta_id': movc.id,
						'importe':data['importe']*data['tipo_cambio']}

			amort = Amortizacion.objects.create(**data_amor)

			cronog = CronogramaPago.objects.get(pk=data['cronograma_pago_id'])

			cronogd = CronogramaPagoCajaVerifSerializer(cronog).data
			if cronogd and cronogd['monto'] <= cronogd['pagado']:
				CronogramaPago.objects.filter(id=data['cronograma_pago_id']
					).update(estado_cronograma_id=estadoc['id'])

				if 'es_contrato' in data and data['es_contrato'] == 'SI':
					Contrato.objects.filter(id=data['contrato_id']).update(estado='2')

		
		if movc and tiene_cronog == 'NO':
				for p in data['cronogramas']:
					if 'completo' in p and p['completo']:
						data_amor = {'cronograma_pago_id':p['id'], 
									'movimiento_cuenta_id': movc.id,
									'importe':p['pagar']}

						amort = Amortizacion.objects.create(**data_amor)
						if p['completo'] == 'SI' and amort:
							CronogramaPago.objects.filter(id=p['id']
								).update(estado_cronograma_id=estadoc['id'])

		if movc:
			if 'es_contrato' not in data:
				upd_deuda = Generales.update_estado_cuenta(data['entidad_id'], data['persona_id'],
					data['importe']*data['tipo_cambio']*-1, 'deuda')

				# model_ec = EstadoCuenta.objects.get(persona_id=data['persona_id'],
				# 	entidad_id=data['entidad_id'])
				# model_ec.deuda = float(model_ec.deuda) - ()
				# model_ec.save()

			cuen = Generales.update_cuenta_empresa(data['cuenta_entidad_id'], data['importe']*data['tipo_cambio'])			

		return Response(self.get_serializer(movc).data)

	@list_route(url_path='anularpago', methods=['put'], permission_classes=[LimonPermission,])
	def get_anularpago(self, request, pk=None):
		usuario_id = request.user.persona_id
		data = request.data
		model = MovimientoCuenta.objects.get(id=data['id'], estado='1')
		importe = float(model.importe)*float(model.tipo_cambio)
		persona_id = model.persona_id
		codigo_t_motivo = model.motivo_mov_cuenta.tipo_movimiento_cuenta.codigo
		cuenta_entidad_id = model.cuenta_entidad_id

		movc = MovimientoCuenta.objects.filter(id=data['id'], estado='1').update(
			estado='0', detalle_anulado=data['detalle_anulado'], anulado_por_id=usuario_id)

		if movc:
			estadoc = EstadoCronograma.objects.filter(codigo='HAB').values('id').first()
			cronograma_pago_ids = Amortizacion.objects.filter(movimiento_cuenta_id=data['id'], 
				cronograma_pago__estado_cronograma__codigo='PAG').values('cronograma_pago_id').distinct()
			new_ids = []
			for p in cronograma_pago_ids:
				new_ids.append(p['cronograma_pago_id'])

			CronogramaPago.objects.filter(id__in=new_ids).update(estado_cronograma_id=estadoc['id'])
			Amortizacion.objects.filter(movimiento_cuenta_id=data['id']).update(estado='0')

			if codigo_t_motivo == 'ING':
				upd_deuda = Generales.update_estado_cuenta(data['entidad_id'], persona_id,
					importe, 'deuda')
				cuen = Generales.update_cuenta_empresa(cuenta_entidad_id, importe*-1)

			if codigo_t_motivo == 'EGR':
				upd_deuda = Generales.update_estado_cuenta(data['entidad_id'], persona_id,
					importe, 'saldo')
				cuen = Generales.update_cuenta_empresa(cuenta_entidad_id, importe)

		return Response({'anulado': True})