from rest_framework import serializers, viewsets
from apps.caja.models.PersonaCuenta import PersonaCuenta
from rest_framework import permissions
from apps.caja.serializers.PersonaCuenta import PersonaCuentaSerializer
from apps.comun.always.Pagination import LimonPagination
from rest_framework.filters import SearchFilter, OrderingFilter
from django.db.models.deletion import ProtectedError

from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.caja.Permissions.PersonaCuenta import PersonaCuentaPermissions as LimonPermission
from apps.segur.always.Permission import PermLimonList
from apps.comun.always.SearchFilter import search_filter
from apps.comun.always.SearchFilter import keys_add_none
from django_filters.rest_framework import DjangoFilterBackend
import django_filters

class PersonaCuentaFilterSet(django_filters.FilterSet):
	exclude_entidad = django_filters.CharFilter(field_name='persona_cuenta_cuenta_entidades__entidad_id',
		lookup_expr='in', exclude=True)
	class Meta:
		model = PersonaCuenta
		fields = ('numero','exclude_entidad',)

class PersonaCuentaViewSet(LimonPagination, viewsets.ModelViewSet):
	queryset = PersonaCuenta.objects.all()
	permission_classes = [permissions.IsAuthenticated, PermLimonList, ]
	serializer_class = PersonaCuentaSerializer
	filter_backends = (SearchFilter,OrderingFilter,DjangoFilterBackend,)
	# filter_fields = ('numero',)
	filter_class = PersonaCuentaFilterSet
	search_fields = ('numero','entidad_financiera__nombre',
		'persona__nombre_completo','persona__persona_persona_documentos__numero',)
	ordering_fields = '__all__'
		

	@list_route(url_path='searchform', methods=['get'], permission_classes=[LimonPermission,])
	def get_searchform(self, request, *args, **kwargs):
		queryset = search_filter(self.get_queryset(), request)
		serializer = self.get_serializer(queryset, many=True)
		return Response(serializer.data)