from rest_framework import serializers, viewsets
from apps.caja.models.CuentaEntidad import CuentaEntidad
from rest_framework import permissions
from apps.caja.serializers.CuentaEntidad import CuentaEntidadSerializer, CuentaEntidadBasicSerializer
from apps.comun.always.Pagination import LimonPagination
from rest_framework.filters import SearchFilter, OrderingFilter
from django.db.models.deletion import ProtectedError

from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.caja.Permissions.CuentaEntidad import CuentaEntidadPermissions as LimonPermission
from apps.segur.always.Permission import PermLimonList
from apps.comun.always.SearchFilter import search_filter
from apps.comun.always.SearchFilter import keys_add_none, extaer_de_column
from apps.caja.models.AccesoCuenta import AccesoCuenta
from apps.caja.serializers.AccesoCuenta import AccesoCuentaSerializer
from datetime import datetime
from django.db.models import Q

class CuentaEntidadViewSet(LimonPagination, viewsets.ModelViewSet):
	queryset = CuentaEntidad.objects.all()
	permission_classes = [permissions.IsAuthenticated, PermLimonList, ]
	serializer_class = CuentaEntidadSerializer
	filter_backends = (SearchFilter,OrderingFilter,)
	search_fields = ('nombre','tipo_cuenta__nombre',)
	ordering_fields = '__all__'

	@list_route(url_path='searchform', methods=['get'], permission_classes=[LimonPermission,])
	def get_searchform(self, request, *args, **kwargs):
		queryset = search_filter(self.get_queryset(), request)
		serializer = self.get_serializer(queryset, many=True)
		return Response(serializer.data)

	@list_route(url_path='add', methods=['post'], permission_classes=[LimonPermission,])
	def get_add(self, request, *args, **kwargs):
		data = request.data
		data['usuario_id'] = request.user.persona_id
		data_ = keys_add_none(data,'entidad_id,tipo_cuenta_id,persona_cuenta_id,nombre,usuario_id,estado')
		cue = CuentaEntidad.objects.create(**data_)

		for p in data['usuarios']:
			data_acc = keys_add_none(p,'persona_id,fecha_inicio,fecha_fin,estado')
			data_acc['cuenta_entidad_id'] = cue.id
			data_acc['usuario_id'] = request.user.persona_id
			acc = AccesoCuenta.objects.create(**data_acc)

		return Response(self.get_serializer(cue).data)

	@list_route(url_path='edit', methods=['get'], permission_classes=[LimonPermission,])
	def get_edit(self, request):
		id = request.query_params.get('id', None)
		model = CuentaEntidad.objects.get(pk=id)
		data = self.get_serializer(model).data
		data['tipo_cuenta_id'] = data['tipo_cuenta']['id']
		data['tipo_cuenta_codigo'] = data['tipo_cuenta']['codigo']

		# list_usarios = AccesoCuenta.objects.filter(cuenta_entidad_id=id)
		data['usuarios'] = data['cuenta_entidad_acceso_cuentas']

		return Response(data)

	@list_route(url_path='update', methods=['put'], permission_classes=[LimonPermission,])
	def get_update(self, request, pk=None):
		data = request.data
		data_ = keys_add_none(data,'tipo_cuenta_id,persona_cuenta_id,nombre,estado')
		p = CuentaEntidad.objects.filter(pk=data['id']).update(**data_)
		model = CuentaEntidad.objects.get(id=data['id'])

		delacc = AccesoCuenta.objects.filter(id__in=extaer_de_column(data['del_usuarios'],'id')).delete()
		for p in data['usuarios']:
			data_acc = keys_add_none(p,'persona_id,fecha_inicio,fecha_fin,estado')
			data_acc['cuenta_entidad_id'] = data['id']
			if 'id' in p:
				acc = AccesoCuenta.objects.filter(id=p['id']).update(**data_acc)
			else:
				data_acc['usuario_id'] = request.user.persona_id
				acc = AccesoCuenta.objects.create(**data_acc)

		return Response(self.get_serializer(model).data)

	@list_route(url_path='delete', methods=['delete'], permission_classes=[LimonPermission,])
	def get_delete(self, request, *args, **kwargs):
		# try:
		id = request.query_params.get('id', None)
		model = CuentaEntidad.objects.get(pk=id).delete()
		return Response({'status':True, 'id':id})

	@list_route(url_path='searchformcaja', methods=['get'], permission_classes=[LimonPermission,])
	def get_searchformcaja(self, request, *args, **kwargs):
		date_now = datetime.now().date()
		user = request.user
		queryset = search_filter(self.get_queryset(), request)
		queryset = queryset.filter(
			Q(cuenta_entidad_acceso_cuentas__fecha_fin__gte=date_now) |
			Q(cuenta_entidad_acceso_cuentas__fecha_fin__isnull=True),
			cuenta_entidad_acceso_cuentas__persona_id=user.persona_id,
			cuenta_entidad_acceso_cuentas__fecha_inicio__lte=date_now,
			cuenta_entidad_acceso_cuentas__estado='1',
			)
		# queryset = queryset.filter(
		# 	cuenta_entidad_acceso_cuentas__fecha_fin__gte=date_now
		# 	# cuenta_entidad_acceso_cuentas__fecha_fin__isnull=True
		# 	)
		serializer = CuentaEntidadBasicSerializer(queryset, many=True)
		return Response(serializer.data)

