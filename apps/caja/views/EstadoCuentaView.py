from rest_framework import serializers, viewsets
from apps.caja.models.EstadoCuenta import EstadoCuenta
from rest_framework import permissions
from apps.caja.serializers.EstadoCuenta import EstadoCuentaSerializer
from apps.comun.always.Pagination import LimonPagination
from rest_framework.filters import SearchFilter, OrderingFilter
from django.db.models.deletion import ProtectedError

from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.caja.Permissions.EstadoCuenta import EstadoCuentaPermissions as LimonPermission
from apps.segur.always.Permission import PermLimonList
from apps.comun.always.SearchFilter import search_filter
from apps.comun.models.Persona import Persona
from apps.comun.models.Entidad import Entidad
from apps.comun.always.InfoModels import PersonaNaturalInfo, PersonaJuridicaInfo
from apps.comun.always.FileUpload import Conversiones
from apps.procesos.serializers.CronogramaPago import CronogramaPagoCajaSerializer
from apps.procesos.models.CronogramaPago import CronogramaPago
from decimal import Decimal as _decimal
from datetime import datetime
import time
from django_filters.rest_framework import DjangoFilterBackend
from apps.caja.models.MovimientoCuenta import MovimientoCuenta

class EstadoCuentaViewSet(LimonPagination, viewsets.ModelViewSet):
	queryset = EstadoCuenta.objects.all()
	permission_classes = [permissions.IsAuthenticated, PermLimonList, ]
	serializer_class = EstadoCuentaSerializer
	filter_backends = (SearchFilter,OrderingFilter,DjangoFilterBackend,)
	filter_fields = ('entidad_id',)
	search_fields = ('persona__nombre_completo','persona__persona_persona_documentos__numero',
		'persona__codigo',)
	ordering_fields = '__all__'

	@list_route(url_path='searchform', methods=['get'], permission_classes=[LimonPermission,])
	def get_searchform(self, request, *args, **kwargs):
		queryset = search_filter(self.get_queryset(), request)
		serializer = self.get_serializer(queryset, many=True)
		return Response(serializer.data)

	@list_route(url_path='reportmovimientos', methods=['get'], permission_classes=[])
	def get_reportmovimientos(self, request, *args, **kwargs):

		entidad_id = request.query_params.get('entidad_id', None)
		persona_id = request.query_params.get('persona_id', None)

		cronogramas = CronogramaPago.objects.filter(
			persona_id=persona_id,
			entidad_id=entidad_id,
			estado_cronograma__codigo__in=['HAB','PAG'],
			motivo_movimiento_cuenta__tipo_movimiento_cuenta__codigo='ING'
			).values('id','created_at',
			'motivo_movimiento_cuenta__nombre','descripcion','monto').order_by('created_at')

		listado = []
		total_cronog = 0
		for p in cronogramas:
			data = {}
			data['motivo'] = p['motivo_movimiento_cuenta__nombre']
			data['descripcion'] = p['descripcion']
			data['monto'] = p['monto']
			data['fecha'] = p['created_at'].strftime("%d/%m/%Y")
			data['hora'] = p['created_at'].strftime("%I:%M:%S %p")
			data['fecha_hora_'] = int(p['created_at'].strftime("%Y%m%d")+p['created_at'].strftime("%H%M%S"))
			# data['hora_'] = 
			listado.append(data)
			total_cronog += p['monto']

		new_movimientos_cuenta = []
		movimientos_cuenta = MovimientoCuenta.objects.filter(
			persona_id=persona_id,
			cuenta_entidad__entidad_id=entidad_id,
			estado='1',
			motivo_mov_cuenta__tipo_movimiento_cuenta__codigo='ING'
			).values('motivo_mov_cuenta__nombre','tipo_medio_pago__nombre','numero','detalle',
			'moneda__nombre','fecha','hora','importe','tipo_cambio',)

		total_pago = 0
		for p in movimientos_cuenta:
			data = {}
			data['motivo'] = p['motivo_mov_cuenta__nombre']
			data['descripcion'] = p['detalle']
			data['medio_pago'] = p['tipo_medio_pago__nombre']
			data['numero'] = p['numero']
			data['importe'] = p['importe']*p['tipo_cambio']
			data['fecha'] = p['fecha'].strftime("%d/%m/%Y")
			data['hora'] = p['hora'].strftime("%I:%M:%S %p")
			data['fecha_hora_'] = int(p['fecha'].strftime("%Y%m%d")+p['hora'].strftime("%H%M%S"))
			# data['hora_'] = 
			listado.append(data)
			total_pago += data['importe']


		estado_cuenta = EstadoCuenta.objects.filter(
			entidad_id=entidad_id, persona_id=persona_id).values('saldo','deuda').first()

		result = dict(
				# cronogramas=new_cronogramas,
				listado=listado,
				total_pago=total_pago,
				total_cronog=total_cronog,
				estado_cuenta=estado_cuenta
				)
		return Response(result)


	@list_route(url_path='estadocuentacrong', methods=['get'], permission_classes=[])
	def get_estadocuentacrong(self, request, *args, **kwargs):
		entidad_id = request.query_params.get('entidad_id', None)
		persona_id = request.query_params.get('persona_id', None)

		data = {}
		model = Persona.objects.get(pk=persona_id)
		if model.tipo_persona.codigo == '01':
			data = PersonaNaturalInfo(request, persona_id)
		else:
			data = PersonaJuridicaInfo(request, persona_id)

		if model.foto_logo:
			data['logo_conver'] = Conversiones.to_base64((model.foto_logo.url).replace('/limon/',''))
		else:
			data['logo_conver'] = Conversiones.to_base64('storage/comun/personas/fotos/sinfoto.png')

		documentos = ''
		for p in data['documentos']:
			documentos +='%s: %s '%(p['tipo_documento']['nombre'], p['numero'])

		direccion = ''
		direccion_ubigeo = ''
		if len(data['direcciones'])>0:
			dire = data['direcciones'][0]
			direccion = dire['direccion_detalle']
			if 'ubigeo' in dire and dire['ubigeo']:
				direccion_ubigeo = dire['ubigeo']['nombre']

				if dire['ubigeo']['padre']:
					direccion_ubigeo += ' - %s'%(dire['ubigeo']['padre']['nombre'])

					if dire['ubigeo']['padre']['padre']:
						direccion_ubigeo += ' - %s'%(dire['ubigeo']['padre']['padre']['nombre'])

			direccion_ubigeo +=', %s'%(dire['pais']['nombre'])

		ent = Entidad.objects.get(pk=entidad_id)
		data_ent = PersonaJuridicaInfo(request, ent.persona_id)

		doc_ent = ''
		if len(data_ent['documentos'])>0:
			ent_doc = data_ent['documentos'][0]
			doc_ent = '%s: %s'%(ent_doc['tipo_documento']['nombre'], ent_doc['numero'])

		dir_ent = ''
		if len(data_ent['direcciones'])>0:
			ent_dir = data_ent['direcciones'][0]
			dir_ent = '%s'%(ent_dir['direccion_detalle'])

			if 'ubigeo' in ent_dir and ent_dir['ubigeo']:
				dir_ent += ' - %s'%(ent_dir['ubigeo']['nombre'])

				if ent_dir['ubigeo']['padre']:
					dir_ent += ' - %s'%(ent_dir['ubigeo']['padre']['nombre'])

					if ent_dir['ubigeo']['padre']['padre']:
						dir_ent += ' - %s'%(ent_dir['ubigeo']['padre']['padre']['nombre'])

		estcue = EstadoCuenta.objects.filter(persona_id=persona_id, entidad_id=entidad_id
			).values('saldo','deuda').first()

		if estcue is None:
			estcue = {}
			estcue['saldo'] = '0.00'
			estcue['deuda'] = '0.00'

		
		cronogramas_fil = CronogramaPago.objects.filter(
			motivo_movimiento_cuenta__tipo_movimiento_cuenta__codigo='ING',
			persona_id=persona_id,
			estado_cronograma__codigo='HAB',
			entidad_id=entidad_id
			).order_by('fecha_cobranza')
		cronogramas =  CronogramaPagoCajaSerializer(cronogramas_fil, many=True).data
		monto_total = 0
		pagado = 0
		deuda_total = 0
		new_cronogramas = []
		index = 1
		for p in cronogramas:
			monto_total += p['monto']
			pagado += p['pagado']
			deuda = _decimal(p['monto']) - _decimal(p['pagado'])
			deuda_total += deuda

			if p['fecha_cobranza']:
				fecha_cobranza = datetime.strptime(p['fecha_cobranza'], '%Y-%m-%d')
				fecha_ = fecha_cobranza.strftime('%d/%m/%Y')
				p['fecha_cobranza'] = fecha_

			item = [
						{'text': index, 'alignment': 'center'},
						{'text': p['motivo_movimiento_cuenta']['nombre'], 'alignment': 'left'},
						{'text': p['descripcion'], 'alignment': 'left'},
						{'text': "%.2f" %p['monto'], 'alignment': 'right'},
						{'text': "%.2f" %p['pagado'], 'alignment': 'right'},
						{'text': "%.2f" %deuda, 'alignment': 'right'},
						{'text': p['fecha_cobranza'], 'alignment': 'center'}
					]
			new_cronogramas.append(item)
			index += 1


		item_total = [
						{'text': 'TOTALES', 'alignment': 'right','colSpan': 3},
						'',
						'',
						{'text': "%.2f" %monto_total, 'alignment': 'right'},
						{'text': "%.2f" %pagado, 'alignment': 'right'},
						{'text': "%.2f" %deuda_total, 'alignment': 'right','style': 'tableHeader','fillColor':'#EEE'},
						{'text': '', 'alignment': 'center'}
					]
		new_cronogramas.append(item_total)

		result = {
			'content': [
				{
					'text': 'ESTADO DE CUENTA | CRONOGRAMAS DE PAGO',
					'style': 'header',
					'alignment': 'center'
				},
				{
					'text': '%s - %s'%(data_ent['nombre_completo'], doc_ent),
					'style': 'header_basic1',
					'alignment': 'center'
				},
				{
					'text': '%s\n\n'%(dir_ent),
					'style': 'header_basic2',
					'alignment': 'center'
				},

				{
					'alignment': 'justify',
					'columns': [
						{
							'image': data['logo_conver'],
							'width': 50,
							'height': 50
						},
						[
							{   
								'style':'nombre_completo',
								'text': data['nombre_completo']
							},
							{   
								'style':'documentos',
								'text': documentos.strip()
							},
							{   
								'style':'direcciones',
								'text': '%s\n%s'%(direccion,direccion_ubigeo)
							}
						],
						{
							'width': 90,
							'text':'DEUDA:  %s\nSALDO:  %s'%(estcue['deuda'], estcue['saldo']),
							'alignment': 'right',
							'style':'direcciones',
						},
					]
				},
				{
					'text': '\nCRONOGRAMAS PENDIENTES\n',
					'style': 'header_cronog',
					'alignment': 'center'
				},     
				{
					'style': 'tableExample',
					'table': {
						'widths': [15, '*','*',40,40,40,80],
						'body': [
							[   
								{'text': '#', 'style': 'tableHeader', 'alignment': 'center','fillColor':'#EEE'},
								{'text': 'MOTIVO CRONOGRAMA', 'style': 'tableHeader', 'alignment': 'left','fillColor':'#EEE'},
								{'text': 'DESCRIPCIÓN', 'style': 'tableHeader', 'alignment': 'left','fillColor':'#EEE'},
								{'text': 'MONTO', 'style': 'tableHeader', 'alignment': 'center','fillColor':'#EEE'},
								{'text': 'PAGÓ', 'style': 'tableHeader', 'alignment': 'center','fillColor':'#EEE'},
								{'text': 'DEBE', 'style': 'tableHeader', 'alignment': 'center','fillColor':'#EEE'},
								{'text': 'VENCE COBRANZA', 'style': 'tableHeader', 'alignment': 'center','fillColor':'#EEE'}
							],
							*new_cronogramas,
						]
					},
				},
				{
					'width': 100,
					'fontSize': 7,
					'alignment':'center',
					'text': 'FECHA DE CONSULTA: %s - %s\nTodos los derechos reservados'%(time.strftime("%d/%m/%Y"), time.strftime("%I:%M:%S %p"))
				},     
			],
			'styles': {
				'header': {
					'fontSize': 14,
					'bold': True
				},
				'header_basic1': {
					'fontSize': 10,
					'bold': False
				},
				'header_basic2': {
					'fontSize': 8,
					'bold': False
				},
				'bigger': {
					'fontSize': 15,
					'italics': True
				},
				'tableExample': {
					'margin': [0, 5, 0, 15],
					'fontSize': 8,
				},
				'tableHeader':{
					'bold': True
				},
				'nombre_completo':{
					'bold': True,
					'fontSize': 11
				},
				'documentos':{
					'bold': False,
					'fontSize': 10
				},
				'direcciones':{
					'bold': False,
					'fontSize': 8
				},
				'header_cronog':{
					'bold': False,
					'fontSize': 11
				}
			},
			'defaultStyle': {
				'columnGap': 20
			}
		}

		

		return Response(result)