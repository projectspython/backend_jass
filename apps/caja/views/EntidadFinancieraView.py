from rest_framework import serializers, viewsets
from apps.caja.models.EntidadFinanciera import EntidadFinanciera
from rest_framework import permissions
from apps.caja.serializers.EntidadFinanciera import EntidadFinancieraSerializer
from apps.comun.always.Pagination import LimonPagination
from rest_framework.filters import SearchFilter, OrderingFilter
from django.db.models.deletion import ProtectedError

from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.caja.Permissions.EntidadFinanciera import EntidadFinancieraPermissions as LimonPermission
from apps.segur.always.Permission import PermLimonList
from apps.comun.always.SearchFilter import search_filter

class EntidadFinancieraViewSet(LimonPagination, viewsets.ModelViewSet):
	queryset = EntidadFinanciera.objects.all()
	permission_classes = [permissions.IsAuthenticated, PermLimonList, ]
	serializer_class = EntidadFinancieraSerializer
	filter_backends = (SearchFilter,OrderingFilter,)
	search_fields = ('nombre', 'tipo_ent_finan__nombre',)
	ordering_fields = '__all__'

	@list_route(url_path='searchform', methods=['get'], permission_classes=[LimonPermission,])
	def get_searchform(self, request, *args, **kwargs):
		queryset = search_filter(self.get_queryset(), request)
		serializer = self.get_serializer(queryset, many=True)
		return Response(serializer.data)