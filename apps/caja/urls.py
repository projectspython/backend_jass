from django.conf.urls import url, include
from rest_framework import routers
from .views.EntidadFinancieraView import EntidadFinancieraViewSet
from .views.TipoCuentaFinanView import TipoCuentaFinanViewSet
from .views.CuentaEntidadView import CuentaEntidadViewSet
from .views.TipoCuentaView import TipoCuentaViewSet
from .views.PersonaCuentaView import PersonaCuentaViewSet
from .views.TipoMedioPagoView import TipoMedioPagoViewSet
from .views.FunctionsView import FunctionsViewSet
from .views.MovimientoCuentaView import MovimientoCuentaViewSet
from .views.EstadoCuentaView import EstadoCuentaViewSet

router = routers.DefaultRouter()
router.register(r'entidadesfinanciera', EntidadFinancieraViewSet)
router.register(r'tiposcuentafinan', TipoCuentaFinanViewSet)
router.register(r'cuentasentidad', CuentaEntidadViewSet)
router.register(r'tiposcuenta', TipoCuentaViewSet)
router.register(r'personascuenta', PersonaCuentaViewSet)
router.register(r'tiposmediopago', TipoMedioPagoViewSet)
router.register(r'functions', FunctionsViewSet, base_name='functions')
router.register(r'movimientoscuenta', MovimientoCuentaViewSet)
router.register(r'estadoscuenta', EstadoCuentaViewSet)

urlpatterns = [
	# url(r'^choices/(?P<param>[^/]+)/$',
	#         ChoicesViewSet.as_view({'get': 'get_choices'}), name='get-enums'),
    url(r'^', include(router.urls)),
]