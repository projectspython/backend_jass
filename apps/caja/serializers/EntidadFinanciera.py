from apps.caja.models.EntidadFinanciera import EntidadFinanciera
from rest_framework import serializers
from .TipoEntFinan import TipoEntFinanBasicSerializer

class EntidadFinancieraSerializer(serializers.ModelSerializer):
	tipo_ent_finan = TipoEntFinanBasicSerializer(many=False, read_only=True)
	class Meta:
		model = EntidadFinanciera
		# fields = '__all__'
		exclude = ('created_at','updated_at',)