from apps.caja.models.AccesoCuenta import AccesoCuenta
from rest_framework import serializers
from apps.comun.serializers.Persona import PersonaBasicSerializer

class AccesoCuentaSerializer(serializers.ModelSerializer):
	persona = PersonaBasicSerializer(many=False, read_only=True)
	class Meta:
		model = AccesoCuenta
		exclude = ('created_at','updated_at',)