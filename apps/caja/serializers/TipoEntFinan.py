from apps.caja.models.TipoEntFinan import TipoEntFinan
from rest_framework import serializers

class TipoEntFinanBasicSerializer(serializers.ModelSerializer):
	class Meta:
		model = TipoEntFinan
		fields = ('id','nombre','abreviatura','concatenar',)