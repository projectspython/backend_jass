from apps.caja.models.EstadoCuenta import EstadoCuenta
from rest_framework import serializers
from apps.comun.serializers.Persona import PersonaBasicSerializer

class EstadoCuentaSerializer(serializers.ModelSerializer):
	persona = PersonaBasicSerializer(many=False, read_only=True)
	class Meta:
		model = EstadoCuenta
		# fields = '__all__'
		exclude=('created_at','updated_at','entidad',)