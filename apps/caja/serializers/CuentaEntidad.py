from apps.caja.models.CuentaEntidad import CuentaEntidad
from rest_framework import serializers
from .TipoCuenta import TipoCuentaSerializer
from .PersonaCuenta import PersonaCuentaInfoSerializer
from .AccesoCuenta import AccesoCuentaSerializer

class CuentaEntidadSerializer(serializers.ModelSerializer):
	tipo_cuenta = TipoCuentaSerializer(many=False, read_only=True)
	persona_cuenta = PersonaCuentaInfoSerializer(many=False, read_only=True)
	cuenta_entidad_acceso_cuentas = AccesoCuentaSerializer(many=True, read_only=True)
	class Meta:
		model = CuentaEntidad
		fields = ('entidad','estado','id','nombre','persona_cuenta',
			'saldo','tipo_cuenta','usuario','cuenta_entidad_acceso_cuentas',)
		# exclude = ('created_at','updated_at',)

class CuentaEntidadBasicSerializer(serializers.ModelSerializer):
	persona_cuenta = PersonaCuentaInfoSerializer(many=False, read_only=True)
	class Meta:
		model = CuentaEntidad
		# fields = '__all__'
		exclude = ('created_at','updated_at','entidad',)