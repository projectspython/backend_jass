from apps.caja.models.TipoCuenta import TipoCuenta
from rest_framework import serializers

class TipoCuentaSerializer(serializers.ModelSerializer):
	class Meta:
		model = TipoCuenta
		fields = '__all__'