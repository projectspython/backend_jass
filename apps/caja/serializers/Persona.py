from apps.comun.models.Persona import Persona
from rest_framework import serializers
from apps.caja.serializers.PersonaCuenta import PersonaCuentaBasicSerializer

class PersonaEditSerializer(serializers.ModelSerializer):
	persona_persona_cuentas = PersonaCuentaBasicSerializer(many=True, read_only=True)
	class Meta:
		model = Persona
		fields = ('persona_persona_cuentas',)