from apps.caja.models.TipoCuentaFinan import TipoCuentaFinan
from rest_framework import serializers

class TipoCuentaFinanSerializer(serializers.ModelSerializer):
	class Meta:
		model = TipoCuentaFinan
		fields = '__all__'