from apps.caja.models.PersonaCuenta import PersonaCuenta
from rest_framework import serializers
from .EntidadFinanciera import EntidadFinancieraSerializer
from apps.comun.serializers.Persona import PersonaBasicSerializer

class PersonaCuentaSerializer(serializers.ModelSerializer):
	entidad_financiera = EntidadFinancieraSerializer(many=False, read_only=True)
	persona = PersonaBasicSerializer(many=False, read_only=True)
	class Meta:
		model = PersonaCuenta
		# fields = '__all__'
		exclude = ('created_at','updated_at',)

class PersonaCuentaBasicSerializer(serializers.ModelSerializer):
	class Meta:
		model = PersonaCuenta
		fields = ('id','numero','numero_cci','entidad_financiera_id','tipo_cuenta_finan_id',)
		# exclude = ('created_at','updated_at',)

class PersonaCuentaInfoSerializer(serializers.ModelSerializer):
	entidad_financiera = EntidadFinancieraSerializer(many=False, read_only=True)
	class Meta:
		model = PersonaCuenta
		fields = ('id','numero','numero_cci','entidad_financiera',)
