from apps.caja.models.MovimientoCuenta import MovimientoCuenta
from rest_framework import serializers
# from apps.procesos.serializers.MotivoMovimientoCuenta import MotivoMovimientoCuentaSerializer, MotivoMovimientoCuentaBasicSerializer

class MovimientoCuentaSerializer(serializers.ModelSerializer):
	motivo_mov_cuenta_nombre = serializers.CharField(source='motivo_mov_cuenta.nombre')
	tipo_medio_pago_nombre = serializers.CharField(source='tipo_medio_pago.nombre')
	persona_nombre_completo = serializers.CharField(source='persona.nombre_completo')
	moneda_nombre = serializers.CharField(source='moneda.nombre')
	moneda_simbolo = serializers.CharField(source='moneda.simbolo')
	class Meta:
		model = MovimientoCuenta
		# fields = '__all__'
		exclude = ('created_at','updated_at',)