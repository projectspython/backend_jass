from apps.caja.models.TipoMedioPago import TipoMedioPago
from rest_framework import serializers

class TipoMedioPagoSerializer(serializers.ModelSerializer):
	class Meta:
		model = TipoMedioPago
		fields = '__all__'