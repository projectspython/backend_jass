from apps.comun.Permissions.Base import BasePermission

class TipoAmbitoPermissions(BasePermission):
	perms_map = {
				'add': 'comun.add_tipoambito',
				'edit': 'comun.view_tipoambito',
				'update': 'comun.update_tipoambito',
				'delete':'comun.delete_tipoambito',}

	permission_from_user = None