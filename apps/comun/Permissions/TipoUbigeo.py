from apps.comun.Permissions.Base import BasePermission

class TipoUbigeoPermissions(BasePermission):
	perms_map = {
				'add': 'comun.add_tipoubigeo',
				'edit': 'comun.view_tipoubigeo',
				'update': 'comun.update_tipoubigeo',
				'delete':'comun.delete_tipoubigeo',
				'searchform': 'comun.view_form_tipoubigeo',}

	permission_from_user = None