from apps.comun.Permissions.Base import BasePermission

class TipoPersonaPermissions(BasePermission):
	perms_map = {
				'add': 'comun.add_tipopersona',
				'edit': 'comun.view_tipopersona',
				'update': 'comun.update_tipopersona',
				'delete':'comun.delete_tipopersona',}

	permission_from_user = None