from apps.comun.Permissions.Base import BasePermission

class ProfesionOficioPermissions(BasePermission):
	perms_map = {
				'add': 'comun.add_profesionoficio',
				'edit': 'comun.view_profesionoficio',
				'update': 'comun.update_profesionoficio',
				'delete':'comun.delete_profesionoficio',}

	permission_from_user = None