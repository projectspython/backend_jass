from apps.comun.Permissions.Base import BasePermission

class TipoEntidadPermissions(BasePermission):
	perms_map = {
				'add': 'comun.add_tipoentidad',
				'edit': 'comun.view_tipoentidad',
				'update': 'comun.update_tipoentidad',
				'delete':'comun.delete_tipoentidad',}

	permission_from_user = None