from apps.comun.Permissions.Base import BasePermission

class MesPermissions(BasePermission):
	perms_map = {
				'add': 'comun.add_mes',
				'edit': 'comun.view_mes',
				'update': 'comun.update_mes',
				'delete':'comun.delete_mes',}

	permission_from_user = None