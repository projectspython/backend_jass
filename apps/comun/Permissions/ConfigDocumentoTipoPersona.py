from apps.comun.Permissions.Base import BasePermission

class ConfigDocumentoTipoPersonaPermissions(BasePermission):
	perms_map = {
				'add': 'comun.add_configdocumentotipopersona',
				'edit': 'comun.view_configdocumentotipopersona',
				'update': 'comun.update_configdocumentotipopersona',
				'delete':'comun.delete_configdocumentotipopersona',}

	permission_from_user = None