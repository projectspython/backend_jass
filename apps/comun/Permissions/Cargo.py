from apps.comun.Permissions.Base import BasePermission

class CargoPermissions(BasePermission):
	perms_map = {
				'add': 'comun.add_cargo',
				'edit': 'comun.view_cargo',
				'update': 'comun.update_cargo',
				'delete':'comun.delete_cargo',}

	permission_from_user = None
