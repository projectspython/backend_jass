from apps.comun.Permissions.Base import BasePermission

class ConfigDireccionTipoPersonaPermissions(BasePermission):
	perms_map = {
				'add': 'comun.add_configdirecciontipopersona',
				'edit': 'comun.view_configdirecciontipopersona',
				'update': 'comun.update_configdirecciontipopersona',
				'delete':'comun.delete_configdirecciontipopersona',}

	permission_from_user = None