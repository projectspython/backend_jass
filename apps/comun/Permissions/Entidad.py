from apps.comun.Permissions.Base import BasePermission

class EntidadPermissions(BasePermission):
	perms_map = {
				'add': 'comun.add_entidad',
				'edit': 'comun.view_entidad',
				'update': 'comun.update_entidad',
				'delete':'comun.delete_entidad',}

	permission_from_user = None