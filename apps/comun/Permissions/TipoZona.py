from apps.comun.Permissions.Base import BasePermission

class TipoZonaPermissions(BasePermission):
	perms_map = {
				'add': 'comun.add_tipozona',
				'edit': 'comun.view_tipozona',
				'update': 'comun.update_tipozona',
				'delete':'comun.delete_tipozona',}

	permission_from_user = None