from apps.comun.Permissions.Base import BasePermission

class PaisPermissions(BasePermission):
	perms_map = {
				'add': 'comun.add_pais',
				'edit': 'comun.view_pais',
				'update': 'comun.update_pais',
				'delete':'comun.delete_pais',
				'searchform': 'comun.view_form_pais',}

	permission_from_user = None