from apps.comun.Permissions.Base import BasePermission

class PeriodoPermissions(BasePermission):
	perms_map = {
				'add': 'comun.add_periodo',
				'edit': 'comun.view_periodo',
				'update': 'comun.update_periodo',
				'delete':'comun.delete_periodo',}

	permission_from_user = None
