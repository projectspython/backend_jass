from apps.comun.Permissions.Base import BasePermission

class EtiquetaContactoPermissions(BasePermission):
	perms_map = {
				'add': 'comun.add_etiquetacontacto',
				'edit': 'comun.view_etiquetacontacto',
				'update': 'comun.update_etiquetacontacto',
				'delete':'comun.delete_etiquetacontacto',}

	permission_from_user = None