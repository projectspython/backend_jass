from apps.comun.Permissions.Base import BasePermission

class TipoViaPermissions(BasePermission):
	perms_map = {
				'add': 'comun.add_tipovia',
				'edit': 'comun.view_tipovia',
				'update': 'comun.update_tipovia',
				'delete':'comun.delete_tipovia',}

	permission_from_user = None