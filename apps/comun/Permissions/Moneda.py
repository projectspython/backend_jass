from apps.comun.Permissions.Base import BasePermission

class MonedaPermissions(BasePermission):
	perms_map = {
				'add': 'comun.add_moneda',
				'edit': 'comun.view_moneda',
				'update': 'comun.update_moneda',
				'delete':'comun.delete_moneda',}

	permission_from_user = None