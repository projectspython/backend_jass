from apps.comun.Permissions.Base import BasePermission

class ContinentePermissions(BasePermission):
	perms_map = {
				'add': 'comun.add_continente',
				'edit': 'comun.view_continente',
				'update': 'comun.update_continente',
				'delete':'comun.delete_continente',}

	permission_from_user = None