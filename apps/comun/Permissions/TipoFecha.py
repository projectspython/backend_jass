from apps.comun.Permissions.Base import BasePermission

class TipoFechaPermissions(BasePermission):
	perms_map = {
				'add': 'comun.add_tipofecha',
				'edit': 'comun.view_tipofecha',
				'update': 'comun.update_tipofecha',
				'delete':'comun.delete_tipofecha',}

	permission_from_user = None