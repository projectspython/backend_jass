from apps.comun.Permissions.Base import BasePermission

class CargoEntidadPermissions(BasePermission):
	perms_map = {
				'add': 'comun.add_cargoentidad',
				'edit': 'comun.view_cargoentidad',
				'update': 'comun.update_cargoentidad',
				'delete':'comun.delete_cargoentidad',}

	permission_from_user = None
