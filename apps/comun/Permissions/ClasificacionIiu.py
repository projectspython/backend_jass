from apps.comun.Permissions.Base import BasePermission

class ClasificacionIiuPermissions(BasePermission):
	perms_map = {
				'add': 'comun.add_clasificacioniiu',
				'edit': 'comun.view_clasificacioniiu',
				'update': 'comun.update_clasificacioniiu',
				'delete':'comun.delete_clasificacioniiu',}

	permission_from_user = None