from apps.comun.Permissions.Base import BasePermission

class ContadorPermissions(BasePermission):
	perms_map = {
				'add': 'comun.add_contador',
				'edit': 'comun.view_contador',
				'update': 'comun.update_contador',
				'delete':'comun.delete_contador',}

	permission_from_user = None