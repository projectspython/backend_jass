from apps.comun.Permissions.Base import BasePermission

class TipoContactoPermissions(BasePermission):
	perms_map = {
				'add': 'comun.add_tipocontacto',
				'edit': 'comun.view_tipocontacto',
				'update': 'comun.update_tipocontacto',
				'delete':'comun.delete_tipocontacto',}

	permission_from_user = None