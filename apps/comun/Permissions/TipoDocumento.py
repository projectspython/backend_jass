from apps.comun.Permissions.Base import BasePermission

class TipoDocumentoPermissions(BasePermission):
	perms_map = {
				'add': 'comun.add_tipodocumento',
				'edit': 'comun.view_tipodocumento',
				'update': 'comun.update_tipodocumento',
				'delete':'comun.delete_tipodocumento',}

	permission_from_user = None