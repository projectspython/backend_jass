from apps.comun.Permissions.Base import BasePermission

class TipoDireccionPermissions(BasePermission):
	perms_map = {
				'add': 'comun.add_tipodireccion',
				'edit': 'comun.view_tipodireccion',
				'update': 'comun.update_tipodireccion',
				'delete':'comun.delete_tipodireccion',}

	permission_from_user = None