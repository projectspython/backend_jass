from apps.comun.Permissions.Base import BasePermission

class TipoNumeroViaPermissions(BasePermission):
	perms_map = {
				'add': 'comun.add_tiponumerovia',
				'edit': 'comun.view_tiponumerovia',
				'update': 'comun.update_tiponumerovia',
				'delete':'comun.delete_tiponumerovia',}

	permission_from_user = None