from apps.comun.Permissions.Base import BasePermission

class TipoContribuyentePermissions(BasePermission):
	perms_map = {
				'add': 'comun.add_tipocontribuyente',
				'edit': 'comun.view_tipocontribuyente',
				'update': 'comun.update_tipocontribuyente',
				'delete':'comun.delete_tipocontribuyente',}

	permission_from_user = None