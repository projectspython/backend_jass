from apps.comun.Permissions.Base import BasePermission

class PersonaPermissions(BasePermission):
	perms_map = {
				'add': 'comun.add_persona',
				'edit': 'comun.view_persona',
				'update': 'comun.update_persona',
				'delete':'comun.delete_persona',
				'verificarnro':'comun.view_form_persona',
				'savefoto':'comun.update_persona',
				'deletefoto':'comun.update_persona',
				'viewinfo':'comun.view_form_persona',}

	permission_from_user = None