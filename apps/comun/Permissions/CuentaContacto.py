from apps.comun.Permissions.Base import BasePermission

class CuentaContactoPermissions(BasePermission):
	perms_map = {
				'add': 'comun.add_cuentacontacto',
				'edit': 'comun.view_cuentacontacto',
				'update': 'comun.update_cuentacontacto',
				'delete':'comun.delete_cuentacontacto',}

	permission_from_user = None