from apps.comun.Permissions.Base import BasePermission

class UbigeoPermissions(BasePermission):
	perms_map = {
				'add': 'comun.add_ubigeo',
				'edit': 'comun.view_ubigeo',
				'update': 'comun.update_ubigeo',
				'delete':'comun.delete_ubigeo',}

	permission_from_user = None