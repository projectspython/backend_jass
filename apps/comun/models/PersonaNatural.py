from django.db import models
from apps.comun.always.Choices import SEXOS
from apps.comun.models.Persona import Persona

class PersonaNatural(models.Model):
	id = models.OneToOneField(Persona, primary_key=True, db_column='id',
        related_name='persona_persona_natural', on_delete=models.CASCADE,
    )
	nombres = models.CharField(max_length=90)
	papellido = models.CharField(max_length=70)
	sapellido = models.CharField(max_length=70, blank=True, null=True)
	sexo = models.CharField(max_length=3, choices=SEXOS, blank=True, null=True)

	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		db_table = 'comun_persona_natural'
		verbose_name = 'Persona Natural'
		verbose_name_plural = 'Personas Naturales'
		default_permissions = ()
		# permissions = (
		# 	('view_personanatural',
		# 	 'Listado de persona natural'),
		# 	('add_personanatural',
		# 	 'Agregar persona natural'),
		# 	('update_personanatural',
		# 	 'Actualizar persona natural'),
		# 	('delete_personanatural',
		# 	 'Eliminar persona natural'),
		# 	('view_form_personanatural',
		# 	 'Listado de persona natural en formulario'),
		# )

	def __str__(self):
		return self.nombres