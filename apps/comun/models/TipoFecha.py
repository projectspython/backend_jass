from ..models.TipoPersona import TipoPersona
from django.db import models

class TipoFecha(models.Model):
	codigo = models.CharField(max_length=8, unique=True)
	nombre = models.CharField(max_length=60, unique=True)
	tipo_persona = models.ForeignKey(TipoPersona, related_name='tipo_fecha_tipo_personas', db_column='tipo_persona_id', on_delete=models.PROTECT)
	orden = models.IntegerField()
	estado = models.CharField(max_length=1, default='1')

	class Meta:
		db_table = 'comun_tipo_fecha'
		verbose_name = 'Tipo Fecha'
		verbose_name_plural = 'Tipos de Fechas'
		default_permissions = ()
		permissions = (
			('view_tipofecha',
			 'Listar Tipos de Fechas'),
			('add_tipofecha',
			 'Agregar Tipos de Fechas'),
			('update_tipofecha',
			 'Actualizar Tipos de Fechas'),
			('delete_tipofecha',
			 'Eliminar Tipos de Fechas'),
			('view_form_tipofecha',
			 'Listar Tipos de Fechas en formulario'),
		)

	def __str__(self):
		return  self.nombre
