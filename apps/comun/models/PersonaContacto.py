from ..models.Persona import Persona
from ..models.TipoContacto import TipoContacto
from ..models.CuentaContacto import CuentaContacto
from ..models.EtiquetaContacto import EtiquetaContacto
from ..models.Pais import Pais
from ..models.Ubigeo import Ubigeo
from django.db import models

class PersonaContacto(models.Model):
	persona = models.ForeignKey(Persona, related_name='persona_persona_contactos', 
		db_column='persona_id', on_delete=models.CASCADE)
	tipo_contacto = models.ForeignKey(TipoContacto, related_name='tipo_contacto_persona_contactos', 
		db_column='tipo_contacto_id', on_delete=models.PROTECT)
	cuenta_contacto = models.ForeignKey(CuentaContacto, related_name='cuenta_contacto_persona_contactos', 
		db_column='cuenta_contacto_id', blank=True, null=True, on_delete=models.PROTECT)
	etiqueta_contacto = models.ForeignKey(EtiquetaContacto, related_name='etiqueta_contacto_persona_contactos', 
		db_column='etiqueta_contacto_id', blank=True, null=True, on_delete=models.PROTECT)
	pais = models.ForeignKey(Pais, related_name='pais_persona_contactos', 
		db_column='pais_id', blank=True, null=True, on_delete=models.PROTECT)
	ubigeo = models.ForeignKey(Ubigeo, related_name='ubigeo_persona_contactos', 
		db_column='ubigeo_id', blank=True, null=True, on_delete=models.PROTECT)
	valor_contacto = models.CharField(max_length=90)
	anexo = models.CharField(max_length=10, blank=True, null=True,)
	area_oficina = models.CharField(max_length=60, blank=True, null=True,)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta(object):
		db_table = 'comun_persona_contacto'
		verbose_name = 'Persona contacto'
		verbose_name_plural = 'Personas contacto'
		default_permissions = ()

	def __str__(self):
		return  self.numero