from django.db import models

class TipoContribuyente(models.Model):
	codigo = models.CharField(max_length=8, unique=True)
	nombre = models.CharField(max_length=60, unique=True)
	abreviatura = models.CharField(max_length=30,blank=True, null=True)
	estado = models.CharField(max_length=1, default='1')

	class Meta:
		db_table = 'comun_tipo_contribuyente'
		verbose_name = 'Tipo contribuyente'
		verbose_name_plural = 'Tipos contribuyentes'
		default_permissions = ()
		permissions = (
			('view_tipocontribuyente',
			 'Listar Tipos de contribuyentes'),
			('add_tipocontribuyente',
			 'Agregar Tipos de contribuyentes'),
			('update_tipocontribuyente',
			 'Actualizar Tipos de contribuyentes'),
			('delete_tipocontribuyente',
			 'Eliminar Tipos de contribuyentes'),
			('view_form_tipocontribuyente',
			 'Listar Tipos de contribuyentes en formulario'),
		)

	def __str__(self):
		return self.nombre