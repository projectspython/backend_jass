from django.db import models

class TipoAmbito(models.Model):
	nombre = models.CharField(max_length=90, unique=True)
	abreviatura = models.CharField(max_length=30, blank=True, null=True)
	estado = models.CharField(max_length=1, default='1')

	class Meta:
		db_table = 'comun_tipo_ambito'
		verbose_name = 'Tipo Ambito'
		verbose_name_plural = 'Tipos de Ambitos'
		default_permissions = ()
		permissions = (
			('view_tipoambito',
			 'Listar Tipos de Ambitos'),
			('add_tipoambito',
			 'Agregar Tipos de Ambitos'),
			('update_tipoambito',
			 'Actualizar Tipos de Ambitos'),
			('delete_tipoambito',
			 'Eliminar Tipos de Ambitos'),
			('view_form_tipoambito',
			 'Listar Tipos de Ambitos en formulario'),
		)

	def __str__(self):
		return  self.nombre
