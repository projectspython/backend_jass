from django.db import models

class TipoDireccion(models.Model):
	codigo = models.CharField(max_length=8, unique=True)
	nombre = models.CharField(max_length=60, unique=True)
	abreviatura = models.CharField(max_length=12)
	orden = models.IntegerField()
	estado = models.CharField(max_length=1, default='1')

	class Meta:
		db_table = 'comun_tipo_direccion'
		verbose_name = 'Tipo Direccion'
		verbose_name_plural = 'Tipos de Direcciones'
		default_permissions = ()
		permissions = (
			('view_tipodireccion',
			 'Listar Tipos de Direcciones'),
			('add_tipodireccion',
			 'Agregar Tipos de Direcciones'),
			('update_tipodireccion',
			 'Actualizar Tipos de Direcciones'),
			('delete_tipodireccion',
			 'Eliminar Tipos de Direcciones'),
			('view_form_tipodireccion',
			 'Listar Tipos de Direcciones en formulario'),
		)

	def __str__(self):
		return  self.nombre