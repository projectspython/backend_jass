from ..models.TipoPersona import TipoPersona
from ..models.TipoDocumento import TipoDocumento
from django.db import models

class ConfigDocumentoTipoPersona(models.Model):
	tipo_persona = models.ForeignKey(TipoPersona, related_name='config_documento_tipo_persona_tipo_personas', db_column='tipo_persona_id', on_delete=models.PROTECT)
	tipo_documento = models.ForeignKey(TipoDocumento, related_name='config_documento_tipo_persona_tipo_direcciones', db_column='tipo_documento_id', on_delete=models.PROTECT)
	descripcion = models.CharField(max_length=60, blank=True, null=True)
	orden = models.IntegerField()

	class Meta(object):
		db_table = 'comun_config_documento_tipo_persona'
		verbose_name = 'Config. Documento de Tipo de Persona'
		verbose_name_plural = 'Config. de Documentos de Tipos de Personas'
		unique_together = ('tipo_persona', 'tipo_documento')
		default_permissions = ()
		permissions = (
			('view_configdocumentotipopersona',
			 'Listar Config. de Documentos de Tipos de Personas'),
			('add_configdocumentotipopersona',
			 'Agregar Config. de Documentos de Tipos de Personas'),
			('update_configdocumentotipopersona',
			 'Actualizar Config. de Documentos de Tipos de Personas'),
			('delete_configdocumentotipopersona',
			 'Eliminar Config. de Documentos de Tipos de Personas'),
			('view_form_configdocumentotipopersona',
			 'Listar Config. de Documentos de Tipos de Personas en formulario'),
		)

	def __str__(self):
		return  self.descripcion
