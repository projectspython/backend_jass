from django.db import models

class Cargo(models.Model):
	codigo = models.CharField(max_length=10, unique=True)
	nombre = models.CharField(max_length=150)
	estado = models.CharField(max_length=1, default='1')

	class Meta:
		db_table = 'comun_cargo'
		verbose_name = 'Cargo'
		verbose_name_plural = 'Cargos'
		default_permissions = ()
		permissions = (
			('view_cargo',
			 'Listar Cargo'),
			('add_cargo',
			 'Agregar Cargo'),
			('update_cargo',
			 'Actualizar Cargo'),
			('delete_cargo',
			 'Eliminar Cargo'),
			('view_form_cargo',
			 'Listar Cargo en formulario'),
		)

	def __str__(self):
		return self.nombre