from ..models.TipoContacto import TipoContacto
from django.db import models

class CuentaContacto(models.Model):
 	codigo = models.CharField(max_length=10, unique=True)
 	tipo_contacto_id = models.ForeignKey(TipoContacto, related_name='tipo_contacto_cuenta_contactos',
		db_column='tipo_contacto_id', on_delete=models.PROTECT)
 	nombre = models.CharField(max_length=60)
 	logo = models.CharField(max_length=200, blank=True, null=True)
 	url_web = models.CharField(max_length=200, blank=True, null=True)
 	orden = models.IntegerField()
 	estado = models.CharField(max_length=1, default='1')

 	class Meta:
 		db_table ='comun_cuenta_contacto'
 		verbose_name ='Cuenta Contacto'
 		verbose_name_plural ='Cuentas Contactos'
 		default_permissions = ()
 		permissions = (
			('view_cuentacontacto',
			 'Listar Cuentas de Contacto'),
			('add_cuentacontacto',
			 'Agregar Cuentas de Contacto'),
			('update_cuentacontacto',
			 'Actualizar Cuentas de Contacto'),
			('delete_cuentacontacto',
			 'Eliminar Cuentas de Contacto'),
			('view_form_cuentacontacto',
			 'Listar Cuentas de Contacto en formulario'),
		)

 	def __str__(self):
 		return  self.nombre

