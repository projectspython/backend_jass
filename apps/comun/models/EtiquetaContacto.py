from ..models.TipoContacto import TipoContacto
from django.db import models

class EtiquetaContacto(models.Model):
	codigo = models.CharField(max_length=10, unique=True)
	tipo_contacto = models.ForeignKey(TipoContacto, related_name='etiqueta_contacto_tipo_contactos', db_column='tipo_contacto_id', on_delete=models.PROTECT)
	nombre = models.CharField(max_length=40)
	tiene_cod_ubigeo = models.CharField(max_length=2, blank=True, null=True)
	tiene_anexo = models.CharField(max_length=2, blank=True, null=True)
	orden = models.IntegerField()
	estado = models.CharField(max_length=1, default='1')

	class Meta:
		db_table = 'comun_etiqueta_contacto'
		verbose_name = 'Etiqueta Contacto'
		verbose_name_plural = 'Etiquetas de Contactos'
		unique_together = ('tipo_contacto', 'nombre')
		default_permissions = ()
		permissions = (
			('view_etiquetacontacto',
			 'Listar Etiquetas de Contactos'),
			('add_etiquetacontacto',
			 'Agregar Etiquetas de Contactos'),
			('update_etiquetacontacto',
			 'Actualizar Etiquetas de Contactos'),
			('delete_etiquetacontacto',
			 'Eliminar Etiquetas de Contactos'),
			('view_form_etiquetacontacto',
			 'Listar Etiquetas de Contactos en formulario'),
		)

	def __str__(self):
		return self.nombre