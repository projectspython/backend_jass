from ..models.Persona import Persona
from ..models.Periodo import Periodo
from ..models.Entidad import Entidad
from ..models.Cargo import Cargo
from django.db import models

class CargoEntidad(models.Model):
	periodo = models.ForeignKey(Periodo, related_name='periodo_cargo_entidad', db_column='periodo_id', on_delete=models.PROTECT)
	entidad = models.ForeignKey(Entidad, related_name='entidad_cargo_entidad', db_column='entidad_id', on_delete=models.PROTECT)
	persona = models.ForeignKey(Persona, db_column='persona_id', related_name="persona_cargo_entidad", on_delete=models.PROTECT)
	cargo = models.ForeignKey(Cargo, db_column='cargo_id', related_name="cargo_cargo_entidad", on_delete=models.PROTECT)
	estado = models.CharField(max_length=1, default='1')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		db_table = 'comun_cargo_entidad'
		verbose_name = 'Cargo de la Entidad'
		verbose_name_plural = 'Cargos de la Entidad'
		unique_together = ('periodo','persona','entidad')
		default_permissions = ()
		permissions = (
			('view_cargoentidad',
			 'Listar Cargos de la Entidad'),
			('add_cargoentidad',
			 'Agregar Cargos de la Entidad'),
			('update_cargoentidad',
			 'Actualizar Cargos de la Entidad'),
			('delete_cargoentidad',
			 'Eliminar Cargos de la Entidad'),
			('view_form_cargoentidad',
			 'Listar Cargos de la Entidad en formulario'),
		)

	def __str__(self):
		return self.persona