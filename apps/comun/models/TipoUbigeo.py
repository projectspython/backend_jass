from django.db import models
class TipoUbigeo(models.Model):
	codigo = models.CharField(max_length=8, unique=True)
	nombre = models.CharField(max_length=50, unique=True)
	nombre_plural = models.CharField(max_length=60)
	orden = models.IntegerField()
	estado = models.CharField(max_length=1, default='1')

	class Meta:
		db_table = 'comun_tipo_ubigeo'
		verbose_name = 'Tipo Ubigeo'
		verbose_name_plural = 'Tipos de Ubigeos'
		default_permissions = ()
		permissions = (
			('view_tipoubigeo',
			 'Listar Tipos de Ubigeos'),
			('add_tipoubigeo',
			 'Agregar Tipos de Ubigeos'),
			('update_tipoubigeo',
			 'Actualizar Tipos de Ubigeos'),
			('delete_tipoubigeo',
			 'Eliminar Tipos de Ubigeos'),
			('view_form_tipoubigeo',
			 'Listar Tipos de Ubigeos en formulario'),
		)

	def __str__(self):
		return  self.nombre
