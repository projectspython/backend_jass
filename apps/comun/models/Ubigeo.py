from ..models.TipoUbigeo import TipoUbigeo
from ..models.Pais import Pais
from ..models.RegionNatural import RegionNatural
from django.db import models

class Ubigeo(models.Model):
	codigo = models.CharField(max_length=20, blank=True, null=True)
	nombre = models.CharField(max_length=70)
	tipo_ubigeo = models.ForeignKey(TipoUbigeo, related_name='tipo_ubigeo_ubigeos', 
		db_column='tipo_ubigeo_id', on_delete=models.PROTECT)
	codigo_telefono = models.CharField(max_length=20, blank=True, null=True)
	pais = models.ForeignKey(Pais, related_name='pais_ubigeos',	db_column='pais_id', on_delete=models.PROTECT)
	region_natural = models.ForeignKey(RegionNatural, related_name='region_natural_ubigeos', db_column='region_natural_id', blank=True, null=True, on_delete=models.PROTECT)
	capital = models.CharField(max_length=80, blank=True, null=True)
	poblacion = models.IntegerField(blank=True, null=True)
	area_km2 = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
	padre = models.ForeignKey('self', related_name='padre_ubigeos', db_column='padre_id', blank=True, null=True, on_delete=models.PROTECT)

	class Meta:
		db_table = 'comun_ubigeo'
		verbose_name = 'Ubigeo'
		verbose_name_plural = 'Ubigeos'
		unique_together = ('pais', 'nombre','tipo_ubigeo', 'padre')
		default_permissions = ()
		permissions = (
			('view_ubigeo',
			 'Listado de ubigeos'),
			('add_ubigeo',
			 'Agregar ubigeos'),
			('update_ubigeo',
			 'Actualizar ubigeos'),
			('delete_ubigeo',
			 'Eliminar ubigeos'),
			('view_form_ubigeo',
			 'Listado de ubigeos en formulario'),
		)
	def __str__(self):
		return  self.nombre