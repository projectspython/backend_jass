from ..models.Persona import Persona
from ..models.TipoEntidad import TipoEntidad
from ..models.Moneda import Moneda
from django.db import models

class Entidad(models.Model):
	persona = models.OneToOneField(Persona, db_column='persona_id', related_name="entidad_persona", on_delete=models.PROTECT)
	tipo_entidad = models.ForeignKey(TipoEntidad, related_name='entidad_tipo_entidad', db_column='tipo_entidad_id', on_delete=models.PROTECT)
	padre = models.ForeignKey('self', related_name='padre_entidades', db_column='padre_id', blank=True, null=True, on_delete=models.PROTECT)
	moneda = models.ForeignKey(Moneda, related_name='entidad_moneda', blank=True, null=True, db_column='moneda_id', on_delete=models.PROTECT)
	logo = models.CharField(max_length=120, blank=True, null=True)
	codigo = models.CharField(max_length=60, blank=True, null=True)
	estado = models.CharField(max_length=1, default='1')

	class Meta:
		db_table = 'entidad'
		verbose_name = 'Entidad'
		verbose_name_plural = 'Entidades'
		default_permissions = ()
		permissions = (
			('view_entidad',
			 'Listar Entidades'),
			('add_entidad',
			 'Agregar Entidades'),
			('update_entidad',
			 'Actualizar Entidades'),
			('delete_entidad',
			 'Eliminar Entidades'),
			('view_form_entidad',
			 'Listar Entidades en formulario'),
		)

	def __str__(self):
		return str(self.persona_id)