from ..models.Persona import Persona
from ..models.TipoFecha import TipoFecha
from ..models.Mes import Mes
from django.db import models
from apps.comun.always.Choices import DIAS

class PersonaFecha(models.Model):
	persona = models.ForeignKey(Persona, related_name='persona_persona_fechas', 
		db_column='persona_id', on_delete=models.CASCADE)
	tipo_fecha = models.ForeignKey(TipoFecha, related_name='tipo_fecha_persona_fechas', 
		db_column='tipo_fecha_id', on_delete=models.PROTECT)
	anio = models.IntegerField(blank=True, null=True)
	mes = models.ForeignKey(Mes, related_name='mes_persona_fechas', 
		db_column='mes_id', blank=True, null=True, on_delete=models.PROTECT)
	dia = models.CharField(max_length=2, choices=DIAS, blank=True, null=True)


	class Meta(object):
		db_table = 'comun_persona_fecha'
		verbose_name = 'Persona fecha'
		verbose_name_plural = 'Personas fecha'
		unique_together = ('persona', 'tipo_fecha')
		default_permissions = ()

	def __str__(self):
		return  self.anio