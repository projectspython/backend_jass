from ..models.Persona import Persona
from ..models.TipoDocumento import TipoDocumento
from django.db import models

class PersonaDocumento(models.Model):
	persona = models.ForeignKey(Persona, related_name='persona_persona_documentos', 
		db_column='persona_id', on_delete=models.CASCADE)
	tipo_documento = models.ForeignKey(TipoDocumento, related_name='tipo_documento_persona_documentos', db_column='tipo_documento_id', on_delete=models.PROTECT)
	numero = models.CharField(max_length=20)
	orden = models.IntegerField()

	class Meta(object):
		db_table = 'comun_persona_documento'
		verbose_name = 'Persona documento'
		verbose_name_plural = 'Personas documento'
		unique_together = ('persona', 'tipo_documento')
		default_permissions = ()

	def __str__(self):
		return  self.numero