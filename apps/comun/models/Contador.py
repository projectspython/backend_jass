from django.db import models

class Contador(models.Model):
	codigo = models.CharField(max_length=6, unique=True)
	descripcion = models.CharField(max_length=60)
	contador = models.IntegerField(default=0)

	class Meta:
		verbose_name = 'Contador'
		verbose_name_plural = 'Contadores'
		default_permissions = ()
		permissions = (
			('view_contador',
			 'Listar Contadores'),
			('add_contador',
			 'Agregar Contadores'),
			('update_contador',
			 'Actualizar Contadores'),
			('delete_contador',
			 'Eliminar Contadores'),
			('view_form_contador',
			 'Listar Contadores en formulario'),
		)

	def __str__(self):
		return self.codigo