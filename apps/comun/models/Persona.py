from ..models.TipoPersona import TipoPersona
from ..models.TipoContribuyente import TipoContribuyente
from ..models.Pais import Pais
from django.db import models
# from django.core.files.storage import FileSystemStorage
# fs = FileSystemStorage(location='/storage/storage/comun/personas/fotos')
dir_storage = "storage/comun/personas/fotos"

class Persona(models.Model):
	codigo = models.CharField(max_length=12, unique=True, blank=True, null=True)
	nombre_completo = models.CharField(max_length=140)
	tipo_persona = models.ForeignKey(TipoPersona, related_name='tipo_persona_personas',
		db_column='tipo_persona_id', blank=True, null=True, on_delete=models.PROTECT)
	tipo_contribuyente = models.ForeignKey(TipoContribuyente, related_name='tipo_contribuyente_personas',
		db_column='tipo_contribuyente_id', blank=True, null=True, on_delete=models.PROTECT)
	nacionalidad = models.ForeignKey(Pais, related_name='nacionalidad_personas',
		db_column='nacionalidad_id', blank=True, null=True, on_delete=models.PROTECT)
	foto_logo = models.ImageField(upload_to=dir_storage, blank=True, null=True)
	foto_logo_rec = models.ImageField(upload_to=dir_storage, blank=True, null=True)

	estado = models.CharField(max_length=1, default='1')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		verbose_name = 'Persona'
		verbose_name_plural = 'Personas'
		default_permissions = ()
		permissions = (
			('view_persona',
			 'Listar Personas'),
			('add_persona',
			 'Agregar Personas'),
			('update_persona',
			 'Actualizar Personas'),
			('delete_persona',
			 'Eliminar Personas'),
			('view_form_persona',
			 'Listar Personas en formulario'),
		)

	def __str__(self):
		return self.nombre_completo
