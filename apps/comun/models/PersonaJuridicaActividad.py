from ..models.PersonaJuridica import PersonaJuridica
from ..models.ClasificacionIiu import ClasificacionIiu
from django.db import models

class PersonaJuridicaActividad(models.Model):
	persona_juridica = models.ForeignKey(PersonaJuridica, related_name='persona_juridica_actividades', 
		db_column='persona_natural_id', on_delete=models.CASCADE)
	clasificacion_iiu = models.ForeignKey(ClasificacionIiu, related_name='clasificacion_iiu_actividades', 
		db_column='clasificacion_iiu_id', on_delete=models.PROTECT)
	orden = models.IntegerField()

	class Meta(object):
		db_table = 'comun_persona_juridica_actividad'
		verbose_name = 'Persona juridica actividad'
		verbose_name_plural = 'Persona juridica actividades'
		unique_together = ('persona_juridica', 'clasificacion_iiu')
		default_permissions = ()

	def __str__(self):
		return  self.clasificacion_iiu.nombre