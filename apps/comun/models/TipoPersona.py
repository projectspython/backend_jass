from django.db import models

class TipoPersona(models.Model):
	codigo = models.CharField(max_length=8, unique=True)
	nombre = models.CharField(max_length=60, unique=True)
	orden = models.IntegerField()
	estado = models.CharField(max_length=1, default='1')

	class Meta:
		db_table = 'comun_tipo_persona'
		verbose_name = 'Tipo persona'
		verbose_name_plural = 'Tipos personas'
		default_permissions = ()
		permissions = (
			('view_tipopersona',
			 'Listar Tipos de persona'),
			('add_tipopersona',
			 'Agregar Tipos de persona'),
			('update_tipopersona',
			 'Actualizar Tipos de persona'),
			('delete_tipopersona',
			 'Eliminar Tipos de persona'),
			('view_form_tipopersona',
			 'Listar Tipos de persona en formulario'),
		)

	def __str__(self):
		return self.nombre