from django.db import models
# from apps.comun.always.Choices import SEXOS
from apps.comun.models.Persona import Persona
from apps.comun.models.TipoAmbito import TipoAmbito

class PersonaJuridica(models.Model):
	id = models.OneToOneField(Persona, primary_key=True, db_column='id',
        related_name='persona_persona_juridica', on_delete=models.CASCADE,
    )
	razon_social = models.CharField(max_length=90)
	nombre_comercial = models.CharField(max_length=70, blank=True, null=True)
	repres_legal = models.ForeignKey(Persona, related_name='repres_legal_persona_juridica', 
		db_column='repres_legal_id', blank=True, null=True, on_delete=models.PROTECT)
	nombre_repres_legal = models.CharField(max_length=120, blank=True, null=True)
	nro_resolucion = models.CharField(max_length=120, blank=True, null=True)
	tipo_ambito = models.ForeignKey(TipoAmbito, related_name='tipo_ambito_persona_juridica', 
		db_column='tipo_ambito_id', on_delete=models.PROTECT, blank=True, null=True)

	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		db_table = 'comun_persona_juridica'
		verbose_name = 'Persona Jurídica'
		verbose_name_plural = 'Personas Jurídicas'
		default_permissions = ()

	def __str__(self):
		return self.razon_social