from apps.comun.always.Choices import ESTADO_PERIODOS
from django.db import models

class Periodo(models.Model):
	codigo = models.CharField(max_length=10, unique=True)
	nombre = models.CharField(max_length=150)
	fecha_inicio = models.DateField()
	fecha_fin = models.DateField()
	estado = models.CharField(max_length=1, choices=ESTADO_PERIODOS, default='1')

	class Meta:
		db_table = 'comun_periodo'
		verbose_name = 'Periodo'
		verbose_name_plural = 'Periodos'
		default_permissions = ()
		permissions = (
			('view_periodo',
			 'Listar periodo'),
			('add_periodo',
			 'Agregar periodo'),
			('update_periodo',
			 'Actualizar periodo'),
			('delete_periodo',
			 'Eliminar periodo'),
			('view_form_periodo',
			 'Listar periodo en formulario'),
		)

	def __str__(self):
		return self.nombre