from django.db import models

class TipoNumeroVia(models.Model):
	codigo = models.CharField(max_length=8, unique=True)
	nombre = models.CharField(max_length=60, unique=True)
	abreviatura= models.CharField(max_length=12)
	orden = models.IntegerField()
	estado = models.CharField(max_length=1, default='1')

	class Meta:
		db_table = 'comun_tipo_numero_via'
		verbose_name = 'Tipo Numero via'
		verbose_name_plural = 'Tipos Numeros Vias'
		default_permissions = ()
		permissions = (
			('view_tiponumerovia',
			 'Listar Tipos Numero Via'),
			('add_tiponumerovia',
			 'Agregar Tipos Numero Via'),
			('update_tiponumerovia',
			 'Actualizar Tipos Numero Via'),
			('delete_tiponumerovia',
			 'Eliminar Tipos Numero Via'),
			('view_form_tiponumerovia',
			 'Listar Tipos Numero Via en formulario'),
		)

	def __str__(self):
		return  self.nombre
