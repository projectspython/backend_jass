from django.db import models

class Moneda(models.Model):
	codigo = models.CharField(max_length=10, unique=True)
	nombre = models.CharField(max_length=50, unique=True)
	nombre_plural = models.CharField(max_length=60)
	simbolo = models.CharField(max_length=8)
	orden = models.IntegerField()
	estado = models.CharField(max_length=1, default='1')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		verbose_name = 'Moneda'
		verbose_name_plural = 'Monedas'
		default_permissions = ()
		permissions = (
			('view_moneda',
			 'Listado de monedas'),
			('add_moneda',
			 'Agregar monedas'),
			('update_moneda',
			 'Actualizar monedas'),
			('delete_moneda',
			 'Eliminar monedas'),
			('view_form_moneda',
			 'Listado de monedas en formulario'),
		)

	def __str__(self):
		return self.nombre