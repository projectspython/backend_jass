from django.db import models

class Continente(models.Model):
	codigo = models.CharField(max_length=12, blank=True, null=True, unique=True)
	nombre = models.CharField(max_length=120, unique=True)
	abreviatura = models.CharField(max_length=30, blank=True, null=True)
	estado = models.CharField(max_length=1, default='1')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		verbose_name = 'Continente'
		verbose_name_plural = 'Continentes'
		default_permissions = ()
		permissions = (
			('view_continente',
			 'Listado de continentes'),
			('add_continente',
			 'Agregar continentes'),
			('update_continente',
			 'Actualizar continentes'),
			('delete_continente',
			 'Eliminar continentes'),
			('view_form_continente',
			 'Listado de continentes en formulario'),
		)

	def __str__(self):
		return self.nombre