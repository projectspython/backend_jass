from django.db import models

class ProfesionOficio(models.Model):
	codigo = models.CharField(max_length=10)
	nombre = models.CharField(max_length=150)
	estado = models.CharField(max_length=1, default='1')

	class Meta:
		db_table = 'comun_profesion_oficio'
		verbose_name = 'Profesion u Oficio'
		verbose_name_plural = 'Profesiones u Oficios'
		default_permissions = ()
		permissions = (
			('view_profesionoficio',
			 'Listar Profesiones u Oficios'),
			('add_profesionoficio',
			 'Agregar Profesiones u Oficios'),
			('update_profesionoficio',
			 'Actualizar Profesiones u Oficios'),
			('delete_profesionoficio',
			 'Eliminar Profesiones u Oficios'),
			('view_form_profesionoficio',
			 'Listar Profesiones u Oficios en formulario'),
		)

	def __str__(self):
		return  self.nombre