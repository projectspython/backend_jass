from django.db import models

class ClasificacionIiu(models.Model):
	codigo = models.CharField(max_length=10, unique=True)
	nombre = models.CharField(max_length=150)
	estado = models.CharField(max_length=1, default='1')

	class Meta:
		db_table = 'comun_clasificacion_iiu'
		verbose_name = 'Clasificacion iiu'
		verbose_name_plural = 'Clasificaciones iiu'
		default_permissions = ()
		permissions = (
			('view_clasificacioniiu',
			 'Listar Clasificaciones iiu'),
			('add_clasificacioniiu',
			 'Agregar Clasificaciones iiu'),
			('update_clasificacioniiu',
			 'Actualizar Clasificaciones iiu'),
			('delete_clasificacioniiu',
			 'Eliminar Clasificaciones iiu'),
			('view_form_clasificacioniiu',
			 'Listar Clasificaciones iiu en formulario'),
		)

	def __str__(self):
		return self.nombre