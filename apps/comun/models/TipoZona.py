from django.db import models

class TipoZona(models.Model):
	codigo = models.CharField(max_length=6, unique=True)
	nombre = models.CharField(max_length=60, unique=True)
	abreviatura= models.CharField(max_length=30)
	estado = models.CharField(max_length=1, default='1')

	class Meta:
		db_table = 'comun_tipo_zona'
		verbose_name = 'Tipo Zona'
		verbose_name_plural = 'Tipos de Zona'
		default_permissions = ()
		permissions = (
			('view_tipozona',
			 'Listar Tipos de Zona'),
			('add_tipozona',
			 'Agregar Tipos de Zona'),
			('update_tipozona',
			 'Actualizar Tipos de Zona'),
			('delete_tipozona',
			 'Eliminar Tipos de Zona'),
			('view_form_tipozona',
			 'Listar Tipos de Zona en formulario'),
		)

	def __str__(self):
		return  self.nombre