from django.db import models

class TipoDocumento(models.Model):
	codigo = models.CharField(max_length=6, unique=True)
	nombre = models.CharField(max_length=40, unique=True)
	descripcion = models.CharField(max_length=60)
	patron_angular = models.CharField(max_length=60, blank=True, null=True)
	orden = models.IntegerField()
	estado = models.CharField(max_length=1, default='1')

	class Meta:
		db_table = 'comun_tipo_documento'
		verbose_name = 'Tipo de Documento'
		verbose_name_plural = 'Tipos de documentos'
		default_permissions = ()
		permissions = (
			('view_tipodocumento',
			 'Listar Tipos de documentos'),
			('add_tipodocumento',
			 'Agregar Tipos de documentos'),
			('update_tipodocumento',
			 'Actualizar Tipos de documentos'),
			('delete_tipodocumento',
			 'Eliminar Tipos de documentos'),
			('view_form_tipodocumento',
			 'Listar Tipos de documentos en formulario'),
		)

	def __str__(self):
		return  self.nombre