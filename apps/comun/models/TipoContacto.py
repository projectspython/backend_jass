from django.db import models

class TipoContacto(models.Model):
	codigo = models.CharField(max_length=10, unique=True)
	nombre = models.CharField(max_length=40, unique=True)
	nombre_plural = models.CharField(max_length=50)
	denom_valor = models.CharField(max_length=40, blank=True, null=True)
	icono_material = models.CharField(max_length=40, blank=True, null=True)
	orden = models.IntegerField()
	estado = models.CharField(max_length=1, default='1')

	class Meta:
		db_table = 'comun_tipo_contacto'
		verbose_name = 'Tipo Contacto'
		verbose_name_plural = 'Tipos de Contactos'
		default_permissions = ()
		permissions = (
			('view_tipocontacto',
			 'Listar Tipos de Contactos'),
			('add_tipocontacto',
			 'Agregar Tipos de Contactos'),
			('update_tipocontacto',
			 'Actualizar Tipos de Contactos'),
			('delete_tipocontacto',
			 'Eliminar Tipos de Contactos'),
			('view_form_tipocontacto',
			 'Listar Tipos de Contactos en formulario'),
		)

	def __str__(self):
		return  self.nombre
