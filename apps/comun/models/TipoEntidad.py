from django.db import models

class TipoEntidad(models.Model):
	codigo = models.CharField(max_length=6, unique=True)
	nombre = models.CharField(max_length=120, unique=True)
	estado = models.CharField(max_length=1, default='1')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		db_table = 'comun_tipo_entidad'
		verbose_name = 'Tipo de entidad'
		verbose_name_plural = 'Tipos de entidades'
		default_permissions = ()
		permissions = (
			('view_tipoentidad',
			 'Listar Tipos de entidades'),
			('add_tipoentidad',
			 'Agregar Tipos de entidades'),
			('update_tipoentidad',
			 'Actualizar Tipos de entidades'),
			('delete_tipoentidad',
			 'Eliminar Tipos de entidades'),
			('view_form_tipoentidad',
			 'Listar Tipos de entidades en formulario'),
		)

	def __str__(self):
		return  self.nombre