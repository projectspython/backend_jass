from django.db import models

class Mes(models.Model):
	id = models.CharField(max_length=2, primary_key=True)
	nombre = models.CharField(max_length=50, unique=True)
	abreviatura = models.CharField(max_length=8)

	class Meta:
		# db_table = 'comun_persona'
		verbose_name = 'Mes'
		verbose_name_plural = 'Meses'
		default_permissions = ()
		permissions = (
			('view_mes',
			 'Listar Meses'),
			('add_mes',
			 'Agregar Meses'),
			('update_mes',
			 'Actualizar Meses'),
			('delete_mes',
			 'Eliminar Meses'),
			('view_form_mes',
			 'Listar Meses en formulario'),
		)

	def __str__(self):
		return self.nombre