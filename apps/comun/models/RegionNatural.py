from django.db import models

class RegionNatural(models.Model):
	nombre = models.CharField(max_length=50)
	orden = models.IntegerField()
	estado = models.CharField(max_length=1, default='1')

	class Meta:
		db_table = 'comun_region_natural'
		verbose_name = 'Region natural'
		verbose_name_plural = 'Regiones naturales'
		default_permissions = ()
		permissions = (
			('view_regionnatural',
			 'Listar Regiones naturales'),
			('add_regionnatural',
			 'Agregar Regiones naturales'),
			('update_regionnatural',
			 'Actualizar Regiones naturales'),
			('delete_regionnatural',
			 'Eliminar Regiones naturales'),
			('view_form_regionnatural',
			 'Listar Regiones naturales en formulario'),
		)

	def __str__(self):
		return  self.nombre
