from ..models.TipoPersona import TipoPersona
from ..models.TipoDireccion import TipoDireccion
from django.db import models

class ConfigDireccionTipoPersona(models.Model):
	tipo_persona = models.ForeignKey(TipoPersona, related_name='config_direccion_tipo_persona_tipo_personas', db_column='tipo_persona_id', on_delete=models.PROTECT)
	tipo_direccion = models.ForeignKey(TipoDireccion, related_name='tipo_direccion_config_direccion_tipo_persona', db_column='tipo_direccion_id', on_delete=models.PROTECT)
	descripcion = models.CharField(max_length=60, blank=True, null=True)
	orden = models.IntegerField()

	class Meta(object):
		db_table = 'comun_config_direccion_tipo_persona'
		verbose_name = 'Config. Direccion de Tipo de Persona'
		verbose_name_plural = 'Config. de Direcciones de Tipos de Personas'
		unique_together = ('tipo_persona', 'tipo_direccion')
		default_permissions = ()
		permissions = (
			('view_configdirecciontipopersona',
			 'Listar Config. de Direcciones de Tipos de Personas'),
			('add_configdirecciontipopersona',
			 'Agregar Config. de Direcciones de Tipos de Personas'),
			('update_configdirecciontipopersona',
			 'Actualizar Config. de Direcciones de Tipos de Personas'),
			('delete_configdirecciontipopersona',
			 'Eliminar Config. de Direcciones de Tipos de Personas'),
			('view_form_configdirecciontipopersona',
			 'Listar Config. de Direcciones de Tipos de Personas en formulario'),
		)

	def __str__(self):
		return  self.descripcion
