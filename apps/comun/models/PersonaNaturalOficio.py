from ..models.PersonaNatural import PersonaNatural
from ..models.ProfesionOficio import ProfesionOficio
from django.db import models

class PersonaNaturalOficio(models.Model):
	persona_natural = models.ForeignKey(PersonaNatural, related_name='persona_natural_oficios', 
		db_column='persona_natural_id', on_delete=models.CASCADE)
	profesion_oficio = models.ForeignKey(ProfesionOficio, related_name='profesion_oficio_oficios', 
		db_column='profesion_oficio_id', on_delete=models.PROTECT)
	descripcion = models.CharField(max_length=60,blank=True, null=True)
	orden = models.IntegerField()

	class Meta(object):
		db_table = 'comun_persona_natural_oficio'
		verbose_name = 'Persona natural oficio'
		verbose_name_plural = 'Persona natural oficios'
		default_permissions = ()

	def __str__(self):
		return  self.descripcion