from django.db import models

class TipoVia(models.Model):
	codigo = models.CharField(max_length=6, unique=True)
	nombre = models.CharField(max_length=60, unique=True)
	abreviatura= models.CharField(max_length=30)
	estado = models.CharField(max_length=1, default='1')

	class Meta:
		db_table = 'comun_tipo_via'
		verbose_name = 'Tipo via'
		verbose_name_plural = 'Tipos Vias'
		default_permissions = ()
		permissions = (
			('view_tipovia',
			 'Listar Tipos de vías'),
			('add_tipovia',
			 'Agregar Tipos de vías'),
			('update_tipovia',
			 'Actualizar Tipos de vías'),
			('delete_tipovia',
			 'Eliminar Tipos de vías'),
			('view_form_tipovia',
			 'Listar Tipos de vías en formulario'),
		)

	def __str__(self):
		return  self.nombre