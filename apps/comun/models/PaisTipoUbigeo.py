from ..models.Pais import Pais
from ..models.TipoUbigeo import TipoUbigeo
from django.db import models

class PaisTipoUbigeo(models.Model):
	pais =  models.ForeignKey(Pais, related_name='pais_pais_tipo_ubigeos',
		db_column='pais_id', on_delete=models.PROTECT)
	tipo_ubigeo =  models.ForeignKey(TipoUbigeo, related_name='tipo_ubigeo_pais_tipo_ubigeos',
		db_column='tipo_ubigeo_id', on_delete=models.PROTECT)
	orden_nivel =  models.IntegerField()

	class Meta:
		db_table = 'comun_pais_tipo_ubigeo'
		verbose_name = 'Pais Tipo Ubigeo'
		verbose_name_plural = 'Paises Tipo Ubigeo'
		unique_together = ('pais', 'tipo_ubigeo')
		default_permissions = ()
		permissions = (
			('view_paistipoubigeo',
			 'Listar Paises Tipo Ubigeo'),
			('add_paistipoubigeo',
			 'Agregar Paises Tipo Ubigeo'),
			('update_paistipoubigeo',
			 'Actualizar Paises Tipo Ubigeo'),
			('delete_paistipoubigeo',
			 'Eliminar Paises Tipo Ubigeo'),
			('view_form_paistipoubigeo',
			 'Listar Paises Tipo Ubigeo en formulario'),
		)

	def __str__(self):
		return  self.pais_id
