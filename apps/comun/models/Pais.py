from ..models.Continente import Continente
from django.db import models

class Pais(models.Model):
	codigo = models.CharField(max_length=12, unique=True, blank=True, null=True)
	nombre = models.CharField(max_length=120, unique=True)
	abreviatura = models.CharField(max_length=30, blank=True, null=True)
	continente = models.ForeignKey(Continente, related_name='continente_paises',
		db_column='continente_id', blank=True, null=True, on_delete=models.PROTECT)
	capital = models.CharField(max_length=80, blank=True, null=True)
	codigo_telefono = models.CharField(max_length=10, blank=True, null=True)
	estado = models.CharField(max_length=1, default='1')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		verbose_name = 'Pais'
		verbose_name_plural = 'Paises'
		default_permissions = ()
		permissions = (
			('view_pais',
			 'Listado de paises'),
			('add_pais',
			 'Agregar paises'),
			('update_pais',
			 'Actualizar paises'),
			('delete_pais',
			 'Eliminar paises'),
			('view_form_pais',
			 'Listado de paises en formulario'),
		)

	def __str__(self):
		return self.nombre



		