from django.conf.urls import url, include
from rest_framework import routers
from .views.PersonaView import PersonaViewSet
from .views.TestView import TestView
from .views.UbigeoView import UbigeoViewSet
from .views.MonedaView import MonedaViewSet
from .views.ContinenteView import ContinenteViewSet
from .views.PaisView import PaisViewSet
from .views.ClasificacionIiuView import ClasificacionIiuViewSet
from .views.TipoUbigeoView import TipoUbigeoViewSet
from .views.TipoViaView import TipoViaViewSet
from .views.TipoContactoView import TipoContactoViewSet
from .views.TipoAmbitoView import TipoAmbitoViewSet
from .views.TipoDireccionView import TipoDireccionViewSet
from .views.TipoEntidadView import TipoEntidadViewSet
from .views.TipoZonaView import TipoZonaViewSet
from .views.TipoNumeroViaView import TipoNumeroViaViewSet
from .views.TipoPersonaView import TipoPersonaViewSet
from .views.ChoicesView import ChoicesViewSet
from .views.TipoContribuyenteView import TipoContribuyenteViewSet
from .views.TipoDocumentoView import TipoDocumentoViewSet
from .views.ConfigDireccionTipoPersonaView import ConfigDireccionTipoPersonaViewSet
from .views.ConfigDocumentoTipoPersonaView import ConfigDocumentoTipoPersonaViewSet
from .views.ContadorView import ContadorViewSet
from .views.CuentaContactoView import CuentaContactoViewSet
from .views.EtiquetaContactoView import EtiquetaContactoViewSet
from .views.MesView import MesViewSet
from .views.TipoFechaView import TipoFechaViewSet
from .views.ProfesionOficioView import ProfesionOficioViewSet
from .views.EntidadView import EntidadViewSet
from .views.CargoView import CargoViewSet
from .views.PeriodoView import PeriodoViewSet
from .views.CargoEntidadView import CargoEntidadViewSet

router = routers.DefaultRouter()
router.register(r'personas', PersonaViewSet)
router.register(r'test', TestView, base_name='test')
router.register(r'ubigeos', UbigeoViewSet)
router.register(r'monedas', MonedaViewSet)
router.register(r'continentes', ContinenteViewSet)
router.register(r'paises', PaisViewSet)
router.register(r'clasificacionesiiu', ClasificacionIiuViewSet)
router.register(r'tiposubigeo', TipoUbigeoViewSet)
router.register(r'tiposvia', TipoViaViewSet)
router.register(r'tiposcontacto', TipoContactoViewSet)
router.register(r'tiposambito', TipoAmbitoViewSet)
router.register(r'tiposdireccion', TipoDireccionViewSet)
router.register(r'tiposentidad', TipoEntidadViewSet)
router.register(r'tiposzona', TipoZonaViewSet)
router.register(r'tiposnumerovia', TipoNumeroViaViewSet)
router.register(r'tipospersona', TipoPersonaViewSet)
router.register(r'tiposcontribuyente', TipoContribuyenteViewSet, base_name='tiposcontribuyente')
router.register(r'tiposdocumentos', TipoDocumentoViewSet)
router.register(r'configdirecciontipospersona', ConfigDireccionTipoPersonaViewSet)
router.register(r'configdocumentotipospersona', ConfigDocumentoTipoPersonaViewSet)
router.register(r'contadores', ContadorViewSet)
router.register(r'cuentascontacto', CuentaContactoViewSet)
router.register(r'etiquetascontacto', EtiquetaContactoViewSet)
router.register(r'meses', MesViewSet)
router.register(r'tiposfecha', TipoFechaViewSet)
router.register(r'profesionesoficio', ProfesionOficioViewSet)
router.register(r'entidades', EntidadViewSet)
router.register(r'cargos', CargoViewSet)
router.register(r'periodos', PeriodoViewSet)
router.register(r'cargosentidad', CargoEntidadViewSet)

urlpatterns = [
	url(r'^choices/(?P<param>[^/]+)/$',
	        ChoicesViewSet.as_view({'get': 'get_choices'}), name='get-enums'),
    url(r'^', include(router.urls)),
]