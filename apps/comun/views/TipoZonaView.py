from rest_framework import serializers, viewsets
from apps.comun.models.TipoZona import TipoZona
from rest_framework import permissions
from apps.comun.serializers.TipoZona import TipoZonaSerializer
from apps.comun.always.Pagination import LimonPagination
from rest_framework.filters import SearchFilter, OrderingFilter
from django.db.models.deletion import ProtectedError

from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.comun.Permissions.TipoZona import TipoZonaPermissions as LimonPermission
from apps.segur.always.Permission import PermLimonList
import json
from apps.comun.always.SearchFilter import search_filter, keys_del

class TipoZonaViewSet(LimonPagination, viewsets.ModelViewSet):
	queryset = TipoZona.objects.all()
	permission_classes = [permissions.IsAuthenticated, PermLimonList, ]
	serializer_class = TipoZonaSerializer
	filter_backends = (SearchFilter,OrderingFilter,)
	search_fields = ('nombre', 'codigo','abreviatura',)
	ordering_fields = '__all__'

	@list_route(url_path='searchform', methods=['get'], permission_classes=[LimonPermission,])
	def get_searchform(self, request, *args, **kwargs):
		queryset = search_filter(self.get_queryset(), request)
		serializer = TipoZonaSerializer(queryset, many=True)
		return Response(serializer.data)