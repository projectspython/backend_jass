from django.conf import settings
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import list_route
from rest_framework import viewsets
from apps.comun.models.TipoContribuyente import TipoContribuyente

class TipoContribuyenteViewSet(viewsets.ViewSet):

	@list_route(url_path='tipopersona', permission_classes=[], methods=['get'])
	def get_listar(self, request, format=None):
		id = request.query_params.get('id', None)
		if id == '02':
			lista = TipoContribuyente.objects.exclude(
				codigo__in=['01','02']).values('id','nombre').order_by('nombre')
		else:
			lista = TipoContribuyente.objects.filter(
				codigo__in=['01','02']).values('id','nombre').order_by('nombre')
		return Response(lista)