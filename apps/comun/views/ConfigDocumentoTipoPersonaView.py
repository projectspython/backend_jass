from rest_framework import serializers, viewsets
from apps.comun.models.ConfigDocumentoTipoPersona import ConfigDocumentoTipoPersona
from rest_framework import permissions
from apps.comun.serializers.ConfigDocumentoTipoPersona import ConfigDocumentoTipoPersonaSerializer
from apps.comun.always.Pagination import LimonPagination
from rest_framework.filters import SearchFilter, OrderingFilter
from django.db.models.deletion import ProtectedError

from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.comun.Permissions.ConfigDocumentoTipoPersona import ConfigDocumentoTipoPersonaPermissions as LimonPermission
from apps.segur.always.Permission import PermLimonList
from apps.comun.always.SearchFilter import search_filter

class ConfigDocumentoTipoPersonaViewSet(LimonPagination, viewsets.ModelViewSet):
	queryset = ConfigDocumentoTipoPersona.objects.all()
	permission_classes = [permissions.IsAuthenticated, PermLimonList, ]
	serializer_class = ConfigDocumentoTipoPersonaSerializer
	filter_backends = (SearchFilter,OrderingFilter,)
	search_fields = ('nombre', 'codigo','simbolo',)
	ordering_fields = '__all__'

	@list_route(url_path='searchform', methods=['get'], permission_classes=[LimonPermission,])
	def get_searchform(self, request, *args, **kwargs):
		queryset = search_filter(self.get_queryset(), request)
		serializer = ConfigDocumentoTipoPersonaSerializer(queryset, many=True)
		return Response(serializer.data)