from rest_framework import viewsets
from rest_framework.response import Response
import unicodedata
from apps.comun.always.Choices import SEXOS, DIAS

class ChoicesViewSet(viewsets.ViewSet):    
    def get_choices(self, request, param=None):
        limit = request.query_params.get('limit', '20')
        query = request.query_params.get('query', None)

        def iterator_list(model):
            lista = []
            for x in model:
                f = dict(id=x[0], nombre=x[1])
                lista.append(f)
            return list(lista)

        def serialize(model):
            lista = []
            count = 0
            for x in model:
                f = dict(id=x[0], nombre=x[1])
                if query is not None:
                    if normalize(query.upper()) in normalize(f['value'].upper()):
                        lista.append(f)
                else:
                    lista.append(f)
                    if count == int(limit):
                        break
                count += 1
            return list(lista[:int(limit)])

        def normalize(s):
            return ''.join((c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn'))

        def iterator_list(model):
            lista = []
            for x in model:
                f = dict(id=x[0], nombre=x[1])
                lista.append(f)
            return list(lista)

        if param == "sexos":
            return Response(serialize(SEXOS))

        elif param == "dias":
            return Response(iterator_list(DIAS))
        else:
            return Response('No hay choices con ' + param)
