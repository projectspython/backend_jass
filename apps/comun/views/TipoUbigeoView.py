from rest_framework import serializers, viewsets
from apps.comun.models.TipoUbigeo import TipoUbigeo
from rest_framework import permissions
from apps.comun.serializers.TipoUbigeo import TipoUbigeoSerializer, TipoUbigeoBasicSerializer
from apps.comun.always.Pagination import LimonPagination
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter, OrderingFilter
from django.db.models.deletion import ProtectedError

from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.comun.Permissions.TipoUbigeo import TipoUbigeoPermissions as LimonPermission
from apps.segur.always.Permission import PermLimonList
from apps.comun.always.SearchFilter import search_filter

class TipoUbigeoViewSet(LimonPagination, viewsets.ModelViewSet):
	queryset = TipoUbigeo.objects.all()
	permission_classes = [permissions.IsAuthenticated, PermLimonList, ]
	serializer_class = TipoUbigeoSerializer
	filter_backends = (SearchFilter,OrderingFilter,)
	search_fields = ('nombre', 'codigo',)
	ordering_fields = '__all__'

	@list_route(url_path='add', methods=['post'], permission_classes=[LimonPermission,])
	def get_add(self, request, *args, **kwargs):
		serializer = self.get_serializer(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

	@list_route(url_path='edit', methods=['get'], permission_classes=[LimonPermission,])
	def get_edit(self, request):
		id = request.query_params.get('id', None)
		model = TipoUbigeo.objects.get(pk=id)
		return Response(self.get_serializer(model).data)


	@list_route(url_path='update', methods=['put'], permission_classes=[LimonPermission,])
	def get_update(self, request, pk=None):
		model = TipoUbigeo.objects.get(pk=request.data['id'])
		serializer = self.get_serializer(model, data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data)
		else:
			return Response(serializer.errors,
							status=status.HTTP_400_BAD_REQUEST)

	@list_route(url_path='delete', methods=['delete'], permission_classes=[LimonPermission,])
	def get_delete(self, request, *args, **kwargs):
		try:
			id = request.query_params.get('id', None)
			model = TipoUbigeo.objects.get(pk=id).delete()
			return Response({'status':True, 'id':id})
		except ProtectedError as e:
			return Response(e.errors, status=status.HTTP_400_BAD_REQUEST)

	@list_route(url_path='searchform', methods=['get'], permission_classes=[LimonPermission,])
	def get_delete(self, request, *args, **kwargs):
		queryset = search_filter(self.get_queryset(), request)
		serializer = TipoUbigeoBasicSerializer(queryset, many=True)
		return Response(serializer.data)