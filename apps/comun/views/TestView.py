from django.conf import settings
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import list_route
from rest_framework import viewsets


class TestView(viewsets.ViewSet):

    @list_route(url_path='listar', permission_classes=[])
    def get_listar(self, request, format=None):
        lista = ["Nuevo","La voz","Voces"]
        return Response(lista)
