from rest_framework import serializers, viewsets
from apps.comun.models.Persona import Persona
from rest_framework import permissions
from apps.comun.serializers.Persona import PersonaSerializer, PersonaBasicSerializer, PersonaEditSerializer, PersonaFotoLogoSerializer
from apps.procesos.serializers.Persona import PersonaEditSerializer as PersonaEditSerialProcesos
from apps.caja.serializers.Persona import PersonaEditSerializer as PersonaEditSerialCaja
from apps.comun.always.Pagination import LimonPagination
from rest_framework.filters import SearchFilter, OrderingFilter
from django.db.models.deletion import ProtectedError

from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.comun.Permissions.Persona import PersonaPermissions as LimonPermission
from apps.segur.always.Permission import PermLimonList
import json
import operator
from apps.comun.always.SearchFilter import search_filter, keys_del, keys_add_none, values_concat
from apps.comun.always.SearchFilter import values_obj_add, is_new_in_array, add_keys_values, extaer_de_column, values_all_none
from apps.comun.models.TipoPersona import TipoPersona
from apps.comun.models.PersonaNatural import PersonaNatural
from apps.comun.models.PersonaDocumento import PersonaDocumento
from apps.comun.models.PersonaFecha import PersonaFecha
from apps.comun.models.PersonaContacto import PersonaContacto
from apps.comun.models.PersonaNaturalOficio import PersonaNaturalOficio
from apps.procesos.always.always import concat_direccion_detalle
from apps.comun.models.PersonaJuridica import PersonaJuridica
from apps.comun.models.PersonaJuridicaActividad import PersonaJuridicaActividad
from apps.comun.always.GenerarCodigo import GenerarCodigo
from apps.comun.models.CuentaContacto import CuentaContacto
from apps.comun.models.EtiquetaContacto import EtiquetaContacto
from apps.comun.models.Ubigeo import Ubigeo
from apps.comun.serializers.PersonaNatural import PersonaNaturalSerializer
from apps.comun.serializers.PersonaJuridica import PersonaJuridicaSerializer
from apps.comun.serializers.TipoPersona import TipoPersonaSerializer
from apps.comun.always.FileUpload import FileUpload
from apps.comun.always.InfoModels import PersonaNaturalInfo
from apps.comun.always.InfoModels import PersonaJuridicaInfo
from apps.procesos.models.Direccion import Direccion
from apps.caja.models.PersonaCuenta import PersonaCuenta
from apps.caja.always.Generales import Generales

class PersonaViewSet(LimonPagination, viewsets.ModelViewSet):
	queryset = Persona.objects.prefetch_related('tipo_persona')
	permission_classes = [permissions.IsAuthenticated, PermLimonList,]
	serializer_class = PersonaSerializer
	filter_backends = (SearchFilter,OrderingFilter,)
	search_fields = ('nombre_completo', 'codigo','persona_persona_documentos__numero',
		'persona_persona_juridica__nombre_comercial',)
	ordering_fields = '__all__'

	@list_route(url_path='add', methods=['post'], permission_classes=[LimonPermission,])
	def get_add(self, request, *args, **kwargs):
		data = request.data
		tipo_per = TipoPersona.objects.get(codigo=data['tipo_persona_id'])
		data['tipo_persona_id'] = tipo_per.id
		if tipo_per.id == 1:
			data['nombre_completo'] = values_concat(data,'nombres,papellido,sapellido')
		else:
			data['nombre_completo'] = data['razon_social']

		data['codigo'] = GenerarCodigo.persona()
		data_per = keys_add_none(data,'codigo,nombre_completo,tipo_contribuyente_id,tipo_persona_id,nacionalidad_id,estado')
		pe = Persona.objects.create(**data_per)
		if tipo_per.id == 1:
			data_pn = keys_add_none(data,'nombres,papellido,sapellido,sexo')
			pn = PersonaNatural.objects.create(id=pe,**data_pn)
			if pn:
				orden = 1
				for p in data['profesionesoficio']:
					for q in values_obj_add(p, 'profesion_oficio_id', 'profesion_oficio.id'):
						p[q['colum']] = q['value']
					data_ofi = keys_add_none(p,'profesion_oficio_id,descripcion')
					data_ofi['persona_natural_id'] = pn.id.id
					data_ofi['orden'] = orden
					ofi = PersonaNaturalOficio.objects.create(**data_ofi)
					orden += 1
		else:
			if data['repres_legal_id']:
				data['nombre_repres_legal'] = None
			data_pj = keys_add_none(data,'razon_social,nombre_comercial,repres_legal_id,nombre_repres_legal,'+
				'nro_resolucion,tipo_ambito_id')
			pj = PersonaJuridica.objects.create(id=pe,**data_pj)
			if pj:
				orden = 1
				existe = []
				for p in data['actividades']:
					if is_new_in_array(p['clasificacion_iiu']['id'], existe):
						data_act = dict(persona_juridica_id=pj.id.id,
							clasificacion_iiu_id=p['clasificacion_iiu']['id'],
							orden=orden)
						act = PersonaJuridicaActividad.objects.create(**data_act)
						orden += 1
						existe.append(p['clasificacion_iiu']['id'])

		orden = 1
		for p in data['documentos']:
			p['persona_id'] = pe.id
			p['orden'] = orden
			data_doc = keys_add_none(p,'persona_id,tipo_documento_id,numero,orden')
			orden +=1
			doc = PersonaDocumento.objects.create(**data_doc)

		tf = keys_add_none(data,'tipo_fecha_id')
		if tf['tipo_fecha_id']:
			data['persona_id'] = pe.id
			data_fec = keys_add_none(data,'persona_id,tipo_fecha_id,anio,mes_id,dia')
			doc = PersonaFecha.objects.create(**data_fec)

		for p in data['contactos']:
			for q in values_obj_add(p, 'pais_id', 'pais.id'):
				p[q['colum']] = q['value']

			p['persona_id'] = pe.id
			data_cont = keys_add_none(p,'persona_id,tipo_contacto_id,cuenta_contacto_id,'+
				'etiqueta_contacto_id,pais_id,ubigeo_id,valor_contacto,anexo,area_oficina')
			cont = PersonaContacto.objects.create(**data_cont)

		for p in data['direcciones']:
			if 'completa' in p and p['completa']:
				p['direccion_detalle'] = concat_direccion_detalle(p)
			for q in values_obj_add(p, 'pais_id,ubigeo_id', 'pais.id,ubigeo.id'):
				p[q['colum']] = q['value']
			p['persona_id'] = pe.id
			data_dir = keys_add_none(p,'persona_id,tipo_direccion_id,pais_id,'+
				'ubigeo_id,completa,direccion_detalle,tipo_via_id,nombre_via,tipo_numero_via_id,'+
				'numero_via,manzana,lote,piso,dpto_interior,tipo_zona_id,nombre_zona,'+
				'referencia1,referencia2,punto_lat,punto_long')
			if data_dir['completa'] is None:
				data_dir['completa'] = 'NO'
			else:
				data_dir['completa'] = 'SI'
			dire = Direccion.objects.create(**data_dir)

		for p in data['cuentas']:
			p['persona_id'] = pe.id
			data_cue = keys_add_none(p,'persona_id,entidad_financiera_id,numero,numero_cci,tipo_cuenta_finan_id')
			try:
				cue = PersonaCuenta.objects.create(**data_cue)
			except:
				pass
			finally:
				pass

		Generales.update_estado_cuenta(data['entidad_id'], pe.id,0, 'deuda')

		return Response(self.get_serializer(pe).data)

	@list_route(url_path='edit', methods=['get'], permission_classes=[LimonPermission,])
	def get_edit(self, request):
		result = {}
		id = request.query_params.get('id', None)
		model = Persona.objects.get(pk=id)
		# data = self.get_serializer(model).data

		pers = {}
		pers['id'] = id
		pers['tipo_persona_id'] = model.tipo_persona.codigo
		pers['tipo_contribuyente_id'] = model.tipo_contribuyente_id
		pers['estado'] = model.estado
		nacionalidad = {}
		if model.nacionalidad_id:
			nacionalidad['id'] = model.nacionalidad_id
			nacionalidad['nombre'] = model.nacionalidad.nombre
			pers['nacionalidad'] = nacionalidad

		if pers['tipo_persona_id'] == '01':
			# nat = PersonaNatural.objects.filter(id=id).values('nombres','papellido','sapellido','sexo').first()
			nat = PersonaNaturalSerializer(PersonaNatural.objects.get(id=id)).data
			pers = add_keys_values(pers, nat)

			result['profesionesoficio'] = nat['persona_natural_oficios']

		if pers['tipo_persona_id'] == '02':
			jur = PersonaJuridicaSerializer(PersonaJuridica.objects.get(id=id)).data
			if jur['repres_legal_id']:
				jur['tiene_res_legal'] = True
			pers = add_keys_values(pers, jur)

			result['actividades'] = jur['persona_juridica_actividades']

		perfe = PersonaFecha.objects.filter(persona_id=id).values(
			'anio','dia','mes_id','tipo_fecha_id','id').first()
		if perfe:
			pers['persona_tipo_fecha_id'] = perfe['id']
			del perfe['id']
			pers = add_keys_values(pers, perfe)

		result['data_person'] = pers

		data_edit = PersonaEditSerializer(model).data
		documentos = []
		for p in data_edit['persona_persona_documentos']:
			p['tipo_documento_id'] = p['tipo_documento']['id']
			p['patron_angular'] = p['tipo_documento']['patron_angular']
			p['codigo'] = p['tipo_documento']['codigo']
			del p['tipo_documento']
			documentos.append(p)
		result['documentos'] = documentos

		direcciones = []
		data_dir = PersonaEditSerialProcesos(model).data
		for p in data_dir['persona_direcciones']:
			if p['completa'] == 'SI':
				p['completa'] = True
			else:
				p['completa'] = False
			direcciones.append(p)

		result['direcciones'] =direcciones

		result['cuentas'] = PersonaEditSerialCaja(model).data['persona_persona_cuentas']

		contactos = []
		for p in data_edit['persona_persona_contactos']:
			p['cuentascontacto'] = CuentaContacto.objects.filter(
				tipo_contacto_id=p['tipo_contacto_id'],
				estado='1',
				).values('id','codigo','nombre','url_web').order_by('orden')

			p['etiquetascontacto'] = EtiquetaContacto.objects.filter(
				tipo_contacto_id=p['tipo_contacto_id'],
				estado='1',
				).values('id','codigo','nombre','tiene_anexo','tiene_cod_ubigeo').order_by('orden')
			#
			p['tipo_contacto_codigo'] = p['tipo_contacto']['codigo']
			p['denom_valor'] = p['tipo_contacto']['denom_valor']
			# tiene_cod_ubigeo
			if p['etiqueta_contacto']:
				p['tiene_cod_ubigeo'] = p['etiqueta_contacto']['tiene_cod_ubigeo']
				p['tiene_anexo'] = p['etiqueta_contacto']['tiene_anexo']

				if p['tiene_cod_ubigeo'] == 'SI' and p['pais']:
					p['ubigeos'] = Ubigeo.objects.filter(
						pais_id=p['pais']['id'],
						tipo_ubigeo__codigo='DEPA'
						).values('id','nombre','codigo','codigo_telefono').order_by('codigo')

			if p['ubigeo']:
				p['codigo_telefono'] = p['ubigeo']['codigo_telefono']

			contactos.append(p)

		result['contactos'] = contactos

		return Response(result)


	@list_route(url_path='update', methods=['put'], permission_classes=[LimonPermission,])
	def get_update(self, request, pk=None):
		# data = request.data
		# data = keys_del(data, 'tipo_Persona,pais,padre,region_natural')
		# p = Persona.objects.filter(pk=data['id']).update(**data)
		data = request.data
		tipo_per = TipoPersona.objects.get(codigo=data['tipo_persona_id'])
		data['tipo_persona_id'] = tipo_per.id
		if tipo_per.id == 1:
			data['nombre_completo'] = values_concat(data,'nombres,papellido,sapellido')
		else:
			data['nombre_completo'] = data['razon_social']

		data_per = keys_add_none(data,'nombre_completo,tipo_contribuyente_id,tipo_persona_id,nacionalidad_id,estado')
		pe = Persona.objects.filter(pk=data['id']).update(**data_per)

		if tipo_per.id == 1:
			data_pn = keys_add_none(data,'nombres,papellido,sapellido,sexo')
			pn = PersonaNatural.objects.filter(pk=data['id']).update(**data_pn)
			del_profoficio = extaer_de_column(data['del_profesionesoficio'],'id')
			delo = PersonaNaturalOficio.objects.filter(id__in=del_profoficio).delete()
			if pn:
				orden = 1
				for p in data['profesionesoficio']:
					for q in values_obj_add(p, 'profesion_oficio_id', 'profesion_oficio.id'):
						p[q['colum']] = q['value']
					data_ofi = keys_add_none(p,'profesion_oficio_id,descripcion')
					data_ofi['persona_natural_id'] = data['id']
					data_ofi['orden'] = orden
					if 'id' in p:
						ofi = PersonaNaturalOficio.objects.filter(id=p['id']).update(**data_ofi)
					else:
						ofi = PersonaNaturalOficio.objects.create(**data_ofi)
					orden += 1
		else:
			if data['repres_legal_id']:
				data['nombre_repres_legal'] = None

			data_pj = keys_add_none(data,'razon_social,nombre_comercial,repres_legal_id,nombre_repres_legal,'+
				'nro_resolucion,tipo_ambito_id')
			pj = PersonaJuridica.objects.filter(pk=data['id']).update(**data_pj)
			orden = 1
			existe = []
			dela = PersonaJuridicaActividad.objects.filter(id__in=extaer_de_column(data['del_actividades'],'id')).delete()
			for p in data['actividades']:
				if is_new_in_array(p['clasificacion_iiu']['id'], existe):
					data_act = dict(persona_juridica_id=data['id'],
						clasificacion_iiu_id=p['clasificacion_iiu']['id'],
						orden=orden)
					if 'id' in p:
						act = PersonaJuridicaActividad.objects.filter(pk=p['id']).update(**data_act)
					else:
						act = PersonaJuridicaActividad.objects.create(**data_act)
					orden += 1
					existe.append(p['clasificacion_iiu']['id'])

		orden = 1
		deldo = PersonaDocumento.objects.filter(id__in=extaer_de_column(data['del_documentos'],'id')).delete()
		for p in data['documentos']:
			p['persona_id'] = data['id']
			p['orden'] = orden
			data_doc = keys_add_none(p,'persona_id,tipo_documento_id,numero,orden')
			orden +=1
			if 'id' in p:
				ofi = PersonaDocumento.objects.filter(id=p['id']).update(**data_doc)
			else:
				doc = PersonaDocumento.objects.create(**data_doc)

		deldi = Direccion.objects.filter(id__in=extaer_de_column(data['del_direcciones'],'id')).delete()
		for p in data['direcciones']:
			if 'completa' in p and p['completa']:
				p['direccion_detalle'] = concat_direccion_detalle(p)
			else:
				p = values_all_none(p, 'tipo_via_id,nombre_via,tipo_numero_via_id,'+
						'numero_via,manzana,lote,piso,dpto_interior,tipo_zona_id,nombre_zona')

			for q in values_obj_add(p, 'pais_id,ubigeo_id', 'pais.id,ubigeo.id'):
				p[q['colum']] = q['value']
			p['persona_id'] = data['id']
			data_dir = keys_add_none(p,'persona_id,tipo_direccion_id,pais_id,'+
				'ubigeo_id,completa,direccion_detalle,tipo_via_id,nombre_via,tipo_numero_via_id,'+
				'numero_via,manzana,lote,piso,dpto_interior,tipo_zona_id,nombre_zona,'+
				'referencia1,referencia2,punto_lat,punto_long')
			if data_dir['completa'] is None:
				data_dir['completa'] = 'NO'
			else:
				data_dir['completa'] = 'SI'

			if 'id' in p:
				dire = Direccion.objects.filter(id=p['id']).update(**data_dir)
			else:
				dire = Direccion.objects.create(**data_dir)

		delc = PersonaContacto.objects.filter(id__in=extaer_de_column(data['del_contactos'],'id')).delete()
		for p in data['contactos']:
			for q in values_obj_add(p, 'pais_id', 'pais.id'):
				p[q['colum']] = q['value']

			p['persona_id'] = data['id']
			data_cont = keys_add_none(p,'persona_id,tipo_contacto_id,cuenta_contacto_id,'+
				'etiqueta_contacto_id,pais_id,ubigeo_id,valor_contacto,anexo,area_oficina')
			if 'id' in p:
				cont = PersonaContacto.objects.filter(id=p['id']).update(**data_cont)
			else:
				cont = PersonaContacto.objects.create(**data_cont)

		delcu = PersonaCuenta.objects.filter(id__in=extaer_de_column(data['del_cuentas'],'id')).delete()
		for p in data['cuentas']:
			p['persona_id'] = data['id']
			data_cue = keys_add_none(p,'persona_id,entidad_financiera_id,numero,numero_cci,tipo_cuenta_finan_id')
			try:
				if 'id' in p:
					cue = PersonaCuenta.objects.filter(id=p['id']).update(**data_cue)
				else:
					cue = PersonaCuenta.objects.create(**data_cue)
			except:
				pass
			finally:
				pass


		model = Persona.objects.get(id=data['id'])
		Generales.update_estado_cuenta(data['entidad_id'], data['id'], 0, 'deuda')
		return Response(self.get_serializer(model).data)

	@list_route(url_path='delete', methods=['delete'], permission_classes=[LimonPermission,])
	def get_delete(self, request, *args, **kwargs):
		# try:
		id = request.query_params.get('id', None)
		mode = Persona.objects.filter(pk=id).values('foto_logo','foto_logo_rec').first()
		# perdoc = PersonaDocumento.objects.filter(persona_id=id).delete()
		model = Persona.objects.get(pk=id).delete()
		if model and mode['foto_logo']:
			del1 = FileUpload.delete_file(mode['foto_logo'])
			del2 = FileUpload.delete_file(mode['foto_logo_rec'])

		return Response({'status':True, 'id':id})
		# except ProtectedError as e:
		# 	return Response(e.errors, status=status.HTTP_400_BAD_REQUEST)

	@list_route(url_path='searchform', methods=['get'], permission_classes=[LimonPermission,])
	def get_searchform(self, request, *args, **kwargs):
		queryset = search_filter(self.get_queryset().distinct(), request)
		serializer = PersonaBasicSerializer(queryset, many=True)
		return Response(serializer.data)

	@list_route(url_path='verificarnro', methods=['get'], permission_classes=[LimonPermission,])
	def get_verificarnro(self, request, *args, **kwargs):
		persona_id = request.query_params.get('persona_id', None)
		numero = request.query_params.get('numero', None)
		tipo_documento_id = request.query_params.get('tipo_documento_id', None)
		if numero and tipo_documento_id:
			listado = Persona.objects.filter(
				persona_persona_documentos__numero=numero,
				persona_persona_documentos__tipo_documento_id=tipo_documento_id
				)
			if persona_id:
				listado = listado.exclude(id=persona_id)
			listado = listado.values('id').distinct()

			return Response({'existe':len(listado)>0})

		else:
			return Response({'existe':False})

	@list_route(url_path='savefoto', methods=['post'], permission_classes=[LimonPermission,])
	def get_savefoto(self, request, *args, **kwargs):
		files = request.FILES
		id = request.data['persona_id']
		if files:
			# recotar = FileUpload.recotar(request.FILES['logo_new'])
			mode = Persona.objects.filter(pk=id).values('foto_logo','foto_logo_rec').first()

			data_per = {'foto_logo':FileUpload.comun_persona('foto_logo',request.FILES['logo_new'])
				, 'foto_logo_rec':FileUpload.comun_persona('foto_logo_rec',request.FILES['logo_new'])}
			pe = Persona.objects.filter(pk=id).update(**data_per)
			if pe and mode['foto_logo']:
				del1 = FileUpload.delete_file(mode['foto_logo'])
				del2 = FileUpload.delete_file(mode['foto_logo_rec'])

			model = Persona.objects.get(pk=id)
			return Response(PersonaFotoLogoSerializer(model).data)

	@list_route(url_path='deletefoto', methods=['post'], permission_classes=[LimonPermission,])
	def get_deletefoto(self, request, *args, **kwargs):
		id = request.data['persona_id']
		model = Persona.objects.filter(pk=id).values('foto_logo','foto_logo_rec').first()
		del1 = FileUpload.delete_file(model['foto_logo'])
		del2 = FileUpload.delete_file(model['foto_logo_rec'])

		data_per = {'foto_logo':None,'foto_logo_rec':None}
		pe = Persona.objects.filter(pk=id).update(**data_per)
		model = Persona.objects.get(pk=id)
		return Response(PersonaFotoLogoSerializer(model).data)

	@list_route(url_path='viewinfo', methods=['get'], permission_classes=[LimonPermission])
	def get_viewinfo(self, request):
		result = {}
		id = request.query_params.get('id', None)
		model = Persona.objects.get(pk=id) # model = Persona.objects.filter(pk=id).values()
		pers = {}
		pers['id'] = id
		pers['tipo_persona_id'] = model.tipo_persona.codigo
		if pers['tipo_persona_id'] == '01':
			data = PersonaNaturalInfo(request, id)

		if pers['tipo_persona_id'] == '02':
			data = PersonaJuridicaInfo(request, id)

		result = data
		return Response(result)

	@list_route(url_path='searchformafiliacion', methods=['get'], permission_classes=[LimonPermission,])
	def get_searchformafiliacion(self, request, *args, **kwargs):
		queryset = self.get_queryset().exclude(persona_afiliaciones__persona_id__isnull=False)
		queryset = search_filter(queryset, request)
		serializer = PersonaSerializer(queryset, many=True)
		return Response(serializer.data)

	@list_route(url_path='searchformrepre', methods=['get'], permission_classes=[LimonPermission,])
	def get_searchformrepre(self, request, *args, **kwargs):
		queryset = self.get_queryset().exclude(persona_representantes__persona_id__isnull=False).exclude(persona_afiliaciones__persona_id__isnull=False)
		queryset = search_filter(queryset, request)
		serializer = PersonaSerializer(queryset, many=True)
		return Response(serializer.data)

