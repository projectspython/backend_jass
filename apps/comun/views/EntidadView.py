from rest_framework import serializers, viewsets
from apps.comun.models.Entidad import Entidad
from rest_framework import permissions
from apps.comun.serializers.Entidad import EntidadSerializer
from apps.comun.always.Pagination import LimonPagination
from rest_framework.filters import SearchFilter, OrderingFilter
from django.db.models.deletion import ProtectedError

from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.comun.Permissions.Entidad import EntidadPermissions as LimonPermission
from apps.segur.always.Permission import PermLimonList
from apps.comun.always.SearchFilter import search_filter, keys_add_none, values_obj_add

class EntidadViewSet(LimonPagination, viewsets.ModelViewSet):
	queryset = Entidad.objects.all()
	permission_classes = [permissions.IsAuthenticated, PermLimonList, ]
	serializer_class = EntidadSerializer
	filter_backends = (SearchFilter,OrderingFilter,)
	search_fields = ('codigo', 'moneda__nombre', 'tipo_entidad__nombre', 'persona__nombre_completo')
	ordering_fields = '__all__'

	@list_route(url_path='add', methods=['post'], permission_classes=[LimonPermission,])
	def get_add(self, request, *args, **kwargs):
		data = request.data
		for q in values_obj_add(data, 'moneda_id,padre_id,persona_id,tipo_entidad_id', 'moneda.id,padre.id,persona.id,tipo_entidad.id'):
			data[q['colum']] = q['value']
		data_entidad = keys_add_none(data,'codigo,moneda_id,padre_id,persona_id,tipo_entidad_id,estado')
		e = Entidad.objects.create(**data_entidad)
		return Response(self.get_serializer(e).data)

	@list_route(url_path='edit', methods=['get'], permission_classes=[LimonPermission,])
	def get_edit(self, request):
		id = request.query_params.get('id', None)
		model = Entidad.objects.get(pk=id)
		data = self.get_serializer(model).data
		return Response(data)

	@list_route(url_path='update', methods=['put'], permission_classes=[LimonPermission,])
	def get_update(self, request, pk=None):
		data = request.data
		for q in values_obj_add(data, 'moneda_id,padre_id,persona_id,tipo_entidad_id', 'moneda.id,padre.id,persona.id,tipo_entidad.id'):
			data[q['colum']] = q['value']
		data_entidad = keys_add_none(data,'codigo,moneda_id,padre_id,persona_id,tipo_entidad_id,estado')
		e = Entidad.objects.filter(id=request.data['id']).update(**data_entidad)
		model = Entidad.objects.get(pk=request.data['id'])
		return Response(self.get_serializer(model).data)

	@list_route(url_path='delete', methods=['delete'], permission_classes=[LimonPermission,])
	def get_delete(self, request, *args, **kwargs):
		try:
			id = request.query_params.get('id', None)
			model = Entidad.objects.get(pk=id).delete()
			return Response({'status':True, 'id':id})
		except ProtectedError as e:
			return Response(e.errors, status=status.HTTP_400_BAD_REQUEST)

	@list_route(url_path='searchform', methods=['get'], permission_classes=[LimonPermission,])
	def get_searchform(self, request, *args, **kwargs):
		queryset = search_filter(self.get_queryset(), request)
		serializer = EntidadSerializer(queryset, many=True)
		return Response(serializer.data)


	@list_route(url_path='datosbasic', methods=['get'], permission_classes=[])
	def get_datosbasic(self, request, *args, **kwargs):
		id = request.query_params.get('id', None)
		data = Entidad.objects.filter(pk=id).values(
			'id','moneda_id','padre_id','moneda__nombre_plural','moneda__simbolo').first()
		return Response(data)

