from rest_framework import serializers, viewsets
from apps.comun.models.TipoContacto import TipoContacto
from rest_framework import permissions
from apps.comun.serializers.TipoContacto import TipoContactoSerializer
from apps.comun.always.Pagination import LimonPagination
from rest_framework.filters import SearchFilter, OrderingFilter
from django.db.models.deletion import ProtectedError

from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.comun.Permissions.TipoContacto import TipoContactoPermissions as LimonPermission
from apps.segur.always.Permission import PermLimonList
import json
from apps.comun.always.SearchFilter import search_filter, keys_del

class TipoContactoViewSet(LimonPagination, viewsets.ModelViewSet):
	queryset = TipoContacto.objects.all()
	permission_classes = [permissions.IsAuthenticated, PermLimonList, ]
	serializer_class = TipoContactoSerializer
	filter_backends = (SearchFilter,OrderingFilter,)
	search_fields = ('codigo', 'nombre','nombre_plural',)
	ordering_fields = '__all__'

	@list_route(url_path='searchform', methods=['get'], permission_classes=[LimonPermission,])
	def get_searchform(self, request, *args, **kwargs):
		queryset = search_filter(self.get_queryset(), request)
		serializer = TipoContactoSerializer(queryset, many=True)
		return Response(serializer.data)