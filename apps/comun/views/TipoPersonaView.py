from rest_framework import serializers, viewsets
from apps.comun.models.TipoPersona import TipoPersona
from rest_framework import permissions
from apps.comun.serializers.TipoPersona import TipoPersonaSerializer
from apps.comun.always.Pagination import LimonPagination
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter, OrderingFilter
from django.db.models.deletion import ProtectedError

from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.segur.always.Permission import PermLimonList
from apps.comun.always.SearchFilter import search_filter

class TipoPersonaViewSet(LimonPagination, viewsets.ModelViewSet):
	queryset = TipoPersona.objects.all()
	permission_classes = [permissions.IsAuthenticated, PermLimonList, ]
	serializer_class = TipoPersonaSerializer
	filter_backends = (SearchFilter,OrderingFilter,)
	search_fields = ('nombre', 'codigo',)
	ordering_fields = '__all__'

	@list_route(url_path='searchform', methods=['get'], permission_classes=[])
	def get_searchform(self, request, *args, **kwargs):
		queryset = search_filter(self.get_queryset(), request)
		serializer = TipoPersonaSerializer(queryset, many=True)
		return Response(serializer.data)
		# return Response({})


	# def get_queryset(self):
	# 	return self.queryset