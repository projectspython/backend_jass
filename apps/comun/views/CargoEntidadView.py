from rest_framework import serializers, viewsets
from apps.comun.models.CargoEntidad import CargoEntidad
from rest_framework import permissions
from apps.comun.serializers.CargoEntidad import CargoEntidadSerializer
from apps.comun.always.Pagination import LimonPagination
from rest_framework.filters import SearchFilter, OrderingFilter
from django.db.models.deletion import ProtectedError

from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.comun.Permissions.CargoEntidad import CargoEntidadPermissions as LimonPermission
from apps.segur.always.Permission import PermLimonList
from apps.comun.always.SearchFilter import search_filter, keys_add_none, values_obj_add

class CargoEntidadViewSet(LimonPagination, viewsets.ModelViewSet):
	queryset = CargoEntidad.objects.all()
	permission_classes = [permissions.IsAuthenticated, PermLimonList, ]
	serializer_class = CargoEntidadSerializer
	filter_backends = (SearchFilter,OrderingFilter,)
	search_fields = ('periodo__nombre', 'entidad__nombre', 'persona__nombre_completo')
	ordering_fields = '__all__'

	@list_route(url_path='add', methods=['post'], permission_classes=[LimonPermission,])
	def get_add(self, request, *args, **kwargs):
		data = request.data
		print(data)
		for q in values_obj_add(data, 'periodo_id,entidad_id,persona_id,cargo_id', 'periodo.id,entidad.id,persona.id,cargo.id'):
			data[q['colum']] = q['value']
		cargo_entidad = keys_add_none(data,'periodo_id,entidad_id,persona_id,cargo_id,estado')
		print(cargo_entidad)
		e = CargoEntidad.objects.create(**cargo_entidad)
		return Response(self.get_serializer(e).data)

	@list_route(url_path='edit', methods=['get'], permission_classes=[LimonPermission,])
	def get_edit(self, request):
		id = request.query_params.get('id', None)
		model = CargoEntidad.objects.get(pk=id)
		data = self.get_serializer(model).data
		return Response(data)

	@list_route(url_path='update', methods=['put'], permission_classes=[LimonPermission,])
	def get_update(self, request, pk=None):
		data = request.data
		for q in values_obj_add(data, 'periodo_id,entidad_id,persona_id,cargo_id', 'periodo.id,entidad.id,persona.id,cargo.id'):
			data[q['colum']] = q['value']
		cargo_entidad = keys_add_none(data,'periodo_id,entidad_id,persona_id,cargo_id,estado')
		e = CargoEntidad.objects.filter(id=request.data['id']).update(**cargo_entidad)
		model = CargoEntidad.objects.get(pk=request.data['id'])
		return Response(self.get_serializer(model).data)

	@list_route(url_path='delete', methods=['delete'], permission_classes=[LimonPermission,])
	def get_delete(self, request, *args, **kwargs):
		try:
			id = request.query_params.get('id', None)
			model = CargoEntidad.objects.get(pk=id).delete()
			return Response({'status':True, 'id':id})
		except ProtectedError as e:
			return Response(e.errors, status=status.HTTP_400_BAD_REQUEST)

	@list_route(url_path='searchform', methods=['get'], permission_classes=[LimonPermission,])
	def get_searchform(self, request, *args, **kwargs):
		queryset = search_filter(self.get_queryset(), request)
		serializer = CargoEntidadSerializer(queryset, many=True)
		return Response(serializer.data)
