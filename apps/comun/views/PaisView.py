from rest_framework import serializers, viewsets
from apps.comun.models.Pais import Pais
from rest_framework import permissions
from apps.comun.serializers.Pais import PaisSerializer, PaisBasicSerializer
from apps.comun.always.Pagination import LimonPagination
from rest_framework.filters import SearchFilter, OrderingFilter
from django.db.models.deletion import ProtectedError

from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.comun.Permissions.Pais import PaisPermissions as LimonPermission
from apps.segur.always.Permission import PermLimonList
import json
from apps.comun.always.SearchFilter import search_filter, keys_del


class PaisViewSet(LimonPagination, viewsets.ModelViewSet):
	queryset = Pais.objects.all()
	permission_classes = [permissions.IsAuthenticated, PermLimonList, ]
	serializer_class = PaisSerializer
	filter_backends = (SearchFilter,OrderingFilter,)
	search_fields = ('nombre', 'codigo','abreviatura', 'continente__nombre',)
	ordering_fields = '__all__'

	@list_route(url_path='add', methods=['post'], permission_classes=[LimonPermission,])
	def get_add(self, request, *args, **kwargs):
		data = request.data
		data = keys_del(data, 'continente')
		p = Pais.objects.create(**data)
		p.save()
		return Response(self.get_serializer(p).data)

	@list_route(url_path='edit', methods=['get'], permission_classes=[LimonPermission,])
	def get_edit(self, request):
		id = request.query_params.get('id', None)
		model = Pais.objects.get(pk=id)
		return Response(self.get_serializer(model).data)


	@list_route(url_path='update', methods=['put'], permission_classes=[LimonPermission,])
	def get_update(self, request, pk=None):
		data = request.data
		data = keys_del(data, 'continente')
		p = Pais.objects.filter(pk=data['id']).update(**data)
		model = Pais.objects.get(id=data['id'])
		return Response(self.get_serializer(model).data)

	@list_route(url_path='delete', methods=['delete'], permission_classes=[LimonPermission,])
	def get_delete(self, request, *args, **kwargs):
		try:
			id = request.query_params.get('id', None)
			model = Pais.objects.get(pk=id).delete()
			return Response({'status':True, 'id':id})
		except ProtectedError as e:
			return Response(e.errors, status=status.HTTP_400_BAD_REQUEST)

	@list_route(url_path='searchform', methods=['get'], permission_classes=[LimonPermission,])
	def get_searchform(self, request, *args, **kwargs):
		queryset = search_filter(self.get_queryset().distinct(), request)
		serializer = PaisBasicSerializer(queryset, many=True)
		return Response(serializer.data)
		# return Response({})


	# def get_queryset(self):
	# 	return self.queryset