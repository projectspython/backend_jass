from rest_framework import serializers, viewsets
from apps.comun.models.ConfigDireccionTipoPersona import ConfigDireccionTipoPersona
from rest_framework import permissions
from apps.comun.serializers.ConfigDireccionTipoPersona import ConfigDireccionTipoPersonaSerializer
from apps.comun.always.Pagination import LimonPagination
from rest_framework.filters import SearchFilter, OrderingFilter
from django.db.models.deletion import ProtectedError

from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.comun.Permissions.ConfigDireccionTipoPersona import ConfigDireccionTipoPersonaPermissions as LimonPermission
from apps.segur.always.Permission import PermLimonList
from apps.comun.always.SearchFilter import search_filter

class ConfigDireccionTipoPersonaViewSet(LimonPagination, viewsets.ModelViewSet):
	queryset = ConfigDireccionTipoPersona.objects.all()
	permission_classes = [permissions.IsAuthenticated, PermLimonList, ]
	serializer_class = ConfigDireccionTipoPersonaSerializer
	filter_backends = (SearchFilter,OrderingFilter,)
	search_fields = ('nombre', 'codigo','simbolo',)
	ordering_fields = '__all__'

	@list_route(url_path='searchform', methods=['get'], permission_classes=[LimonPermission,])
	def get_searchform(self, request, *args, **kwargs):
		queryset = search_filter(self.get_queryset(), request)
		serializer = ConfigDireccionTipoPersonaSerializer(queryset, many=True)
		return Response(serializer.data)