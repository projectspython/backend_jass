from django.contrib import admin

from .models.Persona import Persona

class PersonaAdmin(admin.ModelAdmin):
    list_display = ["codigo","nombre_completo"]
    list_display_links = ["codigo","nombre_completo"]
    list_per_page = 10
    ordering = ["nombre_completo"]
    search_fields = ["codigo","nombre_completo"]

admin.site.register(Persona, PersonaAdmin)