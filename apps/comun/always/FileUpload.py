
from django.conf import settings
import os
from apps.comun.models import Persona
from apps.segur.models import Modulo
from uuid import uuid4
from PIL import Image

folder_storage="storage"

def get_or_create_folder(ruta):
	# ruta = '/storage/HOLA/PERSY/'
	ruta = settings.BASE_DIR+"/"+ruta
	if not os.path.exists(ruta):
		os.makedirs(ruta)
	return ruta

def save_file(ruta, nombre_file, up_file):
	destination = open(ruta + nombre_file, 'wb+')
	for chunk in up_file.chunks():
		destination.write(chunk)
		destination.close()
		return nombre_file

def recortar_img(up_file, size_x, size_y):
	image_obj = Image.open(up_file)
	rec_image = image_obj.resize((size_x, size_y), Image.ANTIALIAS)
	return rec_image

def save_recortado(rec_image, ruta, nombre_file):
	destination = open(ruta + nombre_file, 'wb+')
	rec_image.save(destination)
	return nombre_file

class FileUpload():

	def comun_persona(column, up_file):
		name_uui = str(uuid4()).replace('-','')
		ruta_per = Persona.dir_storage+"/"
		ruta = get_or_create_folder(folder_storage+"/"+ruta_per)
		nombre_file = name_uui+os.path.splitext(up_file.name)[1]
		if column == 'foto_logo':
			return ruta_per+save_file(ruta, nombre_file, up_file)

		if column == 'foto_logo_rec':
			rec_image = recortar_img(up_file, 150, 150)
			return ruta_per+save_recortado(rec_image, ruta, nombre_file)

	def delete_file(ruta_file):
		ruta = settings.BASE_DIR+"/"+folder_storage+"/"+ruta_file
		if os.path.exists(ruta):
			os.remove(ruta)
			return ""

	def segur_modulo(column, up_file):
		name_uui = str(uuid4()).replace('-','')
		ruta_per = Modulo.dir_storage+"/"
		ruta = get_or_create_folder(folder_storage+"/"+ruta_per)
		nombre_file = name_uui+os.path.splitext(up_file.name)[1]
		if column == 'logo':
			return ruta_per+save_file(ruta, nombre_file, up_file)

		if column == 'logo_rec':
			rec_image = recortar_img(up_file, 140, 140)
			return ruta_per+save_recortado(rec_image, ruta, nombre_file)

class Conversiones():
	"""docstring for Conversiones"""
	def to_base64(url_file):
		import base64
		import requests
		url = url = settings.BASE_DIR+'/'+folder_storage+'/'+url_file

		split_url = url_file.split('.')

		with open(url, "rb") as img:
		    thumb_string = base64.b64encode(img.read())

		if len(split_url)>0 and split_url[len(split_url)-1] == 'png':
			base64out = "data:image/png;base64," + str(thumb_string)[2:-1]
		# return(base64out)

		if len(split_url)>0 and split_url[len(split_url)-1] == 'jpg':
			base64out = "data:image/jpg;base64," + str(thumb_string)[2:-1]
		

		return base64out
		
		

		