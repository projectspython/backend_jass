
from apps.comun.models.Persona import Persona
from apps.segur.always.HostValues import HostValues
from apps.comun.serializers.PersonaNatural import PersonaNaturalInfoSerializer
from apps.comun.models.PersonaNatural import PersonaNatural
from apps.comun.always.SearchFilter import add_keys_values, replace_field

from apps.comun.models.PersonaJuridica import PersonaJuridica
from apps.comun.serializers.PersonaJuridica import PersonaJuridicaInfoSerializer

def PersonaNaturalInfo(request, per_id):
	model = Persona.objects.get(pk=per_id)
	dominio = HostValues.dominio(request)
	data = PersonaNaturalInfoSerializer(PersonaNatural.objects.get(pk=per_id)).data
	data = add_keys_values(data, data['id'])
	data = replace_field(data, 'persona_natural_oficios','oficios')
	data = replace_field(data, 'persona_persona_documentos','documentos')
	data = replace_field(data, 'persona_persona_contactos','contactos')
	data = replace_field(data, 'persona_direcciones','direcciones')
	if data['foto_logo_rec']:
		data['foto_logo_rec'] = dominio+data['foto_logo_rec'];
		data['foto_logo'] = dominio+data['foto_logo'];

	return data

def PersonaJuridicaInfo(request, per_id):
	model = Persona.objects.get(pk=per_id)
	dominio = HostValues.dominio(request)
	data = PersonaJuridicaInfoSerializer(PersonaJuridica.objects.get(pk=per_id)).data
	data = add_keys_values(data, data['id'])
	data = replace_field(data, 'persona_juridica_actividades','actividades')
	data = replace_field(data, 'persona_persona_documentos','documentos')
	data = replace_field(data, 'persona_persona_contactos','contactos')
	data = replace_field(data, 'persona_direcciones','direcciones')
	if data['foto_logo_rec']:
		data['foto_logo_rec'] = dominio+data['foto_logo_rec'];
		data['foto_logo'] = dominio+data['foto_logo'];

	return data