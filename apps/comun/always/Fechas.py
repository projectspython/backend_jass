
from datetime import datetime
from apps.comun.models.Mes import Mes

class Fechas():
	"""docstring for ClassName"""
	def dia_mes_anio(fecha):
		
		_fecha = datetime.strptime(fecha, '%Y-%m-%d')
		anio = datetime.strftime(_fecha, '%Y')
		mes = datetime.strftime(_fecha, '%m')
		dia = datetime.strftime(_fecha, '%d')

		mes_model = Mes.objects.get(pk=mes)
		inicial_mes = mes_model.abreviatura

		return '%s - %s - %s'%(dia,inicial_mes.upper(),anio)

	def dia_mes_anio_let(fecha):
		
		nombre_mes = ''
		dia = ''
		anio = ''
		if fecha:
			_fecha = datetime.strptime(fecha, '%Y-%m-%d')
			anio = datetime.strftime(_fecha, '%Y')
			mes = datetime.strftime(_fecha, '%m')
			dia = datetime.strftime(_fecha, '%d')

			mes_model = Mes.objects.get(pk=mes)
			nombre_mes = mes_model.nombre

		return dict(dia=dia,nombre_mes=nombre_mes,anio=anio)


		# return '%s - %s - %s'%(dia,inicial_mes.upper(),anio)
		