
def cargos_entidad(entidad_id, codigos_cargo, fecha):
	from apps.comun.models.CargoEntidad import CargoEntidad

	listado = CargoEntidad.objects.filter(
			periodo__fecha_inicio__lt=fecha,
			periodo__fecha_fin__gte=fecha,
			entidad_id=entidad_id,
			cargo__codigo__in=codigos_cargo
		).values('cargo_id','persona_id').order_by('cargo_id')

	return listado