
from django.utils.text import capfirst
from django.utils.translation import ugettext_lazy as _

SEXOS = (
    ('MAS', capfirst(_('Masculino'))),
    ('FEM', capfirst(_('Femenino'))),
)

DIAS = []
for r in range(1, 32):
    DIAS.append(('%02d' % r,'%02d' % r))

ESTADO_PERIODOS = (
    ('0', capfirst(_('Desactivo'))),
    ('1', capfirst(_('Activo'))),
    ('2', capfirst(_('Anterior'))),
)
# import datetime
# default=datetime.datetime.now().year