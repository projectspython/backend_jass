
from apps.comun.models.Contador import Contador

def correlativo(codigo):
	model = Contador.objects.get(codigo=codigo)
	contador = model.contador + 1
	model.contador = contador
	model.save()
	return contador


class GenerarCodigo():
	"""docstring for ClassName"""
	def persona():
		codigo = 'CDPERS'
		contador = correlativo(codigo)
		return '%06d' % contador

	def zonas():
		codigo = 'ZONAS'
		contador = correlativo(codigo)
		return '%04d' % contador

	def motivosmovimientocuenta():
		codigo = 'MOTMOV'
		contador = correlativo(codigo)
		return '%05d' % contador
		
	def afiliaciones():
		codigo = 'AFI'
		contador = correlativo(codigo)
		return '%06d' % contador


	def codigo_barras_base64(numero):
		from apps.comun.always.FileUpload import FileUpload
		import barcode
		from barcode.writer import ImageWriter

		ean = barcode.get('code128', u'%s'%(numero), writer=ImageWriter())

		options = dict(compress=True, foreground='black',font_size=0, module_width=0.3,
			quiet_zone=3, module_height=24)

		filename = ean.save('ean13', options)

		import base64
		with open(filename, "rb") as imageFile:
		    image = base64.b64encode(imageFile.read())
		    return "data:image/png;base64," + str(image)[2:-1]
		    return "data:image/png;base64," + str(image)[2:-1]


	def predio():
		codigo = 'PREDIO'
		contador = correlativo(codigo)
		return '%06d' % contador
