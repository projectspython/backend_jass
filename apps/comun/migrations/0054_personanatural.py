# Generated by Django 2.0.7 on 2018-07-11 01:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('comun', '0053_entidad'),
    ]

    operations = [
        migrations.CreateModel(
            name='PersonaNatural',
            fields=[
                ('id', models.OneToOneField(db_column='id', on_delete=django.db.models.deletion.CASCADE, primary_key=True, related_name='persona_persona_natural', serialize=False, to='comun.Persona')),
                ('nombres', models.CharField(max_length=90)),
                ('papellido', models.CharField(max_length=70)),
                ('sapellido', models.CharField(blank=True, max_length=70, null=True)),
                ('sexo', models.CharField(blank=True, choices=[('MAS', 'Masculino'), ('FEM', 'Femenino')], max_length=3, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Persona Natural',
                'verbose_name_plural': 'Personas Naturales',
                'db_table': 'comun_persona_natural',
                'default_permissions': (),
            },
        ),
    ]
