# Generated by Django 2.0.7 on 2018-07-04 17:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('comun', '0032_auto_20180704_1158'),
    ]

    operations = [
        migrations.CreateModel(
            name='EtiquetaContacto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('codigo', models.CharField(max_length=10, unique=True)),
                ('nombre', models.CharField(max_length=40)),
                ('tiene_cod_ubigeo', models.CharField(blank=True, max_length=2, null=True)),
                ('tiene_anexo', models.CharField(blank=True, max_length=2, null=True)),
                ('orden', models.IntegerField()),
                ('estado', models.CharField(default='1', max_length=1)),
                ('tipo_contacto', models.ForeignKey(db_column='tipo_contacto_id', on_delete=django.db.models.deletion.PROTECT, related_name='etiqueta_contacto_tipo_contactos', to='comun.TipoContacto')),
            ],
            options={
                'verbose_name': 'Etiqueta Contacto',
                'verbose_name_plural': 'Etiqueta Contactos',
                'db_table': 'comun_etiqueta_contacto',
            },
        ),
        migrations.AlterUniqueTogether(
            name='etiquetacontacto',
            unique_together={('tipo_contacto', 'nombre')},
        ),
    ]
