# Generated by Django 2.0.7 on 2018-07-10 18:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('comun', '0049_tipofecha'),
    ]

    operations = [
        migrations.CreateModel(
            name='ConfigDireccionTipoPersona',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descripcion', models.CharField(blank=True, max_length=60, null=True)),
                ('orden', models.IntegerField()),
                ('tipo_direccion', models.ForeignKey(db_column='tipo_direccion_id', on_delete=django.db.models.deletion.PROTECT, related_name='config_direccion_tipo_persona_tipo_direcciones', to='comun.TipoDireccion')),
                ('tipo_persona', models.ForeignKey(db_column='tipo_persona_id', on_delete=django.db.models.deletion.PROTECT, related_name='config_direccion_tipo_persona_tipo_personas', to='comun.TipoPersona')),
            ],
            options={
                'verbose_name': 'Config. Direccion de Tipo de Persona',
                'verbose_name_plural': 'Config. de Direcciones de Tipos de Personas',
                'db_table': 'comun_config_direccion_tipo_persona',
                'permissions': (('view_configdirecciontipopersona', 'Listar Config. de Direcciones de Tipos de Personas'), ('add_configdirecciontipopersona', 'Agregar Config. de Direcciones de Tipos de Personas'), ('update_configdirecciontipopersona', 'Actualizar Config. de Direcciones de Tipos de Personas'), ('delete_configdirecciontipopersona', 'Eliminar Config. de Direcciones de Tipos de Personas'), ('view_form_configdirecciontipopersona', 'Listar Config. de Direcciones de Tipos de Personas en formulario')),
                'default_permissions': (),
            },
        ),
        migrations.AlterUniqueTogether(
            name='configdirecciontipopersona',
            unique_together={('tipo_persona', 'tipo_direccion')},
        ),
    ]
