# Generated by Django 2.0.7 on 2018-07-04 13:15

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('comun', '0021_tipocontacto'),
    ]

    operations = [
        migrations.CreateModel(
            name='CuentaContacto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('codigo', models.CharField(max_length=10, unique=True)),
                ('nombre', models.CharField(max_length=60)),
                ('logo', models.CharField(blank=True, max_length=200, null=True)),
                ('url_web', models.CharField(blank=True, max_length=200, null=True)),
                ('orden', models.IntegerField()),
                ('estado', models.CharField(default='1', max_length=1)),
                ('tipo_contacto_id', models.ForeignKey(db_column='tipo_contacto_id', on_delete=django.db.models.deletion.PROTECT, related_name='tipo_contacto_cuenta_contactos', to='comun.TipoContacto')),
            ],
            options={
                'verbose_name': 'Cuenta Contacto',
                'verbose_name_plural': 'Cuentas Contactos',
                'db_table': 'comun_cuenta_contacto',
            },
        ),
    ]
