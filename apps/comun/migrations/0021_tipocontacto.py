# Generated by Django 2.0.7 on 2018-07-04 13:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('comun', '0020_ubigeo'),
    ]

    operations = [
        migrations.CreateModel(
            name='TipoContacto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('codigo', models.CharField(max_length=10, unique=True)),
                ('nombre', models.CharField(max_length=40, unique=True)),
                ('nombre_plural', models.CharField(max_length=50)),
                ('denom_valor', models.CharField(blank=True, max_length=40, null=True)),
                ('icono_material', models.CharField(blank=True, max_length=40, null=True)),
                ('orden', models.IntegerField()),
                ('estado', models.CharField(default='1', max_length=1)),
            ],
            options={
                'verbose_name': 'Tipo Contacto',
                'verbose_name_plural': 'Tipos Contactos',
                'db_table': 'comun_tipo_contacto',
            },
        ),
    ]
