from apps.comun.models.ClasificacionIiu import ClasificacionIiu
from rest_framework import serializers

class ClasificacionIiuSerializer(serializers.ModelSerializer):
	class Meta:
		model = ClasificacionIiu
		fields = '__all__'