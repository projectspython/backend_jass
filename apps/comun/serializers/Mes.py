from apps.comun.models.Mes import Mes
from rest_framework import serializers

class MesSerializer(serializers.ModelSerializer):
	class Meta:
		model = Mes
		fields = '__all__'