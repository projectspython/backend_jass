from apps.comun.models.CargoEntidad import CargoEntidad
from rest_framework import serializers
from apps.comun.serializers.Periodo import PeriodoSerializer
from apps.comun.serializers.Persona import PersonaBasicSerializer
from apps.comun.serializers.Entidad import EntidadSerializer
from apps.comun.serializers.Cargo import CargoSerializer

class CargoEntidadSerializer(serializers.ModelSerializer):
	periodo = PeriodoSerializer(many=False, read_only=True)
	# entidad = EntidadSerializer(many=False, read_only=True)
	persona = PersonaBasicSerializer(many=False, read_only=True)
	cargo = CargoSerializer(many=False, read_only=True)
	class Meta:
		model = CargoEntidad
		# fields = '__all__'
		exclude = ('created_at','updated_at',)