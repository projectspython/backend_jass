from apps.comun.models.TipoVia import TipoVia
from rest_framework import serializers

class TipoViaSerializer(serializers.ModelSerializer):
	class Meta:
		model = TipoVia
		fields = '__all__'