from apps.comun.models.PersonaJuridicaActividad import PersonaJuridicaActividad
from rest_framework import serializers
from apps.comun.serializers.ClasificacionIiu import ClasificacionIiuSerializer


class PersonaJuridicaActividadSerializer(serializers.ModelSerializer):
	clasificacion_iiu = ClasificacionIiuSerializer(many=False, read_only=True)
	class Meta:
		model = PersonaJuridicaActividad
		fields = '__all__'
