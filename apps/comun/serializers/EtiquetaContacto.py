from apps.comun.models.EtiquetaContacto import EtiquetaContacto
from rest_framework import serializers

class EtiquetaContactoSerializer(serializers.ModelSerializer):
	class Meta:
		model = EtiquetaContacto
		fields = '__all__'