from apps.comun.models.PersonaContacto import PersonaContacto
from rest_framework import serializers
from apps.comun.serializers.TipoContacto import TipoContactoSerializer
from apps.comun.serializers.Pais import PaisBasicSerializer
from apps.comun.serializers.EtiquetaContacto import EtiquetaContactoSerializer
from apps.comun.serializers.Ubigeo import UbigeoBasicSerializer
from apps.comun.serializers.CuentaContacto import CuentaContactoSerializer

class PersonaContactoSerializer(serializers.ModelSerializer):
	tipo_contacto = TipoContactoSerializer(many=False, read_only=True)
	pais = PaisBasicSerializer(many=False, read_only=True)
	etiqueta_contacto = EtiquetaContactoSerializer(many=False, read_only=True)
	ubigeo = UbigeoBasicSerializer(many=False, read_only=True)
	cuenta_contacto = CuentaContactoSerializer(many=False, read_only=True)
	class Meta:
		model = PersonaContacto
		fields = ('id','cuenta_contacto_id','etiqueta_contacto_id',
			'pais_id','tipo_contacto_id','ubigeo_id','valor_contacto',
			'anexo','area_oficina','tipo_contacto','pais','etiqueta_contacto','ubigeo',
			'cuenta_contacto',)



