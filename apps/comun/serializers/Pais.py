from apps.comun.models.Pais import Pais
from rest_framework import serializers
from apps.comun.serializers.Continente import ContinenteBasicSerializer

class PaisSerializer(serializers.ModelSerializer):
	continente = ContinenteBasicSerializer(many=False, read_only=True)
	class Meta:
		model = Pais
		# fields = '__all__'
		exclude = ('created_at','updated_at',)

class PaisBasicSerializer(serializers.ModelSerializer):
	# continente = ContinenteBasicSerializer(many=False, read_only=True)
	class Meta:
		model = Pais
		fields = ('id','codigo','nombre','codigo_telefono',)
