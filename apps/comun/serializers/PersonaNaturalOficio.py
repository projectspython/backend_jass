from apps.comun.models.PersonaNaturalOficio import PersonaNaturalOficio
from rest_framework import serializers
from apps.comun.serializers.ClasificacionIiu import ClasificacionIiuSerializer


class PersonaNaturalOficioSerializer(serializers.ModelSerializer):
	profesion_oficio = ClasificacionIiuSerializer(many=False, read_only=True)
	class Meta:
		model = PersonaNaturalOficio
		fields = '__all__'
