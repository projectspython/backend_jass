
from apps.comun.models.Ubigeo import Ubigeo
from rest_framework import serializers
from apps.comun.serializers.TipoUbigeo import TipoUbigeoBasicSerializer
from apps.comun.serializers.Pais import PaisBasicSerializer
from apps.comun.serializers.RegionNatural import RegionNaturalBasicSerializer

class Ubigeo2Serializer(serializers.ModelSerializer):
	tipo_ubigeo = TipoUbigeoBasicSerializer(many=False, read_only=True)
	class Meta:
		model = Ubigeo
		fields = '__all__'

class UbigeoSerializer(serializers.ModelSerializer):
	pais = PaisBasicSerializer(many=False, read_only=True)
	tipo_ubigeo = TipoUbigeoBasicSerializer(many=False, read_only=True)
	padre = Ubigeo2Serializer(many=False, read_only=True)
	region_natural = RegionNaturalBasicSerializer(many=False, read_only=True)
	class Meta:
		model = Ubigeo
		fields = '__all__'

# class UbigeoProcesSerializer(serializers.ModelSerializer):
# 	class Meta:
# 		model = Ubigeo
# 		fields = ('id','codigo','nombre','codigo_telefono','capital',
# 			'poblacion','area_km2','padre_id','pais_id','region_natural_id',
# 			'tipo_ubigeo_id',)

class UbigeoBasic__Serializer(serializers.ModelSerializer):
	class Meta:
		model = Ubigeo
		fields = ('id','codigo','nombre','codigo_telefono',)

class UbigeoBasic_Serializer(serializers.ModelSerializer):
	padre = UbigeoBasic__Serializer(many=False, read_only=True)
	class Meta:
		model = Ubigeo
		fields = ('id','codigo','nombre','codigo_telefono','padre',)

class UbigeoBasicSerializer(serializers.ModelSerializer):
	padre = UbigeoBasic_Serializer(many=False, read_only=True)
	pais = PaisBasicSerializer(many=False, read_only=True)
	class Meta:
		model = Ubigeo
		fields = ('id','codigo','nombre','codigo_telefono','padre', 'pais',)

