from apps.comun.models.PersonaDocumento import PersonaDocumento
from rest_framework import serializers
from apps.comun.serializers.TipoDocumento import TipoDocumentoBasicSerializer

class PersonaDocumentoSerializer(serializers.ModelSerializer):
	# tipo_documento = TipoDocumentoBasicSerializer(many=False, read_only=True)
	class Meta:
		model = PersonaDocumento
		fields = '__all__'

class PersonaDocumentoBasicSerializer(serializers.ModelSerializer):
	tipo_documento = TipoDocumentoBasicSerializer(many=False, read_only=True)
	class Meta:
		model = PersonaDocumento
		fields = ('id','tipo_documento','numero',)