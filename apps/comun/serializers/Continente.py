from apps.comun.models.Continente import Continente
from rest_framework import serializers

class ContinenteSerializer(serializers.ModelSerializer):
	class Meta:
		model = Continente
		# fields = '__all__'
		exclude = ('created_at','updated_at',)

class ContinenteBasicSerializer(serializers.ModelSerializer):
	class Meta:
		model = Continente
		fields = ('id','nombre','codigo',)