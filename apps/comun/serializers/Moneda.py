from apps.comun.models.Moneda import Moneda
from rest_framework import serializers

class MonedaSerializer(serializers.ModelSerializer):
	class Meta:
		model = Moneda
		# fields = '__all__'
		exclude = ('created_at','updated_at',)