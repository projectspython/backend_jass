from apps.comun.models.TipoDocumento import TipoDocumento
from rest_framework import serializers

class TipoDocumentoSerializer(serializers.ModelSerializer):
	class Meta:
		model = TipoDocumento
		fields = '__all__'

class TipoDocumentoBasicSerializer(serializers.ModelSerializer):
	class Meta:
		model = TipoDocumento
		fields = ('id','nombre','codigo','patron_angular',)
