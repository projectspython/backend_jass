from apps.comun.models.CuentaContacto import CuentaContacto
from rest_framework import serializers

class CuentaContactoSerializer(serializers.ModelSerializer):
	class Meta:
		model = CuentaContacto
		fields = '__all__'