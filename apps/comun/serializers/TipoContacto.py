from apps.comun.models.TipoContacto import TipoContacto
from rest_framework import serializers

class TipoContactoSerializer(serializers.ModelSerializer):
	class Meta:
		model = TipoContacto
		fields = '__all__'