from apps.comun.models.TipoDireccion import TipoDireccion
from rest_framework import serializers

class TipoDireccionSerializer(serializers.ModelSerializer):
	class Meta:
		model = TipoDireccion
		fields = '__all__'