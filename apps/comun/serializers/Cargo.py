from apps.comun.models.Cargo import Cargo
from rest_framework import serializers

class CargoSerializer(serializers.ModelSerializer):
	class Meta:
		model = Cargo
		fields = '__all__'

class CargoBasicSerializer(serializers.ModelSerializer):
	class Meta:
		model = Cargo
		fields = ('id','nombre','codigo',)
