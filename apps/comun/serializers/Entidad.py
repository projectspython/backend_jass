from apps.comun.models.Entidad import Entidad
from rest_framework import serializers
from apps.comun.serializers.Persona import PersonaBasicSerializer
from apps.comun.serializers.Moneda import MonedaSerializer
from apps.comun.serializers.TipoEntidad import TipoEntidadSerializer

class EntidadBasicSerializer(serializers.ModelSerializer):
	persona = PersonaBasicSerializer(many=False, read_only=True)
	class Meta:
		model = Entidad
		fields = ('persona',)

class EntidadSerializer(serializers.ModelSerializer):
	persona = PersonaBasicSerializer(many=False, read_only=True)
	moneda = MonedaSerializer(many=False, read_only=True)
	tipo_entidad = TipoEntidadSerializer(many=False, read_only=True)
	padre = EntidadBasicSerializer(many=False, read_only=True)
	class Meta:
		model = Entidad
		fields = '__all__'
		#exclude = ('created_at','updated_at',)