from apps.comun.models.TipoEntidad import TipoEntidad
from rest_framework import serializers

class TipoEntidadSerializer(serializers.ModelSerializer):
	class Meta:
		model = TipoEntidad
		# fields = '__all__'
		exclude = ('created_at','updated_at',)