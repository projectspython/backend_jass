from apps.comun.models.PersonaNatural import PersonaNatural
from rest_framework import serializers
from apps.comun.serializers.PersonaNaturalOficio import PersonaNaturalOficioSerializer
from apps.comun.serializers.Persona import PersonaInfoSerializer


class PersonaNaturalSerializer(serializers.ModelSerializer):
	persona_natural_oficios = PersonaNaturalOficioSerializer(many=True, read_only=True)
	class Meta:
		model = PersonaNatural
		fields = ('nombres','papellido','sapellido','sexo','persona_natural_oficios',)


class PersonaNaturalInfoSerializer(serializers.ModelSerializer):
	persona_natural_oficios = PersonaNaturalOficioSerializer(many=True, read_only=True)
	sexo_nombre = serializers.CharField(source='get_sexo_display')
	id = PersonaInfoSerializer(many=False, read_only=True)
	
	class Meta:
		model = PersonaNatural
		fields = ('nombres','papellido','sapellido','sexo',
			'persona_natural_oficios','sexo_nombre','id',)
