from apps.comun.models.TipoZona import TipoZona
from rest_framework import serializers

class TipoZonaSerializer(serializers.ModelSerializer):
	class Meta:
		model = TipoZona
		fields = '__all__'