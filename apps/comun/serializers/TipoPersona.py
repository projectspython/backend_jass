from apps.comun.models.TipoPersona import TipoPersona
from rest_framework import serializers

class TipoPersonaSerializer(serializers.ModelSerializer):
	class Meta:
		model = TipoPersona
		fields = '__all__'

class TipoPersonaBasicSerializer(serializers.ModelSerializer):
	class Meta:
		model = TipoPersona
		fields = ('id','codigo','nombre',)