from apps.comun.models.Periodo import Periodo
from rest_framework import serializers

class PeriodoSerializer(serializers.ModelSerializer):
	class Meta:
		model = Periodo
		fields = '__all__'