from apps.comun.models.PersonaJuridica import PersonaJuridica
from rest_framework import serializers
from apps.comun.serializers.PersonaJuridicaActividad import PersonaJuridicaActividadSerializer
from apps.comun.serializers.Persona import PersonaSerializer
from apps.comun.serializers.Persona import PersonaInfoSerializer

class PersonaJuridicaSerializer(serializers.ModelSerializer):
	persona_juridica_actividades = PersonaJuridicaActividadSerializer(many=True, read_only=True)
	repres_legal = PersonaSerializer(many=False, read_only=True)
	class Meta:
		model = PersonaJuridica
		fields = ('razon_social','nombre_comercial','nombre_repres_legal',
			'nro_resolucion','repres_legal_id','tipo_ambito_id','persona_juridica_actividades',
			'repres_legal',)

class PersonaJuridicaInfoSerializer(serializers.ModelSerializer):
	id = PersonaInfoSerializer(many=False, read_only=True)
	repres_legal = PersonaSerializer(many=False, read_only=True)
	persona_juridica_actividades = PersonaJuridicaActividadSerializer(many=True, read_only=True)

	class Meta:
		model = PersonaJuridica
		fields = ('razon_social','nombre_comercial','nombre_repres_legal',
			'nro_resolucion','repres_legal_id','tipo_ambito_id','persona_juridica_actividades',
			'repres_legal','id',)