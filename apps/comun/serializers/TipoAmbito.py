from apps.comun.models.TipoAmbito import TipoAmbito
from rest_framework import serializers

class TipoAmbitoSerializer(serializers.ModelSerializer):
	class Meta:
		model = TipoAmbito
		fields = '__all__'