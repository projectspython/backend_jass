from apps.comun.models.ProfesionOficio import ProfesionOficio
from rest_framework import serializers

class ProfesionOficioSerializer(serializers.ModelSerializer):
	class Meta:
		model = ProfesionOficio
		fields = '__all__'