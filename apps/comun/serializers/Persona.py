from apps.comun.models.Persona import Persona
from rest_framework import serializers
from apps.comun.serializers.Pais import PaisBasicSerializer
from apps.comun.serializers.TipoPersona import TipoPersonaBasicSerializer
from apps.comun.serializers.PersonaDocumento import PersonaDocumentoBasicSerializer
from apps.comun.serializers.PersonaContacto import PersonaContactoSerializer
from apps.segur.always.HostValues import HostValues
from apps.procesos.serializers.Direccion import DireccionInfoSerializer

class PersonaSerializer(serializers.ModelSerializer):
	nacionalidad = PaisBasicSerializer(many=False, read_only=True)
	tipo_persona = TipoPersonaBasicSerializer(many=False, read_only=True)
	persona_persona_documentos = PersonaDocumentoBasicSerializer(many=True, read_only=True)
	class Meta:
		model = Persona
		# fields = '__all__'
		exclude = ('created_at','updated_at',)

class PersonaBasicSerializer(serializers.ModelSerializer):
	persona_persona_documentos = PersonaDocumentoBasicSerializer(many=True, read_only=True)
	class Meta:
		model = Persona
		fields = ('id','nombre_completo','foto_logo_rec', 'foto_logo','codigo','persona_persona_documentos',)
		# exclude = ('created_at','updated_at',)

class PersonaEditSerializer(serializers.ModelSerializer):
	persona_persona_documentos = PersonaDocumentoBasicSerializer(many=True, read_only=True)
	persona_persona_contactos = PersonaContactoSerializer(many=True, read_only=True)

	class Meta:
		model = Persona
		fields = ('persona_persona_documentos','persona_persona_contactos',)

class PersonaFotoLogoSerializer(serializers.ModelSerializer):
	class Meta:
		model = Persona
		fields = ('id','foto_logo','foto_logo_rec',)

class PersonaInfoSerializer(serializers.ModelSerializer):
	nacionalidad = PaisBasicSerializer(many=False, read_only=True)
	tipo_persona = TipoPersonaBasicSerializer(many=False, read_only=True)
	persona_persona_documentos = PersonaDocumentoBasicSerializer(many=True, read_only=True)
	persona_persona_contactos = PersonaContactoSerializer(many=True, read_only=True)
	persona_direcciones = DireccionInfoSerializer(many=True, read_only=True)
	# foto_logo = serializers.SerializerMethodField('valores')

	# def valores(self, obj):
	# 	return obj.foto_logo.url

	class Meta:
		model = Persona
		fields = ('id','foto_logo','foto_logo_rec','nombre_completo','nacionalidad',
			'tipo_persona','persona_persona_documentos','persona_persona_contactos',
			'persona_direcciones',)
		# exclude = ('created_at','updated_at',)



