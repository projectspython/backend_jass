from apps.comun.models.TipoNumeroVia import TipoNumeroVia
from rest_framework import serializers

class TipoNumeroViaSerializer(serializers.ModelSerializer):
	class Meta:
		model = TipoNumeroVia
		fields = '__all__'