from apps.comun.models.TipoContribuyente import TipoContribuyente
from rest_framework import serializers

class TipoContribuyenteSerializer(serializers.ModelSerializer):
	class Meta:
		model = TipoContribuyente
		fields = '__all__'