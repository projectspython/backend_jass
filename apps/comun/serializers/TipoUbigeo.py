
from apps.comun.models.TipoUbigeo import TipoUbigeo
from rest_framework import serializers

class TipoUbigeoSerializer(serializers.ModelSerializer):
	class Meta:
		model = TipoUbigeo
		fields = '__all__'

class TipoUbigeoBasicSerializer(serializers.ModelSerializer):
	class Meta:
		model = TipoUbigeo
		fields = ('id','codigo','nombre',)