from apps.comun.models.RegionNatural import RegionNatural
from rest_framework import serializers

class RegionNaturalSerializer(serializers.ModelSerializer):
	class Meta:
		model = RegionNatural
		fields = '__all__'
		# exclude = ('created_at','updated_at',)

class RegionNaturalBasicSerializer(serializers.ModelSerializer):
	class Meta:
		model = RegionNatural
		fields = ('id','nombre',)