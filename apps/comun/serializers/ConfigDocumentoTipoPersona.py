from apps.comun.models.ConfigDocumentoTipoPersona import ConfigDocumentoTipoPersona
from rest_framework import serializers

class ConfigDocumentoTipoPersonaSerializer(serializers.ModelSerializer):
	class Meta:
		model = ConfigDocumentoTipoPersona
		fields = '__all__'