from apps.comun.models.TipoFecha import TipoFecha
from rest_framework import serializers
from apps.comun.serializers.TipoPersona import TipoPersonaBasicSerializer

class TipoFechaSerializer(serializers.ModelSerializer):
	tipopersona = TipoPersonaBasicSerializer(many=False, read_only=True)
	class Meta:
		model = TipoFecha
		fields = '__all__'

class TipoFechaBasicSerializer(serializers.ModelSerializer):
	class Meta:
		model = TipoFecha
		fields = '__all__'