from apps.comun.models.Contador import Contador
from rest_framework import serializers

class ContadorSerializer(serializers.ModelSerializer):
	class Meta:
		model = Contador
		fields = '__all__'