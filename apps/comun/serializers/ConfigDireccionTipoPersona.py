from apps.comun.models.ConfigDireccionTipoPersona import ConfigDireccionTipoPersona
from rest_framework import serializers

class ConfigDireccionTipoPersonaSerializer(serializers.ModelSerializer):
	class Meta:
		model = ConfigDireccionTipoPersona
		fields = '__all__'