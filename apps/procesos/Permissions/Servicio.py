from apps.comun.Permissions.Base import BasePermission

class ServicioPermissions(BasePermission):
	perms_map = {
				'add': 'procesos.add_servicio',
				'edit': 'procesos.view_servicio',
				'update': 'procesos.update_servicio',
				'delete':'procesos.delete_servicio',}

	permission_from_user = None