from apps.comun.Permissions.Base import BasePermission

class ContratoServicioPermissions(BasePermission):
	perms_map = {
				'add': 'procesos.add_contratoservicio',
				'edit': 'procesos.view_contratoservicio',
				'update': 'procesos.update_contratoservicio',
				'delete':'procesos.delete_contratoservicio',}

	permission_from_user = None