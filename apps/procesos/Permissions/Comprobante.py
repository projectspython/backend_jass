from apps.comun.Permissions.Base import BasePermission

class ComprobantePermissions(BasePermission):
	perms_map = {
				'add': 'procesos.add_comprobante',
				'edit': 'procesos.view_comprobante',
				'update': 'procesos.update_comprobante',
				'delete':'procesos.delete_comprobante',}

	permission_from_user = None