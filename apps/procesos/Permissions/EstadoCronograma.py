from apps.comun.Permissions.Base import BasePermission

class EstadoCronogramaPermissions(BasePermission):
	perms_map = {
				'add': 'procesos.add_estadocronograma',
				'edit': 'procesos.view_estadocronograma',
				'update': 'procesos.update_estadocronograma',
				'delete':'procesos.delete_estadocronograma',}

	permission_from_user = None