from apps.comun.Permissions.Base import BasePermission

class ReciboPermissions(BasePermission):
	perms_map = {
				'add': 'procesos.add_recibo',
				'addone':'procesos.add_recibo',
				'edit': 'procesos.view_recibo',
				'update': 'procesos.update_recibo',
				'delete':'procesos.delete_recibo',}

	permission_from_user = None