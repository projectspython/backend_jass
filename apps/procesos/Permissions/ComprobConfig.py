from apps.comun.Permissions.Base import BasePermission

class ComprobConfigPermissions(BasePermission):
	perms_map = {
				'add': 'procesos.add_comprobconfig',
				'edit': 'procesos.view_comprobconfig',
				'update': 'procesos.update_comprobconfig',
				'delete':'procesos.delete_comprobconfig',}

	permission_from_user = None