from apps.comun.Permissions.Base import BasePermission

class EventoPermissions(BasePermission):
	perms_map = {
				'add': 'procesos.add_evento',
				'edit': 'procesos.view_evento',
				'update': 'procesos.update_evento',
				'delete':'procesos.delete_evento',}

	permission_from_user = None
