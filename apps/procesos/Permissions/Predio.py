from apps.comun.Permissions.Base import BasePermission

class PredioPermissions(BasePermission):
	perms_map = {
				'add': 'procesos.add_predio',
				'edit': 'procesos.view_predio',
				'update': 'procesos.update_predio',
				'delete':'procesos.delete_predio',}

	permission_from_user = None