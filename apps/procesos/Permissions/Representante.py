from apps.comun.Permissions.Base import BasePermission

class RepresentantePermissions(BasePermission):
	perms_map = {
				'add': 'procesos.add_representante',
				'edit': 'procesos.view_representante',
				'update': 'procesos.update_representante',
				'delete':'procesos.delete_representante',}

	permission_from_user = None
