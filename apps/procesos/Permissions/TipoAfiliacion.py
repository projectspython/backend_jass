from apps.comun.Permissions.Base import BasePermission

class TipoAfiliacionPermissions(BasePermission):
	perms_map = {
				'add': 'procesos.add_tipoafiliacion',
				'edit': 'procesos.view_tipoafiliacion',
				'update': 'procesos.update_tipoafiliacion',
				'delete':'procesos.delete_tipoafiliacion',}

	permission_from_user = None