from apps.comun.Permissions.Base import BasePermission

class AsistenciaPermissions(BasePermission):
	perms_map = {
				'add': 'procesos.add_asistencia',
				'edit': 'procesos.view_asistencia',
				'update': 'procesos.update_asistencia',
				'delete':'procesos.delete_asistencia',}

	permission_from_user = None
