from apps.comun.Permissions.Base import BasePermission

class TipoMovimientoCuentaPermissions(BasePermission):
	perms_map = {
				'add': 'procesos.add_tipomovimientocuenta',
				'edit': 'procesos.view_tipomovimientocuenta',
				'update': 'procesos.update_tipomovimientocuenta',
				'delete':'procesos.delete_tipomovimientocuenta',}

	permission_from_user = None