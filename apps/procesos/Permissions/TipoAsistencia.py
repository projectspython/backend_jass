from apps.comun.Permissions.Base import BasePermission

class TipoAsistenciaPermissions(BasePermission):
	perms_map = {
				'add': 'procesos.add_tipoasistencia',
				'edit': 'procesos.view_tipoasistencia',
				'update': 'procesos.update_tipoasistencia',
				'delete':'procesos.delete_tipoasistencia',}

	permission_from_user = None
