from apps.comun.Permissions.Base import BasePermission

class ZonaPermissions(BasePermission):
	perms_map = {
				'add': 'procesos.add_zona',
				'edit': 'procesos.view_zona',
				'update': 'procesos.update_zona',
				'delete':'procesos.delete_zona',}

	permission_from_user = None