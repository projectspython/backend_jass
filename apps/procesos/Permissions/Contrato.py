from apps.comun.Permissions.Base import BasePermission

class ContratoPermissions(BasePermission):
	perms_map = {
				'add': 'procesos.add_contrato',
				'edit': 'procesos.view_contrato',
				'update': 'procesos.update_contrato',
				'delete':'procesos.delete_contrato',
				'searchform':'procesos.view_form_contrato',
				'inforecibo':'procesos.view_form_contrato',}

	permission_from_user = None