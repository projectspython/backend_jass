from apps.comun.Permissions.Base import BasePermission

class EstadoPredioPermissions(BasePermission):
	perms_map = {
				'add': 'procesos.add_estadopredio',
				'edit': 'procesos.view_estadopredio',
				'update': 'procesos.update_estadopredio',
				'delete':'procesos.delete_estadopredio',}

	permission_from_user = None