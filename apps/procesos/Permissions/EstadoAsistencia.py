from apps.comun.Permissions.Base import BasePermission

class EstadoAsistenciaPermissions(BasePermission):
	perms_map = {
				'add': 'procesos.add_estadoasistencia',
				'edit': 'procesos.view_estadoasistencia',
				'update': 'procesos.update_estadoasistencia',
				'delete':'procesos.delete_estadoasistencia',}

	permission_from_user = None
