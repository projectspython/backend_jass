from apps.comun.Permissions.Base import BasePermission

class MesReciboPermissions(BasePermission):
	perms_map = {
				'add': 'procesos.add_mesrecibo',
				'edit': 'procesos.view_mesrecibo',
				'update': 'procesos.update_mesrecibo',
				'delete':'procesos.delete_mesrecibo',
				'searchform':'procesos.view_form_mesrecibo'}

	permission_from_user = None