from apps.comun.Permissions.Base import BasePermission

class MotivoMovimientoCuentaPermissions(BasePermission):
	perms_map = {
				'add': 'procesos.add_motivomovimientocuenta',
				'edit': 'procesos.view_motivomovimientocuenta',
				'update': 'procesos.update_motivomovimientocuenta',
				'delete':'procesos.delete_motivomovimientocuenta',}

	permission_from_user = None