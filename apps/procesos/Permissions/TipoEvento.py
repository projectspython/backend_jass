from apps.comun.Permissions.Base import BasePermission

class TipoEventoPermissions(BasePermission):
	perms_map = {
				'add': 'procesos.add_tiposevento',
				'edit': 'procesos.view_tiposevento',
				'update': 'procesos.update_tiposevento',
				'delete':'procesos.delete_tiposevento',}

	permission_from_user = None
