from apps.comun.Permissions.Base import BasePermission

class AfiliacionPermissions(BasePermission):
	perms_map = {
				'add': 'procesos.add_afiliacion',
				'edit': 'procesos.view_afiliacion',
				'update': 'procesos.update_afiliacion',
				'delete':'procesos.delete_afiliacion',}

	permission_from_user = None
