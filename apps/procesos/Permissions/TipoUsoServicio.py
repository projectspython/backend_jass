from apps.comun.Permissions.Base import BasePermission

class TipoUsoServicioPermissions(BasePermission):
	perms_map = {
				'add': 'procesos.add_tipousoservicio',
				'edit': 'procesos.view_tipousoservicio',
				'update': 'procesos.update_tipousoservicio',
				'delete':'procesos.delete_tipousoservicio',}

	permission_from_user = None