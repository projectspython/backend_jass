from apps.comun.Permissions.Base import BasePermission

class ContratoFirmaPermissions(BasePermission):
	perms_map = {
				'add': 'procesos.add_contratofirma',
				'edit': 'procesos.view_contratofirma',
				'update': 'procesos.update_contratofirma',
				'delete':'procesos.delete_contratofirma',}

	permission_from_user = None
