from apps.comun.Permissions.Base import BasePermission

class CronogramaPagoPermissions(BasePermission):
	perms_map = {
				'add': 'procesos.add_cronogramapago',
				'edit': 'procesos.view_cronogramapago',
				'update': 'procesos.update_cronogramapago',
				'delete':'procesos.delete_cronogramapag',
				'searchform':'procesos.view_form_cronogramapago',
				'listcronogpagar':'procesos.view_form_cronogramapago',}

	permission_from_user = None