
from apps.comun.models.TipoVia import TipoVia
from apps.comun.models.TipoNumeroVia import TipoNumeroVia
from apps.comun.models.TipoZona import TipoZona
from apps.comun.always.SearchFilter import keys_add_none, values_concat, values_obj_add

def concat_direccion_detalle(data):
	keys = keys_add_none(data,'tipo_via_id,nombre_via,tipo_numero_via_id,'+
				'numero_via,manzana,lote,piso,dpto_interior,tipo_zona_id,nombre_zona')


	if keys['tipo_via_id']:
		tipo_via = TipoVia.objects.get(pk=keys['tipo_via_id'])
		data['tipo_via_abrev'] = tipo_via.abreviatura

	if keys['tipo_numero_via_id']:
		tipo_nro_via = TipoNumeroVia.objects.get(pk=keys['tipo_numero_via_id'])
		data['tipo_nro_via_abrev'] = tipo_nro_via.abreviatura

	if keys['tipo_zona_id']:
		tipo_zona = TipoZona.objects.get(pk=keys['tipo_zona_id'])
		data['tipo_zona_abrev'] = tipo_zona.abreviatura

	concat = values_concat(data,'tipo_via_abrev,nombre_via,tipo_nro_via_abrev,numero_via')

	if keys['manzana']:
		concat +=" Mz. "+keys['manzana']

	if keys['lote']:
		concat +=" Lt. "+keys['lote']

	if keys['piso']:
		concat +=" Piso "+keys['piso']

	if keys['dpto_interior']:
		concat +=" Dep.Int. "+keys['dpto_interior']

	concat2 = values_concat(data,'tipo_zona_abrev,nombre_zona')
	if concat2:
		concat+=" - "+concat2

	return concat




