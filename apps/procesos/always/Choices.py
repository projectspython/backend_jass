from django.utils.text import capfirst
from django.utils.translation import ugettext_lazy as _

ESTADO_MES_RECIBO = (
    ('REG', capfirst(_('Registrado'))),
    ('HAB', capfirst(_('Habilitado'))),
    ('GEN', capfirst(_('Generado'))),
)

ESTADO_RECIBO = (
    ('HAB', capfirst(_('Habilitado'))),
    ('ANU', capfirst(_('Anulado'))),
)

FORMA_COBRANZA = (
    ('SME', capfirst(_('Sin Medidor'))),
    ('CME', capfirst(_('Con Medidor'))),
)

ESTADO_CONTRATO = (
    ('0', capfirst(_('Anulado'))),
    ('1', capfirst(_('Registrado'))),
    ('2', capfirst(_('Habilitado'))),
    ('3', capfirst(_('Terminado'))),
)

PERSONA_ASISTENTE = (
    ('SOC', capfirst(_('Socio'))),
    ('REP', capfirst(_('Representante'))),
    ('USU', capfirst(_('Usuario'))),
)

UNIDAD_TIEMPO = (
    ('HOR', capfirst(_('Horas'))),
    ('MIN', capfirst(_('Minutos'))),
    ('SEG', capfirst(_('Segundos'))),
)
