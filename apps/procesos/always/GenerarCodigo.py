
from apps.procesos.models.ComprobConfig import ComprobConfig

def correlativo(codigo, entidad_id):
	model = ComprobConfig.objects.filter(entidad_id=entidad_id, comprobante__codigo=codigo).values('id','serie','nro_inicial','nro_limite','correlativo','estado').first()
	contador = model['correlativo'] + 1
	update = ComprobConfig.objects.filter(entidad_id=entidad_id, comprobante__codigo=codigo).update(correlativo=contador)
	return contador


class GenerarCodigo():
	"""docstring for ClassName"""
	def contrato(entidad_id):
		codigo = 'COPRESAP'
		contador = correlativo(codigo, entidad_id)
		return '%06d' % contador
