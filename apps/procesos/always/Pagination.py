from rest_framework import pagination
from rest_framework.response import Response
from django.core.paginator import Paginator, InvalidPage
from django.db.models import F, Count
from functools import reduce
from operator import __or__ as OR
from django.db.models import Q
import json
import six
from django.http import HttpResponse, HttpResponseNotFound
import ast

class StandardLimonSetPagination(pagination.PageNumberPagination):
	page_size_query_param = 'per_page'
	page_query_param = 'current_page'


	def paginate_queryset(self, queryset, request, view=None):
		per_page = self.get_page_size(request)

		if not per_page:
			return None

		paginator = self.django_paginator_class(queryset, per_page)

		page_number = request.query_params.get(self.page_query_param, 1)
		if page_number in self.last_page_strings:
			page_number = paginator.num_pages

		try:
			if int(paginator.num_pages) < int(page_number):
				page_number = paginator.num_pages
			self.page = paginator.page(page_number)
		except InvalidPage as exc:
			msg = self.invalid_page_message.format(
				page_number=page_number, message="six.text_type(exc)"
			)
			return HttpResponseNotFound(msg)

		if paginator.num_pages > 1 and self.template is not None:
			self.display_page_controls = True

		self.request = request
		return list(self.page)

	def get_paginated_response(self, data):
		return Response({
			'pagination': {
				'current_page': self.page.number,
				'total': self.page.paginator.count,
				'from': self.page.start_index(),
				'to': self.page.end_index(),
				'per_page': self.page_size,
				'last_page': self.page.paginator.num_pages,
			},
			'data': data
		})


class LimonPagination():
	pagination_class = StandardLimonSetPagination
	page = StandardLimonSetPagination

	# def get_queryset(self):
	#     queryset = self.queryset
	#     orderBy = self.request.query_params.getlist('orderBy', '')
	#     exclude = self.request.query_params.get('exclude', '')
	#     searchList = self.request.query_params.get('searchList', '{}')

	#     if (orderBy == ''):
	#         orderBy = self.serializer_class.Meta.model._meta.ordering

	#     if searchList is not '{}' and searchList is not '':
	#         queryset = queryset.filter(reduce(OR,
	#         	[Q(x) for x in ast.literal_eval(searchList).items()])).distinct()

	#     queryset = queryset.order_by(*orderBy).distinct()
	#     return queryset