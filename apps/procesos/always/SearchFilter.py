
from functools import reduce
from operator import __or__ as OR
from django.db.models import Q
import ast, json


def search_filter(queryset, request):
	limit = request.query_params.get('limit', None)
	q = request.query_params.get('q', None)
	ordering = request.query_params.get('ordering', None)
	f = request.query_params.get('f', None)

	if f:
		queryset = queryset.filter(**json.loads(f))

	if q:
		queryset = queryset.filter(reduce(OR, 
			[Q(x) for x in ast.literal_eval(q).items()]))

	if ordering:
		queryset = queryset.order_by(*ordering.split(","))

	if limit:
		queryset = queryset[:int(limit)]

	return queryset

# http://localhost:8000/api/comun/paises/searchform/
# ?limit=10&q={"nombre__icontains":"e"}&ordering=nombre,codigo&f={"estado":"0"}

def keys_del(data, keys):
	listado = keys.split(",")
	for p in listado:
		if p in data:
			del data[p]

	return data

def keys_add_none(data, keys):
	listado = keys.split(",")
	data_new = {}
	for p in listado:
		if p in data and data[p]:
			data_new[p] = data[p]
		else:
			data_new[p] = None

	return data_new

def values_concat(data, keys):
	listado = keys.split(",")
	data_new = ''
	for p in listado:
		if p in data and data[p]:
			data_new += " "+data[p]

	return data_new