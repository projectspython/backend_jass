from rest_framework import serializers, viewsets, generics
from apps.procesos.models.Asistencia import Asistencia
from rest_framework import permissions
from apps.procesos.serializers.Asistencia import AsistenciaSerializer
from apps.procesos.always.Pagination import LimonPagination
from rest_framework.filters import SearchFilter, OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend
from django.db.models.deletion import ProtectedError

from apps.procesos.models.Representante import Representante

from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.procesos.Permissions.Asistencia import AsistenciaPermissions as LimonPermission
from apps.segur.always.Permission import PermLimonList
import json
from apps.comun.always.SearchFilter import search_filter, keys_add_none, values_obj_add, extaer_de_column
from apps.procesos.always.SearchFilter import search_filter, keys_del
import json
from django_filters import filters
from apps.comun.always.GenerarCodigo import GenerarCodigo
from datetime import datetime
import time


class AsistenciaViewSet(LimonPagination, viewsets.ModelViewSet):
	queryset = Asistencia.objects.all()
	permission_classes = [permissions.IsAuthenticated, PermLimonList, ]
	serializer_class = AsistenciaSerializer
	filter_backends = (SearchFilter,OrderingFilter,DjangoFilterBackend,)
	filter_fields = ('evento_id',)
	search_fields = ('persona__nombre_completo','representante__nombre_completo', 'persona_asistente', 'evento__nombre', 'estado_asistencia__nombre',)
	ordering_fields = '__all__'

	@list_route(url_path='add', methods=['post'], permission_classes=[LimonPermission,])
	def get_add(self, request, *args, **kwargs):
		date_now = datetime.now().date()
		hour_now = datetime.now().time()
		data = request.data
		data['usuario_id'] = request.user.persona_id
		data['fecha_reg'] = date_now
		data['hora_reg'] = hour_now
		data['estado_asistencia_id'] = data['estado_asistencia']['id']
		data_asist = keys_add_none(data,'usuario_id,entidad_id,evento_id,estado_asistencia_id,persona_id,persona_asistente,comentario,fecha_reg,hora_reg,estado',)
		asi = Asistencia.objects.create(**data_asist)
		return Response(self.get_serializer(asi).data)

	@list_route(url_path='add_repre', methods=['post'], permission_classes=[LimonPermission,])
	def get_add_repre(self, request, *args, **kwargs):
		date_now = datetime.now().date()
		hour_now = datetime.now().time()
		data = request.data
		data['usuario_id'] = request.user.persona_id
		data['fecha_reg'] = date_now
		data['hora_reg'] = hour_now
		data['estado_asistencia_id'] = data['estado_asistencia']['id']
		# print(data)
		if data['representante']:
			representante = data['representante']
			data['representante_id'] = representante['persona']['id']
		data_asist = keys_add_none(data,'usuario_id,entidad_id,comentario,evento_id,estado_asistencia_id,persona_id,representante_id,persona_asistente,fecha_reg,hora_reg,estado',)
		asi = Asistencia.objects.create(**data_asist)
		return Response(self.get_serializer(asi).data)

	@list_route(url_path='updateestaasist', methods=['put'], permission_classes=[LimonPermission,])
	def get_update(self, request, pk=None):
		data = request.data
		data['estado_asistencia_id'] = data['estado_asistencia']['id']
		data['persona_id'] = data['persona']['id']
		if 'representante' in data:
			data['representante_id'] = data['representante']['id']
			print('EXITE UN REPRESENTATE SIIIII')
		else:
			print('SOLO ES UNA PERSONA SIN REPRESENTATE')
		# if data['representante']:
		# 	data['representante_id'] = data['representante']['id']

		data_cp = keys_add_none(data,'id,comentario,estado_asistencia_id,persona_id,representante_id')
		print(data_cp)
		afi = Asistencia.objects.filter(id=request.data['id']).update(**data_cp)
		model = Asistencia.objects.get(pk=request.data['id'])
		return Response(self.get_serializer(model).data)

	# @list_route(url_path='edit', methods=['get'], permission_classes=[LimonPermission,])
	# def get_edit(self, request):
	# 	result = {}
	# 	representantes = []
	# 	id = request.query_params.get('id', None)
	# 	model = Asistencia.objects.get(pk=id)
	# 	result = self.get_serializer(model).data
	# 	# return Response(self.get_serializer(model).data)
	# 	return Response(result)

	# @list_route(url_path='update', methods=['put'], permission_classes=[LimonPermission,])
	# def get_update(self, request, pk=None):
	# 	data = request.data
	# 	data_cp = keys_add_none(data,'codigo,entidad_id,persona_id,tipo_Asistencia_id,fecha_ingreso,fecha_Asistencia,estado')
	# 	afi = Asistencia.objects.filter(id=request.data['id']).update(**data_cp)
	# 	model = Asistencia.objects.get(pk=request.data['id'])
	# 	return Response(self.get_serializer(model).data)

	@list_route(url_path='delete', methods=['delete'], permission_classes=[LimonPermission,])
	def get_delete(self, request, *args, **kwargs):
		try:
			id = request.query_params.get('id', None)
			print('el id es == {}'.format(id))
			model = Asistencia.objects.get(pk=id).delete()
			return Response({'status':True, 'id':id})
		except ProtectedError as e:
			return Response(e.errors, status=status.HTTP_400_BAD_REQUEST)

	@list_route(url_path='searchform', methods=['get'], permission_classes=[LimonPermission,])
	def get_searchform(self, request, *args, **kwargs):
		queryset = search_filter(self.get_queryset(), request)
		serializer = AsistenciaSerializer(queryset, many=True)
		return Response(serializer.data)

	@list_route(url_path='searchformper', methods=['get'], permission_classes=[LimonPermission,])
	def get_searchformper(self, request, *args, **kwargs):
		# queryset = self.get_queryset().exclude(persona__persona_asistencias__persona_id__isnull=False).distinct()
		queryset = self.get_queryset().distinct()
		queryset = search_filter(queryset, request)
		serializer = AsistenciaSerializer(queryset, many=True)
		return Response(serializer.data)
