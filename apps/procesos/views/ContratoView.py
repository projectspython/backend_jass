from rest_framework import serializers, viewsets
from rest_framework import permissions

from apps.procesos.models.Contrato import Contrato
from apps.procesos.models.ContratoServicio import ContratoServicio
from apps.procesos.models.ComprobConfig import ComprobConfig
from apps.procesos.models.CronogramaPago import CronogramaPago
from apps.procesos.models.EstadoCronograma import EstadoCronograma
from apps.procesos.models.MotivoMovimientoCuenta import MotivoMovimientoCuenta
from apps.procesos.serializers.Contrato import ContratoSerializer, ContratoPrintSerializer, ContratoBasicSerializer
from apps.procesos.serializers.Contrato import ContratoSerializer, ContratoReciboSerializer, ContratoRecGenerarSerializer
from apps.procesos.always.GenerarCodigo import GenerarCodigo
from apps.comun.always.Pagination import LimonPagination
from rest_framework.filters import SearchFilter, OrderingFilter
from django.db.models.deletion import ProtectedError

from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.procesos.Permissions.Contrato import ContratoPermissions as LimonPermission
from apps.segur.always.Permission import PermLimonList
from apps.comun.always.SearchFilter import search_filter, keys_add_none
from django_filters.rest_framework import DjangoFilterBackend
import django_filters
import time
from apps.comun.always.InfoModels import PersonaNaturalInfo, PersonaJuridicaInfo
from apps.comun.always.FileUpload import Conversiones
from apps.comun.models.PersonaContacto import PersonaContacto
from apps.comun.always.Fechas import Fechas
from apps.comun.models.Persona import Persona
from apps.comun.models.Entidad import Entidad
from apps.comun.always.always import cargos_entidad
from apps.procesos.models.ContratoFirma import ContratoFirma
from apps.comun.always.GenerarCodigo import GenerarCodigo as GenerarCodigoComun

class ContratoViewSet(LimonPagination, viewsets.ModelViewSet):
	queryset = Contrato.objects.all()
	permission_classes = [permissions.IsAuthenticated, PermLimonList, ]
	serializer_class = ContratoSerializer
	filter_backends = (SearchFilter,OrderingFilter,)
	search_fields = ('persona__nombre_completo', 'numero',)
	ordering_fields = '__all__'

	
	@list_route(url_path='add', methods=['post'], permission_classes=[LimonPermission,])
	def get_add(self, request, *args, **kwargs):
		data = request.data
		fecha_actual = time.strftime("%Y-%m-%d")
		compconf = ComprobConfig.objects.filter(entidad_id=data['entidad_id'], 
			comprobante__codigo='COPRESAP', estado='1').values('id').first()

		cont = keys_add_none(data, 'entidad_id,persona_id,forma_cobranza,tipo_uso_servicio_id,predio_id,comentario,fecha_contrato')
		cont['numero'] = GenerarCodigo.contrato(data['entidad_id'])
		cont['usuario_id'] = request.user.persona_id
		cont['comprob_config_id'] = compconf['id']
		cont['estado'] = '2'

		con = Contrato.objects.create(**cont)

		# inscrip = data['inscrip']
		# serv_ = {'servicio_id':inscrip['id'], 'contrato_id':con.id, 'precio':inscrip['precio']}
		# serv__ = ContratoServicio.objects.create(**serv_)

		# if serv__:
		# 	estado_cron = EstadoCronograma.objects.filter(codigo='REG').values('id').first()
		# 	motiv = MotivoMovimientoCuenta.objects.filter(codigo='SERINS',estado='1').values('id','nombre').first()
		# 	cron_ = {
		# 			'entidad_id':data['entidad_id'],
		# 			'contrato_id':con.id,
		# 			'persona_id':data['persona_id'],
		# 			'motivo_movimiento_cuenta_id':motiv['id'],
		# 			'descripcion':'Contrato N° %s'%(cont['numero']),
		# 			'monto':inscrip['precio'],
		# 			'fecha_cobranza':fecha_actual,
		# 			'usuario_id':cont['usuario_id'],
		# 			'estado_cronograma_id':estado_cron['id']
		# 		}
		# 	cron = CronogramaPago.objects.create(**cron_)

		for p in data['servicios']:
			if 'select' in p and p['select']:
				if p['codigo'] == 'ServAguaPo' and data['forma_cobranza'] == 'CME':
					p['precio'] = p['precio_m3']
				p['contrato_id'] = con.id
				p['servicio_id'] = p['id']
				servi = keys_add_none(p, 'contrato_id,servicio_id,precio')
				serv = ContratoServicio.objects.create(**servi)

		# Registrar los que firmarán los documentos
		cargos_entidad_ = cargos_entidad(data['entidad_id'], ['PRESI','SECRE','TESO'], data['fecha_contrato'])
		for p in cargos_entidad_:
			p['contrato_id'] = con.id
			firm = keys_add_none(p, 'contrato_id,persona_id,cargo_id')
			firm_ = ContratoFirma.objects.create(**firm)

		return Response(self.get_serializer(con).data)

	@list_route(url_path='edit', methods=['get'], permission_classes=[LimonPermission,])
	def get_edit(self, request):
		id = request.query_params.get('id', None)
		model = Contrato.objects.get(pk=id)
		return Response(self.get_serializer(model).data)


	@list_route(url_path='update', methods=['put'], permission_classes=[LimonPermission,])
	def get_update(self, request, pk=None):
		model = Contrato.objects.get(pk=request.data['id'])
		serializer = self.get_serializer(model, data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data)
		else:
			return Response(serializer.errors,
							status=status.HTTP_400_BAD_REQUEST)

	@list_route(url_path='print', methods=['get'], permission_classes=[])
	def get_print(self, request, pk=None):
		id = request.query_params.get('id', None)
		model = Contrato.objects.get(pk=id)
		serializer = ContratoPrintSerializer(model)
		
		return Response(serializer.data)


	@list_route(url_path='delete', methods=['delete'], permission_classes=[LimonPermission,])
	def get_delete(self, request, *args, **kwargs):
		try:
			print(request)
			id = request.query_params.get('id', None)
			model = Contrato.objects.get(pk=id).delete()
			return Response({'status':True, 'id':id})
		except ProtectedError as e:
			return Response(e.errors, status=status.HTTP_400_BAD_REQUEST)

	@list_route(url_path='searchform', methods=['get'], permission_classes=[LimonPermission,])
	def get_searchform(self, request, *args, **kwargs):
		queryset = search_filter(self.get_queryset(), request)
		serializer = ContratoBasicSerializer(queryset, many=True)
		return Response(serializer.data)

	@list_route(url_path='pdfcontrato', methods=['post'], permission_classes=[])
	def get_pdfcontrato(self, request, *args, **kwargs):
		# entidad_id = request.query_params.get('entidad_id', None)
		# persona_id = request.query_params.get('persona_id', None)
		data = request.data
		
		ent = Entidad.objects.get(pk=data['entidad_id'])
		data_ent = PersonaJuridicaInfo(request, ent.persona_id)


		if ent.persona.foto_logo:
			logo_conver_ent = Conversiones.to_base64((ent.persona.foto_logo.url).replace('/limon/',''))
		else:
			logo_conver_ent = Conversiones.to_base64('storage/comun/personas/fotos/sinfoto.png')


		ent_contacto = ''
		contacto = PersonaContacto.objects.filter(
			persona_id=ent.persona_id,tipo_contacto__codigo='TEL').values(
			'valor_contacto','etiqueta_contacto__tiene_cod_ubigeo','ubigeo__codigo_telefono').first()

		if contacto:
			if contacto['etiqueta_contacto__tiene_cod_ubigeo'] == 'SI':
				ent_contacto = '(%s) %s'%(contacto['ubigeo__codigo_telefono'], contacto['valor_contacto'])
			else:
				ent_contacto = contacto['valor_contacto']

			ent_contacto = '- Telf. %s'%(ent_contacto)


		tip_doc_ent = ''
		nro_doc_ent = ''
		if len(data_ent['documentos'])>0:
			ent_doc = data_ent['documentos'][0]
			tip_doc_ent = ent_doc['tipo_documento']['nombre']
			nro_doc_ent = ent_doc['numero']

		dir_ent = ''
		dir_ent_dist = ''
		if len(data_ent['direcciones'])>0:
			ent_dir = data_ent['direcciones'][0]
			dir_ent = '%s'%(ent_dir['direccion_detalle'])
			dir_ent_dist = data_ent['direcciones'][0]['ubigeo']['nombre']

		
		model = Contrato.objects.get(pk=data['contrato_id'])
		cont = ContratoPrintSerializer(model).data
		cont_persona_nombre = cont['persona']['nombre_completo'].upper()
		cont_numero = cont['numero']
		firmantes = cont['contrato_contrato_firma']
		new_firmantes = []
		contador = 0
		firmas_colum1 = []
		firmas_colum2 = []
		for p in firmantes:
			contador += 1
			fir_tip_doc = ''
			fir_doc = p['persona']['persona_persona_documentos']
			if len(fir_doc)>0:
				fir_tip_doc = '%s: %s'%(fir_doc[0]['tipo_documento']['nombre'], fir_doc[0]['numero'])

			fir = [   
					{'text': p['cargo']['nombre'].upper(), 'style': 'tableThDatos', 'alignment': 'right', },
					{'text': p['persona']['nombre_completo'].upper(), 'style': 'tableTdDatos', 'alignment': 'left'},
					{'text': fir_tip_doc, 'style': 'tableTdDatos', 'alignment': 'center'},
				]
			new_firmantes.append(fir)

			if contador == 1:
				firmas_colum1.append({	
						'margin': [0, 0, 0, 0],
						'alignment': 'center',
						'columns': [
							{
								'text': p['persona']['nombre_completo'].upper(),
								'style': 'cont_detalle_peq',
								'alignment': 'center',
								'margin': [0, 0, 0, 0],
								'width': 250
							},
							{
								'text': cont_persona_nombre,
								'style': 'cont_detalle_peq',
								'alignment': 'center',
								'margin': [0, 0, 0, 0],
								'width': 250
							}
						]
					})

				firmas_colum1.append({	
						'margin': [0, 0, 0, 0],
						'alignment': 'center',
						'columns': [
							{
								'text': p['cargo']['nombre'].upper(),
								'style': 'cont_detalle',
								'alignment': 'center',
								'margin': [0, 0, 0, 0],
								'width': 250
							},
							{
								'text': 'USUARIO',
								'style': 'cont_detalle',
								'alignment': 'center',
								'margin': [0, 0, 0, 0],
								'width': 250
							}
						]
					})


		if len(firmantes)>2:
			firmas_colum2.append({	
						'margin': [0, 0, 0, 0],
						'alignment': 'center',
						'columns': [
							{
								'text': firmantes[1]['persona']['nombre_completo'].upper(),
								'style': 'cont_detalle_peq',
								'alignment': 'center',
								'margin': [0, 0, 0, 0],
								'width': 250
							},
							{
								'text': firmantes[2]['persona']['nombre_completo'].upper(),
								'style': 'cont_detalle_peq',
								'alignment': 'center',
								'margin': [0, 0, 0, 0],
								'width': 250
							}
						]
					})

			firmas_colum2.append({	
					'margin': [0, 0, 0, 0],
					'alignment': 'center',
					'columns': [
						{
							'text': firmantes[1]['cargo']['nombre'].upper(),
							'style': 'cont_detalle',
							'alignment': 'center',
							'margin': [0, 0, 0, 0],
							'width': 250
						},
						{
							'text': firmantes[2]['cargo']['nombre'].upper(),
							'style': 'cont_detalle',
							'alignment': 'center',
							'margin': [0, 0, 0, 0],
							'width': 250
						}
					]
				})


		
		pers_docs = cont['persona']['persona_persona_documentos']
		
		new_pers_docs = []
		for p in pers_docs:
			per_doc = [   
					{'text': p['tipo_documento']['nombre'], 'style': 'tableThDatos', 'alignment': 'right', },
					{'text': p['numero'], 'style': 'tableTdDatos', 'alignment': 'left'},
				]
			new_pers_docs.append(per_doc)

		data_resul = []

		pers_dir = cont['persona']['persona_direcciones']
		new_pers_dir = []
		if len(pers_dir)>0:
			per_d = pers_dir[0]
			per_dir = [   
						{'text': per_d['tipo_direccion']['nombre'].upper(), 'style': 'tableThDatos', 'alignment': 'right', },
						{'text': '%s - %s'%(per_d['direccion_detalle'], per_d['ubigeo']['nombre']), 'style': 'tableTdDatos', 'alignment': 'left'},
					]
			new_pers_dir.append(per_dir)

		pers_cont = cont['persona']['persona_persona_contactos']
		new_pers_cont = []
		for p in pers_cont:
			if p['tipo_contacto']['codigo'] == 'RSO' and 'cuenta_contacto' in p and p['cuenta_contacto']:
				tipo_contacto = p['cuenta_contacto']['nombre']
			else:
				tipo_contacto = p['tipo_contacto']['nombre']

			if 'etiqueta_contacto' in p and p['etiqueta_contacto'] and p['etiqueta_contacto']['tiene_cod_ubigeo'] == 'SI' and p['ubigeo']:
				valor_contacto = '(%s) %s'%(p['ubigeo']['codigo_telefono'], p['valor_contacto'])
			else:
				valor_contacto = p['valor_contacto']


			per_con = [   
					{'text': tipo_contacto.upper(), 'style': 'tableThDatos', 'alignment': 'right', },
					{'text': valor_contacto, 'style': 'tableTdDatos', 'alignment': 'left'},
				]
			new_pers_cont.append(per_con)

		predio_dir = cont['predio']['predio_direcciones']
		predio_dir_dist = ''
		predio_dir_ref1 = ''
		predio_dir_ref2 = ''
		predio_punto_lat = ''
		predio_punto_long = ''
		if len(predio_dir)>0:
			predio_dir_detalle = predio_dir[0]['direccion_detalle']
			predio_dir_dist = '%s/%s'%(predio_dir[0]['ubigeo']['nombre'], predio_dir[0]['ubigeo']['padre']['nombre'])
			predio_dir_ref1 = predio_dir[0]['referencia1']
			predio_dir_ref2 = predio_dir[0]['referencia2']
			if predio_dir[0]['punto_lat']:
				predio_punto_lat = predio_dir[0]['punto_lat']
			if predio_dir[0]['punto_long']:
				predio_punto_long = predio_dir[0]['punto_long']
		
		predio_zona_nombre = cont['predio']['zona']['nombre']
		predio_estado_predio = cont['predio']['estado_predio']['nombre']

		monto_ins = "%.2f" %cont['monto_ins']

		servicios = cont['servicios']
		new_servicios = []
		total = 0
		otros_serv = ''
		for p in servicios:
			total += float(p['precio'])
			precio_serv_com = ''
			if cont['forma_cobranza'] == 'CME' and p['servicio__codigo'] == 'ServAguaPo':
				precio_serv_com = 'x M³ consumidos'

			if p['servicio__codigo'] != 'ServAguaPo':
				otros_serv +=" + %s"%(p['servicio__nombre'])

			serv = [   
				{'text': p['servicio__nombre'], 'style': 'tableTdDatos', 'alignment': 'right', 'colSpan': 2},
				'',
				{'text': 'S/. %.2f %s'%(p['precio'], precio_serv_com), 'style': 'tableTdDatos', 'alignment': 'left', 'colSpan': 2},
				''
			]
			new_servicios.append(serv)

		if cont['forma_cobranza'] == 'CME':
			total_ = 'Se calculará según consumo en M³ %s'%(otros_serv)
		else:
			total_ = 'S/. %.2f'%(total)
		

		total_serv = [   
						{'text': 'PAGO MENSUAL', 'style': 'tableThDatos', 'alignment': 'right', 'colSpan': 2},
						'',
						{'text': total_, 'style': 'tableTdDatosNeg', 'alignment': 'left', 'colSpan': 2},
						''
					]
		new_servicios.append(total_serv)

		fecha_cont = Fechas.dia_mes_anio_let(cont['fecha_contrato'])

		lineas_firm = {	
						'margin': [0, 80, 0, 0],
						'alignment': 'center',
						'columns': [
							{
								'text': '____________________________',
								'style': 'cont_detalle',
								'alignment': 'center',
								'margin': [0, 0, 0, 0],
								'width': 250
							},
							{
								'text': '____________________________',
								'style': 'cont_detalle',
								'alignment': 'center',
								'margin': [0, 0, 0, 0],
								'width': 250
							}
						]
					},

		for p in [1]:
			obj_rec = [{
						# 'pageBreak': 'before',
						'columns': [
							{
								'image': logo_conver_ent,
								'width': 50,
								'height': 50,
								'margin': [-5, -20, 0, 0]
							},
							[
								{
								   'text': '%s - %s'%(data_ent['nombre_completo'].upper(), dir_ent_dist.upper()),
									'style': 'header',
									'alignment': 'center',
									'color':'#1565c0',
									'margin': [-30, -20, -10, 0]
								},
								{
								   'text': '_______________________________________________________________',
									'style': 'header',
									'alignment': 'center',
									'color':'#1565c0',
									'margin': [-20,-14,0,0]
								},
								{
									'text': 'CONTRATO DE PRESTACIÓN DE SERVICIOS DE AGUA POTABLE',
									'style': 'header_basic2',
									'alignment': 'left',
									'color':'#2196f3',
									'margin': [-8, 8, -10, 0]
								},
							],
						]
					},
					{
						'columns': [
							{
								'text': 'N° %s'%(cont_numero),
								'style': 'nro_contrato',
								'alignment': 'right',
								'color':'#d00c0c',
								'margin': [0, -12, 0, 0]
							}
						]
					},
					{
						'text': 'Conste por el presente contrato que celebramos de una parte la %s, con %s N° %s, con domicilio fiscal en el %s - %s'
						', la cual está debidamente registrada en la Partida electrónica N° 11009045 representada por el '
						'CONCEJO DIRECTIVO quien en adelante se le denomina EL SERVIDOR, de la otra parte en calidad de USUARIO.'
						%(data_ent['nombre_completo'].upper(), tip_doc_ent, nro_doc_ent, dir_ent, dir_ent_dist),
						'style': 'cont_detalle',
						'alignment': 'justify',
						# 'color':'#d00c0c',
						'margin': [0, 6, 0, 0]
					},
					{
						'text': 'I. DATOS DEL SERVIDOR:',
						'style': 'parte_titulo',
						'alignment': 'left',
						# 'color':'#d00c0c',
						'margin': [0, 6, 0, 2]
					}, 
					{
						'style': 'tableDatos',
						'table': {
							'widths': [100,'*',80],
							'body': [
								[   
									{'text': 'CARGO', 'style': 'tableHeaderDatos', 'alignment': 'center', },
									{'text': 'NOMBRES Y APELLIDOS', 'style': 'tableHeaderDatos', 'alignment': 'center'},
									{'text': 'DOC. IDENTIDAD', 'style': 'tableHeaderDatos', 'alignment': 'center'},
								],
								*new_firmantes,							
							]
						}
					},
					{
						'text': 'II. DATOS DEL USUARIO:',
						'style': 'parte_titulo',
						'alignment': 'left',
						# 'color':'#d00c0c',
						'margin': [0, 10, 0, 2]
					},
					{
						'style': 'tableDatos',
						'table': {
							'widths': [100,'*'],
							'body': [
								[   
									{'text': 'NOMBRE COMPLETO', 'style': 'tableThDatos', 'alignment': 'right', },
									{'text': cont_persona_nombre, 'style': 'tableTdDatosNeg', 'alignment': 'left'},
								],
								*new_pers_docs,
								*new_pers_dir,
								*new_pers_cont,
								[   
									{'text': 'DIRECCIÓN DEL PREDIO', 'style': 'tableThDatos', 'alignment': 'right', },
									{'text': predio_dir_detalle, 'style': 'tableTdDatosNeg', 'alignment': 'left'},
								],
								[   
									{'text': 'ZONA', 'style': 'tableThDatos', 'alignment': 'right', },
									{'text': predio_zona_nombre, 'style': 'tableTdDatos', 'alignment': 'left'},
								],
								[   
									{'text': 'DISTRITO/PROVINCIA', 'style': 'tableThDatos', 'alignment': 'right', },
									{'text': predio_dir_dist, 'style': 'tableTdDatos', 'alignment': 'left'},
								],
								[   
									{'text': 'REFERENCIA DERECHA', 'style': 'tableThDatos', 'alignment': 'right', },
									{'text': predio_dir_ref1, 'style': 'tableTdDatos', 'alignment': 'left'},
								],
								[   
									{'text': 'REFERENCIA IZQUIERDA', 'style': 'tableThDatos', 'alignment': 'right', },
									{'text': predio_dir_ref2, 'style': 'tableTdDatos', 'alignment': 'left'},
								],
								[   
									{'text': 'PUNTOS GPS', 'style': 'tableThDatos', 'alignment': 'right', },
									{'text': '[%s, %s]'%(predio_punto_lat, predio_punto_long), 'style': 'tableTdDatos', 'alignment': 'left'},
								],
								[   
									{'text': 'ESTADO DEL PREDIO', 'style': 'tableThDatos', 'alignment': 'right', },
									{'text': predio_estado_predio, 'style': 'tableTdDatos', 'alignment': 'left'},
								],	
							]
						}
					},
					{
						'text': 'III. COSTO DE INSCRIPCIÓN Y SERVICIOS:',
						'style': 'parte_titulo',
						'alignment': 'left',
						# 'color':'#d00c0c',
						'margin': [0, 10, 0, 2]
					},
					{
						'style': 'tableDatos',
						'table': {
							'widths': [100,140,100,'*'],
							'body': [
								[   
									{'text': 'COSTO DE INSCRIPCIÓN', 'style': 'tableThDatos', 'alignment': 'right', },
									{'text': 'S/. %s'%(monto_ins), 'style': 'tableTdDatos', 'alignment': 'left', 'colSpan': 3},
									'',
									''
								],
								[   
									{'text': 'TIPO DE USO SERVICIO', 'style': 'tableThDatos', 'alignment': 'right', },
									{'text': cont['t_uso_ser_nombre'].upper(), 'style': 'tableTdDatos', 'alignment': 'left'},
									{'text': 'CONTROL', 'style': 'tableThDatos', 'alignment': 'right', },
									{'text': cont['forma_cobranza_nombre'].upper(), 'style': 'tableTdDatos', 'alignment': 'left'},
								],
								[   
									{'text': 'COBRANZA MENSUAL DE SERVICIOS', 'style': 'tableThDatos', 
									'alignment': 'center','colSpan': 4 },
									'','',''
								],
								*new_servicios,
								[   
									{'text': 'SUSTENTACIÓN COSTO', 'style': 'tableThDatos', 'alignment': 'right', },
									{'text': cont['comentario'], 'style': 'tableTdDatos', 'alignment': 'left', 'colSpan': 3},
									'',
									''
								],
								
							]
						}
					},
					{
						'text': 'IV. CLAUSULAS ADICIONALES:',
						'style': 'parte_titulo',
						'alignment': 'left',
						# 'color':'#d00c0c',
						'margin': [0, 10, 0, 2]
					},
					
					{
				      'ul': [
					        {
								'text': 'La %s, es una institución descentralizada, autónoma con personería jurídica de derecho público; '
								'se rige por la Ley General de Servicios de saneamiento, Ley N° 26338. Así mismo, considera la Resolución Ministerial '
								'N° 205-2010-Vivienda y la Resolución Ministerial N° 207-2010-Vivienda. También considera el Decreto Supremo N° 203-2005-VIVIENDA.'
								%(data_ent['nombre_completo'].upper()),
								'style': 'clausula',
								'alignment': 'justify',
								# 'color':'#d00c0c',
								'margin': [0, 2, 0, 0]
							},
							{
								'text': 'El tratamiento de agua se realiza mensualmente, y lo cual no asegura para el consumo directo '
										'se recomienda hervir o purificar para: el consumo, lavado de frutas y verduras, según el Ministerio de Salud.',
								'style': 'clausula',
								'alignment': 'justify',
								# 'color':'#d00c0c',
								'margin': [0, 2, 0, 0]
							},
							{
								'text': 'Los pagos deben ser puntuales a la fecha establecida, ya que por deuda de (02) dos meses consecutivos '
										'se suspende el servicio y se pagará un monto por reconexión.',
								'style': 'clausula',
								'alignment': 'justify',
								# 'color':'#d00c0c',
								'margin': [0, 2, 0, 0]
							},
							{
								'text': 'Cada usuario tiene la obligación de cuidar el agua dentro de sus vivienda '
										'y ser vigilante ante cualquier usuario que no esté dando un buen uso del agua. '
										'Y que al desperdiciar el agua será multado y si es grave será suspendido de su inscripción '
										'sin reclamo alguno. La conexión es exclusiva para el usuario y no para compartir, sea el caso '
										'será multado con el 50% de su inscripción. Si el servicio es doméstico está prohibido para el lavado '
										'de vehículos y piscinas.',
								'style': 'clausula',
								'alignment': 'justify',
								# 'color':'#d00c0c',
								'margin': [0, 2, 0, 0]
							},
							{
								'text': 'Los materiales a utilizar para la reconexión serán cubiertos por el usuario como: '
										'caja de cemento, llaves, tubos y otros.',
								'style': 'clausula',
								'alignment': 'justify',
								# 'color':'#d00c0c',
								'margin': [0, 2, 0, 0]
							},
							{
								'text': 'Los costos por inscripción, mensualidades, multas, penalidades son variables establecidos por el Concejo Directivo.',
								'style': 'clausula',
								'alignment': 'justify',
								# 'color':'#d00c0c',
								'margin': [0, 2, 0, 0]
							},
				      	]
				    },
				    {
						'text': [
							{'text': 'Fecha: ','bold': True},
							'En el día %s del mes de %s del año %s.'%(fecha_cont['dia'], 
								fecha_cont['nombre_mes'], fecha_cont['anio']),
						],
						'margin': [0, 20, 0, 10],
						'alignment': 'right',
						'style': 'cont_detalle',
					},
					lineas_firm,
					*firmas_colum1,
					lineas_firm,
					*firmas_colum2
				]
			data_resul.append(obj_rec)
		
		# data_resul = []
		# # for p in [1,2,3,4]:
		# # 	data_resul.append(obj_rec)

		# i = 1
		# while i <= 1:
		#   data_resul.append(obj_rec)
		#   i += 1



		result  = {
			'pageSize': 'A4',
			'pageOrientation': 'portrait', # portrait, landscape
			'content': [
				*data_resul
				# obj_rec[0],
				# obj_rec[1],
				# obj_rec[2],
				# obj_rec[3],
				# obj_rec[4],
				# obj_rec[5],
				# obj_rec[6]
			],
			'styles': {
				'header': {
					'fontSize': 16,
					'bold': True
				},
				'header_basic1': {
					'fontSize': 10,
					'bold': False
				},
				'header_basic2': {
					'fontSize': 11,
					'bold': True
				},
				'bigger': {
					'fontSize': 15,
					'italics': True
				},
				'tableExample': {
					'margin': [0, 5, 0, 15],
				},
				'tableDatos': {
					'margin': [0, 0, 0, 0],
				},
				'tableHeaderDatos':{
					'bold': True,
					'fontSize': 9,
					'color': 'white',
					'fillColor':'black'
				},
				'tableThDatos':{
					'bold': False,
					'fontSize': 9,
					'color': 'black',
					'fillColor':'#eee'
				},
				'tableTdDatos':{
					'bold': False,
					'fontSize': 9,
				},
				'tableTdDatosNeg':{
					'bold': True,
					'fontSize': 9,
				},
				'recomendaciones':{
					'fontSize': 9
				},
				'parte_titulo':{
					'fontSize': 10,
					'bold': True
				},
				'detalle_fact_ant':{
					'fontSize': 8
				},
				'cont_detalle':{
					'bold': False,
					'fontSize': 11
				},
				'cont_detalle_peq':{
					'bold': False,
					'fontSize': 9
				},
				'clausula':{
					'bold': False,
					'fontSize': 10
				},
				'detalle_total':{
					'bold': True,
					'fontSize': 14
				},
				'detalle_total_tes':{
					'bold': False,
					'fontSize': 14
				},
				'detalle_total_numero':{
					'bold': True,
					'fontSize': 11
				},
				'detalle_total_numero_tes':{
					'bold': False,
					'fontSize': 11
				},
				'tableHeader':{
					'bold': True
				},
				'nombre_completo':{
					'bold': False,
					'fontSize': 9
				},
				'documentos':{
					'bold': False,
					'fontSize': 7
				},
				'direcciones':{
					'bold': False,
					'fontSize': 8
				},
				'header_cronog':{
					'bold': False,
					'fontSize': 11
				},
				'doc_ent':{
					'bold': True,
					'fontSize': 8
				},
				'nro_contrato':{
					'bold': True,
					'fontSize': 11
				},
				'horario_titulo':{
					'bold': True,
					'fontSize': 11
				},
				'horario':{
					'bold': False,
					'fontSize': 10
				},
				'tesoreria':{
					'bold': True,
					'fontSize': 14
				},
				'recibo_nro_tes':{
					'bold': True,
					'fontSize': 11
				},
				'moneda':{
					'bold': False,
					'fontSize': 7
				},
				'tipo_uso_servicio_nombre':{
					'bold': False,
					'fontSize': 7
				}
			},
			'defaultStyle': {
				'columnGap': 20
			}
		}
		return Response(result)

	@list_route(url_path='pdfpago', methods=['post'], permission_classes=[])
	def get_pdfpago(self, request, *args, **kwargs):
		# entidad_id = request.query_params.get('entidad_id', None)
		# persona_id = request.query_params.get('persona_id', None)
		data = request.data
		
		ent = Entidad.objects.get(pk=data['entidad_id'])
		data_ent = PersonaJuridicaInfo(request, ent.persona_id)


		if ent.persona.foto_logo:
			logo_conver_ent = Conversiones.to_base64((ent.persona.foto_logo.url).replace('/limon/',''))
		else:
			logo_conver_ent = Conversiones.to_base64('storage/comun/personas/fotos/sinfoto.png')


		ent_contacto = ''
		contacto = PersonaContacto.objects.filter(
			persona_id=ent.persona_id,tipo_contacto__codigo='TEL').values(
			'valor_contacto','etiqueta_contacto__tiene_cod_ubigeo','ubigeo__codigo_telefono').first()

		if contacto:
			if contacto['etiqueta_contacto__tiene_cod_ubigeo'] == 'SI':
				ent_contacto = '%s - %s'%(contacto['ubigeo__codigo_telefono'], contacto['valor_contacto'])
			else:
				ent_contacto = contacto['valor_contacto']

			ent_contacto = '- Teléfono: %s'%(ent_contacto)


		tip_doc_ent = ''
		nro_doc_ent = ''
		if len(data_ent['documentos'])>0:
			ent_doc = data_ent['documentos'][0]
			tip_doc_ent = ent_doc['tipo_documento']['nombre']
			nro_doc_ent = ent_doc['numero']

		dir_ent = ''
		dir_ent_dist = ''
		if len(data_ent['direcciones'])>0:
			ent_dir = data_ent['direcciones'][0]
			dir_ent = '%s'%(ent_dir['direccion_detalle'])
			dir_ent_dist = data_ent['direcciones'][0]['ubigeo']['nombre']

		mi_codigo_barras = GenerarCodigoComun.codigo_barras_base64('00000123')

		data_resul = []
		for p in [1]:
			obj_rec = [
						# 'pageBreak': 'before',
						{
							'style': 'tableDatos',
							'table': {
								'widths': ['*'],
								'body': [
									[   
										{'text': 'INFORMACIÓN DEL PAGO', 'style': 'tableThDatos', 'alignment': 'center', },
									],
								]
							}
						},
						{
							'columns':[
								[	

									{
									   'text': '\n%s'%('MOTIVO PAGO: Inscripción al servicio'),
										'style': 'detalle_info',
										'alignment': 'left',
										'margin':[0,0,-60,0]
									},
									{
									   'text': [ 
									   		'PERSONA: ',
									   		{ 'text':'%s'%('PERSY QUIROZ MENOR'), 'bold': True }
									   	],
										'style': 'detalle_info',
										'alignment': 'left',
										'margin':[0,0,-60,0]
									},
									{
										'text': 'DNI: 48160622',
										'style': 'detalle_info',
										'alignment': 'left',
										'margin':[0,0,-60,0]
									},
									{
										'text': 'DETALLE: Pago por derecho de inscripción',
										'style': 'detalle_info',
										'alignment': 'left',
										'margin':[0,0,-60,0]
									},
									{
										'text': [ 
									   		'IMPORTE: ',
									   		{ 'text':'120.00', 'bold': True }
									   	],
										'style': 'detalle_info',
										'alignment': 'left',
										'margin':[0,0,-60,0]
									},
									{
										'text': 'MONEDA: Sol',
										'style': 'detalle_info',
										'alignment': 'left',
										'margin':[0,0,-60,0]
									},
									{
										'text': 'MEDIO DE PAGO: Efectivo',
										'style': 'detalle_info',
										'alignment': 'left',
										'margin':[0,0,-60,0]
									},
								],
								[	
									{
										'text': '\nNRO. OPER.: 00000123',
										'style': 'detalle_info_min',
										'alignment': 'left',
										'margin':[30,0,-40,0]
									},
									{
										'text': 'FECHA: 20/08/2018',
										'style': 'detalle_info_min',
										'alignment': 'left',
										'margin':[30,0,-40,0]
									},
									{
										'text': 'HORA: 20:10:15',
										'style': 'detalle_info_min',
										'alignment': 'left',
										'margin':[30,0,-40,0]
									},
									{
										'text': 'CAJERO: MARÍA SOLIS GUEVARA',
										'style': 'detalle_info_min',
										'alignment': 'left',
										'margin':[30,0,-40,0]
									},
								],

								[
									{
										'image': mi_codigo_barras,
										'width': 140,
										'height': 50,
										'margin': [30, 10, -20, 0],
										'alignment':'center'
									},
									{
										'text': '00000123',
										'style': 'nro_operacion',
										'alignment': 'center',
										'margin': [30, 0, -20, 0],
									},
									{
										'text': 'NÚMERO DE OPERACIÓN',
										'style': 'nro_operacion_det',
										'alignment': 'center',
										'margin': [30, 0, -20, 0],
									},
								]
							]
						},
						{
							'style': 'tableDatosCron',
							'table': {
								'widths': [15,'*','*',40,40,58],
								'body': [
									[   
										{'text': 'CRONOGRAMAS PAGADOS', 'style': 'tableThDatosCron', 'alignment': 'center', 'colSpan':6},
										'','','','',''
									],
									[   
										{'text': '#', 'style': 'tableThDatosDetCron', 'alignment': 'center', 'fillColor':"#eee"},
										{'text': 'MOTIVO CRONOGRAMA', 'style': 'tableThDatosDetCron', 'alignment': 'center', 'fillColor':"#eee"},
										{'text': 'DESCRIPCIÓN', 'style': 'tableThDatosDetCron', 'alignment': 'center', 'fillColor':"#eee"},
										{'text': 'MONTO', 'style': 'tableThDatosDetCron', 'alignment': 'center', 'fillColor':"#eee"},
										{'text': 'IMPORTE', 'style': 'tableThDatosDetCron', 'alignment': 'center', 'fillColor':"#eee"},
										{'text': 'ESTADO', 'style': 'tableThDatosDetCron', 'alignment': 'center', 'fillColor':"#eee"},
									],
									[   
										{'text': '1', 'style': 'tableThDatosDetCron', 'alignment': 'center', },
										{'text': 'Inscripción al servicio', 'style': 'tableThDatosDetCron', 'alignment': 'left', },
										{'text': 'Contrato N° 000020', 'style': 'tableThDatosDetCron', 'alignment': 'left', },
										{'text': '550.00', 'style': 'tableThDatosDetCron', 'alignment': 'right',},
										{'text': '550.00', 'style': 'tableThDatosDetCron', 'alignment': 'right', },
										{'text': 'Pagado', 'style': 'tableThDatosDetCron', 'alignment': 'center', },
									],
									[   
										{'text': '2', 'style': 'tableThDatosDetCron', 'alignment': 'center', },
										{'text': 'Inscripción al servicio', 'style': 'tableThDatosDetCron', 'alignment': 'left', },
										{'text': 'Contrato N° 000020', 'style': 'tableThDatosDetCron', 'alignment': 'left', },
										{'text': '550.00', 'style': 'tableThDatosDetCron', 'alignment': 'right',},
										{'text': '550.00', 'style': 'tableThDatosDetCron', 'alignment': 'right', },
										{'text': 'Pagado', 'style': 'tableThDatosDetCron', 'alignment': 'center', },
									],
								]
							}
						},
				]
			data_resul.append(obj_rec)

		result  = {
			'pageSize': 'A5',
			'pageOrientation': 'landscape', # portrait, landscape
			'header': {
				'columns': [
					{ 	
						'text': '%s - %s: %s\n%s - %s %s'%(data_ent['nombre_completo'].upper(),
							tip_doc_ent, nro_doc_ent, 
							dir_ent, dir_ent_dist, ent_contacto), 
						'alignment': 'center',
						# 'color':'#707070',
						'style':'header_page',
						'margin': [0,7,0,0]
					}
				],
			},
			'footer': {
				'columns': [
					{ 	
						'text': '\n\nFECHA Y HORA DE CONSULTA: 20/08/2018 20:59.10', 
						'alignment': 'center',
						'style':'header_page',
						'margin': [0,7,0,0]
					}
				],
			},
			'content': [
				*data_resul
			],
			'styles': {
				'header': {
					'fontSize': 13,
					'bold': True
				},
				'detalle_info': {
					'fontSize': 10,
					'bold': False
				},
				'detalle_info_min':{
					'fontSize': 8,
					'bold': False
				},
				'header_page': {
					'fontSize': 6,
					'bold': False
				},
				'tableDatos': {
					'margin': [0, 0, 0, 0],
				},
				'tableThDatos':{
					'bold': True,
					'fontSize': 12,
					# 'fillColor':'#eee'
				},
				'tableDatosCron': {
					'margin': [0, 10, 0, 0],
				},
				'tableThDatosCron':{
					'bold': False,
					'fontSize': 10,
					# 'fillColor':'#eee'
				},
				'tableThDatosDetCron':{
					'bold': False,
					'fontSize': 8,
				},
				'nro_operacion':{
					'bold': False,
					'fontSize': 14,
				},
				'nro_operacion_det':{
					'bold': False,
					'fontSize': 7,
				}
			},
			'defaultStyle': {
				'columnGap': 20
			}
		}
		return Response(result)

	



class ContratoRecFilterSet(django_filters.FilterSet):
	exclude_mes_recibo = django_filters.CharFilter(field_name='contrato_recibos__mes_recibo_id',
		lookup_expr='in', exclude=True)
	fecha_contrato_menor = django_filters.DateFilter(field_name='fecha_contrato',
		lookup_expr='lt', exclude=False)
	class Meta:
		model = Contrato
		fields = ('forma_cobranza','entidad_id','exclude_mes_recibo','estado','fecha_contrato_menor')

class ContratoRecibosViewSet(LimonPagination, viewsets.ModelViewSet):
	queryset = Contrato.objects.all()
	permission_classes = [permissions.IsAuthenticated, PermLimonList, ]
	serializer_class = ContratoReciboSerializer
	filter_backends = (SearchFilter,OrderingFilter,DjangoFilterBackend,)
	search_fields = ('nombre', 'codigo','simbolo',)
	# filter_fields = ('forma_cobranza','entidad_id',)
	filter_class = ContratoRecFilterSet
	ordering_fields = '__all__'

	@list_route(url_path='inforecibo', methods=['get'], permission_classes=[LimonPermission,])
	def get_inforecibo(self, request, *args, **kwargs):

		id = request.query_params.get('id', None)
		model = Contrato.objects.get(pk=id)
		serializer = ContratoRecGenerarSerializer(model)
		return Response(serializer.data)