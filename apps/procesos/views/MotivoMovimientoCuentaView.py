from rest_framework import serializers, viewsets, generics
from apps.procesos.models.MotivoMovimientoCuenta import MotivoMovimientoCuenta
from rest_framework import permissions
from apps.procesos.serializers.MotivoMovimientoCuenta import MotivoMovimientoCuentaSerializer
from apps.procesos.always.Pagination import LimonPagination
from rest_framework.filters import SearchFilter, OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend
from django.db.models.deletion import ProtectedError

from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.procesos.Permissions.MotivoMovimientoCuenta import MotivoMovimientoCuentaPermissions as LimonPermission
from apps.segur.always.Permission import PermLimonList
import json
from apps.procesos.always.SearchFilter import search_filter, keys_del
from apps.comun.always.GenerarCodigo import GenerarCodigo


class MotivoMovimientoCuentaViewSet(LimonPagination, viewsets.ModelViewSet):
	queryset = MotivoMovimientoCuenta.objects.all()
	permission_classes = [permissions.IsAuthenticated, PermLimonList, ]
	serializer_class = MotivoMovimientoCuentaSerializer
	filter_backends = (SearchFilter,OrderingFilter,DjangoFilterBackend,)	
	filter_fields = ('tipo_movimiento_cuenta_id',)	
	search_fields = ('nombre', 'codigo','abreviatura',)
	ordering_fields = '__all__'

	@list_route(url_path='add', methods=['post'], permission_classes=[LimonPermission,])
	def get_add(self, request, *args, **kwargs):
		data = request.data
		data['codigo'] = GenerarCodigo.motivosmovimientocuenta()
		data = keys_del(data, 'tipo_movimiento_cuenta')
		p = MotivoMovimientoCuenta.objects.create(**data)
		return Response(self.get_serializer(p).data)

	@list_route(url_path='edit', methods=['get'], permission_classes=[LimonPermission,])
	def get_edit(self, request):
		id = request.query_params.get('id', None)
		model = MotivoMovimientoCuenta.objects.get(pk=id)
		return Response(self.get_serializer(model).data)


	@list_route(url_path='update', methods=['put'], permission_classes=[LimonPermission,])
	def get_update(self, request, pk=None):
		data = request.data
		data = keys_del(data, 'tipo_movimiento_cuenta,codigo')
		p = MotivoMovimientoCuenta.objects.filter(pk=data['id']).update(**data)
		model = MotivoMovimientoCuenta.objects.get(id=data['id'])
		return Response(self.get_serializer(model).data)

	@list_route(url_path='delete', methods=['delete'], permission_classes=[LimonPermission,])
	def get_delete(self, request, *args, **kwargs):
		try:
			id = request.query_params.get('id', None)
			model = MotivoMovimientoCuenta.objects.get(pk=id).delete()
			return Response({'status':True, 'id':id})
		except ProtectedError as e:
			return Response(e.errors, status=status.HTTP_400_BAD_REQUEST)

	@list_route(url_path='searchform', methods=['get'], permission_classes=[LimonPermission,])
	def get_searchform(self, request, *args, **kwargs):
		queryset = search_filter(self.get_queryset(), request)
		serializer =  MotivoMovimientoCuentaSerializer(queryset, many=True)
		return Response(serializer.data)