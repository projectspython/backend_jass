from rest_framework import serializers, viewsets
from apps.procesos.models.ComprobConfig import ComprobConfig
from rest_framework import permissions
from apps.procesos.serializers.ComprobConfig import ComprobConfigSerializer
from apps.comun.always.Pagination import LimonPagination
from rest_framework.filters import SearchFilter, OrderingFilter
from django.db.models.deletion import ProtectedError

from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.procesos.Permissions.ComprobConfig import ComprobConfigPermissions as LimonPermission
from apps.segur.always.Permission import PermLimonList
from apps.comun.always.SearchFilter import search_filter, keys_add_none, values_obj_add

class ComprobConfigViewSet(LimonPagination, viewsets.ModelViewSet):
	queryset = ComprobConfig.objects.all()
	permission_classes = [permissions.IsAuthenticated, PermLimonList, ]
	serializer_class = ComprobConfigSerializer
	filter_backends = (SearchFilter,OrderingFilter,)
	search_fields = ('serie','correlativo','entidad__persona__nombre_completo', 'comprobante__nombre',)
	ordering_fields = '__all__'

	@list_route(url_path='add', methods=['post'], permission_classes=[LimonPermission,])
	def get_add(self, request, *args, **kwargs):
		data = request.data
		data_compconf = keys_add_none(data,'serie,nro_inicial,nro_limite,correlativo,nro_autoriz,completa_ceros,orden,entidad_id,comprobante_id,estado')
		cc = ComprobConfig.objects.create(**data_compconf)
		return Response(self.get_serializer(cc).data)

	@list_route(url_path='edit', methods=['get'], permission_classes=[LimonPermission,])
	def get_edit(self, request):
		id = request.query_params.get('id', None)
		model = ComprobConfig.objects.get(pk=id)
		return Response(self.get_serializer(model).data)


	@list_route(url_path='update', methods=['put'], permission_classes=[LimonPermission,])
	def get_update(self, request, pk=None):
		data = request.data
		data_compconf = keys_add_none(data,'serie,nro_inicial,nro_limite,correlativo,nro_autoriz,completa_ceros,orden,entidad_id,comprobante_id,estado')
		cc = ComprobConfig.objects.filter(id=request.data['id']).update(**data_compconf)
		model = ComprobConfig.objects.get(pk=request.data['id'])
		return Response(self.get_serializer(model).data)

	@list_route(url_path='delete', methods=['delete'], permission_classes=[LimonPermission,])
	def get_delete(self, request, *args, **kwargs):
		try:
			id = request.query_params.get('id', None)
			model = ComprobConfig.objects.get(pk=id).delete()
			return Response({'status':True, 'id':id})
		except ProtectedError as e:
			return Response(e.errors, status=status.HTTP_400_BAD_REQUEST)

	@list_route(url_path='searchform', methods=['get'], permission_classes=[LimonPermission,])
	def get_searchform(self, request, *args, **kwargs):
		queryset = search_filter(self.get_queryset(), request)
		serializer = ComprobConfigSerializer(queryset, many=True)
		return Response(serializer.data)