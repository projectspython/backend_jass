from rest_framework import serializers, viewsets
from apps.procesos.models.TipoEvento import TipoEvento
from rest_framework import permissions
from apps.procesos.serializers.TipoEvento import TipoEventoSerializer
from apps.procesos.always.Pagination import LimonPagination
from rest_framework.filters import SearchFilter, OrderingFilter
from django.db.models.deletion import ProtectedError

from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.procesos.Permissions.TipoEvento import TipoEventoPermissions as LimonPermission
from apps.segur.always.Permission import PermLimonList
from apps.procesos.always.SearchFilter import search_filter

class TipoEventoViewSet(LimonPagination, viewsets.ModelViewSet):

	queryset = TipoEvento.objects.all()
	permission_classes = [permissions.IsAuthenticated, PermLimonList, ]
	serializer_class = TipoEventoSerializer
	filter_backends = (SearchFilter,OrderingFilter,)
	search_fields = ('codigo', 'nombre', 'nombre_plural')
	ordering_fields = '__all__'

	@list_route(url_path='searchform', methods=['get'], permission_classes=[LimonPermission,])
	def get_searchform(self, request, *args, **kwargs):
		queryset = search_filter(self.get_queryset(), request)
		serializer = TipoEventoSerializer(queryset, many=True)
		return Response(serializer.data)