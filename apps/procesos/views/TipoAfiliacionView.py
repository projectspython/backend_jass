from rest_framework import serializers, viewsets
from apps.procesos.models.TipoAfiliacion import TipoAfiliacion
from rest_framework import permissions
from apps.procesos.serializers.TipoAfiliacion import TipoAfiliacionSerializer
from apps.procesos.always.Pagination import LimonPagination
from rest_framework.filters import SearchFilter, OrderingFilter
from django.db.models.deletion import ProtectedError

from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.procesos.Permissions.TipoAfiliacion import TipoAfiliacionPermissions as LimonPermission
from apps.segur.always.Permission import PermLimonList
from apps.procesos.always.SearchFilter import search_filter

class TipoAfiliacionViewSet(LimonPagination, viewsets.ModelViewSet):
	queryset = TipoAfiliacion.objects.all()
	permission_classes = [permissions.IsAuthenticated, PermLimonList, ]
	serializer_class = TipoAfiliacionSerializer
	filter_backends = (SearchFilter,OrderingFilter,)
	search_fields = ('codigo', 'nombre', 'nombre_plural',)
	ordering_fields = '__all__'

	@list_route(url_path='searchform', methods=['get'], permission_classes=[LimonPermission,])
	def get_searchform(self, request, *args, **kwargs):
		queryset = search_filter(self.get_queryset(), request)
		serializer = TipoAfiliacionSerializer(queryset, many=True)
		return Response(serializer.data)