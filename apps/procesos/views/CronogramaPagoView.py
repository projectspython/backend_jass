from rest_framework import serializers, viewsets, generics
from apps.procesos.models.CronogramaPago import CronogramaPago
from apps.procesos.models.MotivoMovimientoCuenta import MotivoMovimientoCuenta
from apps.caja.models.EstadoCuenta import EstadoCuenta
from apps.procesos.models.EstadoCronograma import EstadoCronograma

from rest_framework import permissions
from apps.procesos.serializers.CronogramaPago import CronogramaPagoSerializer, CronogramaPagoCajaSerializer
from apps.procesos.always.Pagination import LimonPagination
from rest_framework.filters import SearchFilter, OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend
from django.db.models.deletion import ProtectedError

from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.procesos.Permissions.CronogramaPago import CronogramaPagoPermissions as LimonPermission
from apps.segur.always.Permission import PermLimonList
import json
from apps.comun.always.SearchFilter import search_filter, keys_add_none, values_obj_add, keys_del
from django_filters import filters
from apps.caja.always.Generales import Generales


# filters.LOOKUP_TYPES = [
# 	('', '---------'),
# 	('exact', 'Is equal to'),
# 	('not_exact', 'Is not equal to'),
# 	('lt', 'Lesser than'),
# 	('gt', 'Greater than'),
# 	('gte', 'Greater than or equal to'),
# 	('lte', 'Lesser than or equal to'),
# 	('startswith', 'Starts with'),
# 	('endswith', 'Ends with'),
# 	('contains', 'Contains'),
# 	('not_contains', 'Does not contain'),
# ]


class CronogramaPagoViewSet(LimonPagination, viewsets.ModelViewSet):
	queryset = CronogramaPago.objects.all()
	permission_classes = [permissions.IsAuthenticated, PermLimonList, ]
	serializer_class = CronogramaPagoSerializer
	filter_backends = (SearchFilter,OrderingFilter,DjangoFilterBackend,)
	filter_fields = ('motivo_movimiento_cuenta_id','fecha_cobranza','motivo_movimiento_cuenta__tipo_movimiento_cuenta__codigo')
	search_fields = ('persona__nombre_completo','motivo_movimiento_cuenta__nombre')
	ordering_fields = '__all__'

	@list_route(url_path='add', methods=['post'], permission_classes=[LimonPermission,])
	def get_add(self, request, *args, **kwargs):
		data = request.data
		data['usuario_id'] = request.user.persona_id
		# data['entidad_id'] = 1
		data_cp = keys_add_none(data,'contrato_id,entidad_id,estado_cronograma_id,'+
			'motivo_movimiento_cuenta_id,padre_id,persona_id,usuario_id,fecha_cobranza,porc_interes,'+
			'monto,descripcion')
		cp = CronogramaPago.objects.create(**data_cp)

		estadoc = EstadoCuenta.objects.filter(persona_id=data['persona_id'],
			entidad_id=data['entidad_id']).values('saldo','id','deuda').first()

		motivo = MotivoMovimientoCuenta.objects.filter(id=data['motivo_movimiento_cuenta_id'],
			tipo_movimiento_cuenta__codigo='EGR').values('id').first()

		estado = EstadoCronograma.objects.filter(id=data['estado_cronograma_id'],codigo='HAB').values('id').first()

		if cp and estado:
			if motivo:
				upd_deuda = Generales.update_estado_cuenta(data['entidad_id'], data['persona_id'],
					data['monto'], 'saldo')
			else:
				upd_deuda = Generales.update_estado_cuenta(data['entidad_id'], data['persona_id'],
					data['monto'], 'deuda')
		# if motivo:
		# 	if estado:
		# 		if estadoc:
		# 			saldonuevo = float(estadoc['saldo'])+float(data['monto'])
		# 			ec = EstadoCuenta.objects.filter(id=estadoc['id']).update(saldo=saldonuevo)
		# 		else:
		# 			data['deuda'] = '0'
		# 			data['estado'] = '1'
		# 			data['saldo'] = float(data['monto'])
		# 			data_ec = keys_add_none(data,'saldo,deuda,estado,entidad_id,persona_id')
		# 			ec = EstadoCuenta.objects.create(**data_ec)
		# else:
		# 	if estado:
		# 		if estadoc:
		# 			deudanuevo = float(estadoc['deuda'])+float(data['monto'])
		# 			ec = EstadoCuenta.objects.filter(id=estadoc['id']).update(deuda=deudanuevo)
		# 		else:
		# 			data['saldo'] = '0'
		# 			data['estado'] = '1'
		# 			data['deuda'] = float(data['monto'])
		# 			data_ec = keys_add_none(data,'saldo,deuda,estado,entidad_id,persona_id')
		# 			ec = EstadoCuenta.objects.create(**data_ec)

		return Response(self.get_serializer(cp).data)

	@list_route(url_path='edit', methods=['get'], permission_classes=[LimonPermission,])
	def get_edit(self, request):
		id = request.query_params.get('id', None)
		model = CronogramaPago.objects.get(pk=id)
		return Response(self.get_serializer(model).data)

	@list_route(url_path='update', methods=['put'], permission_classes=[LimonPermission,])
	def get_update(self, request, pk=None):
		model = CronogramaPago.objects.get(pk=request.data['id'])
		montoanterior = model.monto
		estadocodigoanterior = model.estado_cronograma.codigo
		data = request.data
		data_cp = keys_add_none(data,'id,contrato_id,entidad_id,estado_cronograma_id,'+
			'motivo_movimiento_cuenta_id,padre_id,persona_id,fecha_cobranza,porc_interes,'+
			'monto,descripcion')
		cp = CronogramaPago.objects.filter(id=request.data['id']).update(**data_cp)
		model = CronogramaPago.objects.get(pk=request.data['id'])

		motivo = MotivoMovimientoCuenta.objects.filter(id=data['motivo_movimiento_cuenta_id'],
			tipo_movimiento_cuenta__codigo='EGR').values('id').first()

		estado = EstadoCronograma.objects.filter(id=data['estado_cronograma_id'],codigo='HAB').values('id').first()



		if estadocodigoanterior == 'HAB' and estado:
			diferencia = model.monto - montoanterior
		elif estadocodigoanterior != 'HAB' and estado:
			diferencia = model.monto
		elif estadocodigoanterior == 'HAB' and estado is None:
			diferencia = montoanterior*-1
		else:
			diferencia = 0

		if cp:
			if motivo:
				upd_deuda = Generales.update_estado_cuenta(data['entidad_id'], data['persona_id'],
					float(diferencia), 'saldo')
			else:
				upd_deuda = Generales.update_estado_cuenta(data['entidad_id'], data['persona_id'],
					float(diferencia), 'deuda')

		return Response(self.get_serializer(model).data)

	@list_route(url_path='delete', methods=['delete'], permission_classes=[LimonPermission,])
	def get_delete(self, request, *args, **kwargs):

		id = request.query_params.get('id', None)
		motivo = CronogramaPago.objects.get(pk=id)
		estadocodigoanterior = motivo.estado_cronograma.codigo
		diferencia = motivo.monto*-1
		entidad_id= motivo.entidad_id
		persona_id = motivo.persona_id
		tipo_codigo = motivo.motivo_movimiento_cuenta.tipo_movimiento_cuenta.codigo
		delete = motivo.delete()

		if delete:
			if tipo_codigo == 'EGR':
				if 	estadocodigoanterior == 'HAB':
					upd_deuda = Generales.update_estado_cuenta(entidad_id, persona_id,
						float(diferencia), 'saldo')
			else:
				if 	estadocodigoanterior == 'HAB':
					upd_deuda = Generales.update_estado_cuenta(entidad_id, persona_id,
						float(diferencia), 'deuda')


		return Response({'status':True, 'id':id})

	@list_route(url_path='searchform', methods=['get'], permission_classes=[LimonPermission,])
	def get_searchform(self, request, *args, **kwargs):
		queryset = search_filter(self.get_queryset(), request)
		serializer =  CronogramaPagoSerializer(queryset, many=True)
		return Response(serializer.data)

	@list_route(url_path='listcronogpagar', methods=['get'], permission_classes=[LimonPermission,])
	def get_listcronogpagar(self, request, *args, **kwargs):
		queryset = search_filter(self.get_queryset(), request)

		result = {}
		p_monto_total = 0
		p_pagado = 0

		contratos = queryset.values('contrato_id','contrato__numero',
			'contrato__predio__predio_direcciones__direccion_detalle','contrato__predio__zona__nombre',
			).exclude(contrato_id__isnull=True).distinct()
		contratos_new = []
		for p in contratos:
			# query_cronog = search_filter(self.get_queryset(), request)
			query_cronog = queryset.filter(contrato_id=p['contrato_id'])
			cronogramas = CronogramaPagoCajaSerializer(query_cronog, many=True).data
			p['cronogramas'] = cronogramas
			monto_total = 0
			pagado = 0
			for q in cronogramas:
				monto_total += q['monto']
				pagado += q['pagado']

			p['monto_total'] = monto_total
			p['pagado'] = pagado
			contratos_new.append(p)
			p_monto_total += monto_total
			p_pagado += pagado

		sin_contratos = queryset.values('id').filter(contrato_id__isnull=True).distinct()
		if len(sin_contratos)>0:
			query_cronog_sn = queryset.filter(contrato_id__isnull=True)
			cronogramas_sn = CronogramaPagoCajaSerializer(query_cronog_sn, many=True).data

			monto_total = 0
			pagado = 0
			for q in cronogramas_sn:
				monto_total += q['monto']
				pagado += q['pagado']
			otros = {
				'contrato_id':None,
				'detalle':'OTROS',
				'monto_total':monto_total,
				'pagado':pagado,
				'cronogramas': cronogramas_sn
			}
			contratos_new.append(otros)
			p_monto_total += monto_total
			p_pagado += pagado

		result['contratos'] = contratos_new
		result['monto_total'] = p_monto_total
		result['pagado'] = p_pagado

		return Response(result)