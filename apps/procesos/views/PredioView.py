from rest_framework import serializers, viewsets
from apps.procesos.models.Predio import Predio
from apps.procesos.models.Direccion import Direccion
from apps.comun.models.TipoDireccion import TipoDireccion
from apps.procesos.always.always import concat_direccion_detalle

from rest_framework import permissions
from apps.procesos.serializers.Predio import PredioSerializer, MisPredioSerializer
from apps.comun.always.Pagination import LimonPagination
from rest_framework.filters import SearchFilter, OrderingFilter
from django.db.models.deletion import ProtectedError

from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.procesos.Permissions.Predio import PredioPermissions as LimonPermission
from apps.segur.always.Permission import PermLimonList
from apps.comun.always.SearchFilter import search_filter, keys_del, keys_add_none, values_obj_add, values_all_none
from apps.comun.always.GenerarCodigo import GenerarCodigo

class PredioViewSet(LimonPagination, viewsets.ModelViewSet):
	queryset = Predio.objects.all()
	permission_classes = [permissions.IsAuthenticated, PermLimonList, ]
	serializer_class = PredioSerializer
	filter_backends = (SearchFilter,OrderingFilter,)
	search_fields = ('codigo','zona__nombre', 'estado_predio__nombre', 'duenio__nombre_completo', 
		'representante__nombre_completo',)
	ordering_fields = '__all__'

	@list_route(url_path='add', methods=['post'], permission_classes=[LimonPermission,])
	def get_add(self, request, *args, **kwargs):
		data = request.data
		data['usuario_id'] = request.user.persona_id
		data['codigo'] = GenerarCodigo.predio()
		
		if data['estado_predio_codigo'] != 'Otros':
			data['estado_descripcion'] = None

		data_pre = keys_add_none(data, 'codigo,representante_id,duenio_id,zona_id,partida_registral,'+
				'estado_predio_id,estado_descripcion,usuario_id,estado')
		pred = Predio.objects.create(**data_pre)

		if pred:
			data_dir = data['direcciones']
			
			if data_dir['completa'] == 'SI':
					data_dir['direccion_detalle'] = concat_direccion_detalle(data['direcciones'])
					
			dir_ = keys_add_none(data_dir,'ubigeo_id,completa,direccion_detalle,tipo_via_id,nombre_via,tipo_numero_via_id,'+
					'numero_via,manzana,lote,piso,dpto_interior,tipo_zona_id,nombre_zona,'+
					'referencia1,referencia2,punto_lat,punto_long')
			
			tipo_dire = TipoDireccion.objects.filter(codigo='DOMFIS').values('id').first()
			dir_['predio_id'] = pred.id
			dir_['tipo_direccion_id'] = tipo_dire['id']
			dir_['pais_id'] = data_dir['ubigeo']['pais']['id']

			dire = Direccion.objects.create(**dir_)

		return Response(self.get_serializer(pred).data)

	@list_route(url_path='edit', methods=['get'], permission_classes=[LimonPermission,])
	def get_edit(self, request):
		id = request.query_params.get('id', None)
		model = Predio.objects.get(pk=id)
		data = self.get_serializer(model).data
		data['zona_id'] = data['zona']['id']
		data['estado_predio_id'] = data['estado_predio']['id']
		data['estado_predio_codigo'] = data['estado_predio']['codigo']
		return Response(data)

	@list_route(url_path='update', methods=['put'], permission_classes=[LimonPermission,])
	def get_update(self, request, pk=None):
		# dir_id = None
		# model = Predio.objects.get(pk=request.data['id'])
		data = request.data

		if data['estado_predio_codigo'] != 'Otros':
			data['estado_descripcion'] = None

		if data['codigo']:
			pass
		else:
			data['codigo'] = GenerarCodigo.predio()

		data_pre = keys_add_none(data, 'codigo,representante_id,duenio_id,zona_id,partida_registral,'+
				'estado_predio_id,estado_descripcion,estado')

		pred = Predio.objects.filter(id=request.data['id']).update(**data_pre)

		if pred:
			data_dir = data['direcciones']

			if data_dir['completa'] == 'SI':
				data_dir['direccion_detalle'] = concat_direccion_detalle(data['direcciones'])

			dir_ = keys_add_none(data_dir,'ubigeo_id,completa,direccion_detalle,tipo_via_id,nombre_via,tipo_numero_via_id,'+
					'numero_via,manzana,lote,piso,dpto_interior,tipo_zona_id,nombre_zona,'+
					'referencia1,referencia2,punto_lat,punto_long')
			
			if 'id' in data_dir:
				dire = Direccion.objects.filter(id=data_dir['id']).update(**dir_)
			else:
				tipo_dire = TipoDireccion.objects.filter(codigo='DOMFIS').values('id').first()
				dir_['predio_id'] = pred.id
				dir_['tipo_direccion_id'] = tipo_dire['id']
				dir_['pais_id'] = data_dir['ubigeo']['pais']['id']
				dire = Direccion.objects.create(**dir_)

		model = Predio.objects.get(pk=request.data['id'])
		return Response(self.get_serializer(model).data)

	@list_route(url_path='delete', methods=['delete'], permission_classes=[LimonPermission,])
	def get_delete(self, request, *args, **kwargs):
		try:
			id = request.query_params.get('id', None)
			model = Predio.objects.get(pk=id).delete()
			return Response({'status':True, 'id':id})
		except ProtectedError as e:
			return Response(e.errors, status=status.HTTP_400_BAD_REQUEST)

	@list_route(url_path='searchform', methods=['get'], permission_classes=[LimonPermission,])
	def get_searchform(self, request, *args, **kwargs):
		queryset = search_filter(self.get_queryset(), request)
		serializer = PredioSerializer(queryset, many=True)
		return Response(serializer.data)

	@list_route(url_path='mispredios', methods=['get'], permission_classes=[LimonPermission,])
	def get_mispredios(self, request, *args, **kwargs):
		queryset = search_filter(self.get_queryset(), request)

		serializer = MisPredioSerializer(queryset, many=True)
		return Response(serializer.data)