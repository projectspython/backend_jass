from rest_framework import serializers, viewsets, generics
from apps.procesos.models.Afiliacion import Afiliacion
from rest_framework import permissions
from apps.procesos.serializers.Afiliacion import AfiliacionSerializer
from apps.procesos.always.Pagination import LimonPagination
from rest_framework.filters import SearchFilter, OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend
from django.db.models.deletion import ProtectedError

from apps.procesos.models.Representante import Representante

from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.procesos.Permissions.Afiliacion import AfiliacionPermissions as LimonPermission
from apps.segur.always.Permission import PermLimonList
import json
from apps.comun.always.SearchFilter import search_filter, keys_add_none, values_obj_add, extaer_de_column
from apps.procesos.always.SearchFilter import search_filter, keys_del
from apps.comun.always.GenerarCodigo import GenerarCodigo


class AfiliacionViewSet(LimonPagination, viewsets.ModelViewSet):
	queryset = Afiliacion.objects.all()
	permission_classes = [permissions.IsAuthenticated, PermLimonList, ]
	serializer_class = AfiliacionSerializer
	filter_backends = (SearchFilter,OrderingFilter,DjangoFilterBackend,)
	filter_fields = ('tipo_afiliacion_id','fecha_ingreso','fecha_afiliacion',)
	search_fields = ('persona__nombre_completo', 'tipo_afiliacion__nombre',)
	ordering_fields = '__all__'

	@list_route(url_path='add', methods=['post'], permission_classes=[LimonPermission,])
	def get_add(self, request, *args, **kwargs):
		data = request.data
		# print(data)
		data['codigo'] = GenerarCodigo.afiliaciones()
		tipo_afiliacion = data['tipo_afiliacion']
		data_cp = keys_add_none(data,'codigo,entidad_id,persona_id,tipo_afiliacion_id,fecha_ingreso,fecha_afiliacion,estado')
		cp = Afiliacion.objects.create(**data_cp)

		if tipo_afiliacion['codigo'] == 'SOCIO':
			if data['representantes']:
				for p in data['representantes']:
					rep = p['representante']
					rep['persona_id'] = rep['id']
					rep['afiliacion_id'] = cp.id
					data_rep = keys_del(rep,'id,codigo,tipo_persona,tipo_contribuyente,nacionalidad,estado,nombre_completo,foto_logo,foto_logo_rec,persona_persona_documentos')
					repres = Representante.objects.create(**data_rep)
		return Response(self.get_serializer(cp).data)

	@list_route(url_path='edit', methods=['get'], permission_classes=[LimonPermission,])
	def get_edit(self, request):
		result = {}
		representantes = []
		id = request.query_params.get('id', None)
		model = Afiliacion.objects.get(pk=id)
		result = self.get_serializer(model).data
		repres = Representante.objects.filter(afiliacion_id=id, estado='1').values('id','afiliacion_id','persona__id','persona__nombre_completo','persona__foto_logo','persona__foto_logo_rec')
		# , 'persona__persona_persona_documentos', 'persona__persona_persona_documentos__numero', 'persona__persona_persona_documentos__tipo_documento__nombre'
		for r in repres:
			representante = {}
			representante['id'] = r['id']
			representante['afiliacion_id'] = r['afiliacion_id']
			representante['persona_id'] = r['persona__id']
			representante['nombre_completo'] = r['persona__nombre_completo']
			representante['foto_logo'] = r['persona__foto_logo']
			representante['foto_logo_rec'] = r['persona__foto_logo_rec']
			# representante['persona_persona_documentos'] = r['persona__persona_persona_documentos']
			representantes.append({'representante':representante})

		result['representantes'] = representantes
		# return Response(self.get_serializer(model).data)
		return Response(result)

	@list_route(url_path='update', methods=['put'], permission_classes=[LimonPermission,])
	def get_update(self, request, pk=None):
		data = request.data
		tipo_afiliacion = data['tipo_afiliacion']
		data_cp = keys_add_none(data,'codigo,entidad_id,persona_id,tipo_afiliacion_id,fecha_ingreso,fecha_afiliacion,estado')
		afi = Afiliacion.objects.filter(id=request.data['id']).update(**data_cp)

		if tipo_afiliacion['codigo'] == 'SOCIO':
			representantes = data['representantes']
			del_representantes = extaer_de_column(data['del_representantes'],'id')
			delr = Representante.objects.filter(id__in=del_representantes).delete()
			for r in representantes:
				if 'id' and 'persona_id' in r['representante']:
					representante = keys_del(r['representante'],'codigo,nombre_completo,foto_logo,foto_logo_rec,persona_persona_documentos')
					rep = Representante.objects.filter(pk=representante['id']).update(**representante)
				else:
					rep = r['representante']
					rep['persona_id'] = rep['id']
					rep['afiliacion_id'] = request.data['id']
					data_rep = keys_del(rep,'id,codigo,tipo_persona,tipo_contribuyente,nacionalidad,estado,nombre_completo,foto_logo,foto_logo_rec,persona_persona_documentos')
					repres = Representante.objects.create(**data_rep)
		else:
			representantes = data['representantes']

			del_representantes = []
			for r in representantes:
				# print(r['representante'])
				del_representantes.append(r['representante']['id'])
			# print(del_representantes)
			delr = Representante.objects.filter(id__in=del_representantes).delete()

		model = Afiliacion.objects.get(pk=request.data['id'])
		return Response(self.get_serializer(model).data)

	@list_route(url_path='delete', methods=['delete'], permission_classes=[LimonPermission,])
	def get_delete(self, request, *args, **kwargs):
		try:
			id = request.query_params.get('id', None)
			model = Afiliacion.objects.get(pk=id).delete()
			return Response({'status':True, 'id':id})
		except ProtectedError as e:
			return Response(e.errors, status=status.HTTP_400_BAD_REQUEST)

	@list_route(url_path='searchform', methods=['get'], permission_classes=[LimonPermission,])
	def get_searchform(self, request, *args, **kwargs):
		queryset = search_filter(self.get_queryset().distinct(), request)
		serializer = AfiliacionSerializer(queryset, many=True)
		return Response(serializer.data)

	@list_route(url_path='searchformperasist', methods=['get'], permission_classes=[LimonPermission,])
	def get_searchformperasist(self, request, *args, **kwargs):
		queryset = self.get_queryset().exclude(persona__persona_asistencias__persona_id__isnull=False).distinct()
		queryset = search_filter(queryset, request)
		serializer = AfiliacionSerializer(queryset, many=True)
		return Response(serializer.data)