from rest_framework import viewsets
from rest_framework.response import Response
import unicodedata
from apps.procesos.always.Choices import FORMA_COBRANZA, ESTADO_MES_RECIBO, UNIDAD_TIEMPO, PERSONA_ASISTENTE


class ChoicesViewSet(viewsets.ViewSet):
    def get_choices(self, request, param=None):
        limit = request.query_params.get('limit', '20')
        query = request.query_params.get('query', None)

        def iterator_list(model):
            lista = []
            for x in model:
                f = dict(id=x[0], nombre=x[1])
                lista.append(f)
            return list(lista)

        def serialize(model):
            lista = []
            count = 0
            for x in model:
                f = dict(id=x[0], nombre=x[1])
                if query is not None:
                    if normalize(query.upper()) in normalize(f['value'].upper()):
                        lista.append(f)
                else:
                    lista.append(f)
                    if count == int(limit):
                        break
                count += 1
            return list(lista[:int(limit)])

        def normalize(s):
            return ''.join((c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn'))

        def iterator_list(model):
            lista = []
            for x in model:
                f = dict(id=x[0], nombre=x[1])
                lista.append(f)
            return list(lista)

        if param == "formacobranza":
            return Response(serialize(FORMA_COBRANZA))

        elif param == "estadosmesrecibo":
            return Response(iterator_list(ESTADO_MES_RECIBO))

        elif param == "unidadestiempo":
            return Response(iterator_list(UNIDAD_TIEMPO))

        elif param == "personasasistente":
            return Response(iterator_list(PERSONA_ASISTENTE))

        else:
            return Response('No hay choices con ' + param)