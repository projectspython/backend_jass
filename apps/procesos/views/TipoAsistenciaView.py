from rest_framework import serializers, viewsets
from apps.procesos.models.TipoAsistencia import TipoAsistencia
from rest_framework import permissions
from apps.procesos.serializers.TipoAsistencia import TipoAsistenciaSerializer
from apps.procesos.always.Pagination import LimonPagination
from rest_framework.filters import SearchFilter, OrderingFilter
from django.db.models.deletion import ProtectedError

from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.procesos.Permissions.TipoAsistencia import TipoAsistenciaPermissions as LimonPermission
from apps.segur.always.Permission import PermLimonList
import json
from apps.procesos.always.SearchFilter import search_filter

class TipoAsistenciaViewSet(LimonPagination, viewsets.ModelViewSet):

	queryset = TipoAsistencia.objects.all()
	permission_classes = [permissions.IsAuthenticated, PermLimonList, ]
	serializer_class = TipoAsistenciaSerializer
	filter_backends = (SearchFilter,OrderingFilter,)
	search_fields = ('codigo', 'nombre', 'nombre_singular')
	ordering_fields = '__all__'


	@list_route(url_path='searchform', methods=['get'], permission_classes=[LimonPermission,])
	def get_searchform(self, request, *args, **kwargs):
		queryset = search_filter(self.get_queryset(), request)
		serializer = TipoAsistenciaSerializer(queryset, many=True)
		return Response(serializer.data)