from rest_framework import serializers, viewsets, generics
from apps.procesos.models.Representante import Representante
from rest_framework import permissions
from apps.procesos.serializers.Representante import RepresentanteSerializer
from apps.procesos.always.Pagination import LimonPagination
from rest_framework.filters import SearchFilter, OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend
from django.db.models.deletion import ProtectedError

from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.procesos.Permissions.Representante import RepresentantePermissions as LimonPermission
from apps.segur.always.Permission import PermLimonList
import json
from apps.comun.always.SearchFilter import search_filter, keys_add_none, values_obj_add
from apps.comun.always.GenerarCodigo import GenerarCodigo


class RepresentanteViewSet(LimonPagination, viewsets.ModelViewSet):
	queryset = Representante.objects.all()
	permission_classes = [permissions.IsAuthenticated, PermLimonList, ]
	serializer_class = RepresentanteSerializer
	filter_backends = (SearchFilter,OrderingFilter,)
	# filter_backends = (SearchFilter,OrderingFilter,DjangoFilterBackend,)
	filter_fields = ('persona__nombre_completo',)
	# search_fields = ('persona__nombre_completo', 'tipo_Representante__nombre',)
	ordering_fields = '__all__'

	@list_route(url_path='add', methods=['post'], permission_classes=[LimonPermission,])
	def get_add(self, request, *args, **kwargs):
		data = request.data
		data['persona_id'] = 1
		# data_cp = keys_add_none(data,'codigo,entidad_id,persona_id,tipo_Representante_id,fecha_ingreso,fecha_Representante,estado')
		# cp = Representante.objects.create(**data_cp)
		return Response(self.get_serializer(cp).data)

	@list_route(url_path='edit', methods=['get'], permission_classes=[LimonPermission,])
	def get_edit(self, request):
		id = request.query_params.get('id', None)
		model = Representante.objects.get(pk=id)
		return Response(self.get_serializer(model).data)


	@list_route(url_path='update', methods=['put'], permission_classes=[LimonPermission,])
	def get_update(self, request, pk=None):
		data = request.data
		data_cp = keys_add_none(data,'afiliacion_id,persona_id,estado')
		afi = Representante.objects.filter(id=request.data['id']).update(**data_cp)
		model = Representante.objects.get(pk=request.data['id'])
		return Response(self.get_serializer(model).data)

	@list_route(url_path='delete', methods=['delete'], permission_classes=[LimonPermission,])
	def get_delete(self, request, *args, **kwargs):
		try:
			id = request.query_params.get('id', None)
			model = Representante.objects.get(pk=id).delete()
			return Response({'status':True, 'id':id})
		except ProtectedError as e:
			return Response(e.errors, status=status.HTTP_400_BAD_REQUEST)

	@list_route(url_path='searchform', methods=['get'], permission_classes=[LimonPermission,])
	def get_searchform(self, request, *args, **kwargs):
		queryset = search_filter(self.get_queryset(), request)
		serializer = RepresentanteSerializer(queryset, many=True)
		return Response(serializer.data)

	@list_route(url_path='searchformrepreasist', methods=['get'], permission_classes=[LimonPermission,])
	def get_searchformrepreasist(self, request, *args, **kwargs):
		queryset = self.get_queryset().exclude(afiliacion__persona__persona_asistencias__persona_id__isnull=False).exclude(persona__representante_asistencias__representante_id__isnull=False).distinct()
		queryset = search_filter(queryset, request)
		serializer = RepresentanteSerializer(queryset, many=True)
		return Response(serializer.data)