from rest_framework import serializers, viewsets
from apps.procesos.models.Evento import Evento
from apps.procesos.models.Afiliacion import Afiliacion
from apps.procesos.models.Asistencia import Asistencia
from rest_framework import permissions
from apps.procesos.serializers.Evento import EventoSerializer
from apps.comun.always.Pagination import LimonPagination
from rest_framework.filters import SearchFilter, OrderingFilter
from django.db.models.deletion import ProtectedError

from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.procesos.Permissions.Evento import EventoPermissions as LimonPermission
from apps.segur.always.Permission import PermLimonList
from apps.comun.always.SearchFilter import search_filter, keys_add_none, values_obj_add
from django_filters.rest_framework import DjangoFilterBackend

class EventoViewSet(LimonPagination, viewsets.ModelViewSet):

	queryset = Evento.objects.all()
	permission_classes = [permissions.IsAuthenticated, PermLimonList, ]
	serializer_class = EventoSerializer
	filter_backends = (SearchFilter,OrderingFilter,DjangoFilterBackend,)
	filter_fields = ('entidad_id',)
	search_fields = ('nombre', 'tipo_asistencia__nombre', 'tipo_evento__nombre',)
	ordering_fields = '__all__'

	@list_route(url_path='add', methods=['post'], permission_classes=[LimonPermission,])
	def get_add(self, request, *args, **kwargs):
		data = request.data
		# tipo_asistencia = data['tipo_asistencia']
		data_tipoeven = keys_add_none(data,'nombre,fecha,hora_inicio,hora_fin,hora_tolerancia,entidad_id,tipo_evento_id,tipo_asistencia_id,descripcion,estado')
		# print(data_tipoeven)
		even = Evento.objects.create(**data_tipoeven)
		# print('EVENTO_ID = {}'.format(even.id))
		return Response(self.get_serializer(even).data)

	@list_route(url_path='edit', methods=['get'], permission_classes=[LimonPermission,])
	def get_edit(self, request):
		id = request.query_params.get('id', None)
		model = Evento.objects.get(pk=id)
		return Response(self.get_serializer(model).data)

	@list_route(url_path='update', methods=['put'], permission_classes=[LimonPermission,])
	def get_update(self, request, pk=None):
		# model = Evento.objects.get(pk=request.data['id'])
		data = request.data
		for q in values_obj_add(data, 'tipo_evento_id,tipo_asistencia_id', 'tipo_evento.id,tipo_asistencia.id'):
			data[q['colum']] = q['value']
		data_evento = keys_add_none(data,'nombre,fecha,hora_inicio,hora_fin,hora_tolerancia,estado,entidad_id,tipo_evento_id,descripcion,tipo_asistencia_id')
		# print(data_evento)
		even = Evento.objects.filter(id=request.data['id']).update(**data_evento)
		model = Evento.objects.get(pk=request.data['id'])
		return Response(self.get_serializer(model).data)
		# representantes = data['representantes']
			# del_representantes = extaer_de_column(data['del_representantes'],'id')
			# delr = Representante.objects.filter(id__in=del_representantes).delete()

	@list_route(url_path='delete', methods=['delete'], permission_classes=[LimonPermission,])
	def get_delete(self, request, *args, **kwargs):
		try:
			print(request)
			id = request.query_params.get('id', None)
			model = Evento.objects.get(pk=id).delete()
			return Response({'status':True, 'id':id})
		except ProtectedError as e:
			return Response(e.errors, status=status.HTTP_400_BAD_REQUEST)

	@list_route(url_path='searchform', methods=['get'], permission_classes=[LimonPermission,])
	def get_searchform(self, request, *args, **kwargs):
		queryset = search_filter(self.get_queryset(), request)
		serializer = EventoSerializer(queryset, many=True)
		return Response(serializer.data)
