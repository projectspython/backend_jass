from rest_framework import serializers, viewsets
from apps.procesos.models.Servicio import Servicio
from rest_framework import permissions
from apps.procesos.serializers.Servicio import ServicioSerializer
from apps.comun.always.Pagination import LimonPagination
from rest_framework.filters import SearchFilter, OrderingFilter
from django.db.models.deletion import ProtectedError

from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.procesos.Permissions.Servicio import ServicioPermissions as LimonPermission
from apps.segur.always.Permission import PermLimonList
from apps.comun.always.SearchFilter import search_filter

class ServicioViewSet(LimonPagination, viewsets.ModelViewSet):
	queryset = Servicio.objects.all()
	permission_classes = [permissions.IsAuthenticated, PermLimonList, ]
	serializer_class = ServicioSerializer
	filter_backends = (SearchFilter,OrderingFilter,)
	search_fields = ('nombre', 'codigo',)
	ordering_fields = '__all__'

	@list_route(url_path='add', methods=['post'], permission_classes=[LimonPermission,])
	def get_add(self, request, *args, **kwargs):
		serializer = self.get_serializer(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

	@list_route(url_path='edit', methods=['get'], permission_classes=[LimonPermission,])
	def get_edit(self, request):
		id = request.query_params.get('id', None)
		model = Servicio.objects.get(pk=id)
		data_res = self.get_serializer(model).data
		if 'precio' in data_res and data_res['precio']:
			data_res['precio'] = float(data_res['precio'])
		return Response(data_res)


	@list_route(url_path='update', methods=['put'], permission_classes=[LimonPermission,])
	def get_update(self, request, pk=None):
		model = Servicio.objects.get(pk=request.data['id'])
		serializer = self.get_serializer(model, data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data)
		else:
			return Response(serializer.errors,
							status=status.HTTP_400_BAD_REQUEST)

	@list_route(url_path='delete', methods=['delete'], permission_classes=[LimonPermission,])
	def get_delete(self, request, *args, **kwargs):
		try:
			id = request.query_params.get('id', None)
			model = Servicio.objects.get(pk=id).delete()
			return Response({'status':True, 'id':id})
		except ProtectedError as e:
			return Response(e.errors, status=status.HTTP_400_BAD_REQUEST)

	@list_route(url_path='searchform', methods=['get'], permission_classes=[LimonPermission,])
	def get_searchform(self, request, *args, **kwargs):
		queryset = search_filter(self.get_queryset(), request)
		serializer = ServicioSerializer(queryset, many=True)
		return Response(serializer.data)

	@list_route(url_path='searchcontrato', methods=['get'], permission_classes=[LimonPermission,])
	def get_searchcontrato(self, request, *args, **kwargs):
		queryset = search_filter(self.get_queryset(), request)
		listado = ServicioSerializer(queryset, many=True).data

		inscripcion = {}
		servicios = []
		for p in listado:
			if p['codigo'] == 'Inscrip':
				inscripcion = p
			else:
				if p['codigo'] == 'ServAguaPo':
					p['select'] = True
					p['readonly'] = True
				if p['precio']:
					p['precio'] = float(p['precio'])
				p['precio_m3'] = 8.50
				servicios.append(p)

		return Response({'inscripcion':inscripcion,'servicios':servicios})


		