from rest_framework import serializers, viewsets
from apps.procesos.models.MesRecibo import MesRecibo
from apps.procesos.models.Direccion import Direccion
from apps.comun.models.TipoDireccion import TipoDireccion
from apps.procesos.always.always import concat_direccion_detalle

from rest_framework import permissions
from apps.procesos.serializers.MesRecibo import MesReciboSerializer
from apps.comun.always.Pagination import LimonPagination
from rest_framework.filters import SearchFilter, OrderingFilter
from django.db.models.deletion import ProtectedError

from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.procesos.Permissions.MesRecibo import MesReciboPermissions as LimonPermission
from apps.segur.always.Permission import PermLimonList
from apps.comun.always.SearchFilter import search_filter, keys_del, keys_add_none, values_obj_add, values_all_none, extaer_de_column
from apps.procesos.models.MesReciboRecom import MesReciboRecom

class MesReciboViewSet(LimonPagination, viewsets.ModelViewSet):
	queryset = MesRecibo.objects.all()
	permission_classes = [permissions.IsAuthenticated, PermLimonList, ]
	serializer_class = MesReciboSerializer
	filter_backends = (SearchFilter,OrderingFilter,)
	search_fields = ('anio', 'mes__nombre',)
	ordering_fields = '__all__'

	@list_route(url_path='add', methods=['post'], permission_classes=[LimonPermission,])
	def get_add(self, request, *args, **kwargs):
		data = request.data
		data['usuario_id'] = request.user.persona_id
		data_ = keys_add_none(data,'entidad_id,mes_id,anio,fecha_emision,fecha_vencimiento,fecha_corte,texto_html,estado,usuario_id')
		mesr = MesRecibo.objects.create(**data_)

		return Response(self.get_serializer(mesr).data)

	@list_route(url_path='edit', methods=['get'], permission_classes=[LimonPermission,])
	def get_edit(self, request):
		id = request.query_params.get('id', None)
		model = MesRecibo.objects.get(pk=id)
		data = self.get_serializer(model).data

		data['recomendaciones'] = MesReciboRecom.objects.filter(mes_recibo_id=id).values('id','detalle')
		return Response(data)

	@list_route(url_path='update', methods=['put'], permission_classes=[LimonPermission,])
	def get_update(self, request, pk=None):
		data = request.data
		data_ = keys_add_none(data,'mes_id,anio,fecha_emision,fecha_vencimiento,fecha_corte,texto_html,estado')
		p = MesRecibo.objects.filter(pk=data['id']).update(**data_)

		deldo = MesReciboRecom.objects.filter(id__in=extaer_de_column(data['del_recomendaciones'],'id')).delete()
		for p in data['recomendaciones']:
			p['mes_recibo_id'] = data['id']
			data_det = keys_add_none(p,'mes_recibo_id,detalle')
			if 'id' in p:
				mdet = MesReciboRecom.objects.filter(id=p['id']).update(**data_det)
			else:
				mdet = MesReciboRecom.objects.create(**data_det)

		model = MesRecibo.objects.get(id=data['id'])

		return Response(self.get_serializer(model).data)

	@list_route(url_path='delete', methods=['delete'], permission_classes=[LimonPermission,])
	def get_delete(self, request, *args, **kwargs):
		id = request.query_params.get('id', None)
		model = MesRecibo.objects.get(pk=id).delete()
		return Response({'status':True, 'id':id})

	@list_route(url_path='searchform', methods=['get'], permission_classes=[LimonPermission,])
	def get_searchform(self, request, *args, **kwargs):
		queryset = search_filter(self.get_queryset(), request)
		serializer = self.get_serializer(queryset, many=True)
		return Response(serializer.data)