from rest_framework import serializers, viewsets
from apps.procesos.models.Zona import Zona
from rest_framework import permissions
from apps.procesos.serializers.Zona import ZonaSerializer
from apps.comun.always.Pagination import LimonPagination
from rest_framework.filters import SearchFilter, OrderingFilter
from django.db.models.deletion import ProtectedError

from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.procesos.Permissions.Zona import ZonaPermissions as LimonPermission
from apps.segur.always.Permission import PermLimonList
from apps.comun.always.SearchFilter import search_filter, keys_add_none, values_obj_add
from apps.comun.always.GenerarCodigo import GenerarCodigo
from django_filters.rest_framework import DjangoFilterBackend

class ZonaViewSet(LimonPagination, viewsets.ModelViewSet):
	queryset = Zona.objects.all()
	permission_classes = [permissions.IsAuthenticated, PermLimonList, ]
	serializer_class = ZonaSerializer
	filter_backends = (SearchFilter,OrderingFilter,DjangoFilterBackend,)
	filter_fields = ('entidad_id',)
	search_fields = ('nombre', 'codigo', 'ubigeo__nombre', 'tipo_zona__nombre')
	ordering_fields = '__all__'

	@list_route(url_path='add', methods=['post'], permission_classes=[LimonPermission,])
	def get_add(self, request, *args, **kwargs):
		data = request.data
		data['codigo'] = GenerarCodigo.zonas()

		for q in values_obj_add(data, 'tipo_zona_id,ubigeo_id', 'tipo_zona.id,ubigeo.id'):
			data[q['colum']] = q['value']
		data_zona = keys_add_none(data,'codigo,tipo_zona_id,nombre,ubigeo_id,estado,entidad_id')
		ofi = Zona.objects.create(**data_zona)
		return Response(self.get_serializer(ofi).data)

	@list_route(url_path='edit', methods=['get'], permission_classes=[LimonPermission,])
	def get_edit(self, request):
		id = request.query_params.get('id', None)
		model = Zona.objects.get(pk=id)
		data = self.get_serializer(model).data
		data['pais'] = data['ubigeo']['pais']
		return Response(data)


	@list_route(url_path='update', methods=['put'], permission_classes=[LimonPermission,])
	def get_update(self, request, pk=None):
		# model = Zona.objects.get(pk=request.data['id'])
		data = request.data
		for q in values_obj_add(data, 'tipo_zona_id,ubigeo_id', 'tipo_zona.id,ubigeo.id'):
			data[q['colum']] = q['value']
		data_zona = keys_add_none(data,'codigo,tipo_zona_id,nombre,ubigeo_id,estado,entidad_id')
		ofi = Zona.objects.filter(id=request.data['id']).update(**data_zona)
		model = Zona.objects.get(pk=request.data['id'])
		return Response(self.get_serializer(model).data)

	@list_route(url_path='delete', methods=['delete'], permission_classes=[LimonPermission,])
	def get_delete(self, request, *args, **kwargs):
		try:
			id = request.query_params.get('id', None)
			model = Zona.objects.get(pk=id).delete()
			return Response({'status':True, 'id':id})
		except ProtectedError as e:
			return Response(e.errors, status=status.HTTP_400_BAD_REQUEST)

	@list_route(url_path='searchform', methods=['get'], permission_classes=[LimonPermission,])
	def get_searchform(self, request, *args, **kwargs):
		queryset = search_filter(self.get_queryset(), request)
		serializer = ZonaSerializer(queryset, many=True)
		return Response(serializer.data)