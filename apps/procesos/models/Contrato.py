from apps.comun.models.Persona import Persona
from ..models.TipoUsoServicio import TipoUsoServicio
from ..models.Predio import Predio
from ..models.ComprobConfig import ComprobConfig
from apps.comun.models.Entidad import Entidad
from apps.procesos.always.Choices import FORMA_COBRANZA
from apps.procesos.always.Choices import ESTADO_CONTRATO
from django.db import models

class Contrato(models.Model):
	entidad = models.ForeignKey(Entidad, related_name='entidad_contratos', db_column='entidad_id', on_delete=models.PROTECT)
	comprob_config = models.ForeignKey(ComprobConfig, related_name='comprob_config_contratos', db_column='comprob_config_id', blank=True, null=True, on_delete=models.PROTECT)
	persona = models.ForeignKey(Persona, related_name='persona_contratos', db_column='persona_id', on_delete=models.PROTECT)
	numero = models.CharField(max_length=10, blank=True, null=True)
	forma_cobranza = models.CharField(max_length=3, choices=FORMA_COBRANZA, blank=True, null=True)
	tipo_uso_servicio = models.ForeignKey(TipoUsoServicio, related_name='tipo_uso_servicio_contratos', 
		db_column='tipo_uso_servicio_id', on_delete=models.PROTECT)
	predio = models.ForeignKey(Predio, related_name='predio_contratos', db_column='predio_id', blank=True, null=True, on_delete=models.PROTECT)
	usuario = models.ForeignKey(Persona, related_name='usuario_contratos', db_column='usuario_id', on_delete=models.PROTECT)
	comentario = models.CharField(max_length=250, blank=True, null=True)
	fecha_contrato = models.DateField(blank=True, null=True)
	estado = models.CharField(max_length=1, choices=ESTADO_CONTRATO, blank=True, null=True, default='1')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		verbose_name = 'contrato'
		verbose_name_plural = 'Contratos'
		unique_together = ('comprob_config', 'numero')
		default_permissions = ()
		permissions = (
			('view_contrato',
			 'Listar Contratos'),
			('add_contrato',
			 'Agregar Contratos'),
			('update_contrato',
			 'Actualizar Contratos'),
			('delete_contrato',
			 'Eliminar Contratos'),
			('view_form_contrato',
			 'Listar Contratos en formulario'),
		)

	def __str__(self):
		return self.numero