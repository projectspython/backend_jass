from ..models.TipoMovimientoCuenta import TipoMovimientoCuenta
from django.db import models

class MotivoMovimientoCuenta(models.Model):
	tipo_movimiento_cuenta = models.ForeignKey(TipoMovimientoCuenta, 
		related_name='tipo_movimiento_cuenta_motivos_movimiento_cuenta',
		db_column='tipo_movimiento_cuenta_id', on_delete=models.PROTECT)
	codigo = models.CharField(max_length=12, unique=True, blank=True, null=True)
	nombre = models.CharField(max_length=120, unique=True)
	nombre_plural = models.CharField(max_length=120, blank=True, null=True)
	abreviatura = models.CharField(max_length=80, blank=True, null=True)
	estado = models.CharField(max_length=1, default='1')

	class Meta:
		db_table = 'procesos_motivo_movimiento_cuenta'
		verbose_name = 'Motivo movimiento cuenta'
		verbose_name_plural = 'Motivos movimiento cuenta'
		default_permissions = ()
		permissions = (
			('view_motivomovimientocuenta',
			 'Listado de motivos movimiento cuenta'),
			('add_motivomovimientocuenta',
			 'Agregar motivos movimiento cuenta'),
			('update_motivomovimientocuenta',
			 'Actualizar motivos movimiento cuenta'),
			('delete_motivomovimientocuenta',
			 'Eliminar motivos movimiento cuenta'),
			('view_form_motivomovimientocuenta',
			 'Listado de motivos movimiento cuenta en formulario'),
		)

	def __str__(self):
		return self.nombre



		