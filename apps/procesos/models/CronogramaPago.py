from apps.comun.models.Entidad import Entidad
from ..models.Contrato import Contrato
from ..models.Recibo import Recibo
from apps.comun.models.Persona import Persona
from apps.procesos.models.EstadoCronograma import EstadoCronograma
from ..models.MotivoMovimientoCuenta import MotivoMovimientoCuenta
from django.db import models

class CronogramaPago(models.Model):
	entidad = models.ForeignKey(Entidad, related_name='entidad_cronogramas_pago', 
		db_column='entidad_id', on_delete=models.PROTECT)
	contrato = models.ForeignKey(Contrato, related_name='contrato_cronogramas_pago', 
		db_column='contrato_id', blank=True, null=True, on_delete=models.PROTECT)
	recibo = models.ForeignKey(Recibo, related_name='recibo_cronogramas_pago', 
		db_column='recibo_id', blank=True, null=True, on_delete=models.PROTECT)
	persona = models.ForeignKey(Persona, related_name='persona_cronogramas_pago', 
		db_column='persona_id', on_delete=models.PROTECT)
	motivo_movimiento_cuenta = models.ForeignKey(MotivoMovimientoCuenta, related_name='motivo_mov_cuenta_cronogramas_pago', 
		db_column='motivo_movimiento_cuenta_id', on_delete=models.PROTECT)
	descripcion = models.CharField(max_length=120, blank=True, null=True)
	monto = models.DecimalField(max_digits=12, decimal_places=2)
	porc_interes = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
	fecha_cobranza = models.DateField(max_length=80, blank=True, null=True)
	padre = models.ForeignKey('self', related_name='padre_cronogramas_pago',
		db_column='padre_id', blank=True, null=True, on_delete=models.PROTECT)
	usuario = models.ForeignKey(Persona, related_name='usuario_cronogramas_pago',
		db_column='usuario_id', on_delete=models.PROTECT)
	estado_cronograma = models.ForeignKey(EstadoCronograma, related_name='estado_cronograma_cronogramas_pago',
		db_column='estado_cronograma_id', on_delete=models.PROTECT)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)


	class Meta:
		db_table = 'procesos_cronograma_pago'
		verbose_name = 'Cronograma Pago'
		verbose_name_plural = 'Cronogramas Pago'
		# unique_together = ('entidad','contrato','persona','motivo_mov_cuenta','padre','usuario','estado')
		default_permissions = ()
		permissions = (
			('view_cronogramapago',
			 'Listado de cronogramas pago'),
			('add_cronogramapago',
			 'Agregar cronogramas pago'),
			('update_cronogramapago',
			 'Actualizar cronogramas pago'),
			('delete_cronogramapago',
			 'Eliminar cronogramas pago'),
			('view_form_cronogramapago',
			 'Listado de cronogramas pago en formulario'),
		)
	def __str__(self):
		return  self.descripcion