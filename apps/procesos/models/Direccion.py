from apps.comun.models.Persona import Persona
from apps.comun.models.TipoDireccion import TipoDireccion
from apps.comun.models.Pais import Pais
from apps.comun.models.Ubigeo import Ubigeo
from apps.comun.models.TipoVia import TipoVia
from apps.comun.models.TipoNumeroVia import TipoNumeroVia
from apps.comun.models.TipoZona import TipoZona
from ..models.Predio import Predio
from django.db import models

class Direccion(models.Model):
	persona = models.ForeignKey(Persona, related_name='persona_direcciones', 
		db_column='persona_id', blank=True, null=True, on_delete=models.CASCADE)
	predio = models.ForeignKey(Predio, related_name='predio_direcciones', 
		db_column='predio_id', blank=True, null=True, on_delete=models.CASCADE)
	tipo_direccion = models.ForeignKey(TipoDireccion, related_name='tipo_direccion_direcciones', 
		db_column='tipo_direccion_id', on_delete=models.PROTECT)
	
	pais = models.ForeignKey(Pais, related_name='pais_direcciones', 
		db_column='pais_id', on_delete=models.PROTECT)
	ubigeo = models.ForeignKey(Ubigeo, related_name='ubigeo_direcciones', 
		db_column='ubigeo_id', blank=True, null=True, on_delete=models.PROTECT)
	completa = models.CharField(max_length=2, default='SI')
	direccion_detalle = models.TextField()
	tipo_via = models.ForeignKey(TipoVia, related_name='tipo_via_direcciones', 
		db_column='tipo_via_id', blank=True, null=True, on_delete=models.PROTECT)
	nombre_via = models.CharField(max_length=90, blank=True, null=True,)
	tipo_numero_via = models.ForeignKey(TipoNumeroVia, related_name='tipo_numero_via_direcciones', 
		db_column='tipo_numero_via_id', blank=True, null=True, on_delete=models.PROTECT)
	numero_via = models.CharField(max_length=20, blank=True, null=True,)
	manzana = models.CharField(max_length=20, blank=True, null=True,)
	lote = models.CharField(max_length=20, blank=True, null=True,)
	piso = models.CharField(max_length=12, blank=True, null=True,)
	dpto_interior = models.CharField(max_length=30, blank=True, null=True,)
	tipo_zona = models.ForeignKey(TipoZona, related_name='tipo_zona_direcciones', 
		db_column='tipo_zona_id', blank=True, null=True, on_delete=models.PROTECT)
	nombre_zona = models.CharField(max_length=60, blank=True, null=True,)
	referencia1 = models.CharField(max_length=150, blank=True, null=True,)
	referencia2 = models.CharField(max_length=150, blank=True, null=True,)
	punto_lat = models.CharField(max_length=30, blank=True, null=True,)
	punto_long = models.CharField(max_length=30, blank=True, null=True,)
	
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta(object):
		db_table = 'procesos_direccion'
		verbose_name = 'Dirección'
		verbose_name_plural = 'Direcciones'
		default_permissions = ()


	def __str__(self):
		return  self.direccion_detalle