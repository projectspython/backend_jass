from apps.comun.models.Persona import Persona
from apps.procesos.models.Recibo import Recibo
from apps.procesos.models.Servicio import Servicio
from django.db import models
from apps.procesos.always.Choices import ESTADO_RECIBO
from apps.procesos.models.ComprobConfig import ComprobConfig

class ReciboDetalle(models.Model):
	recibo = models.ForeignKey(Recibo, related_name='recibo_recibo_detalles', 
		db_column='recibo_id', on_delete=models.CASCADE)
	servicio = models.ForeignKey(Servicio, related_name='servicio_recibo_detalles', 
		db_column='servicio_id', on_delete=models.PROTECT)

	cantidad = models.DecimalField(max_digits=12, decimal_places=2)
	precio = models.DecimalField(max_digits=12, decimal_places=2)
	total = models.DecimalField(max_digits=12, decimal_places=2)
	
	descripcion = models.CharField(max_length=120,blank=True, null=True,)

	usuario = models.ForeignKey(Persona, related_name='usuario_recibo_detalles', 
		db_column='usuario_id', on_delete=models.PROTECT)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		db_table = 'procesos_recibo_detalle'
		verbose_name = 'Recibo Detalle'
		verbose_name_plural = 'Recibos Detalles'
		default_permissions = ()
		permissions = (
			('view_recibodetalle','Listar Recibo Detalle'),
			('add_recibodetalle','Agregar Recibo Detalle'),
			('update_recibodetalle','Actualizar Recibo Detalle'),
			('delete_recibodetalle','Eliminar Recibo Detalle'),
			('view_form_recibodetalle','Listar Recibo Detalle en formulario'),
		)

	def __str__(self):
		return self.recibo.numero