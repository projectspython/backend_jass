from ..models.Contrato import Contrato
from ..models.Servicio import Servicio
from django.db import models

class ContratoServicio(models.Model):
	contrato = models.ForeignKey(Contrato, related_name='contrato_contrato_servicios', db_column='contrato_id', 
		on_delete=models.CASCADE)
	servicio = models.ForeignKey(Servicio, related_name='servicio_contrato_servicios', db_column='servicio_id', 
		on_delete=models.PROTECT)
	precio = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		db_table = 'procesos_contrato_servicio'
		verbose_name = 'Contrato de Servicio'
		verbose_name_plural = 'Contratos de Servicios'
		default_permissions = ()
		permissions = (
			('view_contratoservicio',
			 'Listar Contratos de Servicios'),
			('add_contratoservicio',
			 'Agregar Contratos de Servicios'),
			('update_contratoservicio',
			 'Actualizar Contratos de Servicios'),
			('delete_contratoservicio',
			 'Eliminar Contratos de Servicios'),
			('view_form_contratoservicio',
			 'Listar Contratos de Servicios en formulario'),
		)

	def __str__(self):
		return self.contrato