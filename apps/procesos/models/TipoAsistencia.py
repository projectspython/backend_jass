from django.db import models

class TipoAsistencia(models.Model):
	codigo = models.CharField(max_length=12, unique=True)
	nombre = models.CharField(max_length=30, unique=True)
	nombre_singular = models.CharField(max_length=30, unique=True)
	estado = models.CharField(max_length=1, default='1')

	class Meta:
		db_table = 'procesos_tipo_asistencia'
		verbose_name = 'Tipo de Asistencia'
		verbose_name_plural = 'Tipos de Asistencia'
		default_permissions = ()
		permissions = (
			('view_tipoasistencia',
			 'Listar Tipos de Asistencia'),
			('add_tipoasistencia',
			 'Agregar Tipos de Asistencia'),
			('update_tipoasistencia',
			 'Actualizar Tipos de Asistencia'),
			('delete_tipoasistencia',
			 'Eliminar Tipos de Asistencia'),
			('view_form_tipoasistencia',
			 'Listar Tipos de Asistencia en formulario'),
		)

	def __str__(self):
		return self.nombre