from django.db import models

class TipoEvento(models.Model):

	codigo = models.CharField(max_length=12, unique=True)
	nombre = models.CharField(max_length=60, unique=True)
	nombre_plural = models.CharField(max_length=50, unique=True)
	estado = models.CharField(max_length=1, default='1')

	class Meta:
		db_table = 'procesos_tipo_evento'
		verbose_name = 'Tipo de evento'
		verbose_name_plural = 'Tipos de eventos'
		default_permissions = ()
		permissions = (
			('view_tiposevento',
			 'Listado de tipos eventos'),
			('add_tiposevento',
			 'Agregar tipos de eventos'),
			('update_tiposevento',
			 'Actualizar tipos de eventos'),
			('delete_tiposevento',
			 'Eliminar tipos de eventos'),
			('view_form_tiposevento',
			 'Listado de tipos de eventos en formulario'),
		)

	def __str__(self):
		return self.nombre
