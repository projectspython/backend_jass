from apps.comun.models.Entidad import Entidad
from ..models.Comprobante import Comprobante
from django.db import models

class ComprobConfig(models.Model):
	entidad = models.ForeignKey(Entidad, related_name='entidad_comprob_configs', db_column='entidad_id', on_delete=models.PROTECT)
	comprobante = models.ForeignKey(Comprobante, related_name='comprobante_comprob_configs', db_column='comprobante_id', on_delete=models.PROTECT)
	serie = models.CharField(max_length=8)
	nro_inicial = models.IntegerField()
	nro_limite = models.IntegerField(blank=True, null=True)
	correlativo = models.IntegerField()
	nro_autoriz = models.CharField(max_length=40, blank=True, null=True)
	completa_ceros = models.CharField(max_length=10, blank=True, null=True)
	orden = models.IntegerField()
	estado = models.CharField(max_length=1, default='1')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		db_table = 'procesos_comprob_config'
		verbose_name = 'Comprob. Config'
		verbose_name_plural = 'Comprob. Configs'
		default_permissions = ()
		permissions = (
			('view_comprobconfig',
			 'Listar Comprob. Configs'),
			('add_comprobconfig',
			 'Agregar Comprob. Configs'),
			('update_comprobconfig',
			 'Actualizar Comprob. Configs'),
			('delete_comprobconfig',
			 'Eliminar Comprob. Configs'),
			('view_form_comprobconfig',
			 'Listar Comprob. Configs en formulario'),
		)

	def __str__(self):
		return self.serie