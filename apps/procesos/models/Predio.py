from apps.comun.models.Persona import Persona
from ..models.EstadoPredio import EstadoPredio
from ..models.Zona import Zona
from django.db import models

class Predio(models.Model):
	codigo = models.CharField(max_length=10, unique=True, blank=True, null=True)
	representante = models.ForeignKey(Persona, related_name='representante_predios', db_column='representante_id', blank=True, null=True, on_delete=models.PROTECT)
	duenio = models.ForeignKey(Persona, related_name='duenio_predios', db_column='duenio_id', blank=True, null=True, on_delete=models.PROTECT)
	zona = models.ForeignKey(Zona, related_name='zona_predios', db_column='zona_id', on_delete=models.PROTECT)
	partida_registral = models.CharField(max_length=20, blank=True, null=True)
	estado_predio = models.ForeignKey(EstadoPredio, related_name='estado_predio_predios', db_column='estado_predio_id', on_delete=models.PROTECT)
	estado_descripcion = models.CharField(max_length=60, blank=True, null=True)
	usuario = models.ForeignKey(Persona, related_name='usuario_predios', db_column='usuario_id', on_delete=models.PROTECT)
	estado = models.CharField(max_length=1, default='1')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		verbose_name = 'predio'
		verbose_name_plural = 'Predios'
		default_permissions = ()
		permissions = (
			('view_predio',
			 'Listar Predios'),
			('add_predio',
			 'Agregar Predios'),
			('update_predio',
			 'Actualizar Predios'),
			('delete_predio',
			 'Eliminar Predios'),
			('view_form_predio',
			 'Listar Predios en formulario'),
		)

	def __str__(self):
		return self.duenio