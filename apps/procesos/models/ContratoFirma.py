from ..models.Contrato import Contrato
from apps.comun.models.Persona import Persona
from apps.comun.models.Cargo import Cargo
from django.db import models

class ContratoFirma(models.Model):
	contrato = models.ForeignKey(Contrato, related_name='contrato_contrato_firma', 
		db_column='contrato_id', on_delete=models.CASCADE)
	persona = models.ForeignKey(Persona, related_name='persona_contrato_firma',
		db_column='persona_id', on_delete=models.PROTECT)
	cargo = models.ForeignKey(Cargo, db_column='cargo_id', related_name="cargo_contrato_firma", 
		on_delete=models.PROTECT)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		db_table = 'procesos_contrato_firma'
		verbose_name = 'Contrato firma'
		verbose_name_plural = 'Contratos firma'
		default_permissions = ()
		# permissions = (
		# 	('view_contratofirma',
		# 	 'Listar Contrato Firma'),
		# 	('add_contratofirma',
		# 	 'Agregar Contrato Firma'),
		# 	('update_contratofirma',
		# 	 'Actualizar Contrato Firma'),
		# 	('delete_contratofirma',
		# 	 'Eliminar Contrato Firma'),
		# 	('view_form_contratofirma',
		# 	 'Listar Contrato Firma en formulario'),
		# )

	def __str__(self):
		return self.numero