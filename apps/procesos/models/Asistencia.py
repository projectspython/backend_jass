from apps.comun.models.Entidad import Entidad
from ..models.Evento import Evento
from ..models.EstadoAsistencia import EstadoAsistencia
from apps.comun.models.Persona import Persona
from ..models.Representante import Representante
from apps.procesos.always.Choices import PERSONA_ASISTENTE
from django.db import models

class Asistencia(models.Model):
	usuario = models.ForeignKey(Persona, related_name='usuario_asistencias', db_column='usuario_id', on_delete=models.PROTECT)
	entidad = models.ForeignKey(Entidad, related_name='entidad_asistencias', db_column='entidad_id', on_delete=models.PROTECT)
	evento = models.ForeignKey(Evento, related_name='evento_asistencias', db_column='evento_id', on_delete=models.PROTECT)
	estado_asistencia = models.ForeignKey(EstadoAsistencia, related_name='estado_asistencia_asistencias', db_column='estado_asistencia_id', blank=True, null=True, on_delete=models.PROTECT)
	persona = models.ForeignKey(Persona, related_name='persona_asistencias', db_column='persona_id', on_delete=models.PROTECT)
	representante = models.ForeignKey(Persona, related_name='representante_asistencias', db_column='representante_id', blank=True, null=True, on_delete=models.PROTECT)
	persona_asistente = models.CharField(max_length=3, choices=PERSONA_ASISTENTE, blank=True, null=True)
	fecha_reg = models.DateField()
	hora_reg = models.TimeField()
	comentario = models.TextField(null=True, blank=True)
	estado = models.CharField(max_length=1, default='1')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		db_table = 'procesos_asistencia'
		verbose_name = 'Asistencia'
		verbose_name_plural = 'Asistencias'
		unique_together = ('evento', 'persona')
		default_permissions = ()
		permissions = (
			('view_asistencia',
			 'Listar Asistencias'),
			('add_asistencia',
			 'Agregar Asistencias'),
			('update_asistencia',
			 'Actualizar Asistencias'),
			('delete_asistencia',
			 'Eliminar Asistencias'),
			('view_form_asistencia',
			 'Listar Asistencias en formulario'),
		)

	def __str__(self):
		return self.evento