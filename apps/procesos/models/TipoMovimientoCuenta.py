from django.db import models

class TipoMovimientoCuenta(models.Model):
	codigo = models.CharField(max_length=12, unique=True, blank=True, null=True)
	nombre = models.CharField(max_length=120, unique=True)
	nombre_plural = models.CharField(max_length=120, blank=True, null=True)
	estado = models.CharField(max_length=1, default='1')

	class Meta:
		db_table = 'procesos_tipo_movimiento_cuenta'
		verbose_name = 'Tipo Movimiento Cuenta'
		verbose_name_plural = 'Tipos Movimientos Cuentas'
		default_permissions = ()
		permissions = (
			('view_tipomovimientocuenta',
			 'Listado de tipos de movimientos de cuentas'),
			('add_tipomovimientocuenta',
			 'Agregar tipos de movimientos de cuentas'),
			('update_tipomovimientocuenta',
			 'Actualizar tipos de movimientos de cuentas'),
			('delete_tipomovimientocuenta',
			 'Eliminar tipos de movimientos de cuentas'),
			('view_form_tipomovimientocuenta',
			 'Listado de tipos de movimientos de cuentas en formulario'),
		)

	def __str__(self):
		return self.nombre
