from apps.comun.models.Entidad import Entidad
from apps.comun.models.Persona import Persona
from ..models.TipoAfiliacion import TipoAfiliacion
from django.db import models

class Afiliacion(models.Model):

	codigo = models.CharField(max_length=12, unique=True, default='0')
	entidad = models.ForeignKey(Entidad, related_name='entidad_afiliaciones', 
		db_column='entidad_id', on_delete=models.PROTECT)
	persona = models.ForeignKey(Persona, related_name='persona_afiliaciones', 
		db_column='persona_id', on_delete=models.PROTECT)
	tipo_afiliacion = models.ForeignKey(TipoAfiliacion, related_name='tipoafiliacion_afiliaciones', 
		db_column='tipo_afiliacion_id', on_delete=models.PROTECT)
	fecha_ingreso = models.DateField(max_length=80)
	fecha_afiliacion = models.DateField(max_length=80)
	estado = models.CharField(max_length=1, default='1')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		verbose_name = 'Afiliacion'
		verbose_name_plural = 'Afiliaciones'
		default_permissions = ()
		unique_together = ('entidad', 'persona')
		permissions = (
			('view_afiliacion',
			 'Listado de Afiliaciones'),
			('add_afiliacion',
			 'Agregar Afiliaciones'),
			('update_afiliacion',
			 'Actualizar Afiliaciones'),
			('delete_afiliacion',
			 'Eliminar Afiliaciones'),
			('view_form_afiliacion',
			 'Listado de Afiliaciones en formulario'),
		)

	def __str__(self):
		return self.tipo_afiliacion
