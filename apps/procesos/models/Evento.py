from ..models.TipoEvento import TipoEvento
from apps.comun.models.Entidad import Entidad
from ..models.TipoAsistencia import TipoAsistencia
# from apps.procesos.always.Choices import UNIDAD_TIEMPO
from django.db import models

class Evento(models.Model):
	entidad = models.ForeignKey(Entidad, related_name='entidad_eventos', db_column='entidad_id', on_delete=models.PROTECT)
	tipo_evento = models.ForeignKey(TipoEvento, related_name='tipo_evento_eventos', db_column='tipo_evento_id', on_delete=models.PROTECT)
	nombre = models.CharField(max_length=150, blank=True, null=True)
	tipo_asistencia = models.ForeignKey(TipoAsistencia, related_name='tipo_asistencia_eventos', db_column='tipo_asistencia_id', on_delete=models.PROTECT)
	descripcion = models.TextField(null=True, blank=True)
	fecha = models.DateField()
	hora_inicio = models.TimeField()
	hora_tolerancia = models.TimeField(blank=True, null=True)
	hora_fin = models.TimeField(blank=True, null=True)
	# tolerancia = models.IntegerField(blank=True, null=True)
	# unidad_tiempo = models.CharField(max_length=3, choices=UNIDAD_TIEMPO, blank=True, null=True)
	estado = models.CharField(max_length=1, default='1')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		db_table = 'procesos_evento'
		verbose_name = 'Evento'
		verbose_name_plural = 'Eventos'
		default_permissions = ()
		permissions = (
			('view_evento',
			 'Listar Eventos'),
			('add_evento',
			 'Agregar Eventos'),
			('update_evento',
			 'Actualizar Eventos'),
			('delete_evento',
			 'Eliminar Eventos'),
			('view_form_evento',
			 'Listar Eventos en formulario'),
		)

	def __str__(self):
		return str(self.tipo_evento_id)