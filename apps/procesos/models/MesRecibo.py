from apps.comun.models.Persona import Persona
from apps.comun.models.Entidad import Entidad
from apps.comun.models.Mes import Mes
from django.db import models
from apps.procesos.always.Choices import ESTADO_MES_RECIBO

class MesRecibo(models.Model):
	entidad = models.ForeignKey(Entidad, related_name='entidad_mes_recibos', db_column='entidad_id', on_delete=models.PROTECT)
	mes = models.ForeignKey(Mes, related_name='mes_mes_recibos', db_column='mes_id', on_delete=models.PROTECT)
	anio = models.IntegerField()
	fecha_emision = models.DateField()
	fecha_vencimiento = models.DateField()
	fecha_corte = models.DateField(blank=True, null=True)
	texto_html = models.TextField(blank=True, null=True)
	estado = models.CharField(max_length=3, choices=ESTADO_MES_RECIBO)
	usuario = models.ForeignKey(Persona, related_name='usuario_mes_recibos', 
		db_column='usuario_id', on_delete=models.PROTECT)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		db_table = 'procesos_mes_recibo'
		verbose_name = 'Mes recibo'
		verbose_name_plural = 'Meses recibo'
		unique_together = ('entidad', 'mes','anio')
		default_permissions = ()
		permissions = (
			('view_mesrecibo','Listar Mes recibo'),
			('add_mesrecibo','Agregar Mes recibo'),
			('update_mesrecibo','Actualizar Mes recibo'),
			('delete_mesrecibo','Eliminar Mes recibo'),
			('view_form_mesrecibo','Listar Mes recibo en formulario'),
		)

	def __str__(self):
		return self.anio