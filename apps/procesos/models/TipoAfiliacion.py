from django.db import models

class TipoAfiliacion(models.Model):
	codigo = models.CharField(max_length=12, unique=True)
	nombre = models.CharField(max_length=30, unique=True)
	nombre_plural = models.CharField(max_length=50, unique=True)
	estado = models.CharField(max_length=1, default='1')

	class Meta:
		db_table = 'procesos_tipo_afiliacion'
		verbose_name = 'Tipo de Afiliacion'
		verbose_name_plural = 'Tipos de Afiliacion'
		default_permissions = ()
		permissions = (
			('view_tipoafiliacion',
			 'Listar Tipos de Afiliacion'),
			('add_tipoafiliacion',
			 'Agregar Tipos de Afiliacion'),
			('update_tipoafiliacion',
			 'Actualizar Tipos de Afiliacion'),
			('delete_tipoafiliacion',
			 'Eliminar Tipos de Afiliacion'),
			('view_form_tipoafiliacion',
			 'Listar Tipos de Afiliacion en formulario'),
		)

	def __str__(self):
		return self.nombre