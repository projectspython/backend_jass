from django.db import models

class EstadoAsistencia(models.Model):

	codigo = models.CharField(max_length=12, unique=True)
	nombre = models.CharField(max_length=60, unique=True)
	nombre_plural = models.CharField(max_length=50, unique=True, blank=True, null=True)
	estado = models.CharField(max_length=1, default='1')

	class Meta:
		db_table = 'procesos_estado_asistencia'
		verbose_name = 'Estado de Asistencia'
		verbose_name_plural = 'Estados de Asistencias'
		default_permissions = ()
		permissions = (
			('view_estadoasistencia',
			 'Listado de estados de asistencias'),
			('add_estadoasistencia',
			 'Agregar estados de asistencias'),
			('update_estadoasistencia',
			 'Actualizar estados de asistencias'),
			('delete_estadoasistencia',
			 'Eliminar estados de asistencias'),
			('view_form_estadoasistencia',
			 'Listado de estados de asistencias en formulario'),
		)

	def __str__(self):
		return self.nombre
