from django.db import models

class EstadoPredio(models.Model):
	codigo = models.CharField(max_length=10, unique=True)
	nombre = models.CharField(max_length=30, unique=True)
	estado = models.CharField(max_length=1, default='1')

	class Meta:
		db_table = 'procesos_estado_predio'
		verbose_name = 'Estado de Predio'
		verbose_name_plural = 'Estados de los Predios'
		default_permissions = ()
		permissions = (
			('view_estadopredio',
			 'Listar Estados de los Predios'),
			('add_estadopredio',
			 'Agregar Estados de los Predios'),
			('update_estadopredio',
			 'Actualizar Estados de los Predios'),
			('delete_estadopredio',
			 'Eliminar Estados de los Predios'),
			('view_form_estadopredio',
			 'Listar Estados de los Predios en formulario'),
		)

	def __str__(self):
		return self.nombre