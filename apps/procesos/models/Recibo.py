from apps.comun.models.Persona import Persona
from apps.procesos.models.MesRecibo import MesRecibo
from apps.procesos.models.Contrato import Contrato
from django.db import models
from apps.procesos.always.Choices import ESTADO_RECIBO
from apps.procesos.models.ComprobConfig import ComprobConfig
from ..models.TipoUsoServicio import TipoUsoServicio
from apps.procesos.always.Choices import FORMA_COBRANZA

class Recibo(models.Model):
	mes_recibo = models.ForeignKey(MesRecibo, related_name='mes_recibo_recibos', 
		db_column='mes_recibo_id', on_delete=models.PROTECT)
	contrato = models.ForeignKey(Contrato, related_name='contrato_recibos', 
		db_column='contrato_id', on_delete=models.PROTECT)
	comprob_config = models.ForeignKey(ComprobConfig, related_name='comprob_config_recibos', 
		db_column='comprob_config_id', blank=True, null=True, on_delete=models.PROTECT)
	numero = models.CharField(max_length=10)
	fecha_emision = models.DateField()
	monto_total = models.DecimalField(max_digits=12, decimal_places=2)
	estado = models.CharField(max_length=3, choices=ESTADO_RECIBO)
	usuario = models.ForeignKey(Persona, related_name='usuario_recibos', 
		db_column='usuario_id', on_delete=models.PROTECT)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	forma_cobranza = models.CharField(max_length=3, choices=FORMA_COBRANZA, blank=True, null=True)
	tipo_uso_servicio = models.ForeignKey(TipoUsoServicio, related_name='tipo_uso_servicio_recibos', 
		db_column='tipo_uso_servicio_id', on_delete=models.PROTECT, blank=True, null=True)

	class Meta:
		verbose_name = 'Recibo'
		verbose_name_plural = 'Recibos'
		default_permissions = ()
		permissions = (
			('view_recibo','Listar Recibo'),
			('add_recibo','Agregar Recibo'),
			('update_recibo','Actualizar Recibo'),
			('delete_recibo','Eliminar Recibo'),
			('view_form_recibo','Listar Recibo en formulario'),
		)

	def __str__(self):
		return self.numero