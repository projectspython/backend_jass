from django.db import models

class Servicio(models.Model):
	codigo = models.CharField(max_length=10, unique=True)
	nombre = models.CharField(max_length=60, unique=True)
	nombre_plural = models.CharField(max_length=80)
	precio = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
	estado = models.CharField(max_length=1, default='1')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		verbose_name = 'servicio'
		verbose_name_plural = 'Servicios'
		default_permissions = ()
		permissions = (
			('view_servicio',
			 'Listar Servicios'),
			('add_servicio',
			 'Agregar Servicios'),
			('update_servicio',
			 'Actualizar Servicios'),
			('delete_servicio',
			 'Eliminar Servicios'),
			('view_form_servicio',
			 'Listar Servicios en formulario'),
		)

	def __str__(self):
		return self.nombre