from django.db import models

class Comprobante(models.Model):
	codigo = models.CharField(max_length=10, unique=True)
	nombre = models.CharField(max_length=60, unique=True)
	nombre_plural = models.CharField(max_length=70)
	abreviatura = models.CharField(max_length=40)
	codigo_une = models.CharField(max_length=10, blank=True, null=True)
	orden = models.IntegerField()
	url_entrada = models.CharField(max_length=120, blank=True, null=True)
	url_salida = models.CharField(max_length=120, blank=True, null=True)
	estado = models.CharField(max_length=1, default='1')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		verbose_name = 'comprobante'
		verbose_name_plural = 'Comprobantes'
		default_permissions = ()
		permissions = (
			('view_comprobante',
			 'Listar Comprobantes'),
			('add_comprobante',
			 'Agregar Comprobantes'),
			('update_comprobante',
			 'Actualizar Comprobantes'),
			('delete_comprobante',
			 'Eliminar Comprobantes'),
			('view_form_comprobante',
			 'Listar Comprobantes en formulario'),
		)

	def __str__(self):
		return self.nombre