from apps.procesos.models.MesRecibo import MesRecibo
from django.db import models

class MesReciboRecom(models.Model):
	mes_recibo = models.ForeignKey(MesRecibo, related_name='entidad_mes_recibos', 
		db_column='mes_recibo_id', on_delete=models.CASCADE)
	detalle = models.TextField()
	class Meta:
		db_table = 'procesos_mes_recibo_recom'
		verbose_name = 'Mes recibo Recom'
		verbose_name_plural = 'Meses recibo Recom'
		default_permissions = ()
		# permissions = (
		# 	('view_mesrecibo','Listar Mes recibo'),
		# 	('add_mesrecibo','Agregar Mes recibo'),
		# 	('update_mesrecibo','Actualizar Mes recibo'),
		# 	('delete_mesrecibo','Eliminar Mes recibo'),
		# 	('view_form_mesrecibo','Listar Mes recibo en formulario'),
		# )

	def __str__(self):
		return self.detalle