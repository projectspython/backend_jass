from django.db import models

class TipoUsoServicio(models.Model):
	codigo = models.CharField(max_length=10, unique=True)
	nombre = models.CharField(max_length=60, unique=True)
	estado = models.CharField(max_length=1, default='1')

	class Meta:
		db_table = 'procesos_tipo_uso_servicio'
		verbose_name = 'Tipo de uso del servicio'
		verbose_name_plural = 'Tipos de uso de los servicios'
		default_permissions = ()
		permissions = (
			('view_tipousoservicio',
			 'Listar Tipos de uso de servicios'),
			('add_tipousoservicio',
			 'Agregar Tipos de uso de servicios'),
			('update_tipousoservicio',
			 'Actualizar Tipos de uso de servicios'),
			('delete_tipousoservicio',
			 'Eliminar Tipos de uso de servicios'),
			('view_form_tipousoservicio',
			 'Listar Tipos de uso de servicios en formulario'),
		)

	def __str__(self):
		return self.nombre