from apps.comun.models.Ubigeo import Ubigeo
from apps.comun.models.TipoZona import TipoZona
from django.db import models
from apps.comun.models.Entidad import Entidad

class Zona(models.Model):
	entidad = models.ForeignKey(Entidad, related_name='entidad_zonas', 
		db_column='entidad_id', on_delete=models.PROTECT, blank=True, null=True)
	codigo = models.CharField(max_length=10, unique=True)
	nombre = models.CharField(max_length=60)
	ubigeo = models.ForeignKey(Ubigeo, related_name='ubigeo_zonas', db_column='ubigeo_id', on_delete=models.PROTECT)
	tipo_zona = models.ForeignKey(TipoZona, related_name='tipo_zona_zonas', db_column='tipo_zona_id', on_delete=models.PROTECT)
	estado = models.CharField(max_length=1, default='1')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		verbose_name = 'zona'
		verbose_name_plural = 'zonas'
		unique_together = ('nombre', 'ubigeo', 'tipo_zona')
		default_permissions = ()
		permissions = (
			('view_zona',
			 'Listar zonas'),
			('add_zona',
			 'Agregar zonas'),
			('update_zona',
			 'Actualizar zonas'),
			('delete_zona',
			 'Eliminar zonas'),
			('view_form_zona',
			 'Listar zonas en formulario'),
		)

	def __str__(self):
		return self.nombre