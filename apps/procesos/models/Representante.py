from apps.procesos.models.Afiliacion import Afiliacion
from apps.comun.models.Persona import Persona
from django.db import models

class Representante(models.Model):

	persona = models.ForeignKey(Persona, related_name='persona_representantes',
		db_column='persona_id', on_delete=models.PROTECT)
	afiliacion = models.ForeignKey(Afiliacion, related_name='afiliacion_representantes',
		db_column='afiliacion_id', on_delete=models.PROTECT)
	estado = models.CharField(max_length=1, default='1')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		db_table = 'procesos_representante'
		verbose_name = 'representante'
		verbose_name_plural = 'representantes'
		default_permissions = ()
		unique_together = ('persona', 'afiliacion')
		permissions = (
			('view_representante',
			 'Listado de representantes'),
			('add_representante',
			 'Agregar representantes'),
			('update_representante',
			 'Actualizar representantes'),
			('delete_representante',
			 'Eliminar representantes'),
			('view_form_representante',
			 'Listado de representantes en formulario'),
		)

	def __str__(self):
		return self.persona
