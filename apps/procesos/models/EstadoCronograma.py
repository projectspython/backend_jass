from django.db import models

class EstadoCronograma(models.Model):
	codigo = models.CharField(max_length=12, unique=True)
	nombre = models.CharField(max_length=30, unique=True)
	estado = models.CharField(max_length=1, default='1')
	color = models.CharField(max_length=20)

	class Meta:
		db_table = 'procesos_estado_cronograma'
		verbose_name = 'Estado de Cronograma'
		verbose_name_plural = 'Estados de los Cronogramas'
		default_permissions = ()
		permissions = (
			('view_estadocronograma',
			 'Listar Estados de los Cronogramas'),
			('add_estadocronograma',
			 'Agregar Estados de los Cronogramas'),
			('update_estadocronograma',
			 'Actualizar Estados de los Cronogramas'),
			('delete_estadocronograma',
			 'Eliminar Estados de los Cronogramas'),
			('view_form_estadocronograma',
			 'Listar Estados de los Cronogramas en formulario'),
		)

	def __str__(self):
		return self.nombre