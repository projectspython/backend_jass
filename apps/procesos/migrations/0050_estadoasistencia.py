# Generated by Django 2.0.7 on 2018-09-12 12:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('procesos', '0049_tipoevento'),
    ]

    operations = [
        migrations.CreateModel(
            name='EstadoAsistencia',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('codigo', models.CharField(max_length=12, unique=True)),
                ('nombre', models.CharField(max_length=60, unique=True)),
                ('nombre_plural', models.CharField(blank=True, max_length=50, null=True, unique=True)),
                ('estado', models.CharField(default='1', max_length=1)),
            ],
            options={
                'verbose_name': 'Estado de Asistencia',
                'verbose_name_plural': 'Estados de Asistencias',
                'db_table': 'procesos_estado_asistencia',
                'permissions': (('view_estadoasistencia', 'Listado de estados de asistencias'), ('add_estadoasistencia', 'Agregar estados de asistencias'), ('update_estadoasistencia', 'Actualizar estados de asistencias'), ('delete_estadoasistencia', 'Eliminar estados de asistencias'), ('view_form_estadoasistencia', 'Listado de estados de asistencias en formulario')),
                'default_permissions': (),
            },
        ),
    ]
