# Generated by Django 2.0.7 on 2018-07-12 15:59

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('comun', '0057_auto_20180711_1731'),
        ('procesos', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ComprobConfig',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('serie', models.CharField(max_length=8)),
                ('nro_inicial', models.IntegerField()),
                ('nro_limite', models.IntegerField(blank=True, null=True)),
                ('correlativo', models.IntegerField()),
                ('nro_autoriz', models.CharField(blank=True, max_length=40, null=True)),
                ('completa_ceros', models.CharField(blank=True, max_length=10, null=True)),
                ('orden', models.IntegerField()),
                ('estado', models.CharField(default='1', max_length=1)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('comprobante', models.ForeignKey(db_column='comprobante_id', on_delete=django.db.models.deletion.PROTECT, related_name='comprobante_comprob_configs', to='procesos.Comprobante')),
                ('entidad', models.ForeignKey(db_column='entidad_id', on_delete=django.db.models.deletion.PROTECT, related_name='entidad_comprob_configs', to='comun.Entidad')),
            ],
            options={
                'verbose_name': 'Comprob. Config',
                'verbose_name_plural': 'Comprob. Configs',
                'db_table': 'procesos_comprob_config',
                'permissions': (('view_comprobconfig', 'Listar Comprob. Configs'), ('add_comprobconfig', 'Agregar Comprob. Configs'), ('update_comprobconfig', 'Actualizar Comprob. Configs'), ('delete_comprobconfig', 'Eliminar Comprob. Configs'), ('view_form_comprobconfig', 'Listar Comprob. Configs en formulario')),
                'default_permissions': (),
            },
        ),
    ]
