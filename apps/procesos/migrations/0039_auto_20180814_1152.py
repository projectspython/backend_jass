# Generated by Django 2.0.7 on 2018-08-14 11:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('procesos', '0038_zona_entidad'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='contrato',
            name='detalle_firma',
        ),
        migrations.RemoveField(
            model_name='contrato',
            name='nro_cuotas',
        ),
        migrations.AlterField(
            model_name='contrato',
            name='estado',
            field=models.CharField(blank=True, choices=[('0', 'Anulado'), ('1', 'Registrado'), ('2', 'Habilitado'), ('3', 'Terminado')], default='1', max_length=1, null=True),
        ),
        migrations.AlterField(
            model_name='contrato',
            name='forma_cobranza',
            field=models.CharField(blank=True, choices=[('SME', 'Sin Medidor'), ('CME', 'Con Medidor')], max_length=3, null=True),
        ),
        migrations.AlterField(
            model_name='recibo',
            name='forma_cobranza',
            field=models.CharField(blank=True, choices=[('SME', 'Sin Medidor'), ('CME', 'Con Medidor')], max_length=3, null=True),
        ),
    ]
