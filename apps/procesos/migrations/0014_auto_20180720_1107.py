# Generated by Django 2.0.7 on 2018-07-20 11:07

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('procesos', '0013_auto_20180720_1103'),
    ]

    operations = [
        migrations.AddField(
            model_name='cronogramapago',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='cronogramapago',
            name='updated_at',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
