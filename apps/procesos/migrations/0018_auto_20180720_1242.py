# Generated by Django 2.0.7 on 2018-07-20 12:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('procesos', '0017_auto_20180720_1235'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comprobante',
            name='abreviatura',
            field=models.CharField(max_length=20),
        ),
        migrations.AlterField(
            model_name='comprobante',
            name='nombre_plural',
            field=models.CharField(max_length=70),
        ),
    ]
