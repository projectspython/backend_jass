# Generated by Django 2.0.7 on 2018-08-03 11:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('procesos', '0027_remove_servicio_padre'),
    ]

    operations = [
        migrations.AddField(
            model_name='contrato',
            name='detalle_firma',
            field=models.CharField(blank=True, max_length=250, null=True),
        ),
        migrations.AlterField(
            model_name='contrato',
            name='estado',
            field=models.CharField(blank=True, choices=[('0', 'Anulado'), ('1', 'Registrado'), ('2', 'Habilitado')], max_length=1, null=True),
        ),
    ]
