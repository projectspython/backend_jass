# Generated by Django 2.0.7 on 2018-08-19 13:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('procesos', '0043_remove_afiliacion_codigo'),
    ]

    operations = [
        migrations.AddField(
            model_name='afiliacion',
            name='codigo',
            field=models.CharField(default='0', max_length=12, unique=True),
        ),
    ]
