# Generated by Django 2.0.7 on 2018-08-15 14:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('procesos', '0040_auto_20180814_1209'),
    ]

    operations = [
        migrations.AddField(
            model_name='contrato',
            name='fecha_contrato',
            field=models.DateField(blank=True, null=True),
        ),
    ]
