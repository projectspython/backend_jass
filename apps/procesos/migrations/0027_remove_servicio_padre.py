# Generated by Django 2.0.7 on 2018-08-03 10:57

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('procesos', '0026_contrato_comentario'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='servicio',
            name='padre',
        ),
    ]
