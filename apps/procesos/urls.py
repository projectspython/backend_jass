from django.conf.urls import url, include
from rest_framework import routers
from .views.ComprobanteView import ComprobanteViewSet
from .views.ComprobConfigView import ComprobConfigViewSet
from .views.ContratoServicioView import ContratoServicioViewSet
from .views.ContratoView import ContratoViewSet, ContratoRecibosViewSet
from .views.EstadoPredioView import EstadoPredioViewSet
from .views.PredioView import PredioViewSet
from .views.ServicioView import ServicioViewSet
from .views.TipoUsoServicioView import TipoUsoServicioViewSet
from .views.ZonaView import ZonaViewSet
from .views.TipoMovimientoCuentaView import TipoMovimientoCuentaViewSet
from .views.MotivoMovimientoCuentaView import MotivoMovimientoCuentaViewSet
from .views.CronogramaPagoView import CronogramaPagoViewSet
from .views.EstadoCronogramaView import EstadoCronogramaViewSet
from .views.MesReciboView import MesReciboViewSet
from .views.ChoicesView import ChoicesViewSet
from .views.ReciboView import ReciboViewSet
from .views.TipoAfiliacionView import TipoAfiliacionViewSet
from .views.AfiliacionView import AfiliacionViewSet
from .views.TipoEventoView import TipoEventoViewSet
from .views.EstadoAsistenciaView import EstadoAsistenciaViewSet
from .views.EventoView import EventoViewSet
from .views.AsistenciaView import AsistenciaViewSet
from .views.TipoAsistenciaView import TipoAsistenciaViewSet
from .views.RepresentanteView import RepresentanteViewSet

router = routers.DefaultRouter()
router.register(r'comprobantes', ComprobanteViewSet)
router.register(r'comprobantesconfig', ComprobConfigViewSet)
router.register(r'contratosservicio', ContratoServicioViewSet)
router.register(r'contratos', ContratoViewSet)
router.register(r'estadospredio', EstadoPredioViewSet)
router.register(r'predios', PredioViewSet)
router.register(r'servicios', ServicioViewSet)
router.register(r'tiposusoservicio', TipoUsoServicioViewSet)
router.register(r'zonas', ZonaViewSet)
router.register(r'tiposmovimientocuenta', TipoMovimientoCuentaViewSet)
router.register(r'motivosmovimientocuenta', MotivoMovimientoCuentaViewSet)
router.register(r'cronogramaspago', CronogramaPagoViewSet)
router.register(r'estadoscronograma', EstadoCronogramaViewSet)
router.register(r'mesrecibos', MesReciboViewSet)
router.register(r'contratosrecibos', ContratoRecibosViewSet)
router.register(r'recibos', ReciboViewSet)
router.register(r'tiposafiliacion', TipoAfiliacionViewSet)
router.register(r'afiliaciones', AfiliacionViewSet)
router.register(r'tiposevento', TipoEventoViewSet)
router.register(r'estadosasistencia', EstadoAsistenciaViewSet)
router.register(r'eventos', EventoViewSet)
router.register(r'asistencias', AsistenciaViewSet)
router.register(r'tiposasistencia', TipoAsistenciaViewSet)
router.register(r'representantes',RepresentanteViewSet)

urlpatterns = [
	url(r'^choices/(?P<param>[^/]+)/$',
	        ChoicesViewSet.as_view({'get': 'get_choices'}), name='get-enums'),
    url(r'^', include(router.urls)),
]