from apps.procesos.models.EstadoAsistencia import EstadoAsistencia
from rest_framework import serializers

class EstadoAsistenciaSerializer(serializers.ModelSerializer):
	class Meta:
		model = EstadoAsistencia
		fields = '__all__'
