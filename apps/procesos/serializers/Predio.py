from apps.procesos.models.Predio import Predio
from rest_framework import serializers
from apps.procesos.serializers.Zona import ZonaSerializer
from apps.comun.serializers.Persona import PersonaBasicSerializer
from apps.procesos.serializers.EstadoPredio import EstadoPredioSerializer
from apps.procesos.serializers.Direccion import DireccionBasicSerializer, DireccionInfoSerializer
from apps.procesos.models.Contrato import Contrato
from django.db.models import Count

class PredioSerializer(serializers.ModelSerializer):
	zona = ZonaSerializer(many=False, read_only=True)
	duenio = PersonaBasicSerializer(many=False, read_only=True)
	representante = PersonaBasicSerializer(many=False, read_only=True)
	estado_predio = EstadoPredioSerializer(many=False, read_only=True)
	predio_direcciones = DireccionBasicSerializer(many=True, read_only=True)
	class Meta:
		model = Predio
		# fields = '__all__'
		exclude = ('created_at','updated_at',)




class PredioBasicRecSerializer(serializers.ModelSerializer):
	zona = ZonaSerializer(many=False, read_only=True)
	predio_direcciones = DireccionBasicSerializer(many=True, read_only=True)
	estado_predio = EstadoPredioSerializer(many=False, read_only=True)
	class Meta:
		model = Predio
		fields = ('zona','predio_direcciones','estado_predio',)
		# exclude = ('created_at','updated_at',)

class MisPredioSerializer(serializers.ModelSerializer):
	predio_direcciones = DireccionInfoSerializer(many=True, read_only=True)
	zona = ZonaSerializer(many=False, read_only=True)
	estado_predio = EstadoPredioSerializer(many=False, read_only=True)
	contratos = serializers.SerializerMethodField('nro_contratos')
	def nro_contratos(self, obj):
		contador = Contrato.objects.filter(predio_id__exact=obj.id,
			estado__in=['1','2']).aggregate(nro_cont=Count('id'))
		if contador['nro_cont']:
			result = contador['nro_cont']
		else:
			result = 0
		return result

	class Meta:
		model = Predio
		fields = ('id','predio_direcciones','contratos','zona','estado_predio','estado_descripcion',)


