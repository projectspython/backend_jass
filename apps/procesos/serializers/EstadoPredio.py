from apps.procesos.models.EstadoPredio import EstadoPredio
from rest_framework import serializers

class EstadoPredioSerializer(serializers.ModelSerializer):
	class Meta:
		model = EstadoPredio
		fields = '__all__'
		#exclude = ('created_at','updated_at',)