from apps.procesos.models.ContratoServicio import ContratoServicio
from rest_framework import serializers

class ContratoServicioSerializer(serializers.ModelSerializer):
	class Meta:
		model = ContratoServicio
		# fields = '__all__'
		exclude = ('created_at','updated_at',)