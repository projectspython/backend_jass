from apps.procesos.models.Direccion import Direccion
from rest_framework import serializers
from apps.comun.serializers.TipoDireccion import TipoDireccionSerializer
from apps.comun.serializers.Pais import PaisBasicSerializer
from apps.comun.serializers.Ubigeo import UbigeoBasicSerializer
# from apps.comun.serializers.TipoDocumento import TipoDocumentoBasicSerializer

class DireccionBasicSerializer(serializers.ModelSerializer):
	tipo_direccion = TipoDireccionSerializer(many=False, read_only=True)
	pais = PaisBasicSerializer(many=False, read_only=True)
	ubigeo = UbigeoBasicSerializer(many=False, read_only=True)
	class Meta:
		model = Direccion
		fields = ('id','direccion_detalle','completa','dpto_interior','lote','manzana','piso',
					'nombre_via','nombre_zona','numero_via','pais_id','punto_lat','punto_long',
					'referencia1','referencia2','tipo_direccion_id','tipo_direccion',
					'tipo_numero_via_id','tipo_via_id','tipo_zona_id','ubigeo_id','pais',
					'ubigeo',)

class DireccionInfoSerializer(serializers.ModelSerializer):
	tipo_direccion = TipoDireccionSerializer(many=False, read_only=True)
	pais = PaisBasicSerializer(many=False, read_only=True)
	ubigeo = UbigeoBasicSerializer(many=False, read_only=True)
	class Meta:
		model = Direccion
		fields = ('id','direccion_detalle','pais',
					'ubigeo','tipo_direccion','referencia1','referencia2','punto_lat','punto_long',)