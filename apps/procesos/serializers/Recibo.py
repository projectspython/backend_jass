from apps.procesos.models.Recibo import Recibo
from rest_framework import serializers
# from apps.procesos.serializers.Zona import ZonaSerializer
# from apps.comun.serializers.Persona import PersonaBasicSerializer
# from apps.procesos.serializers.EstadoRecibo import EstadoReciboSerializer
from apps.procesos.serializers.MesRecibo import MesReciboBasicSerializer, MesReciboPrintSerializer
from apps.procesos.serializers.Contrato import ContratoBasicRecSerializer
from apps.procesos.serializers.ReciboDetalle import ReciboDetalleSerializer

class ReciboSerializer(serializers.ModelSerializer):
	mes_recibo = MesReciboBasicSerializer(many=False, read_only=True)
	contrato = ContratoBasicRecSerializer(many=False, read_only=True)
	# estado_nombre = serializers.CharField(source='get_estado_display')
	class Meta:
		model = Recibo
		# fields = ('id','mes_id','mes','anio','fecha_emision','fecha_vencimiento',
		# 	'fecha_corte','texto_html','estado','estado_nombre',)
		exclude = ('created_at','updated_at',)



class ReciboPrintSerializer(serializers.ModelSerializer):
	mes_recibo = MesReciboPrintSerializer(many=False, read_only=True)
	contrato = ContratoBasicRecSerializer(many=False, read_only=True)
	recibo_recibo_detalles = ReciboDetalleSerializer(many=True, read_only=True)
	tipo_uso_servicio_nombre = serializers.ReadOnlyField(source='tipo_uso_servicio.nombre')
	forma_cobranza_nombre = serializers.CharField(source='get_forma_cobranza_display')

	class Meta:
		model = Recibo
		exclude = ('created_at','updated_at',)

class ReciboPendienteSerializer(serializers.ModelSerializer):
	mes_recibo = MesReciboBasicSerializer(many=False, read_only=True)
	class Meta:
		model = Recibo
		fields = ('id','mes_recibo','mes_recibo_id',)