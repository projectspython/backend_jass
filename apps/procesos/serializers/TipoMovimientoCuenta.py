from apps.procesos.models.TipoMovimientoCuenta import TipoMovimientoCuenta
from rest_framework import serializers

class TipoMovimientoCuentaSerializer(serializers.ModelSerializer):
	class Meta:
		model = TipoMovimientoCuenta
		fields = '__all__'