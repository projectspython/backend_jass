from apps.comun.models.Persona import Persona
from rest_framework import serializers
from apps.procesos.serializers.Direccion import DireccionBasicSerializer

class PersonaEditSerializer(serializers.ModelSerializer):
	persona_direcciones = DireccionBasicSerializer(many=True, read_only=True)
	class Meta:
		model = Persona
		fields = ('persona_direcciones',)
		