from apps.procesos.models.Contrato import Contrato
from apps.comun.serializers.Entidad import EntidadSerializer
from apps.comun.serializers.Persona import PersonaBasicSerializer, PersonaInfoSerializer
from apps.procesos.serializers.ComprobConfig import ComprobConfigSerializer
from apps.procesos.serializers.Predio import PredioSerializer, PredioBasicRecSerializer
from apps.procesos.serializers.TipoUsoServicio import TipoUsoServicioSerializer
from rest_framework import serializers
from apps.procesos.models.ContratoServicio import ContratoServicio
from django.db.models import Sum
from apps.procesos.serializers.ContratoFirma import ContratoFirmaPrintSerializer

from datetime import date
from datetime import datetime
f_a = datetime.now()
anio_a = int(datetime.strftime(f_a, '%Y'))
mes_a = int(datetime.strftime(f_a, '%m'))
dia_a = int(datetime.strftime(f_a, '%d'))
d2 = date(anio_a,mes_a,dia_a)

class ContratoSerializer(serializers.ModelSerializer):
	forma_cobranza_nombre = serializers.CharField(source='get_forma_cobranza_display')
	persona = PersonaBasicSerializer(many=False, read_only=True)
	predio = PredioSerializer(many=False, read_only=True)
	tipo_uso_servicio = TipoUsoServicioSerializer(many=False, read_only=True)
	usuario = PersonaBasicSerializer(many=False, read_only=True)
	estado_nombre = serializers.CharField(source='get_estado_display')

	monto_cobranza = serializers.SerializerMethodField('cobranza_monto')
	def cobranza_monto(self, obj):
		suma = ContratoServicio.objects.filter(contrato_id=obj.id,
			).exclude(servicio__codigo='Inscrip').aggregate(sum_importe=Sum('precio'))
		if suma['sum_importe']:
			result = suma['sum_importe']
		else:
			result = 0
		return result

	class Meta:
		model = Contrato
		exclude = ('entidad','created_at','updated_at','comprob_config',)

class ContratoBasicSerializer(serializers.ModelSerializer):
	# entidad = EntidadSerializer(many=False, read_only=True)
	# comprob_config = ComprobConfigSerializer(many=False, read_only=True)
	# forma_cobranza_nombre = serializers.CharField(source='get_forma_cobranza_display')
	# persona = PersonaBasicSerializer(many=False, read_only=True)
	predio = PredioBasicRecSerializer(many=False, read_only=True)
	# tipo_uso_servicio = TipoUsoServicioSerializer(many=False, read_only=True)
	# usuario = PersonaBasicSerializer(many=False, read_only=True)
	class Meta:
		model = Contrato
		fields = ('id','numero','predio',)
		# exclude = ('id','numero',)

class ContratoBasicRecSerializer(serializers.ModelSerializer):
	persona = PersonaBasicSerializer(many=False, read_only=True)
	predio = PredioBasicRecSerializer(many=False, read_only=True)
	class Meta:
		model = Contrato
		fields = ('id','persona','numero','predio',)

class ContratoPrintSerializer(serializers.ModelSerializer):
	persona = PersonaInfoSerializer(many=False, read_only=True)
	predio = PredioBasicRecSerializer(many=False, read_only=True)
	forma_cobranza_nombre = serializers.CharField(source='get_forma_cobranza_display')
	t_uso_ser_nombre = serializers.CharField(source='tipo_uso_servicio.nombre')

	servicios = serializers.SerializerMethodField('listado_servicios')
	def listado_servicios(self, obj):
		listado = ContratoServicio.objects.filter(contrato_id=obj.id,
			).exclude(servicio__codigo='Inscrip').values('id','precio','servicio__nombre','servicio__codigo')
		return listado

	monto_ins = serializers.SerializerMethodField('total_monto_ins')

	def total_monto_ins(self, obj):
		listado = ContratoServicio.objects.filter(contrato_id=obj.id,
			servicio__codigo='Inscrip'
			).values('id','precio').first()
		if listado:
			return float(listado['precio'])
		else:
			return 0

	contrato_contrato_firma = ContratoFirmaPrintSerializer(many=True, read_only=True)
	class Meta:
		model = Contrato
		fields = ('id','persona','numero','predio','forma_cobranza_nombre','forma_cobranza',
			't_uso_ser_nombre', 'fecha_contrato','comentario','estado','servicios','monto_ins',
			'contrato_contrato_firma',)
		

class ContratoReciboSerializer(serializers.ModelSerializer):
	persona = PersonaBasicSerializer(many=False, read_only=True)
	predio = PredioSerializer(many=False, read_only=True)
	tipo_uso_servicio = TipoUsoServicioSerializer(many=False, read_only=True)

	monto_cobranza = serializers.SerializerMethodField('cobranza_monto')
	def cobranza_monto(self, obj):
		suma = ContratoServicio.objects.filter(contrato_id=obj.id,
			).exclude(servicio__codigo='Inscrip').aggregate(sum_importe=Sum('precio'))
		if suma['sum_importe']:
			result = suma['sum_importe']
		else:
			result = 0
		return result

	nro_dias = serializers.SerializerMethodField('def_nro_dias')

	def def_nro_dias(self, obj):
		return abs(d2 - obj.fecha_contrato).days

	class Meta:
		model = Contrato
		fields = ('id','forma_cobranza','numero','tipo_uso_servicio','persona',
			'predio','monto_cobranza','fecha_contrato','nro_dias',)
		# exclude = ('created_at','updated_at',)

class ContratoReciboCalSerializer(serializers.ModelSerializer):

	monto_cobranza = serializers.SerializerMethodField('cobranza_monto')
	def cobranza_monto(self, obj):
		suma = ContratoServicio.objects.filter(contrato_id=obj.id,
			).exclude(servicio__codigo='Inscrip').aggregate(sum_importe=Sum('precio'))
		if suma['sum_importe']:
			result = suma['sum_importe']
		else:
			result = 0
		return result

	listado = serializers.SerializerMethodField('listado_valores')
	def listado_valores(self, obj):
		listado = ContratoServicio.objects.filter(contrato_id=obj.id,
			).exclude(servicio__codigo='Inscrip').values('id','precio','servicio__nombre','servicio_id')
		return listado

	class Meta:
		model = Contrato
		fields = ('id','persona_id','monto_cobranza','listado','forma_cobranza','tipo_uso_servicio_id',)

class ContratoRecGenerarSerializer(serializers.ModelSerializer):
	persona = PersonaBasicSerializer(many=False, read_only=True)
	predio = PredioBasicRecSerializer(many=False, read_only=True)
	monto_cobranza = serializers.SerializerMethodField('cobranza_monto')
	def cobranza_monto(self, obj):
		suma = ContratoServicio.objects.filter(contrato_id=obj.id,
			).exclude(servicio__codigo='Inscrip').aggregate(sum_importe=Sum('precio'))
		if suma['sum_importe']:
			result = suma['sum_importe']
		else:
			result = 0
		return result

	listado = serializers.SerializerMethodField('listado_valores')
	def listado_valores(self, obj):
		listado = ContratoServicio.objects.filter(contrato_id=obj.id,
			).exclude(servicio__codigo='Inscrip').values('id','precio','servicio__nombre',
			'servicio_id','servicio__codigo')
		return listado

	nro_dias = serializers.SerializerMethodField('def_nro_dias')

	def def_nro_dias(self, obj):
		return abs(d2 - obj.fecha_contrato).days
		# return split2

	class Meta:
		model = Contrato
		fields = ('id','persona_id','monto_cobranza','listado','forma_cobranza',
			'persona','predio','numero','nro_dias',)


