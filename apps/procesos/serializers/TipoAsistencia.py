from apps.procesos.models.TipoAsistencia import TipoAsistencia
from rest_framework import serializers

class TipoAsistenciaSerializer(serializers.ModelSerializer):
	class Meta:
		model = TipoAsistencia
		fields = '__all__'