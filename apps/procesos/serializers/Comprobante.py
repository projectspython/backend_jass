from apps.procesos.models.Comprobante import Comprobante
from rest_framework import serializers

class ComprobanteSerializer(serializers.ModelSerializer):
	class Meta:
		model = Comprobante
		# fields = '__all__'
		exclude = ('created_at','updated_at',)