from apps.procesos.models.Representante import Representante
from rest_framework import serializers
from apps.procesos.serializers.Afiliacion import AfiliacionSerializer
from apps.comun.serializers.Persona import PersonaBasicSerializer

class RepresentanteSerializer(serializers.ModelSerializer):
	persona = PersonaBasicSerializer(many=False, read_only=True)
	afiliacion = AfiliacionSerializer(many=False, read_only=True)
	class Meta:
		model = Representante
		# fields = '__all__'
		exclude = ('created_at','updated_at',)

# class Representante2Serializer(serializers.ModelSerializer):
# 	class Meta:
# 		model = Representante
# 		fields = '__all__'
