from apps.procesos.models.CronogramaPago import CronogramaPago
from rest_framework import serializers
from apps.comun.serializers.Entidad import EntidadSerializer
from apps.procesos.serializers.Contrato import ContratoSerializer, ContratoBasicSerializer
from apps.procesos.serializers.Recibo import ReciboPendienteSerializer
from apps.comun.serializers.Persona import PersonaSerializer, PersonaBasicSerializer
from apps.procesos.serializers.MotivoMovimientoCuenta import MotivoMovimientoCuentaSerializer, MotivoMovimientoCuentaBasicSerializer
from apps.caja.models.Amortizacion import Amortizacion
from apps.procesos.serializers.EstadoCronograma import EstadoCronogramaBasicSerializer
from django.db.models import Sum

class CronogramaPago2Serializer(serializers.ModelSerializer):
	class Meta:
		model = CronogramaPago
		fields = '__all__'

class CronogramaPagoSerializer(serializers.ModelSerializer):
	# entidad = EntidadSerializer(many=False, read_only=True)
	contrato = ContratoBasicSerializer(many=False, read_only=True)
	persona = PersonaBasicSerializer(many=False, read_only=True)
	motivo_movimiento_cuenta = MotivoMovimientoCuentaSerializer(many=False, read_only=True)
	# padre = CronogramaPago2Serializer(many=False, read_only=True) 
	# usuario = PersonaSerializer(many=False, read_only=True)
	usuario_nombre_completo = serializers.CharField(source='usuario.nombre_completo')
	estado_cronograma = EstadoCronogramaBasicSerializer(many=False, read_only=True)
	class Meta:
		model = CronogramaPago
		# fields = '__all__'
		exclude = ('created_at','updated_at','entidad')

class CronogramaPagoCajaSerializer(serializers.ModelSerializer):
	motivo_movimiento_cuenta = MotivoMovimientoCuentaBasicSerializer(many=False, read_only=True)
	pagado = serializers.SerializerMethodField('amortizaciones')
	monto = serializers.SerializerMethodField('montooficial')
	def amortizaciones(self, obj):
		suma = Amortizacion.objects.filter(cronograma_pago_id=obj.id,
			estado='1').aggregate(sum_importe=Sum('importe'))
		if suma['sum_importe']:
			result = suma['sum_importe']
		else:
			result = 0
		return result

	def montooficial(self, obj):
		return float(obj.monto)

	class Meta:
		model = CronogramaPago
		fields = ('id','descripcion','fecha_cobranza','created_at','updated_at',
			'contrato','motivo_movimiento_cuenta','monto',
			'pagado',)

class CronogramaPagoCajaVerifSerializer(serializers.ModelSerializer):
	pagado = serializers.SerializerMethodField('amortizaciones')
	monto = serializers.SerializerMethodField('montooficial')

	def amortizaciones(self, obj):
		suma = Amortizacion.objects.filter(cronograma_pago_id=obj.id,
			estado='1').aggregate(sum_importe=Sum('importe'))
		if suma['sum_importe']:
			result = suma['sum_importe']
		else:
			result = 0
		return result

	def montooficial(self, obj):
		return float(obj.monto)

	class Meta:
		model = CronogramaPago
		fields = ('id','monto','pagado','persona_id',)

class CronogramaPagoPendientesSerializer(serializers.ModelSerializer):
	deuda = serializers.SerializerMethodField('amortizaciones')
	recibo = ReciboPendienteSerializer(many=False, read_only=True)

	def amortizaciones(self, obj):
		suma = Amortizacion.objects.filter(cronograma_pago_id=obj.id,
			estado='1').aggregate(sum_importe=Sum('importe'))
		if suma['sum_importe']:
			result = suma['sum_importe']
		else:
			result = 0
		return float(obj.monto) - float(result)

	class Meta:
		model = CronogramaPago
		fields = ('id','descripcion','deuda','persona_id','recibo','recibo_id',)