from apps.procesos.models.ReciboDetalle import ReciboDetalle
from apps.procesos.serializers.Servicio import ServicioSerializer

from rest_framework import serializers

class ReciboDetalleSerializer(serializers.ModelSerializer):
	servicio = ServicioSerializer(many=False, read_only=True)
	class Meta:
		model = ReciboDetalle
		# fields = '__all__'
		exclude = ('created_at','updated_at',)