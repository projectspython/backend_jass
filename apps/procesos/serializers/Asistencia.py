from apps.procesos.models.Asistencia import Asistencia
from rest_framework import serializers
from apps.comun.serializers.Entidad import EntidadSerializer
from apps.procesos.serializers.Evento import EventoSerializer
from apps.procesos.serializers.EstadoAsistencia import EstadoAsistenciaSerializer
from apps.comun.serializers.Persona import PersonaSerializer
from apps.procesos.serializers.Evento import EventoSerializer

class Asistencia2Serializer(serializers.ModelSerializer):
	class Meta:
		model = Asistencia
		fields = '__all__'

class AsistenciaSerializer(serializers.ModelSerializer):
	entidad = EntidadSerializer(many=False, read_only=True)
	evento = EventoSerializer(many=False, read_only=True)
	estado_asistencia = EstadoAsistenciaSerializer(many=False, read_only=True)
	persona = PersonaSerializer(many=False, read_only=True)
	representante = PersonaSerializer(many=False, read_only=True)
	persona_asistente = serializers.CharField(source='get_persona_asistente_display')
	class Meta:
		model = Asistencia
		fields = '__all__'
