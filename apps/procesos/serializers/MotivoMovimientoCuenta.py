from apps.procesos.models.MotivoMovimientoCuenta import MotivoMovimientoCuenta
from rest_framework import serializers
from apps.procesos.serializers.TipoMovimientoCuenta import TipoMovimientoCuentaSerializer

class MotivoMovimientoCuentaSerializer(serializers.ModelSerializer):
	tipo_movimiento_cuenta = TipoMovimientoCuentaSerializer(many=False, read_only=True)
	class Meta:
		model = MotivoMovimientoCuenta
		fields = '__all__'

class MotivoMovimientoCuentaBasicSerializer(serializers.ModelSerializer):
	class Meta:
		model = MotivoMovimientoCuenta
		fields = ('id','codigo','nombre','nombre_plural',)
		