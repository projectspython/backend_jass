from apps.procesos.models.ContratoFirma import ContratoFirma
from rest_framework import serializers
from apps.comun.serializers.Persona import PersonaBasicSerializer
from apps.comun.serializers.Cargo import CargoBasicSerializer

class ContratoFirmaSerializer(serializers.ModelSerializer):
	class Meta:
		model = ContratoFirma
		# fields = '__all__'
		exclude = ('created_at','updated_at',)

class ContratoFirmaPrintSerializer(serializers.ModelSerializer):
	persona = PersonaBasicSerializer(many=False, read_only=True)
	cargo = CargoBasicSerializer(many=False, read_only=True)
	class Meta:
		model = ContratoFirma
		fields = ('cargo','persona',)
		# exclude = ('created_at','updated_at',)