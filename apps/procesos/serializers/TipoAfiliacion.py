from apps.procesos.models.TipoAfiliacion import TipoAfiliacion
from rest_framework import serializers

class TipoAfiliacionSerializer(serializers.ModelSerializer):
	class Meta:
		model = TipoAfiliacion
		fields = '__all__'