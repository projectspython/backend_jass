from apps.procesos.models.MesRecibo import MesRecibo
from rest_framework import serializers
# from apps.procesos.serializers.Zona import ZonaSerializer
# from apps.comun.serializers.Persona import PersonaBasicSerializer
# from apps.procesos.serializers.EstadoMesRecibo import EstadoMesReciboSerializer
from apps.comun.serializers.Mes import MesSerializer

class MesReciboSerializer(serializers.ModelSerializer):
	mes = MesSerializer(many=False, read_only=True)
	estado_nombre = serializers.CharField(source='get_estado_display')
	class Meta:
		model = MesRecibo
		fields = ('id','mes_id','mes','anio','fecha_emision','fecha_vencimiento',
			'fecha_corte','texto_html','estado','estado_nombre',)
		# exclude = ('created_at','updated_at',)

class MesReciboBasicSerializer(serializers.ModelSerializer):
	mes = MesSerializer(many=False, read_only=True)
	class Meta:
		model = MesRecibo
		fields = ('id','mes','anio',)

class MesReciboPrintSerializer(serializers.ModelSerializer):
	mes = MesSerializer(many=False, read_only=True)
	class Meta:
		model = MesRecibo
		fields = ('id','mes','anio','fecha_vencimiento','fecha_corte','fecha_emision',)
		


