from apps.procesos.models.TipoEvento import TipoEvento
from rest_framework import serializers

class TipoEventoSerializer(serializers.ModelSerializer):
	class Meta:
		model = TipoEvento
		fields = '__all__'
