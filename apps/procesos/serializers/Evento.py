from apps.procesos.models.Evento import Evento
from rest_framework import serializers
from apps.comun.serializers.Entidad import EntidadSerializer
from apps.procesos.serializers.TipoEvento import TipoEventoSerializer
from apps.procesos.serializers.TipoAsistencia import TipoAsistenciaSerializer

class Evento2Serializer(serializers.ModelSerializer):
	class Meta:
		model = Evento
		fields = '__all__'

class EventoSerializer(serializers.ModelSerializer):
	# entidad = EntidadSerializer(many=False, read_only=True)
	tipo_evento = TipoEventoSerializer(many=False, read_only=True)
	tipo_asistencia = TipoAsistenciaSerializer(many=False, read_only=True)
	# unidad_tiempo_nombre = serializers.CharField(source='get_unidad_tiempo_display')
	class Meta:
		model = Evento
		# fields = '__all__'
		exclude = ('created_at','updated_at',)
