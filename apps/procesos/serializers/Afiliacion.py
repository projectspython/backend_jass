from apps.procesos.models.Afiliacion import Afiliacion
from rest_framework import serializers
from apps.comun.serializers.Entidad import EntidadSerializer
from apps.comun.serializers.Persona import PersonaBasicSerializer
from apps.procesos.serializers.TipoAfiliacion import TipoAfiliacionSerializer

class Afiliacion2Serializer(serializers.ModelSerializer):
	class Meta:
		model = Afiliacion
		fields = '__all__'

class AfiliacionSerializer(serializers.ModelSerializer):
	# entidad = EntidadSerializer(many=False, read_only=True)
	persona = PersonaBasicSerializer(many=False, read_only=True)
	tipo_afiliacion = TipoAfiliacionSerializer(many=False, read_only=True)
	class Meta:
		model = Afiliacion
		fields = '__all__'
