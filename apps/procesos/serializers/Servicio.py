from apps.procesos.models.Servicio import Servicio
from rest_framework import serializers

class ServicioSerializer(serializers.ModelSerializer):
	class Meta:
		model = Servicio
		# fields = '__all__'
		exclude = ('created_at','updated_at',)