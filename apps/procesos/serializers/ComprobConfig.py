from apps.procesos.models.ComprobConfig import ComprobConfig
from rest_framework import serializers
from apps.comun.serializers.Entidad import EntidadBasicSerializer
from apps.procesos.serializers.Comprobante import ComprobanteSerializer

class ComprobConfigSerializer(serializers.ModelSerializer):
	# entidad = EntidadBasicSerializer(many=False, read_only=True)
	comprobante = ComprobanteSerializer(many=False, read_only=True)
	class Meta:
		model = ComprobConfig
		# fields = '__all__'
		exclude = ('created_at','updated_at',)