from apps.procesos.models.TipoUsoServicio import TipoUsoServicio
from rest_framework import serializers

class TipoUsoServicioSerializer(serializers.ModelSerializer):
	class Meta:
		model = TipoUsoServicio
		fields = '__all__'
		# exclude = ('created_at','updated_at',)