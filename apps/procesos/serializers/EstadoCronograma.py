from apps.procesos.models.EstadoCronograma import EstadoCronograma
from rest_framework import serializers

class EstadoCronogramaSerializer(serializers.ModelSerializer):
	class Meta:
		model = EstadoCronograma
		fields = '__all__'

class EstadoCronogramaBasicSerializer(serializers.ModelSerializer):
	class Meta:
		model = EstadoCronograma
		fields = ('id','codigo','nombre')