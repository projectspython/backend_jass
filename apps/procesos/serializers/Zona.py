from apps.procesos.models.Zona import Zona
from rest_framework import serializers
from apps.comun.serializers.TipoZona import TipoZonaSerializer
from apps.comun.serializers.Ubigeo import UbigeoBasicSerializer

class ZonaSerializer(serializers.ModelSerializer):
	tipo_zona = TipoZonaSerializer(many=False, read_only=True)
	ubigeo = UbigeoBasicSerializer(many=False, read_only=True)
	class Meta:
		model = Zona
		# fields = '__all__'
		exclude = ('created_at','updated_at',)