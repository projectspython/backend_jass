from django.apps import AppConfig


class SegurConfig(AppConfig):
    name = 'segur'
