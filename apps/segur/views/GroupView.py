from rest_framework import serializers, viewsets
from django.contrib.auth.models import Group
from rest_framework import permissions
from apps.segur.serializers.Group import GroupSerializer
from apps.comun.always.Pagination import LimonPagination
from rest_framework.filters import SearchFilter, OrderingFilter
from django.db.models.deletion import ProtectedError

from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.segur.Permissions.Group import GroupPermissions as LimonPermission
from apps.segur.always.Permission import PermLimonList
from django.contrib.auth.models import Permission
from apps.comun.always.SearchFilter import search_filter

class GroupViewSet(LimonPagination, viewsets.ModelViewSet):
	queryset = Group.objects.all()
	permission_classes = [permissions.IsAuthenticated, PermLimonList, ]
	serializer_class = GroupSerializer
	filter_backends = (SearchFilter,OrderingFilter,)
	search_fields = ('name',)
	ordering_fields = '__all__'

	@list_route(url_path='add', methods=['post'], permission_classes=[LimonPermission,])
	def get_add(self, request, *args, **kwargs):
		serializer = self.get_serializer(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

	@list_route(url_path='edit', methods=['get'], permission_classes=[LimonPermission,])
	def get_edit(self, request):
		id = request.query_params.get('id', None)
		model = Group.objects.get(pk=id)
		return Response(self.get_serializer(model).data)


	@list_route(url_path='update', methods=['put'], permission_classes=[LimonPermission,])
	def get_update(self, request, pk=None):
		model = Group.objects.get(pk=request.data['id'])
		serializer = self.get_serializer(model, data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data)
		else:
			return Response(serializer.errors,
							status=status.HTTP_400_BAD_REQUEST)

	@list_route(url_path='delete', methods=['delete'], permission_classes=[LimonPermission,])
	def get_delete(self, request, *args, **kwargs):
		try:
			id = request.query_params.get('id', None)
			model = Group.objects.get(pk=id).delete()
			return Response({'status':True, 'id':id})
		except ProtectedError as e:
			return Response(e.errors, status=status.HTTP_400_BAD_REQUEST)

	@list_route(url_path='updatepermission', methods=['put'], permission_classes=[LimonPermission,])
	def get_updatepermission(self, request, pk=None):

		data = request.data

		permissions = []
		for p in data['permisos']:
			permissions.append(p['id'])

		gr = Group.objects.get(pk=data['rol_id'])
		gr.permissions.set(permissions)
		gr.save

		return Response(permissions)

	@list_route(url_path='editpermission', methods=['get'], permission_classes=[LimonPermission,])
	def get_editpermission(self, request, pk=None):
		id = request.query_params.get('rol_id', None)
		model = Group.objects.get(pk=id)
		permissions = self.get_serializer(model).data['permissions']
		listado = Permission.objects.filter(id__in=permissions).values()
		return Response(listado)

	@list_route(url_path='searchform', methods=['get'], permission_classes=[])
	def get_searchform(self, request, *args, **kwargs):
		queryset = search_filter(self.get_queryset(), request)
		serializer = self.get_serializer(queryset, many=True)
		return Response(serializer.data)
	