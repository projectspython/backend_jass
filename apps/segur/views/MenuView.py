from rest_framework import serializers, viewsets
from apps.segur.models.Menu import Menu
from rest_framework import permissions
from apps.segur.serializers.Menu import MenuSerializer
from apps.comun.always.Pagination import LimonPagination
from rest_framework.filters import SearchFilter, OrderingFilter
from django.db.models.deletion import ProtectedError

from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.segur.Permissions.Menu import MenuPermissions as LimonPermission
from apps.segur.always.Permission import PermLimonList
from apps.comun.always.SearchFilter import search_filter, keys_add_none

class MenuViewSet(LimonPagination, viewsets.ModelViewSet):
	queryset = Menu.objects.all()
	permission_classes = [permissions.IsAuthenticated, PermLimonList, ]
	serializer_class = MenuSerializer
	filter_backends = (SearchFilter,OrderingFilter,)
	search_fields = ('nombre', 'codigo','nombre_plural',)
	ordering_fields = '__all__'

	@list_route(url_path='add', methods=['post'], permission_classes=[LimonPermission,])
	def get_add(self, request, *args, **kwargs):
		data = request.data
		data_men = keys_add_none(data,'modulo_id,tipo_menu_id,codigo,nombre,'+
						'nombre_plural,estado_router,icono,padre_id,orden,permiso_id,estado')

		men = Menu.objects.create(**data_men)

		# model = Menu.objects.get(pk=data['id'])
		return Response(self.get_serializer(men).data)

	@list_route(url_path='edit', methods=['get'], permission_classes=[LimonPermission,])
	def get_edit(self, request):
		id = request.query_params.get('id', None)
		model = Menu.objects.get(pk=id)
		return Response(self.get_serializer(model).data)


	@list_route(url_path='update', methods=['put'], permission_classes=[LimonPermission,])
	def get_update(self, request, pk=None):
		data = request.data
		# model = Menu.objects.get(pk=request.data['id'])
		data_men = keys_add_none(data,'modulo_id,tipo_menu_id,codigo,nombre,'+
						'nombre_plural,estado_router,icono,padre_id,orden,permiso_id,estado')

		men = Menu.objects.filter(id=data['id']).update(**data_men)

		model = Menu.objects.get(pk=data['id'])
		return Response(self.get_serializer(model).data)

		# serializer = self.get_serializer(model, data=request.data)
		# if serializer.is_valid():
		# 	serializer.save()
		# 	return Response(serializer.data)
		# else:
		# 	return Response(serializer.errors,
		# 					status=status.HTTP_400_BAD_REQUEST)

	@list_route(url_path='delete', methods=['delete'], permission_classes=[LimonPermission,])
	def get_delete(self, request, *args, **kwargs):
		try:
			id = request.query_params.get('id', None)
			model = Menu.objects.get(pk=id).delete()
			return Response({'status':True, 'id':id})
		except ProtectedError as e:
			return Response(e.errors, status=status.HTTP_400_BAD_REQUEST)

	@list_route(url_path='searchform', methods=['get'], permission_classes=[LimonPermission,])
	def get_searchform(self, request, *args, **kwargs):
		queryset = search_filter(self.get_queryset(), request)
		serializer = MenuSerializer(queryset, many=True)
		return Response(serializer.data)