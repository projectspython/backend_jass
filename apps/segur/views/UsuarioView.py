from rest_framework import serializers, viewsets
from apps.segur.models.Usuario import Usuario
from apps.segur.serializers.Usuario import UsuarioSerializer
from apps.comun.always.Pagination import LimonPagination
from rest_framework.filters import SearchFilter, OrderingFilter
from django.db.models.deletion import ProtectedError
from rest_framework import permissions

from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.segur.Permissions.Usuario import UsuarioPermissions as LimonPermission
from apps.segur.always.Permission import PermLimonList
from apps.comun.always.SearchFilter import search_filter, keys_add_none
from django.contrib.auth.models import Permission, Group
from apps.comun.models.Entidad import Entidad
from apps.comun.serializers.Entidad import EntidadSerializer

class UsuarioViewSet(LimonPagination, viewsets.ModelViewSet):
	queryset = Usuario.objects.all()
	permission_classes = [permissions.IsAuthenticated, PermLimonList, ]
	serializer_class = UsuarioSerializer
	filter_backends = (SearchFilter,OrderingFilter,)
	search_fields = ('username','persona__nombre_completo')
	ordering_fields = '__all__'

	@list_route(url_path='add', methods=['post'], permission_classes=[LimonPermission,])
	def get_add(self, request, *args, **kwargs):

		data = request.data
		data_us = keys_add_none(data,'username,persona_id,is_active,is_staff,is_superuser,password')
		usu = Usuario.objects.create_user(**data_us)
		if usu:
			model = Usuario.objects.get(pk=usu.id)
			return Response(self.get_serializer(model).data)

	@list_route(url_path='edit', methods=['get'], permission_classes=[LimonPermission,])
	def get_edit(self, request):
		id = request.query_params.get('id', None)
		model = Usuario.objects.get(pk=id)
		data = self.get_serializer(model).data
		if data['is_active']:
			data['is_active'] = '1'
		else:
			data['is_active'] = '0'

		if data['is_staff']:
			data['is_staff'] = '1'
		else:
			data['is_staff'] = '0'

		if data['is_superuser']:
			data['is_superuser'] = '1'
		else:
			data['is_superuser'] = '0'

		return Response(data)

	@list_route(url_path='update', methods=['put'], permission_classes=[LimonPermission,])
	def get_update(self, request, pk=None):
		data = request.data
		data_usu = keys_add_none(data,'username,persona_id,is_active,is_staff,is_superuser')

		usu = Usuario.objects.filter(id=data['id']).update(**data_usu)

		model = Usuario.objects.get(pk=data['id'])
		return Response(self.get_serializer(model).data)


	@list_route(url_path='delete', methods=['delete'], permission_classes=[LimonPermission,])
	def get_delete(self, request, *args, **kwargs):
		try:
			id = request.query_params.get('id', None)
			model = Usuario.objects.get(pk=id).delete()
			return Response({'status':True, 'id':id})
		except ProtectedError as e:
			return Response(e.errors, status=status.HTTP_400_BAD_REQUEST)

	@list_route(url_path='changepassword', permission_classes=[LimonPermission,], methods=['post'])
	def get_changepassword(self, request, format=None):
		data = request.data
		if data['id']:
			id = data['id']
			us = Usuario.objects.get(pk=id)
			us.set_password(data['password'])
			us.save()
			resul = {'estado':True,'msg':"Correcto"}
			return Response(resul)

		else:
			data = { 'error': 'Usuario no encontrato' }
			return Response(data, status=404)

	@list_route(url_path='editpermissions', methods=['get'], permission_classes=[LimonPermission,])
	def get_editpermissions(self, request, pk=None):
		id = request.query_params.get('user_id', None)
		model = Usuario.objects.get(pk=id)
		permissions = self.get_serializer(model).data['user_permissions']
		listado = Permission.objects.filter(id__in=permissions).values()
		return Response(listado)

	@list_route(url_path='updatepermissions', methods=['put'], permission_classes=[LimonPermission,])
	def get_updatepermissions(self, request, pk=None):

		data = request.data
		permissions = []
		for p in data['permisos']:
			permissions.append(p['id'])

		gr = Usuario.objects.get(pk=data['user_id'])
		gr.user_permissions.set(permissions)
		gr.save

		return Response(permissions)

	@list_route(url_path='editroles', methods=['get'], permission_classes=[LimonPermission,])
	def get_editroles(self, request, pk=None):
		id = request.query_params.get('user_id', None)
		model = Usuario.objects.get(pk=id)
		data_ = self.get_serializer(model).data

		listado1 = Group.objects.filter(id__in=data_['groups']).values()
		

		queryset = Entidad.objects.filter(id__in=data_['entidades'])
		listado2 = EntidadSerializer(queryset, many=True)
		data = dict(
			groups=listado1,
			entidades=listado2.data
			)
		return Response(data)

	@list_route(url_path='updateroles', methods=['put'], permission_classes=[LimonPermission,])
	def get_updateroles(self, request, pk=None):

		data = request.data
		groups = []
		for p in data['groups']:
			groups.append(p['id'])

		gr = Usuario.objects.get(pk=data['user_id'])
		gr.groups.set(groups)

		entidades = []
		for p in data['entidades']:
			entidades.append(p['id'])
		
		gr.entidades.set(entidades)

		gr.save
		return Response(groups)