from rest_framework import serializers, viewsets
from apps.segur.models.Modulo import Modulo
from rest_framework import permissions
from apps.segur.serializers.Modulo import ModuloSerializer
from apps.comun.always.Pagination import LimonPagination
from rest_framework.filters import SearchFilter, OrderingFilter
from django.db.models.deletion import ProtectedError

from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
from apps.segur.Permissions.Modulo import ModuloPermissions as LimonPermission
from apps.segur.always.Permission import PermLimonList
from apps.comun.always.SearchFilter import search_filter, keys_add_none
from apps.comun.always.FileUpload import FileUpload

class ModuloViewSet(LimonPagination, viewsets.ModelViewSet):
	queryset = Modulo.objects.all()
	permission_classes = [permissions.IsAuthenticated, PermLimonList, ]
	serializer_class = ModuloSerializer
	filter_backends = (SearchFilter,OrderingFilter,)
	search_fields = ('nombre', 'codigo','simbolo',)
	ordering_fields = '__all__'

	@list_route(url_path='add', methods=['post'], permission_classes=[LimonPermission,])
	def get_add(self, request, *args, **kwargs):

		data = request.data
		files = request.FILES
		if files:
			data['logo'] = FileUpload.segur_modulo('logo',request.FILES['logo_new'])
			data['logo_rec'] = FileUpload.segur_modulo('logo_rec',request.FILES['logo_new'])

		data_mod = keys_add_none(data,'codigo,nombre,orden,estado,logo,logo_rec')
		mod = Modulo.objects.create(**data_mod)
		if mod:
			model = Modulo.objects.get(pk=mod.id)

			return Response(self.get_serializer(model).data)

	@list_route(url_path='edit', methods=['get'], permission_classes=[LimonPermission,])
	def get_edit(self, request):
		id = request.query_params.get('id', None)
		model = Modulo.objects.get(pk=id)
		return Response(self.get_serializer(model).data)


	@list_route(url_path='update', methods=['put'], permission_classes=[LimonPermission,])
	def get_update(self, request, pk=None):
		data = request.data
		files = request.FILES
		if files:
			data['logo'] = FileUpload.segur_modulo('logo',request.FILES['logo_new'])
			data['logo_rec'] = FileUpload.segur_modulo('logo_rec',request.FILES['logo_new'])
			data_mod = keys_add_none(data,'codigo,nombre,orden,estado,logo,logo_rec')
		else:
			data_mod = keys_add_none(data,'codigo,nombre,orden,estado')

		mod = Modulo.objects.filter(pk=data['id']).update(**data_mod)
		if mod:
			model = Modulo.objects.get(pk=data['id'])
			return Response(self.get_serializer(model).data)

	@list_route(url_path='delete', methods=['delete'], permission_classes=[LimonPermission,])
	def get_delete(self, request, *args, **kwargs):
		try:
			id = request.query_params.get('id', None)
			mode = Modulo.objects.filter(pk=id).values('logo','logo_rec').first()
			# perdoc = PersonaDocumento.objects.filter(persona_id=id).delete()
			model = Modulo.objects.get(pk=id).delete()
			if model and mode['logo']:
				del1 = FileUpload.delete_file(mode['logo'])
				del2 = FileUpload.delete_file(mode['logo_rec'])

			return Response({'status':True, 'id':id})
		except ProtectedError as e:
			return Response(e.errors, status=status.HTTP_400_BAD_REQUEST)

	@list_route(url_path='searchform', methods=['get'], permission_classes=[LimonPermission,])
	def get_searchform(self, request, *args, **kwargs):
		queryset = search_filter(self.get_queryset(), request)
		serializer = ModuloSerializer(queryset, many=True)
		return Response(serializer.data)