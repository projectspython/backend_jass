from rest_framework.response import Response
from rest_framework.decorators import list_route
from rest_framework import viewsets
from oauth2_provider.models import get_application_model
from apps.segur.models.Modulo import Modulo
from apps.segur.models.Menu import Menu
from django.db.models import Value
from django.db.models.functions import Concat
from django.db.models import CharField
from apps.comun.serializers.Persona import PersonaFotoLogoSerializer
from apps.comun.models.Persona import Persona
from apps.segur.always.HostValues import HostValues
from apps.comun.always.InfoModels import PersonaNaturalInfo, PersonaJuridicaInfo
from apps.segur.models.Usuario import Usuario
from django.contrib.auth.models import Permission
from apps.segur.serializers.Usuario import UsuarioSerializer
from apps.comun.serializers.Entidad import EntidadSerializer
from apps.comun.models.Entidad import Entidad

class FunctionsViewSet(viewsets.ViewSet):
    # DATOS DE UNA APLICACIÓN OAUTH
    @list_route(url_path='clienteapp',  permission_classes=[], methods=['post'])
    def get_clienteapp(self, request, format=None):
    	name_app = request.data['name_app']
    	Application = get_application_model()
    	model = Application.objects.filter(
    		name=name_app).values('client_id',
    		'client_secret','authorization_grant_type','redirect_uris',
    		'client_type').first()
    	if(model):
    		model['grant_type'] = model['authorization_grant_type']
    	# lista = ["Nuevo","La voz","Voces"]
    		return Response(model)
    	else:
    		data = {
    			'error':'No encuentra aplicación cliente',
    			'error_description':name_app+' no está registrado en nuestro sistema de autenticación',
    		}
    		return Response(data, status=404)

    @list_route(url_path='menu',  permission_classes=[], methods=['get'])
    def get_menu(self, request, format=None):
        user = request.user
        # print("USUARIO: ", user.persona_id)
        if user:
            lista_new = []

            if user.is_superuser:
                modulos = Modulo.objects.annotate(
                    type=Value('heading', output_field=CharField())).extra(
                        select={'name':'nombre'},
                        ).filter(estado='1').values(
                    'id','codigo','name','type').order_by('orden')

                for p in modulos:
                    children = Menu.objects.annotate(type=Value('toggle', output_field=CharField())).extra(
                        select={'icon': 'icono', 'name':'nombre'},
                        ).filter(modulo_id=p['id'], tipo_menu_id=1,
                        ).values('id','codigo','name','icon','type').order_by('orden')
                    new_children = []
                    for q in children:
                        pages = Menu.objects.annotate(type=Value('menu_list', output_field=CharField())).extra(
                            select={'icon': 'icono', 'name':'nombre_plural', 'state':'estado_router'},
                            ).filter(padre_id=q['id'], tipo_menu_id=2,
                            ).values('id','codigo','name','icon','type','state').order_by('orden')
                        q['pages'] = pages
                        new_children.append(q)

                    p['children'] = new_children
                    lista_new.append(p)
            else:
                permisssions = user.get_all_permissions()
                ids_permissions = []
                for p in permisssions:
                    filt = p.split(".")
                    perm = Permission.objects.filter(
                        content_type__app_label=filt[0],
                        codename=filt[1]).values('id').first()
                    if perm:
                        ids_permissions.append(perm['id'])

                modulos = Modulo.objects.filter(estado='1',
                        modulo_menus__permiso_id__in=ids_permissions
                        ).values('id','codigo','nombre').order_by('orden').distinct()

                for p in modulos:
                    p['name'] = p['nombre']
                    p['type'] = 'heading'
                    del p['nombre']
                    children = Menu.objects.filter(modulo_id=p['id'], tipo_menu_id=1,
                        padre_menus__permiso_id__in=ids_permissions
                        ).values('id','codigo','nombre','icono').order_by('orden').distinct()
                    new_children = []
                    for q in children:
                        q['name'] = q['nombre']
                        q['type'] = 'toggle'
                        q['icon'] = q['icono']
                        pages = Menu.objects.annotate(type=Value('menu_list', output_field=CharField())).extra(
                            select={'icon': 'icono', 'name':'nombre_plural', 'state':'estado_router'},
                            ).filter(padre_id=q['id'], tipo_menu_id=2,
                            permiso_id__in=ids_permissions
                            ).values('id','codigo','name','icon','type','state').order_by('orden')
                        q['pages'] = pages
                        new_children.append(q)

                    p['children'] = new_children
                    lista_new.append(p)

            return Response(lista_new)
        else:
            data = { 'error': 'No autenticado correctamente' }
            return Response(data, status=403)

    @list_route(url_path='datosusuario',  permission_classes=[], methods=['get'])
    def get_datosusuario(self, request, format=None):
        user = request.user
        if user.id:
            per_id = user.persona_id
            model = Persona.objects.get(pk=per_id)
            dominio = HostValues.dominio(request)
            data = {'foto_logo_rec':dominio+model.foto_logo_rec.url,
                    'nombres':model.persona_persona_natural.nombres}
            # data
            return Response(data)
        else:
            data = { 'error': 'No autenticado correctamente' }
            return Response(data, status=403)

    @list_route(url_path='datosusuarioperson',  permission_classes=[], methods=['get'])
    def get_datosusuarioperson(self, request, format=None):
        user = request.user
        if user.id:
            per_id = user.persona_id
            data = PersonaNaturalInfo(request, per_id)
            return Response(data)
        else:
            data = { 'error': 'No autenticado correctamente' }
            return Response(data, status=403)

    @list_route(url_path='changepassword',  permission_classes=[], methods=['post'])
    def get_changepassword(self, request, format=None):
        user = request.user
        data = request.data
        if user.id:
            id = user.id
            us = Usuario.objects.get(pk=id)
            actual_correc = us.check_password(data['password_actual'])

            if actual_correc:
                if data['password'] == data['repit_password']:
                    us.set_password(data['password'])
                    us.save()
                    resul = {'estado':True,'msg':"Correcto"}
                    return Response(resul)
                else:
                    error = {'estado':False,'error':"La contraseña nueva y repetida no coinciden"}
                    return Response(error)
            else:
                error = {'estado':False,'error':"La contraseña actual es incorrecta"}
                return Response(error)
        else:
            data = { 'error': 'No autenticado correctamente' }
            return Response(data, status=403)
    
    @list_route(url_path='misentidades',  permission_classes=[], methods=['get'])
    def get_misentidades(self, request, format=None):
        user = request.user
        if user.id:
            id = user.id
            model = Usuario.objects.get(pk=id)
            data_ = UsuarioSerializer(model).data
            queryset = Entidad.objects.filter(id__in=data_['entidades'])
            result = EntidadSerializer(queryset, many=True)
            return Response(result.data)
        else:
            data = { 'error': 'No autenticado correctamente' }
            return Response(data, status=403)

    @list_route(url_path='datosentidad',  permission_classes=[], methods=['get'])
    def get_datosentidad(self, request, format=None):
        entidad_id = request.query_params.get('id', None)
        user = request.user
        if user.id:
            id = user.id
            listado = Usuario.objects.filter(id=id, entidades__in=[entidad_id]).values('id')
            if len(listado)>0:
                model = Entidad.objects.get(pk=entidad_id)
                data = PersonaJuridicaInfo(request, model.persona_id)
                data['entidad_id'] = entidad_id
                return Response(data)
            else:
                data = { 'error': 'No autenticado correctamente' }
                return Response(data, status=403)
        else:
            data = { 'error': 'No autenticado correctamente' }
            return Response(data, status=403)