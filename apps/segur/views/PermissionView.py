from rest_framework import serializers, viewsets
from django.contrib.auth.models import Permission
from rest_framework import permissions
from apps.segur.serializers.Permission import PermissionSerializer, PermissionBasicSerializer
from apps.comun.always.Pagination import LimonPagination
from rest_framework.filters import SearchFilter, OrderingFilter
from django.db.models.deletion import ProtectedError

from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.response import Response
# from apps.procesos.Permissions.Permission import PermissionPermissions as LimonPermission
from apps.segur.always.Permission import PermLimonList
from apps.comun.always.SearchFilter import search_filter

class PermissionViewSet(LimonPagination, viewsets.ModelViewSet):
	queryset = Permission.objects.all()
	permission_classes = [permissions.IsAuthenticated, PermLimonList, ]
	serializer_class = PermissionSerializer
	filter_backends = (SearchFilter,OrderingFilter,)
	search_fields = ('content_type__app_label','content_type__model','name', 'codename',)
	ordering_fields = '__all__'

	@list_route(url_path='searchform', methods=['get'], permission_classes=[])
	def get_searchform(self, request, *args, **kwargs):
		queryset = search_filter(self.get_queryset(), request)
		serializer = PermissionBasicSerializer(queryset, many=True)
		return Response(serializer.data)