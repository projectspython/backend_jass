from apps.segur.models.Menu import Menu
from rest_framework import serializers
from apps.segur.serializers.TipoMenu import TipoMenuSerializer
from apps.segur.serializers.Modulo import ModuloBasicSerializer
from apps.segur.serializers.Permission import PermissionBasicSerializer

class MenuBasicSerializer(serializers.ModelSerializer):
	class Meta:
		model = Menu
		fields = ('id','nombre','codigo',)

class MenuSerializer(serializers.ModelSerializer):
	tipo_menu = TipoMenuSerializer(many=False, read_only=True)
	modulo = ModuloBasicSerializer(many=False, read_only=True)
	permiso = PermissionBasicSerializer(many=False, read_only=True)
	padre = MenuBasicSerializer(many=False, read_only=True)
	class Meta:
		model = Menu
		fields = '__all__'