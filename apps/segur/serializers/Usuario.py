from apps.segur.models.Usuario import Usuario
from rest_framework import serializers
from apps.comun.serializers.Persona import PersonaBasicSerializer

class UsuarioSerializer(serializers.ModelSerializer):
	# tipo_persona = TipoPersonaBasicSerializer(many=False, read_only=True)
	persona = PersonaBasicSerializer(many=False, read_only=True)
	class Meta:
		model = Usuario
		# fields = '__all__'
		exclude = ('password',)