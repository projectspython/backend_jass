from apps.segur.models.Modulo import Modulo
from rest_framework import serializers

class ModuloSerializer(serializers.ModelSerializer):
	class Meta:
		model = Modulo
		fields = '__all__'

class ModuloBasicSerializer(serializers.ModelSerializer):
	class Meta:
		model = Modulo
		fields = ('id','nombre','codigo',)
		