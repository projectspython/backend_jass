from django.contrib.auth.models import Group
from rest_framework import serializers

class GroupSerializer(serializers.ModelSerializer):
	# persona_direcciones = DireccionBasicSerializer(many=True, read_only=True)
	# app_label = serializers.ReadOnlyField(source='content_type.app_label')

	# permi = serializers.SerializerMethodField('valores')
	# def valores(self, obj):
	# 	return obj.permissions
	
	class Meta:
		model = Group
		fields = '__all__'