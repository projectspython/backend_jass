from apps.segur.models.TipoMenu import TipoMenu
from rest_framework import serializers

class TipoMenuSerializer(serializers.ModelSerializer):
	class Meta:
		model = TipoMenu
		fields = '__all__'