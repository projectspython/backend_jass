
from django.contrib.auth.models import Permission
from rest_framework import serializers

class PermissionSerializer(serializers.ModelSerializer):
	# persona_direcciones = DireccionBasicSerializer(many=True, read_only=True)
	app_label = serializers.ReadOnlyField(source='content_type.app_label')
	model = serializers.ReadOnlyField(source='content_type.model')
	class Meta:
		model = Permission
		fields = '__all__'

class PermissionBasicSerializer(serializers.ModelSerializer):
	# # persona_direcciones = DireccionBasicSerializer(many=True, read_only=True)
	# app_label = serializers.ReadOnlyField(source='content_type.app_label')
	# model = serializers.ReadOnlyField(source='content_type.model')
	class Meta:
		model = Permission
		fields = '__all__'
		