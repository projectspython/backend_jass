from django.db import models
from ..models.Modulo import Modulo
from ..models.TipoMenu import TipoMenu
from django.contrib.auth.models import Permission

class Menu(models.Model):
	modulo = models.ForeignKey(Modulo, related_name='modulo_menus',
		db_column='modulo_id', on_delete=models.PROTECT)
	tipo_menu = models.ForeignKey(TipoMenu, related_name='tipo_menu_menus',
		db_column='tipo_menu_id', on_delete=models.PROTECT)
	codigo = models.CharField(max_length=30, unique=True)
	nombre = models.CharField(max_length=60)
	nombre_plural = models.CharField(max_length=70)
	estado_router = models.CharField(max_length=60)
	icono = models.CharField(max_length=20, blank=True, null=True)
	padre = models.ForeignKey('self', related_name='padre_menus',
		db_column='padre_id', blank=True, null=True, on_delete=models.PROTECT)

	orden = models.IntegerField()
	permiso = models.ForeignKey(Permission, related_name='permiso_menus',
		db_column='permiso_id', blank=True, null=True, on_delete=models.PROTECT)
	estado = models.CharField(max_length=1, default='1')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		verbose_name = 'Menú'
		verbose_name_plural = 'Menús'
		db_table = 'segur_menu'
		default_permissions = ()
		permissions = (
			('view_menu','Listado de Menús'),
			('add_menu','Agregar Menús'),
			('update_menu','Actualizar Menús'),
			('delete_menu','Eliminar Menús'),
			('view_form_menu','Listado de Menús en formulario'),
		)

	def __str__(self):
		return self.nombre