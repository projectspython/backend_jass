from django.db import models

class TipoMenu(models.Model):
	codigo = models.CharField(max_length=10, unique=True)
	nombre = models.CharField(max_length=30, unique=True)
	orden = models.IntegerField()
	estado = models.CharField(max_length=1, default='1')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		verbose_name = 'Tipo Menu'
		verbose_name_plural = 'Tipos de Menú'
		db_table = 'segur_tipo_menu'
		default_permissions = ()
		permissions = (
			('view_tipomenu',
			 'Listado de tipos de menu'),
			('add_tipomenu',
			 'Agregar tipos de menu'),
			('update_tipomenu',
			 'Actualizar tipos de menu'),
			('delete_tipomenu',
			 'Eliminar tipos de menu'),
			('view_form_tipomenu',
			 'Listado de tipos de menu en formulario'),
		)

	def __str__(self):
		return self.nombre