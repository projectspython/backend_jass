from django.db import models
dir_storage = "storage/segur/modulos/logos"

class Modulo(models.Model):
	codigo = models.CharField(max_length=10, unique=True)
	nombre = models.CharField(max_length=60, unique=True)
	logo = models.ImageField(upload_to=dir_storage, blank=True, null=True)
	logo_rec = models.ImageField(upload_to=dir_storage, blank=True, null=True)
	orden = models.IntegerField()
	estado = models.CharField(max_length=1, default='1')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		verbose_name = 'Módulo'
		verbose_name_plural = 'Módulos'
		db_table = 'segur_modulo'
		default_permissions = ()
		permissions = (
			('view_modulo',
			 'Listado de Módulos'),
			('add_modulo',
			 'Agregar Módulos'),
			('update_modulo',
			 'Actualizar Módulos'),
			('delete_modulo',
			 'Eliminar Módulos'),
			('view_form_modulo',
			 'Listado de Módulos en formulario'),
		)

	def __str__(self):
		return self.nombre