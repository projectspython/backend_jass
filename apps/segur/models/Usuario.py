from django.db import models 
from uuid import uuid4
from django.contrib.auth.models import AbstractUser
from apps.comun.models.Persona import Persona
from apps.comun.models.Entidad import Entidad
from django.utils.translation import ugettext_lazy as _

class Usuario(AbstractUser):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    persona = models.OneToOneField(
        Persona, db_column='persona_id',
        null=True, blank=True,
        related_name="persona_usuario",
        on_delete=models.PROTECT,
        )
    entidades = models.ManyToManyField(Entidad,
        verbose_name=_('entidades'), blank=True)

    class Meta:
        verbose_name = 'Usuario'
        verbose_name_plural = 'Usuarios'

    def __str__(self):
        return self.username