from apps.comun.Permissions.Base import BasePermission

class UsuarioPermissions(BasePermission):
	perms_map = {
				'add': 'segur.add_usuario',
				'edit': 'segur.view_usuario',
				'update': 'segur.change_usuario',
				'delete':'segur.delete_usuario',
				'changepassword': 'segur.change_usuario',}

	permission_from_user = None