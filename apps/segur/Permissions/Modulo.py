from apps.comun.Permissions.Base import BasePermission

class ModuloPermissions(BasePermission):
	perms_map = {
				'add': 'segur.add_modulo',
				'edit': 'segur.view_modulo',
				'update': 'segur.change_modulo',
				'delete':'segur.delete_modulo', }

	permission_from_user = None