from apps.comun.Permissions.Base import BasePermission

class MenuPermissions(BasePermission):
	perms_map = {
				'add': 'segur.add_menu',
				'edit': 'segur.view_menu',
				'update': 'segur.update_menu',
				'delete':'segur.delete_menu', }

	permission_from_user = None