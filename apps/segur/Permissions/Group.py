from apps.comun.Permissions.Base import BasePermission

class GroupPermissions(BasePermission):
	perms_map = {
				'add': 'auth.add_group',
				'edit': 'auth.view_group',
				'update': 'auth.change_group',
				'delete':'auth.delete_group',
				'updatepermission':'auth.change_group',
				'editpermission':'auth.change_group', }

	permission_from_user = None