from apps.comun.Permissions.Base import BasePermission

class TipoMenuPermissions(BasePermission):
	perms_map = {
				'add': 'segur.add_tipomenu',
				'edit': 'segur.view_tipomenu',
				'update': 'segur.update_tipomenu',
				'delete':'segur.delete_tipomenu', }

	permission_from_user = None