from django.conf.urls import url, include
from rest_framework import routers
from .views.FunctionsView import FunctionsViewSet
from .views.PermissionView import PermissionViewSet
from .views.GroupView import GroupViewSet
from .views.UsuarioView import UsuarioViewSet
from .views.ModuloView import ModuloViewSet
from .views.TipoMenuView import TipoMenuViewSet
from .views.MenuView import MenuViewSet

router = routers.DefaultRouter()
router.register(r'functions', FunctionsViewSet, base_name='functions')
router.register(r'permissions', PermissionViewSet)
router.register(r'groups', GroupViewSet)
router.register(r'usuarios', UsuarioViewSet)
router.register(r'modulos', ModuloViewSet)
router.register(r'tiposmenu', TipoMenuViewSet)
router.register(r'menus', MenuViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
]