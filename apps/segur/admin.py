from django.contrib import admin
from apps.segur.models.Usuario import Usuario

# admin.site.register(Usuario,)

class UsuarioAdmin(admin.ModelAdmin):
	  list_display = ["username"]
	  list_display_links = ["username"]
	  list_per_page = 15
	  ordering = ['username']

admin.site.register(Usuario, UsuarioAdmin)